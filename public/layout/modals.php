<div id="carregando" class="modal carregando">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div id="carregandoBody" class="modal-body col-center" >
                <i class="fa fa-5x fa-refresh fa-spin" style="color: #9F2925; font-size: 58px; margin-top: 35px; margin-bottom: 35px;"></i>
            </div>
            <div class="modal-footer col-center">
                <h4>Aguarde...</h4>
            </div>
        </div>
    </div>
</div>

<div id="obrigatorio" class="modal">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="center" >Por favor preencha os campos obrigatórios</h4>
            </div>
        </div> 
    </div>
</div>

<div id="sessionSmartModalLogin" class="modal">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header" >
                <h3>Sessão expirada</h3>
            </div>
            <div class="modal-body" >
                <p><span class="label label-danger">Importante</span> Sua sessão com o servidor expirou. Mas não se preocupe, você não perderá seu trabalho, basta refazer o seu login.</p>
                <div id="modalLogin-alert" class="alert alert-danger hide">
                    <strong>Erro!</strong><span id="modalLogin-span"></span>
                </div>
                <form id="loginJson">
                    <label class="control-label">Login</label>
                    <input id="login" type="text" class="form-control">
                    <label class="control-label">Senha</label>
                    <input id="senha" type="password" class="form-control">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btn-loginJson">Entrar</button>
            </div>
        </div>
    </div>
</div>

<div id="sessionSmartModalConexao" class="modal" >
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header" >
                <h3>Conexão Perdida</h3>
            </div>
            <div class="modal-body" >
                <p><span class="label label-important">Importante</span> Sua conexão com a internet parece ter caído. Mas não se preocupe, você não perderá o seu trabalho desde que permaneça nessa página.</p>
                <p><br>Essa janela será fechada assim que a conexão voltar ao normal</p>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<div id="modalExcluirArquivo" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <h4>Você deseja apagar esse arquivo?</h4>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" class="arquivo-id" id="arquivo_id">
                <a class="btn btn-default" data-dismiss="modal">não</a>
                <!-- <a data-arquivo-id="" href="?m=Adm&c=Arquivo&a=Desativar" class="btn btn-primary btn-removeArquivo">sim</a> -->
                <a class="btn btn-primary btn-removeArquivo">sim</a>
            </div>
        </div>
    </div>
</div>

<!--<div id="unidadePickerModal" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h3>Unidade</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                    <?php echo Util_Form::makeSelectUnidadesInserir(1); ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="selecionar-unidadePicker" class="btn btn-primary">ok</button>
            </div>
        </div>
    </div>
</div>-->