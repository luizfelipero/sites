<div class="row">
    <div class="col-sm-12">
        <?php echo Util_FlashMessage::read(); ?>
    </div>
</div>
<div class="row">
    <?php $this->renderView(); ?>
    <script>
        $(function() {
            Ajax.history.push("<?php echo Util_Link::stringfyUrl(); ?>");
        })
    </script>
</div>