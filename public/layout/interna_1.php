<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <meta name="viewport" content="initial-scale=1">

        <title>Viana & Moura</title>
        <?php $versaoJs = "v".date("ymd"); ?>
        <!-- start css -->
        <link href="bootstrap/css/bootstrap.min.css?<?php echo $versaoJs; ?>" rel="stylesheet" media="all">
        <link href="css_p/ajuste2.css?<?php echo $versaoJs; ?>" rel="stylesheet" media="screen">
        <link href="css_p/print.css?<?php echo $versaoJs; ?>" rel="stylesheet" media="print">
        <link href="css_p/alocacao2.css?<?php echo $versaoJs; ?>" rel="stylesheet" media="screen">
        <!--        <link href="css_p/calendario.css?v2" rel="stylesheet" media="screen">
               <link href="css_p/casa.css?v2" rel="stylesheet" media="screen">-->
        <link href="plugins/font-awesome-4.4.0/css/font-awesome.min.css?<?php echo $versaoJs; ?>" rel="stylesheet">
        <link href="plugins/jasny-bootstrap/css/jasny-bootstrap.min.css?<?php echo $versaoJs; ?>" rel="stylesheet">
        <!--        <link href="plugins/select2/css/select2.min.css" rel="stylesheet" media="screen"/>-->
        <!--        <link href="plugins/select2/select2_boot3.css" rel="stylesheet" media="screen"/>-->
        <link href="plugins/jquery-ui-1.11.4.custom/jquery-ui.css" rel="stylesheet" media="screen"/>
        <link href="plugins/jquery-ui-1.11.4.custom/jquery-ui.theme.min.css" rel="stylesheet" media="screen"/>
        <link href="plugins/TableFilter/filtergrid.css" rel="stylesheet" media="screen"/>
        <link href="plugins/TableFilter/TF_Themes/Default/TF_Default.css" rel="stylesheet" media="screen"/>
        <!-- end css -->

        <!-- start js -->
        <script src="plugins/jquery-2.1.1.min.js?v3"></script>
        <script src="plugins/jquery-migrate-1.2.1.min.js?v3"></script>
        <script src="bootstrap/js/bootstrap.min.js?v3"></script>
        <script src="plugins/jquery.ba-throttle-debounce.js?v3"></script>
        <script src="plugins/jquery.knob.js?v3"></script>
        <script src="plugins/jasny-bootstrap/js/jasny-bootstrap.min.js?v3"></script>
        <script src="plugins/elastic.js?v3"></script>
        <script src="plugins/jquery.price_format.js?v3"></script>
        <script src="plugins/jquery_maskedinput.js?v3"></script>
        <script src="plugins/typeahead.bundle.js?v3"></script>
        <script src="plugins/jquery.svg.package-1.5.0/jquery.svgdom.min.js?v3"></script>
        <script src="plugins/jquery.svg.package-1.5.0/jquery.svg.min.js?v3"></script>
        <script src="plugins/bootstrap2-autocomplete.js?v3"></script>
        <script src="plugins/draggabilly.pkgd.js?v3"></script>
        <script src="plugins/Sortable.js?v3"></script>
        <script src="plugins/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
        <script src="plugins/jquery-ui-1.11.4.custom/datePickerptbr.js"></script>
        <script type="text/javascript" src="plugins/jquery.tablesorter/jquery.tablesorter.js"></script> 
        <script src="plugins/Highcharts-4.1.9/js/highcharts.js"></script>
        <script src="plugins/Highcharts-4.1.9/js/modules/exporting.js"></script>
        <script src="plugins/TableFilter/tablefilter.js"></script>
        <script src="plugins/TableFilter/tablefilter_all.js"></script>
        <!-- end js -->


        <meta name="theme-color" content="#972A27">


<!--        <script src="plugins/select2/js/select2.js"></script>-->
        <link href="plugins/select2-4/select2-master/dist/css/select2.min.css" rel="stylesheet" />
        <script src="plugins/select2-4/select2-master/dist/js/select2.min.js"></script>

        <!-- start js_p -->
        <script src="js_p/alertModal.js?<?php echo $versaoJs; ?>"></script>
        <script src="js_p/smartSessionNovo.js?<?php echo $versaoJs; ?>"></script>

        <?php //if ($GLOBALS['user']->getCentro_id() == 1 || 1 == 1) { ?>
        <script src="js_p/complementarNovo.js?<?php echo $versaoJs; ?>"></script>
        <?php /* } else { 
          <script src="js_p/complementarNovoSync.js?v4"></script>
          } */ ?>


        <script src="js_p/totalizador2.js?<?php echo $versaoJs; ?>"></script>
        <script src="js_p/autocomplete.js?<?php echo $versaoJs; ?>"></script>
        <script src="js_p/selectupdate.js?<?php echo $versaoJs; ?>"></script>
        <script src="js_p/navegador.js?<?php echo $versaoJs; ?>"></script>
        <script src="js_p/validacoes.js?<?php echo $versaoJs; ?>"></script>
        <script src="js_p/ajax.js?<?php echo $versaoJs; ?>"></script>
        <script src="js_p/alocacao.js?<?php echo $versaoJs; ?>"></script>
        <script src="js_p/diarioObra.js?<?php echo $versaoJs; ?>"></script>
        <script src="js_p/util.js?<?php echo $versaoJs; ?>"></script>
        <!-- end js_p -->

        <!-- API google charts-->
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
        <script> google.load("visualization", "1", {packages: ["corechart", "timeline"]});</script>
        <style type="text/css">
            body {
                padding-top: 0px;
                padding-bottom: 40px;
                background-image: url("img_p/bg-light.jpg");
                overflow-y: scroll;
            }
        </style>
    </head>
    <body>
        <nav id="navbar-desktop" class="navbar navbar-default hidden-sm hidden-xs" role="navigation">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="fa fa-user"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo Util_Link::link("Index", "Index", "Home"); ?>"><img src="img_p/logo.png"></a>

                </div>
                <p class="navbar-text">
                    <?php if ($GLOBALS['server']['nome'] == "local") { ?>
                        <span class="label label-warning">LOCALHOST!</span>
                    <?php } ?>
                    <span class="label label-danger hide" id="semconexao2">Sem Conexão!</span>
                </p>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <?php $pendencia = $GLOBALS['user']->getQtdPendencia();
                        ?>
                        <li><a href="<?php echo Util_Link::link("Acao", "Acao", "Index"); ?>" class="">Ações <span class="label label-danger label-acoes"><?php echo ($pendencia['acoes'] > 0) ? $pendencia['acoes'] : ''; ?></span></a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php
                                echo $GLOBALS['user']->getNome();
                                //echo (count($treinamentosRestantes) != 0 || count($subordinadosATreinar) != "") ? ' <span class="label label-important">!</span>' : "";
                                ?> <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <?php if ($GLOBALS['user']->getTipo() < 90) { ?>
                                    <li class="disabled"><a href="#"><?php echo $GLOBALS['user']->getCargo()->getNome(); ?></a></li>
                                    <li class="divider"></li>
                                    <li class="disabled">
                                        <a href="#">
                                            <?php
                                            echo $GLOBALS['user']->getNomeUnidade("<br>", 1);
                                            ?>
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                <?php } ?>
                                <li><a href="<?php echo Util_Link::link("Adm", "Pessoa", "EditarSelf"); ?>"><i class="fa fa-pencil"></i> Editar</a></li>
                                <li><a href="<?php echo Util_Link::link("Index", "Index", "Logoff"); ?>" data-sync="1"><i class="fa fa-power-off"></i> Sair</a></li>
                            </ul>
                        </li>
                    </ul>    
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <nav id="menu-mobile-nav" class="navbar navbar-default visible-xs visible-sm" role="navigation">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-mobile">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo Util_Link::link("Index", "Index", "Home"); ?>"><img src="img_p/logo.png"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="menu-mobile">
                    <ul class="nav navbar-nav">
                        <?php include("layout/menu.php"); ?>
                        <li><a href="<?php echo Util_Link::link("Acao", "Acao", "Index"); ?>" class="">Ações <span class="label label-danger label-acoes"><?php echo $qtdAcoes['MyNova']; ?></span></a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php
                                echo $GLOBALS['user']->getNome();
                                //echo (count($treinamentosRestantes) != 0 || count($subordinadosATreinar) != "") ? ' <span class="label label-important">!</span>' : "";
                                ?> <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <?php if ($GLOBALS['user']->getTipo() < 90) { ?>
                                    <li class="disabled"><a href="#"><?php echo $GLOBALS['user']->getCargo()->getNome(); ?></a></li>
                                    <li class="divider"></li>
                                    <li class="disabled">
                                        <a href="#">
                                            <?php
                                            echo $GLOBALS['user']->getNomeUnidade("<br>", 1);
                                            ?>
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                <?php } ?>
                                <li><a href="<?php echo Util_Link::link("Adm", "Pessoa", "EditarSelf"); ?>"><i class="fa fa-pencil"></i> Editar</a></li>
                                <li><a href="<?php echo Util_Link::link("Index", "Index", "Logoff"); ?>" data-sync="1"><i class="fa fa-power-off"></i> Sair</a></li>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <div class="container-fluid">
            <div class="row">
                <div id="menu-desktop" class="col-lg-2 col-md-3 hidden-xs hidden-sm">
                    <ul class="nav nav-stacked">
                        <?php include("layout/menu.php"); ?>
                    </ul>
                </div>
                <div id="conteudoMacro" class="col-lg-10 col-md-9">
                    <noscript>
                    <div class="row msg-sig">
                        <div class="col-lg-12">
                            <div class="alert alert-danger">
                                <strong>Atenção!</strong> Identificamos que o javascript do seu navegador está desabilitado, desta forma o sistema não funcionará, habilite este recurso.
                            </div>
                        </div>
                    </div>
                    </noscript>
                    <div class="row msg-sig hide" id="msgSIG">
                        <div class="col-lg-12">
                            <div class="alert alert-warning ">
                                <strong>Atenção!</strong> O Sistema entrará em manutenção às 12:30 do dia (25/07), com previsão inicial de retorno às 14:00.
                            </div>
                        </div>
                    </div>
                    <div class="row" id="">
                        <div class="col-sm-12" id="msgAjax">      
                            <?php echo Util_FlashMessage::read(); ?>
                        </div>
                    </div>
                    <div class="conteudo" id="internaConteudo">
                        <div class="row">
                            <?php $this->renderView(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include("layout/modals.php"); ?>
        <footer>
            <hr>
            <span>&nbsp;&copy; Viana & Moura Construções S/A</span>
            <span class="pull-right"><i class="fa fa-question"></i> Dúvidas e Sugestões: 
                <a href="<?php echo Util_Link::link("Help", "Chamado", "Ajuda"); ?>" class="btn btn-warning btn-mini">Central de Ajuda &nbsp;
                    <span class="label label-info pull-right label-helpAjustes"><?php echo ($pendencia['helpAjustes'] > 0) ? $pendencia['helpAjustes'] : ''; ?></span>
                    <span class="label label-danger pull-right label-helpBugs"><?php echo ($pendencia['helpBugs'] > 0) ? $pendencia['helpBugs'] : ''; ?></span>
                </a>
                ou (81) 99103-9875<br>
                <span class="pull-right label <?php echo $GLOBALS['server']['label']; ?> pull-right"><?php echo $GLOBALS['server']['nome']; ?></span>
            </span>

        </footer>
    </body>
</html>