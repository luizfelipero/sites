<?php
if ($GLOBALS['user']->getTipo() == 1) {
    ?>
    <ul class="nav nav-stacked menu" id="menu-geral">
        <li class="dropdown"><a class="submenu" data-menu="menu-servicos" href="#">Serviços     <i class="fa fa-caret-right pull-right"></i> <span class="badge pull-right label-servicos"><?php echo ($pendencia['servicos'] > 0) ? $pendencia['servicos'] : ''; ?></span>     </a></li>
        <li class="dropdown"><a class="submenu" data-menu="menu-gestao" href="#">Gestão         <i class="fa fa-caret-right pull-right"></i> <span class="badge pull-right label-gestao"><?php echo ($pendencia['gestao'] > 0) ? $pendencia['gestao'] : ''; ?></span>           </a></li>
        <li class="dropdown"><a class="submenu" data-menu="menu-obra" href="#">Obra             <i class="fa fa-caret-right pull-right"></i> <span class="badge pull-right label-obra"><?php echo ($pendencia['obra'] > 0) ? $pendencia['obra'] : ''; ?></span>                 </a></li>
        <li class="dropdown"><a class="submenu" data-menu="menu-gdh" href="#">Pessoas           <i class="fa fa-caret-right pull-right"></i> <span class="badge pull-right label-gdh"><?php echo ($pendencia['gdh'] > 0) ? $pendencia['gdh'] : ''; ?></span>                    </a></li>
        <li class="dropdown"><a class="submenu" data-menu="menu-vnd" href="#">Vendas            <i class="fa fa-caret-right pull-right"></i> <span class="badge pull-right label-vndGeral"><?php echo ($pendencia['vndGeral'] > 0) ? $pendencia['vndGeral'] : ''; ?></span>     </a></li>
    </ul>
    <!-- serviços -->
    <ul class="nav nav-stacked menu hide" id="menu-servicos">
        <li class="dropdown"><a class="submenu" data-menu="menu-geral" href="#"><i class="fa fa-chevron-left"></i> Voltar</a></li>
        <li class="dropdown"><a class="submenu" data-menu="menu-servicos-adm" href="#">Administrador <i class="fa fa-caret-right pull-right"></i><span class="badge pull-right label-pessoas"><?php echo ($pendencia['pessoas'] > 0) ? $pendencia['pessoas'] : ''; ?></span></a></li>
        <li class="dropdown"><a class="submenu" data-menu="menu-servicos-contatos" href="#">Contatos <i class="fa fa-caret-right pull-right"></i></a></li>
        <li class="dropdown"><a href="?m=Sro&c=Agendamento&a=Index">Reserva de Sala</a></li>
        <li class="dropdown"><a href="?m=Adm&c=Ausencia&a=Index">Ausências</a></li>
        <?php if ($GLOBALS['user']->getDepartamento_id() == 76) { ?>
            <li class="dropdown"><a class="submenu" data-menu="menu-servicos-sig" href="#">SIG <i class="fa fa-caret-right pull-right"></i></a></li>
        <?php } ?>
        <?php if ($GLOBALS['user']->getDepartamento_id() == 76 || $GLOBALS['user']->getDepartamento_id() == 54) { ?>
            <li class="dropdown"><a href="?m=Tot&c=Totvs&a=Travas">Travas Totvs </a></li>
        <?php } ?>
        <?php
        $permitidosViagens = array(117, 1818, 758, 1113, 1108, 168, 165, 883, 198, 1475);

        if (in_array($GLOBALS['user']->getId(), $permitidosViagens)) {
            ?>
            <li class="dropdown"><a class="submenu" data-menu="menu-servicos-viagens" href="#">Viagens <i class="fa fa-caret-right pull-right"></i></a>
            <?php } ?>
    </ul>
    <!-- serviços -->
    <!-- servicos-adm -->
    <ul class="nav nav-stacked menu hide" id="menu-servicos-adm">
        <li class="dropdown"><a class="submenu" data-menu="menu-servicos" href="#"><i class="fa fa-chevron-left"></i> Voltar</a></li>
        <li class="dropdown"><a href="?m=Adm&c=Pessoa&a=Index">Pessoas <span class="badge pull-right label-pessoas"><?php echo ($pendencia['pessoas'] > 0) ? $pendencia['pessoas'] : ''; ?></span></a></li>
        <li class="dropdown"><a href="?m=Adm&c=Unidade&a=Index">Unidades</a></li>
    </ul>
    <!-- servicos-adm -->
    <!-- servicos-contatos -->
    <ul class="nav nav-stacked menu hide" id="menu-servicos-contatos">
        <li class="dropdown"><a class="submenu" data-menu="menu-servicos" href="#"><i class="fa fa-chevron-left"></i> Voltar</a></li>
        <li class="dropdown"><a href="?m=Adm&c=Pessoa&a=Contato">Colaboradores</a></li>
        <li class="dropdown"><a href="?m=Adm&c=Pessoa&a=Unidades">Unidades</a></li>
        <li class="dropdown"><a href="?m=Adm&c=Pessoa&a=GruposEmail">Grupos de E-Mail</a></li>
    </ul>
    <!-- servicos-contatos -->
    <!-- servicos-sig -->
    <ul class="nav nav-stacked menu hide" id="menu-servicos-sig">
        <li class="dropdown"><a class="submenu" data-menu="menu-servicos" href="#"><i class="fa fa-chevron-left"></i> Voltar</a></li>
        <li class="dropdown"><a href="?m=Scaf&c=Scaffold&a=Index">Scaffold</a></li>
        <li class="dropdown"><a href="?m=Aloc&c=Custo&a=Painel">Painel Custo</a></li>
        <li class="dropdown"><a class="submenu" data-menu="menu-servicos-sig-helpdesk" href="#">HelpDesk <i class="fa fa-caret-right pull-right"></i></a></li>
        <li class="dropdown"><a href="?m=Adm&c=Permissao&a=Index">Permissões</a></li>
    </ul>
    <!-- servicos-sig -->
    <!-- servicos-viagens -->
    <ul class="nav nav-stacked menu hide" id="menu-servicos-viagens">
        <li class="dropdown"><a class="submenu" data-menu="menu-servicos" href="#"><i class="fa fa-chevron-left"></i> Voltar</a></li>
        <li class="dropdown"><a href="?m=Vgn&c=Agendamento&a=Inserir">Agendar</a></li>
        <li class="dropdown"><a href="?m=Vgn&c=Agendamento&a=Index">Meus Agendamentos</a></li>
        <li class="dropdown"><a href="?m=Vgn&c=Agendamento&a=Agendamentos">Todos Agendamentos</a></li>
        <?php
        $permitidosViagensAdm = array(117, 1818, 758, 1113, 1108, 168, 165);
        if (in_array($GLOBALS['user']->getId(), $permitidosViagensAdm)) {
            ?>
            <li class="dropdown"><a href="?m=Vgn&c=Agendamento&a=Administracao">Administração</a></li>
        <?php } ?>
    </ul>
    <!-- servicos-viagens -->
    <!-- gestao -->
    <ul class="nav nav-stacked menu hide" id="menu-gestao">
        <li class="dropdown"><a class="submenu" data-menu="menu-geral" href="#"><i class="fa fa-chevron-left"></i> Voltar</a></li>
        <li class="dropdown"><a class="submenu" data-menu="menu-gestao-acao" href="#">Ações <i class="fa fa-caret-right pull-right"></i><span class="badge pull-right label-acaogeral"><?php echo ($pendencia['acaogeral'] > 0) ? $pendencia['acaogeral'] : ''; ?></span></a></li>
        <li class="dropdown"><a class="submenu" data-menu="menu-gestao-asp" href="#">ASP <i class="fa fa-caret-right pull-right"></i><span class="badge pull-right label-asp"><?php echo ($pendencia['asp'] > 0) ? $pendencia['asp'] : ''; ?></span></a></li>
        <li class="dropdown"><a href="?m=Spg&c=Formulario&a=Painel">Solicitação de <br> Produtos <span class="badge pull-right label-spg"><?php echo ($pendencia['spg'] > 0) ? $pendencia['spg'] : ''; ?></span></a></li>
        <li class="dropdown"><a class="submenu" data-menu="menu-gestao-forn" href="#">Fornecedores <i class="fa fa-caret-right pull-right"></i></a></li>
        <li class="dropdown"><a class="submenu" data-menu="menu-gestao-pdca" href="#">Indicador <i class="fa fa-caret-right pull-right"></i></a></li>
        <li class="dropdown"><a class="submenu" data-menu="menu-gestao-matgestao" href="#">Materiais de Gestão <i class="fa fa-caret-right pull-right"></i><span class="badge pull-right label-materialGestao"><?php echo ($pendencia['materialGestao'] > 0) ? $pendencia['materialGestao'] : ''; ?></span></a></li>
        <li class="dropdown"><a href="?m=Cont&c=RelatorioExecutivo&a=Visualizar">Relatório Executivo</a>
    </ul>
    <!-- gestao -->
    <!-- gestao-acao -->
    <ul class="nav nav-stacked menu hide" id="menu-gestao-acao">
        <li class="dropdown"><a class="submenu" data-menu="menu-gestao" href="#"><i class="fa fa-chevron-left"></i> Voltar</a></li>
        <li class="dropdown"><a href="?m=Acao&c=Acao&a=Index">Ações <span class="badge pull-right label-acoes"><?php echo ($pendencia['acoes'] > 0) ? $pendencia['acoes'] : ''; ?></span></a></li>
        <li class="dropdown"><a href="?m=Acao&c=RelatorioAnomalia&a=Index">Relatórios de Anomalia </a></li>
        <li class="dropdown"><a href="?m=Acao&c=RelatorioAnomalia&a=Index&eficaz=0&criador_id=<?php echo $GLOBALS['user']->getId(); ?>">Pendências RA <span class="badge pull-right label-ra"><?php echo ($pendencia['ra'] > 0) ? $pendencia['ra'] : ''; ?></span></a></li>
        <li class="dropdown"><a href="?m=Acao&c=PlanoAcao&a=Index">Planos de Ação</a></li>
        <li class="dropdown"><a href="?m=Acao&c=AtaReuniao&a=Index">Atas de Reunião</a></li>
        <li class="dropdown"><a href="?m=Acao&c=Fca&a=Index">FCA</a></li>
        <li class="dropdown"><a href="?m=Acao&c=Acao&a=FollowUp">Follow up</a></li>
        <li class="dropdown"><a href="?m=Acao&c=AtaReuniao&a=AcoesGrupo">Grupo</a></li>
    </ul>
    <!-- gestao-acao -->
    <!-- gestao-asp -->
    <ul class="nav nav-stacked menu hide" id="menu-gestao-asp">
        <li class="dropdown"><a class="submenu" data-menu="menu-gestao" href="#"><i class="fa fa-chevron-left"></i> Voltar</a></li>
        <li class="dropdown"><a href="?m=Acao&c=Asp&a=Index">Ata de Solicitação de Produtos</a></li>
        <li class="dropdown"><a href="?m=Acao&c=Asp&a=Pendencias">Pendências <span class="badge pull-right label-asp"><?php echo ($pendencia['asp'] > 0) ? $pendencia['asp'] : ''; ?></span></a></li>
    </ul>
    <!-- gestao-asp -->
    <!-- gestao-forn -->
    <ul class="nav nav-stacked menu hide" id="menu-gestao-forn">
        <li class="dropdown"><a class="submenu" data-menu="menu-gestao" href="#"><i class="fa fa-chevron-left"></i> Voltar</a></li>
        <li class="dropdown"><a href="?m=Forn&c=Fornecedor&a=Index">Cadastro</a></li>
        <li class="dropdown"><a href="?m=Forn&c=Funcionario&a=Index">Funcionários</a></li>
        <li class="dropdown"><a href="?m=Forn&c=AvaliacaoFornecedor&a=Index">Avaliação de Fornecedor</a></li>
    </ul>
    <!-- gestao-forn -->
    <!-- gestao-pdca -->
    <ul class="nav nav-stacked menu hide" id="menu-gestao-pdca">
        <li class="dropdown"><a class="submenu" data-menu="menu-gestao" href="#"><i class="fa fa-chevron-left"></i> Voltar</a></li>
        <li class="dropdown"><a href="?m=Pdca&c=Apresentacao&a=Index">Apresentação</a></li>
        <li class="dropdown"><a href="?m=Pdca&c=Dados&a=ExibirGrafico">Exibir Gráfico</a></li>
        <li class="dropdown"><a href="?m=Pdca&c=Dados&a=Inserir">Inserir Valor/Meta</a></li>
        <li class="dropdown"><a href="?m=Gv&c=Apresentacao&a=Index">Gestão à Vista</a></li>
        <li class="dropdown"><a href="?m=Pdca&c=Especificacao&a=Index">Especificações</a></li>
        <li class="dropdown"><a href="?m=Pdca&c=ModeloIndicador&a=Index">Modelos Indicador</a></li>
        <li class="dropdown"><a href="?m=Bol&c=Boletim&a=Index">Boletim Mensal</a></li>
    </ul>
    <!-- gestao-pdca -->
    <!-- gestao-matgestao -->
    <ul class="nav nav-stacked menu hide" id="menu-gestao-matgestao">
        <li class="dropdown"><a class="submenu" data-menu="menu-gestao" href="#"><i class="fa fa-chevron-left"></i> Voltar</a></li>
        <li class="dropdown"><a href="?m=Doc&c=Form&a=Index">Formulários</a></li>
        <li class="dropdown"><a class="submenu" data-menu="menu-lista-mestra" href="#">Lista Mestra         <i class="fa fa-caret-right pull-right"></i> <span class="badge pull-right label-listaLegalizacao"><?php echo ($pendencia['listaLegalizacao'] > 0) ? $pendencia['listaLegalizacao'] : ''; ?></span>           </a></li>
        <li class="dropdown"><a href="?m=Doc&c=Documento&a=Index&tipo_id=1">POP's</a></li>
        <li class="dropdown"><a href="?m=Doc&c=Documento&a=Index&tipo_id=11">PS's</a></li>
        <li class="dropdown"><a href="?m=Doc&c=Documento&a=Index&tipo_id=12">DCF</a></li>
        <li class="dropdown"><a href="?m=Doc&c=Documento&a=Index&tipo_id=27">Políticas</a></li>
        <li class="dropdown"><a href="?m=Doc&c=Documento&a=Index">Todos</a></li>
        <li class="dropdown"><a href="?m=Doc&c=Documento&a=Validar">Validar <span class="badge pull-right label-doc"><?php echo ($pendencia['doc'] > 0) ? $pendencia['doc'] : ''; ?></span></a></li>
        <li class="dropdown"><a href="?m=Doc&c=Documento&a=Vencimentos">Vencimentos <span class="badge pull-right label-vencimentos"><?php echo ($pendencia['vencimentos'] > 0) ? $pendencia['vencimentos'] : ''; ?></span></a></li>
    </ul>
    <!-- gestao-matgestao -->
    <!-- listas mestra -->
    <ul class="nav nav-stacked menu hide" id="menu-lista-mestra">
        <li class="dropdown"><a class="submenu" data-menu="menu-gestao-matgestao" href="#"><i class="fa fa-chevron-left"></i> Voltar</a></li>
        <li class="dropdown"><a href="?m=Doc&c=ListaLegalizacao&a=Index">Legalização <span class="badge pull-right label-listaLegalizacao"><?php echo ($pendencia['listaLegalizacao'] > 0) ? $pendencia['listaLegalizacao'] : ''; ?></span></a></li>
        <li class="dropdown"><a href="?m=Doc&c=ListaMestra&a=Index">Projetos</a></li>
        <li class="dropdown"><a href="?m=Doc&c=Matriz&a=Index">Produção</a></li>
    </ul>
    <!-- listas mestra -->
    <!-- obra -->
    <ul class="nav nav-stacked menu hide" id="menu-obra">
        <li class="dropdown"><a class="submenu" data-menu="menu-geral" href="#"><i class="fa fa-chevron-left"></i> Voltar</a></li>
        <li class="dropdown"><a class="submenu" data-menu="menu-obra-ass" href="#">Assistência Técnica <i class="fa fa-caret-right pull-right"></i><span class="badge pull-right label-assInfra"><?php echo ($pendencia['assGeral'] > 0) ? $pendencia['assGeral'] : ''; ?></span></a></li>
        <li class="dropdown"><a class="submenu" data-menu="menu-obra-aloc" href="#">Alocação <i class="fa fa-caret-right pull-right"></i></a></li>
        <li class="dropdown"><a class="submenu" data-menu="menu-obra-pan" href="#">Panorama <i class="fa fa-caret-right pull-right"></i></a></li>
        <li class="dropdown"><a class="submenu" data-menu="menu-obra-ris" href="#">RIS <i class="fa fa-caret-right pull-right"></i><span class="badge pull-right label-risConcessao"><?php echo ($pendencia['risConcessao'] > 0) ? $pendencia['risConcessao'] : ''; ?></span></a></li>
        <li class="dropdown"><a class="submenu" data-menu="menu-obra-supri" href="#">Suprimentos <i class="fa fa-caret-right pull-right"></i><span class="badge pull-right label-suprimentos"><?php echo ($pendencia['suprimentos'] > 0) ? $pendencia['suprimentos'] : ''; ?></span></a></li>
        <li class="dropdown"><a class="submenu" data-menu="menu-obra-req" href="#">Infra - Requisição <i class="fa fa-caret-right pull-right"></i><span class="badge pull-right label-requisicao"><?php echo ($pendencia['requisicao'] > 0) ? $pendencia['requisicao'] : ''; ?></span></a></li>
        <li class="dropdown"><a class="submenu" data-menu="menu-obra-infra" href="#">Infra - Gestão <i class="fa fa-caret-right pull-right"></i></a>
        <li class="dropdown"><a class="submenu" data-menu="menu-obra-ppr" href="#">PPR Diretos <i class="fa fa-caret-right pull-right"></i><span class="badge badge-success pull-right label-pprConferido"><?php echo ($pendencia['pprConferido'] > 0) ? $pendencia['pprConferido'] : ''; ?></span><span class="badge pull-right label-ppr"><?php echo ($pendencia['ppr'] > 0) ? $pendencia['ppr'] : ''; ?></span></a></li>
    </ul>
    <!-- obra -->
    <!-- obra-ass -->
    <ul class="nav nav-stacked menu hide" id="menu-obra-ass">
        <li class="dropdown"><a class="submenu" data-menu="menu-obra" href="#"><i class="fa fa-chevron-left"></i> Voltar</a></li>
        <li class="dropdown"><a href="?m=Ass&c=Chamado&a=Gestao&tipoAbertura=2">Chamados Internos</a></li>
        <li class="dropdown"><a href="?m=Ass&c=Chamado&a=Gestao&tipoAbertura=1">Chamados Clientes</a></li>
        <li class="dropdown"><a href="?m=Ass&c=Chamado&a=Gestao&tipoAbertura=2&retrabalho=1">Chamados GA</a></li>
        <li class="dropdown"><a href="?m=Ass&c=Checklist&a=Index">Checklist</a></li>
        <li class="dropdown"><a href="?m=Ass&c=Equipe&a=Index">Equipe</a></li>
        <li class="dropdown"><a href="?m=Ass&c=Chamado&a=Manual">Manual do Proprietário</a></li>
        <li class="dropdown"><a href="?m=Ass&c=Problema&a=Index">Problemas e Especificações</a></li>
        <li class="dropdown"><a href="?m=Prod&c=Relatorio&a=Reprovacoes">Relatório Reprovações</a></li>
        <li class="dropdown"><a href="?m=Ass&c=Chamado&a=PendenciaAvaliacao">Pendência Pesquisas <span class="badge pull-right label-pesquisas"><?php echo ($pendencia['assPesquisas'] > 0) ? $pendencia['assPesquisas'] : ''; ?></span></a></li>
        <li class="dropdown"><a href="?m=Ass&c=Relatorio&a=RelatorioAvaliacao">Gráfico Avaliação</a></li>
    </ul>
    <!-- obra-ass -->
    <!-- obra-aloc -->
    <ul class="nav nav-stacked menu hide" id="menu-obra-aloc">
        <li class="dropdown"><a class="submenu" data-menu="menu-obra" href="#"><i class="fa fa-chevron-left"></i> Voltar</a></li>
        <li class="dropdown"><a href="?m=Crono&c=Cronograma&a=RelatorioCasas">Relatório Casas</a></li>
        <li class="dropdown"><a href="?m=Prod&c=Relatorio&a=Reprovacoes">Relatório Reprovações</a></li>
        <li class="dropdown"><a href="?m=Aloc&c=Alocacao&a=Alocar">Alocar Quadro</a></li>
        <li class="dropdown"><a href="?m=Aloc&c=Alocacao&a=GerenciarFaltas">Gerenciar Faltas</a></li>
        <li class="dropdown"><a href="?m=Aloc&c=Relatorio&a=Colaborador">Relatórios</a></li>
        <li class="dropdown"><a href="?m=Aloc&c=Relatorio&a=Status">Status</a></li>
        <li class="dropdown"><a href="?m=Prod&c=Pcp&a=CasaListarTermino">Validação de Casas</a></li>
        <li class="dropdown"><a class="submenu" data-menu="menu-obra-aloc-modelos" href="#">Modelos <i class="fa fa-caret-right pull-right"></i></a></li>
    </ul>
    <!-- obra-aloc -->
    <!-- obra-aloc-modelos -->
    <ul class="nav nav-stacked menu hide" id="menu-obra-aloc-modelos">
        <li class="dropdown"><a class="submenu" data-menu="menu-obra-aloc" href="#"><i class="fa fa-chevron-left"></i> Voltar</a></li>
        <li class="dropdown"><a href="?m=Crono&c=Cronograma&a=CronogramaAvulso">Cronograma</a></li>
    </ul>
    <!-- obra-aloc-modelos -->
    <!-- obra-pan -->
    <ul class="nav nav-stacked menu hide" id="menu-obra-pan">
        <li class="dropdown"><a class="submenu" data-menu="menu-obra" href="#"><i class="fa fa-chevron-left"></i> Voltar</a></li>
        <li class="dropdown"><a href="?m=Prod&c=Pcp&a=Panorama">Panorama Vila</a></li>
        <li class="dropdown"><a href="?m=Prod&c=Pcp&a=RelatorioCasas">Relatório de Casas</a></li>
        <li class="dropdown"><a href="?m=Prod&c=Pcp&a=CasaListar">Exportar</a></li>
        <li class="dropdown"><a href="?m=Prod&c=Pcp&a=AtualizarDados">Atualizar Dados</a></li>
        <li class="dropdown"><a href="?m=Prod&c=Pcp&a=EtapaInserir">Etapas</a></li>
    </ul>
    <!-- obra-pan -->
    <!-- obra-ris -->
    <ul class="nav nav-stacked menu hide" id="menu-obra-ris">
        <li class="dropdown"><a class="submenu" data-menu="menu-obra" href="#"><i class="fa fa-chevron-left"></i> Voltar</a></li>
        <li class="dropdown"><a href="?m=Ris&c=Ris&a=Relatorio">Relatório RIS</a></li>
        <li class="dropdown"><a href="?m=Ris&c=Concessao&a=Index">Concessão<span class="badge pull-right label-risConcessao"><?php echo ($pendencia['risConcessao'] > 0) ? $pendencia['risConcessao'] : ''; ?></span></a></li>
    </ul>
    <!-- obra-ris -->
    <!-- obra-supri -->
    <ul class="nav nav-stacked menu hide" id="menu-obra-supri">
        <li class="dropdown"><a class="submenu" data-menu="menu-obra" href="#"><i class="fa fa-chevron-left"></i> Voltar</a></li>
        <?php if ($GLOBALS['user']->getUgb_id() == 2 || $GLOBALS['user']->getUgb_id() == 3) { ?>
            <li class="dropdown"><a href="?m=Supri&c=MovimentacaoMaterial&a=PainelNovo">Pedidos<span class="badge pull-right label-movimentacaoMaterial"><?php echo ($pendencia['movimentacaoMaterial'] > 0) ? $pendencia['movimentacaoMaterial'] : ''; ?></span></a></li>
        <?php } else { ?>
            <li class="dropdown"><a href="?m=Supri&c=MovimentacaoMaterial&a=Painel">Pedidos<span class="badge pull-right label-movimentacaoMaterial"><?php echo ($pendencia['movimentacaoMaterial'] > 0) ? $pendencia['movimentacaoMaterial'] : ''; ?></span></a></li>
        <?php } ?>
        <li class="dropdown"><a href="?m=Supri&c=Atraso&a=Index">Atrasos<span class="badge pull-right label-pedidoAtrasado"><?php echo ($pendencia['pedidoAtrasado'] > 0) ? $pendencia['pedidoAtrasado'] : ''; ?></span></a></li>
        <li class="dropdown"><a href="?m=Supri&c=KitItem&a=Index">Lista de Kits</a></li>
        <?php if ($GLOBALS['user']->getDepartamento_id() == 76 || $GLOBALS['user']->getUgb_id() != 0) { ?>
            <li class="dropdown"><a href="?m=Tot&c=Totvs&a=Index">Totvs<span class="badge pull-right label-totvm"><?php echo ($pendencia['totvm'] > 0) ? $pendencia['totvm'] : ''; ?></span></a></li>
        <?php } ?>
    </ul>
    <!-- obra-supri -->
    <!-- obra-req -->
    <ul class="nav nav-stacked menu hide" id="menu-obra-req">
        <li class="dropdown"><a class="submenu" data-menu="menu-obra" href="#"><i class="fa fa-chevron-left"></i> Voltar</a></li>
        <li class="dropdown"><a href="?m=Infra&c=Requisicao&a=Pendencia">Pendências <span class="badge pull-right label-requisicao"><?php echo ($pendencia['requisicao'] > 0) ? $pendencia['requisicao'] : ''; ?></span></a></li>
        <li class="dropdown"><a href="?m=Infra&c=Requisicao&a=Index">Requisições</a></li>
        <li class="dropdown"><a href="?m=Infra&c=Nota&a=Index">Notas Fiscais</a></li>
        <li class="dropdown"><a href="?m=Infra&c=Item&a=Index">Itens</a></li>
        <li class="dropdown"><a href="?m=Infra&c=Orcamento&a=Index">Contas Orçamento</a></li>
        <li class="dropdown"><a href="?m=Infra&c=Orcamento&a=VisualizarItens">Itens Orçamento</a></li>
    </ul>
    <!-- obra-req -->
    <!-- obra-infra -->
    <ul class="nav nav-stacked menu hide" id="menu-obra-infra">
        <li class="dropdown"><a class="submenu" data-menu="menu-obra" href="#"><i class="fa fa-chevron-left"></i> Voltar</a></li>
        <li class="dropdown"><a href="?m=Aloc&c=DiarioObra&a=Index">Diário de Obra</a></li>
        <li class="dropdown"><a href="?m=Aloc&c=IndiceTecnico&a=Index">Índice Técnico</a></li>
        <li class="dropdown"><a href="?m=Aloc&c=ServicoExecutado&a=Index">Serviço Executado</a></li>
        <li class="dropdown"><a href="?m=Aloc&c=ServicoPrevisto&a=Index">Serviço Previsto</a></li>
        <li class="dropdown"><a href="?m=Aloc&c=RelatorioInfra&a=Alocacao">Relatórios</a></li>
    </ul>
    <!-- obra-infra -->
    <!-- obra-ppr -->
    <ul class="nav nav-stacked menu hide" id="menu-obra-ppr">
        <li class="dropdown"><a class="submenu" data-menu="menu-obra" href="#"><i class="fa fa-chevron-left"></i> Voltar</a></li>
        <li class="dropdown"><a href="?m=Ppr&c=PremioPpr&a=Index">Tabela</a></li>
        <li class="dropdown"><a href="?m=Ppr&c=Poupanca&a=Index">Poupança <span class="badge badge-success pull-right label-pprConferido"><?php echo ($pendencia['pprConferido'] > 0) ? $pendencia['pprConferido'] : ''; ?></span></a></li>
        <li class="dropdown"><a href="?m=Ppr&c=Manobra&a=Index">Manobras<span class="badge pull-right label-pprManobra"><?php echo ($pendencia['pprManobra'] > 0) ? $pendencia['pprManobra'] : ''; ?></span></a></li>
        <li class="dropdown"><a href="?m=Ppr&c=Poupanca&a=Abertos">Em Aberto<span class="badge pull-right label-pprAberto"><?php echo ($pendencia['pprAberto'] > 0) ? $pendencia['pprAberto'] : ''; ?></span></a></li>
        <li class="dropdown"><a href="?m=Ppr&c=Acrescimo&a=Index">Ajustes</a></li>
        <li class="dropdown"><a href="?m=Ppr&c=Acrescimo&a=Pendentes">Pendentes<span class="badge pull-right label-pprAcrescimo"><?php echo ($pendencia['pprAcrescimo'] > 0) ? $pendencia['pprAcrescimo'] : ''; ?></span></a></li>
    </ul>
    <!-- obra-ppr -->
    <!-- gdh -->
    <ul class="nav nav-stacked menu hide" id="menu-gdh">
        <li class="dropdown"><a class="submenu" data-menu="menu-geral" href="#"><i class="fa fa-chevron-left"></i> Voltar</a></li>
        <li class="dropdown"><a class="submenu" data-menu="menu-gdh-sap" href="#">SAP <i class="fa fa-caret-right pull-right"></i><span class="badge pull-right label-sap"><?php echo ($pendencia['sap'] > 0) ? $pendencia['sap'] : ''; ?></span></a></li>
        <li class="dropdown"><a class="submenu" data-menu="menu-gdh-desemp" href="#">Desempenho <i class="fa fa-caret-right pull-right"></i><span class="badge pull-right label-desempenho"><?php echo ($pendencia['desempenho'] > 0) ? $pendencia['desempenho'] : ''; ?></span></a></li>
        <li class="dropdown"><a class="submenu" data-menu="menu-gdh-saude" href="#">Saúde <i class="fa fa-caret-right pull-right"></i><span class="badge pull-right label-exame"><?php echo ($pendencia['exame'] > 0) ? $pendencia['exame'] : ''; ?></span></a></li>
        <li class="dropdown"><a class="submenu" data-menu="menu-gdh-trein" href="#">Treinamento <i class="fa fa-caret-right pull-right"></i><span class="badge badge-warning pull-right label-trein"><?php echo ($pendencia['treinAtrasado'] > 0) ? $pendencia['treinAtrasado'] : ''; ?></span><span class="badge pull-right label-treinAtrasado"><?php echo ($pendencia['treinAtrasado'] > 0) ? $pendencia['treinAtrasado'] : ''; ?></span><span class="badge badge-info pull-right label-treinSetor"><?php echo ($pendencia['treinSetor'] > 0) ? $pendencia['treinSetor'] : ''; ?></span></a></li>
    </ul>
    <!-- gdh -->
    <!-- gdh-sap -->
    <ul class="nav nav-stacked menu hide" id="menu-gdh-sap">
        <li class="dropdown"><a class="submenu" data-menu="menu-gdh" href="#"><i class="fa fa-chevron-left"></i> Voltar</a></li>
        <li class="dropdown"><a href="?m=Sap&c=Sap&a=Index">SAP</a></li>
        <li class="dropdown"><a href="?m=Sap&c=Sap&a=Pendencia">Pendências<span class="badge pull-right label-sap"><?php echo ($pendencia['sap'] > 0) ? $pendencia['sap'] : ''; ?></span></a></li>
    </ul>
    <!-- gdh-sap -->
    <!-- gdh-desemp -->
    <ul class="nav nav-stacked menu hide" id="menu-gdh-desemp">
        <li class="dropdown"><a class="submenu" data-menu="menu-gdh" href="#"><i class="fa fa-chevron-left"></i> Voltar</a></li>
        <li class="dropdown"><a href="?m=Mat&c=Feedback&a=Index">Feedbacks <span class="badge pull-right label-feedbacks"><?php echo ($pendencia['feedbacks'] > 0) ? $pendencia['feedbacks'] : ''; ?></span></a></li>
        <li class="dropdown"><a href="?m=Mat&c=AvaliacaoHabilidade&a=Index">Desempenho do Colaborador</a></li>
        <li class="dropdown"><a href="?m=Mat&c=Habilidade&a=Index">Habilidades do Cargo</a></li>
        <li class="dropdown"><a href="?m=Mat&c=AvaliacaoHabilidade&a=MatrizSetor">Matriz de Desempenho</a></li>
        <li class="dropdown"><a href="?m=Acao&c=AnaliseCritica&a=Index">Plano de Desenvolvimento Individual</a></li>
        <li class="dropdown"><a href="?m=Mat&c=TerminoExperiencia&a=Index">Términos de Experiência</a></li>
        <li class="dropdown"><a href="?m=Mat&c=TerminoExperiencia&a=Pendencia">Pendências TE <span class="badge pull-right label-termino"><?php echo ($pendencia['termino'] > 0) ? $pendencia['termino'] : ''; ?></span></a></li>
        <li class="dropdown"><a href="?m=Mat&c=AvaliacaoHabilidade&a=MediaAlinhamento">Alinhamento com a Cultura</a></li>
    </ul>
    <!-- gdh-desemp -->
    <!-- gdh-saude -->
    <ul class="nav nav-stacked menu hide" id="menu-gdh-saude">
        <li class="dropdown"><a class="submenu" data-menu="menu-gdh" href="#"><i class="fa fa-chevron-left"></i> Voltar</a></li>
        <li class="dropdown"><a href="?m=Sust&c=Exame&a=Index">Exames Realizados</a></li>
        <li class="dropdown"><a href="?m=Sust&c=Exame&a=ExameMes">Exames à Realizar</a></li>
        <li class="dropdown"><a href="?m=Sust&c=TipoExame&a=Index">Tipos de Exame</a></li>
        <li class="dropdown"><a href="?m=Sust&c=Exame&a=PendenciaNovato">Pendência Novatos</a></li>
        <li class="dropdown"><a href="?m=Sust&c=Exame&a=Pendentes">Pendências <span class="badge pull-right label-exame"><?php echo ($pendencia['exame'] > 0) ? $pendencia['exame'] : ''; ?></span></a></li>
    </ul>
    <!-- gdh-saude -->
    <!-- gdh-trein -->
    <ul class="nav nav-stacked menu hide" id="menu-gdh-trein">
        <li class="dropdown"><a class="submenu" data-menu="menu-gdh" href="#"><i class="fa fa-chevron-left"></i> Voltar</a></li>
        <li class="dropdown"><a href="?m=Doc&c=Treinamento&a=MatrizIndividual">Matriz Individual</a></li>
        <li class="dropdown"><a href="?m=Doc&c=Treinamento&a=MatrizSetor">Matriz do Setor</a></li>
        <li class="dropdown"><a href="?m=Doc&c=Plano&a=Index">Plano</a></li>
        <li class="dropdown"><a href="?m=Doc&c=Treinamento&a=Pendentes">Pendentes <span class="badge badge-warning pull-right label-trein"><?php echo ($pendencia['trein'] > 0) ? $pendencia['trein'] : ''; ?></span><span class="badge pull-right label-treinAtrasado"><?php echo ($pendencia['treinAtrasado'] > 0) ? $pendencia['treinAtrasado'] : ''; ?></span></a></li>
        <li class="dropdown"><a href="?m=Doc&c=Treinamento&a=PendentesSetor">Pend. Setor <span class="badge badge-info pull-right label-treinSetor"><?php echo ($pendencia['treinSetor'] > 0) ? $pendencia['treinSetor'] : ''; ?></span></a></li>
        <li class="dropdown"><a href="?m=Doc&c=Treinamento&a=Lpt">LPT</a></li>
    </ul>
    <!-- gdh-trein -->
    <!-- servicos-sig-helpdesk -->
    <ul class="nav nav-stacked menu hide" id="menu-servicos-sig-helpdesk">
        <li class="dropdown"><a class="submenu" data-menu="menu-servicos-sig" href="#"><i class="fa fa-chevron-left"></i> Voltar</a></li>
        <li class="dropdown"><a href="?m=Help&c=Chamado&a=Painel">Chamados</a></li>
        <li class="dropdown"><a href="?m=Help&c=Chamado&a=PainelTi">Chamados TI</a></li>
        <li class="dropdown"><a href="?m=Help&c=Equipamento&a=Index">Equipamentos</a></li>
        <li class="dropdown"><a href="?m=Help&c=Licenca&a=ListarLicencas">Licenças p/ Equip.</a></li>
        <li class="dropdown"><a href="?m=Help&c=Licenca&a=ListarEquipamentos">Equip. p/ Licenças</a></li>
    </ul>
    <!-- servicos-sig-helpdesk -->

    <!-- vnd -->
    <ul class="nav nav-stacked menu hide" id="menu-vnd">
        <li class="dropdown"><a class="submenu" data-menu="menu-geral" href="#"><i class="fa fa-chevron-left"></i> Voltar</a></li>
        <li class="dropdown"><a href="?m=Vnd&c=Contratacao&a=ListaEspera">Lista de Espera</a></li>
        <li class="dropdown"><a href="?m=Vnd&c=Contratacao&a=AcompVnd">Acomp. Vendas <span class="badge pull-right label-vnd"><?php echo ($pendencia['vnd'] > 0) ? $pendencia['vnd'] : ''; ?></span></a></li>
        <li class="dropdown"><a href="?m=Vnd&c=Contratacao&a=Imobiliaria">Imobiliária</a></li>
        <li class="dropdown"><a href="?m=Vnd&c=Contratacao&a=Empreendimento">Empreendimento</a></li>
        <li class="dropdown"><a href="?m=Vnd&c=Contratacao&a=PainelContratacoes">Painel de Contratações</a></li>
        <li class="dropdown"><a href="?m=Vnd&c=Meta&a=Visualizar">Cronograma de Vendas</a></li>
        <li class="dropdown"><a href="?m=Vnd&c=Cliente&a=Index">Todos</a></li>
        <li class="dropdown"><a href="?m=Vnd&c=Cliente&a=Relatorios">Relatórios</a></li>
        <li class="dropdown"><a href="?m=Vnd&c=Cliente&a=Agendamento">Agendamentos <span class="badge pull-right label-vndAgendamento"><?php echo ($pendencia['vndAgendamento'] > 0) ? $pendencia['vndAgendamento'] : ''; ?></span></a></a></li>
        <li class="dropdown"><a href="?m=Vnd&c=Cliente&a=Pendencia">Pendências Prosperidade <span class="badge pull-right label-vndProsperidade"><?php echo ($pendencia['vndProsperidade'] > 0) ? $pendencia['vndProsperidade'] : ''; ?></span></a></li>
        <li class="dropdown"><a href="?m=Vnd&c=Cliente&a=PendenciaValores">Pendências Valores <span class="badge pull-right label-vndProsperidade"><?php echo ($pendencia['vndValores'] > 0) ? $pendencia['vndValores'] : ''; ?></span></a></li>
        <?php
        if ($GLOBALS['user']->getDepartamento_id() == 75 || $GLOBALS['user']->getId() == 169) {
            ?>
            <li class="dropdown"><a href="?m=Vnd&c=Contratacao&a=PendenciaPreco">Pendências Preço <span class="badge pull-right label-vndPreco"><?php echo ($pendencia['vndPreco'] > 0) ? $pendencia['vndPreco'] : ''; ?></span></a></li>
        <?php } ?>
        <li class="dropdown"><a href="?m=Vnd&c=Cliente&a=ClientesProsperidade">VM Prosperidade</a></li>
        <?php
    }
//PARCEIROS 97
    else if ($GLOBALS['user']->getTipo() == 97) {
        ?>
        <ul class="nav nav-stacked menu" id="">
            <li class="dropdown"><a href="?m=Acao&c=Acao&a=Index">Ações <span class="badge pull-right label-acoes"><?php echo ($pendencia['acoes'] > 0) ? $pendencia['acoes'] : ''; ?></span></a></li>
            <li class="dropdown"><a href="?m=Acao&c=PlanoAcao&a=Index">Planos de Ação</a></li>
            <li class="dropdown"><a href="?m=Acao&c=AtaReuniao&a=Index">Atas de Reunião</a></li>
            <li class="dropdown"><a href="?m=Sro&c=Agendamento&a=Index">Reserva de Sala</a></li>
        </ul>

        <?php
    }
//PARCEIROS Fornecedores
    else if ($GLOBALS['user']->getTipo() == 98) {
        ?>
        <ul class="nav nav-stacked menu" id="">
            <li class="dropdown"><a href="?m=Forn&c=Funcionario&a=Index">Cadastro Funcionários</a></li>
            <li class="dropdown"><a href="?m=Forn&c=Documento&a=Index">Cadastro Documentos Empresa</a></li>
        </ul>
    <?php } ?>
</ul>
<!-- vnd -->
<script>

    $(".submenu").click(function () {

        $(".menu").addClass('hide');
        var destino = $(this).data('menu');
        $("#" + destino).removeClass('hide');

    })

</script>