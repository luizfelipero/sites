<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <title>Viana &amp; Moura Constru&ccedil;&otilde;es</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Site Viana e Moura Construções">
        <meta name="author" content="Bruno Falcão">

        <!-- Le styles -->
        <link href="cliente/bootstrap/css/bootstrap.css" rel="stylesheet">
        <link href="cliente/css/cliente.css" rel="stylesheet">

        <style type="text/css">
            body {
                padding-top: 20px;
                padding-bottom: 0px;
                background-image: url("cliente/img/background.jpg");
                background-position: center top;
                background-repeat: no-repeat;
                background-color: #FAF9E7;
            }


        </style>

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="../assets/js/html5shiv.js"></script>
        <![endif]-->

        <!-- Le javascript
                ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="plugins/jquery-2.1.1.min.js"></script> 
        <script src="plugins/jquery-migrate-1.2.1.min.js"></script> 
        <script src="plugins/jquery_maskedinput.js"></script>
        <script src="plugins/jquery.ba-throttle-debounce.js"></script>
        <script src="plugins/jquery.price_format.js"></script>
        <script src="plugins/elastic.js"></script>


        <script src="cliente/js/assistencia.js"></script>
        <script src="cliente/js/complementar.js"></script>

        <script src="js_p/autocomplete.js"></script>
        <script src="js_p/selectupdate.js"></script>
        <script src="js_p/navegador.js"></script>
        <script src="js_p/validacoes.js"></script>

        <script src="cliente/bootstrap/js/bootstrap.js"></script>  
    </head>
    <body>
        <?php // include ("js/analytics.inc"); ?>
        <div class="container">
            <!--            <div class="row">
                            <div id="cabecalho" class="col-lg-10 col-lg-offset-1">
                                <div class="row">
                                    <div class="col-xs-3" style="padding: 10px 20px;">
                                        <a href="?m=Serv&c=Cliente&a=Home"><img src="cliente/img/logo160.png" class="img-responsive"></a>
                                    </div>
                                    <div class="col-xs-9">
                                        <img src="cliente/img/portalcliente.jpg" class="img-responsive">
                                    </div>
                                </div>
                            </div>
                        </div>-->

            <div class="row visible-xs visible-sm">
                <div class="col-xs-3 col-center" style="padding: 10px 20px;">
                    <a href="?m=Serv&c=Cliente&a=Home"><img src="cliente/img/logo160.png" class=""></a>
                </div>
                <br>
            </div>

            <div class="row hidden-sm hidden-xs">
                <div id="cabecalho" class="col-lg-10 col-lg-offset-1">
                    <div class="row">
                        <div class="col-md-3 padding-logo col-center">
                            <a href="?m=Serv&c=Cliente&a=Home"><img src="cliente/img/logo160.png"></a>
                        </div>
                        <div class="col-md-9">
                            <div class="menu-nav">
                            </div>
                            <ul id="menu2" class="nav nav-pills nav-justified li-menu">
                                <li class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">A&nbsp;Empresa <span class="caret"></span></a>
                                    <ul class="dropdown-menu li-menu">
                                        <li><a href="http://vianaemoura.com.br/p/empresa">&nbsp;Sobre Nós</a></li>
                                        <li><a href="http://vianaemoura.com.br/p/cultura">&nbsp;Nossa Cultura</a></li>
                                        <li><a href="http://vianaemoura.com.br/p/sustentabilidade">&nbsp;Sustentabilidade</a></li>
                                    </ul>
                                </li>
                                <li><a href="http://vianaemoura.com.br/p/casa">A&nbsp;Casa</a></li>
                                <li><a href="http://vianaemoura.com.br/p/vendas">Vendas</a></li>                          
                                <li><a href="http://vianaemoura.com.br/p/assistencia">Assistência&nbsp;Técnica</a></li>
                                <li><a href="http://cliente.vianaemoura.com.br">Portal</a></li>
                                <li><a href="http://vianaemoura.com.br/p/trabalhe">Carreira</a></li>
                                <li><a href="http://vianaemoura.com.br/p/contato">Contato</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!-- celular -->
            <nav id="menu-mobile-nav" class="navbar navbar-default visible-xs visible-sm" role="navigation">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-mobile">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div id="menu-mobile" class="container-fluid collapse navbar-collapse">                    
                    <ul id="menu-sm" class="nav navbar-nav">
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">A&nbsp;Empresa <span class="caret"></span></a>
                            <ul class="dropdown-menu li-menu">
                                <li><a href="http://vianaemoura.com.br/p/empresa">&nbsp;Sobre Nós</a></li>
                                <li><a href="http://vianaemoura.com.br/p/cultura">&nbsp;Nossa Cultura</a></li>
                                <li><a href="http://vianaemoura.com.br/p/sustentabilidade">&nbsp;Sustentabilidade</a></li>
                            </ul>
                        </li>
                        <li><a href="http://vianaemoura.com.br/p/casa">A&nbsp;Casa</a></li>
                        <li><a href="http://vianaemoura.com.br/p/vendas">Vendas</a></li>                          
                        <li><a href="http://vianaemoura.com.br/p/assistencia">Assistência&nbsp;Técnica</a></li>
                        <li><a href="http://cliente.vianaemoura.com.br">Portal</a></li>
                        <li><a href="http://vianaemoura.com.br/p/trabalhe">Carreira</a></li>
                        <li><a href="http://vianaemoura.com.br/p/contato">Contato</a></li>
                    </ul>
                </div>
            </nav>


            <div class="row">
                <div class="col-lg-10 col-lg-offset-1" style="background-color: #fff; padding: 15px">
                    <div id="form-alert" class="row">
                        <div class="col-sm-12">
                            <?php echo Util_FlashMessage::read(); ?>
                        </div>
                    </div>
                    <div class="row" style="padding-bottom: 20px;">
                        <!-- <div class="col-sm-12">
                            <div class="alert alert-warning">
                                <p>Prezados Clientes,</p>
                                <p>Informamos que no período de 26/12/2016 até 08/01/2017 estaremos de recesso. As assistências abertas durante esse período serão avaliadas no nosso retorno.</p>
                                <p>Nós da Viana & Moura desejamos um Feliz Natal e próspero Ano Novo!</p>
                            </div>
                        </div> -->
                        <?php $this->renderView(); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1 vinheta ">
                    <img src="cliente/img/miolo_home.png" class="img-responsive" />
                </div>
            </div>
            <div class="row" id="rodape-barra">
                <div class="col-lg-10 col-lg-offset-1" id="rodape">
                    <div id="rodape-copyright" class="col-lg-12 col-center">Viana & Moura Construções S/A</div>
                </div>
            </div>
        </div>

        <script type="text/javascript" charset="utf-8">

            $(function () {

                $("#email").focus();
                $("#cpf").mask("999.999.999-99");
                $("#cpfLogin").mask("999.999.999-99");

                SelectUpdate.init();
                ComplementarJs.init();
            });

        </script>
    </body>
</html>