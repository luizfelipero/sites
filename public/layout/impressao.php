<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <title>Viana &amp; Moura</title>
        <link href="bootstrap/css/bootstrap.css?v2" rel="stylesheet" type="text/css" />
        <link href="css_p/print.css" rel="stylesheet" media="print">
        <script src="js_t/jquery-1.7.2.min.js?v2"></script>
        <script src="bootstrap/js/bootstrap.js?v2"></script>
        <?php if ($this->page == "landscape") { ?>            
            <style>
                @page { 
                    size : A4 landscape;
                }     
            </style>          
        <?php } else { ?>
            <style>
                @page { 
                    size: A4;
                }
            </style>
        <?php } ?>
        <style>
<?php if ($this->tamanhoNatural != 1) { ?>      
                * {
                    font-size: 11px ;
                }
                .break { 
                    page-break-inside:  avoid; 
                }
<?php } ?>
        </style>
    </head>

    <body>

        <div id="content">
            <div class="row">
                <div id="" class="col-xs-12">
                    <img src="img_p/logo.png"/>
                </div>
                <!-- navbar com a logo da vm -->
            </div>         
            <?php $this->renderView(); ?>
        </div>
    </body>
</html>
<script type="text/javascript" charset="utf-8">

    window.print();

    $(function () {

        $(".elastic").elastic();

    });

</script>