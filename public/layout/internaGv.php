<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <meta name="viewport" content="initial-scale=1">

        <title>Viana & Moura</title>

        <!-- start css -->
        <link href="bootstrap/css/bootstrap.min.css?v3" rel="stylesheet" media="all">
        <link href="css_p/ajuste2.css?v6" rel="stylesheet" media="screen">
        <link href="plugins/font-awesome-4.4.0/css/font-awesome.min.css?v3" rel="stylesheet">
        <link href="plugins/jquery-ui-1.11.4.custom/jquery-ui.css" rel="stylesheet" media="screen"/>
        <link href="plugins/jquery-ui-1.11.4.custom/jquery-ui.theme.min.css" rel="stylesheet" media="screen"/>
        <!-- end css -->

        <!-- start js -->
        <script src="plugins/jquery-2.1.1.min.js?v3"></script>
        <script src="bootstrap/js/bootstrap.min.js?v3"></script>
        <script src="plugins/jquery-migrate-1.2.1.min.js?v3"></script>
        <script src="plugins/Highcharts-4.1.9/js/highcharts.js"></script>
        <script src="plugins/Highcharts-4.1.9/js/modules/exporting.js"></script>
        <!-- end js -->

        <meta name="theme-color" content="#972A27">

        <!-- start js_p -->
        <script src="js_p/alertModal.js?v3"></script>
        <script src="js_p/util.js"></script>
        <!-- end js_p -->


        <style type="text/css">
            body {
                padding-top: 0px;
                padding-bottom: 40px;
                background-image: url("img_p/bg-light.jpg");
                overflow-y: scroll;
            }
        </style>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div id="conteudoMacro" class="col-lg-12 col-md-12">
                    <noscript>
                    <div class="row msg-sig">
                        <div class="col-lg-12">
                            <div class="alert alert-danger">
                                <strong>Atenção!</strong> Identificamos que o javascript do seu navegador está desabilitado, desta forma o sistema não funcionará, habilite este recurso.
                            </div>
                        </div>
                    </div>
                    </noscript>
                    <div class="row msg-sig hide" id="msgSIG">
                        <div class="col-lg-12">
                            <div class="alert alert-warning ">
                                <strong>Atenção!</strong> O Sistema entrará em manutenção às 12:30 do dia (25/07), com previsão inicial de retorno às 14:00.
                            </div>
                        </div>
                    </div>
                    <div class="row" id="">
                        <div class="col-sm-12" id="msgAjax">      
                            <?php echo Util_FlashMessage::read(); ?>
                        </div>
                    </div>
                    <div class="conteudo" id="internaConteudo">
                        <div class="row">
                            <?php $this->renderView(); ?>
                        </div>
                    </div>
                    <input type="hidden" id="sleep" value="">
                </div>
            </div>
        </div>
    </body>
</html>