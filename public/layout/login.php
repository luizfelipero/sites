<!DOCTYPE html>
<html>
    <head>
        <title>Login</title>
        <!-- Bootstrap -->
        <link href="bootstrap/css/bootstrap.min.css?v2" rel="stylesheet" media="screen">
        <script src="plugins/jquery-2.1.1.min.js?v2"></script>
        <script src="bootstrap/js/bootstrap.min.js?v2"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="theme-color" content="#972A27">
        <style type="text/css">

            #full-content { 
                background: url("img_p/bg-light.jpg") no-repeat center center fixed; 
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
            }
            #form-login {
                background-color: #fff;
                border-radius: 5px;
                padding: 45px;
                padding-top: 15px;
                margin-top: 25%;
            }
            #form-login input {
                margin-top: 10px;
            }
            #form-login button {
                margin-top: 20px;
            }
            #footer {
                color: #fff;

            }

        </style>
    </head>
    <body>
        <div id="full-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="
                         col-sm-10 col-sm-offset-1
                         col-md-8 col-md-offset-2
                         col-lg-6 col-lg-offset-3
                         ">
                        <div class="row">
                            <div id="form-login" class="col-xs-8 col-xs-offset-2
                                 col-sm-6 col-sm-offset-3
                                 col-md-6 col-md-offset-3
                                 col-lg-6 col-lg-offset-3">
                                <form class="form-signin" action="<?php echo Util_Link::link("Index", "Index", "Index", "Login"); ?>" method="post">
                                    <?php echo Util_FlashMessage::read(); ?>
                                    <h4 class="form-signin-heading">Área Restrita</h4>
                                    <input name="login" id="login" type="text" class="form-control" placeholder="Login" value="albertopequeno">
                                    <input name="senha" type="password" class="form-control" placeholder="Senha" value="albertopequeno">
                                    <button class="btn btn-large btn-block btn-primary" type="submit">Entrar</button>
                                    <a href="<?php echo Util_Link::link("Serv", "Pessoa", "RecuperarSenha"); ?>">Esqueceu sua senha?</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" charset="utf-8">

            $(function () {

                $("#login").focus();
                $("#full-content").css('height', $(window).height());
                $(window).resize(function(){
                    $("#full-content").css('height', $(window).height());
                });

            })

        </script>
    </body>
</html>