<html>
    <head>
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        
        <link href="plugins/font-awesome-4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        
        <script src="plugins/jquery-2.1.1.min.js"></script>
        <script src="plugins/jquery-migrate-1.2.1.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="plugins/jquery-ui-1.8.21.custom.min.js"></script>
        
        <script src="js_t/jquery-1.7.2.min.js"></script>
        <script src="js_t/jquery_maskedinput.js"></script>
        <script src="js_p/validacoes.js"></script>
        <script src="js_t/jquery.price_format.js"></script>
        <script>
            $(function() {
                $(".cpf").mask("999.999.999-99");
                $(".telddd").mask("(99)9999-9999");
                $(".cep").mask("99999-999");
                $('input.valor,textarea.valor').priceFormat({
                    allowNegative: false,
                    prefix: '',
                    centsSeparator: ',',
                    thousandsSeparator: '.'
                }); 
            });
        </script>
        <style>
            input.obrigatorio-alert,
            .obrigatorio-alert {
                border-color: rgba(236, 100, 82, 0.8);
                outline: 0;
                outline: thin dotted \9;
                -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(236,100,82,.6);
                -moz-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(236,100,82,.6);
                box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(236,100,82,.6);
            }
        </style>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <?php $this->renderView(); ?>
                </div>
            </div>
        </div>
    </body>
</html>