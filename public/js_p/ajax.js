var Ajax = {
    telaLogin: 0,
    firstTime: true,
    init: function() {
        $(document).on('click', 'a', function(e) {
            Ajax.click(e, $(this));
        });
        $(document).on('click', 'input[type="submit"], button[type="submit"]', function(e) {

            Ajax.submit(e, $(this));
        });
        window.onpopstate = Ajax.history.back;

        $("#btn-loginJson").click(Ajax.login.submit);

        setInterval(Ajax.atualizaIndices, 120000);

        setTimeout(function() {
            Ajax.firstTime = false;
        }, 2500);

    },
    click: function(e, $link) {

        var url = $link.attr('href');
        //não faz nada
        // 1- a url é uma hastag
        if (url == undefined || url.indexOf("#") >= 0) {
            e.preventDefault();
        }
        // deixa ir sem unload e target blank
        // 2- target = blank
        // 3- a url é para uma tela de impressao
        // 4- a url é link para arquivo
        else if (
                $link.attr('target') == '_blank' ||
                url.indexOf("Imprimir") >= 0 ||
                Ajax.checaSeArquivo(url)
                ) {
            $(window).unbind('beforeunload');
            $link.attr('target', '_blank');
        }
        // deixa ir sem unload
        // 5- sync = 1
        else if ($link.data('sync') == 1) {
            $(window).unbind('beforeunload');
        }
        // 6- então faz ajax
        else {
            e.preventDefault();
            Ajax.connect(url, {ajax: 1}, 'get');
        }

    },
    checaSeArquivo: function(url) {
        //se tem ponto
        if (url.indexOf(".") >= 0) {
            var retorno = true;
        } else {
            //se é arquivo exibir
            if (url.indexOf("?m=Adm&c=Arquivo&a=Exibir") >= 0) {
                var retorno = true;
            } else {
                var retorno = false;
            }
        }
        return retorno;
    },
    submit: function(e, $submit) {
        Ajax.emptyMsg();
        var $form = $submit.closest('form');

        var url = $form.attr('action');

        if (!Complementar.obrigatorio.checkForm($form)) {
            e.preventDefault();
        } else {

            if ($form.data('sync') != 1) {
                e.preventDefault();
                $('#carregando').modal({
                    backdrop: 'static'
                });
                var dados = $form.serialize();
                if (url == undefined || url == '') {
                    url = "?";
                }
                if ($form.attr('method') == 'post' || $form.attr('method') == 'POST' || $form.attr('method') === undefined) {
                    var method = 'post';
                    url += "&ajax=1";
                } else {
                    var method = 'get';
                    dados += "&ajax=1";
                }

                Ajax.connect(url, dados, method);


            } else if ($form.data('sync') == 1) {

                $(window).unbind('beforeunload');
            }
        }
    },
    connect: function(url, dados, method) {

        $('#carregando').modal({
            backdrop: 'static'
        });

        $.ajax(url, {data: dados, type: method, cache: false}).done(function(dataAjax) {

            Ajax.trataRespostas(dataAjax);

        }).always(function() {

            if (Ajax.telaLogin == 0) {
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
            }
            $("#menu-desktop").click();
            $('.navbar-collapse').collapse('hide');
            $('#carregando').modal('hide');

        }).fail(function() {
            alert("Falha de conexão, tente novamente");
        });

    },
    trataRespostas: function(dataAjax) {
        //se é msg
        if (dataAjax.substring(0, 1) == "[") {
            Ajax.readMsg(dataAjax);
        }
        //add situação e que não está logado
        else if (dataAjax == "logoff") {

            Ajax.login.exibeTelaLogin();
        }
        //então é codigo
        else {
            Ajax.emptyMsg();
            $('#internaConteudo').html(dataAjax).hide().fadeIn(700);
            Complementar.initExtensoes();
        }
    },
    readMsg: function(dataAjax) {
        Ajax.emptyMsg();
        var retorno = "";
        var msgs = jQuery.parseJSON(dataAjax);
        for (var i = 0; i < msgs.length; i++) {
            retorno += '<div class="alert alert-' + msgs[i].tipo + ' alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Aviso!</strong>  ' + msgs[i].texto + '</div>';
        }

        $("#msgAjax").hide().html(retorno).fadeIn(300).fadeOut(200).fadeIn(500);

    },
    emptyMsg: function() {
        $("#msgAjax").html("");
    },
    history: {
        push: function(url) {
            Ajax.atual = url;
            url = url.replace("&ajax=1", "&sync=1");
            //url = Ajax.urlBase + url;            
            console.log(url);
            history.pushState('', 'Viana & Moura', url);
        },
        restore: function() {
            var url = Ajax.atual;
            url = url.replace("&ajax=1", "&sync=1");
            //url = Ajax.urlBase + url;            
            console.log(url);
            history.pushState('', 'Viana & Moura', url);
        },
        back: function() {
            if (Ajax.firstTime) {
                Ajax.firstTime = false;
            } else {
                var r = confirm("Tem certeza de que quer voltar?");
                if (r == true) {
                    $(window).unbind('beforeunload');
                    location.reload();
                } else {
                    Ajax.history.restore();
                }
            }
        }
    },
    login: {
        exibeTelaLogin: function() {
            $("#sessionSmartModalLogin").modal('show');
            $("#loginJson #login").val("");
            $("#loginJson #senha").val("");
            Ajax.telaLogin = 1;
        },
        submit: function() {
            var url = '?m=Index&c=Index&a=Login';
            $("#carregando").modal('show');
            //console.log(url);
            $.post(url, {json: 1, login: $("#loginJson #login").val(), senha: $("#loginJson #senha").val()}, function(data) {
                //console.log(data);
                if (data === "ok") {
                    $("#carregando").modal('hide');
                    $("#sessionSmartModalLogin").modal('hide');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    Ajax.telaLogin = 0;
                } else {
                    $("#carregando").modal('hide');
                    //$("#sessionSmartModalLogin").modal('show');
                    $("#modalLogin-span").text("Login ou senha inválidos");
                    $("#modalLogin-alert").removeClass('hide');
                }

            }).fail(function(jqXHR, exception) {
                $("#carregando").modal('hide');
                //$("#sessionSmartModalLogin").modal('show');
                $("#modalLogin-span").text("Falha de Conexão");
                $("#modalLogin-alert").removeClass('hide');

            });
        }

    },
    atualizaIndices: function() {
        $.ajax({
            type: "GET",
            url: "?m=Index&c=Index&a=SetQtdPendencias",
            success: function(data) {
                data = jQuery.parseJSON(data);
                $.each(data, function(index, value) {
                    if (value != null && value != 0) {
                        if (value == $(".label-" + index).html()) {
                            //faz nada
                        } else {
                            //troca e anima
                            $(".label-" + index).hide().html(value).fadeIn(700);
                        }
                    } else {
                        $(".label-" + index).html("").hide();
                    }
                });
            }
        });
    }
} 