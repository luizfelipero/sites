function removeMascara2(texto) {
    if (texto.indexOf(",") > -1) { //tem virgula
        texto = texto.replace(/\./g, "");
        texto = texto.replace(",", "");
        texto = Number(texto);
    } else {
        texto = Number(texto) * 100;
    }
    return texto;

}

function trataDecimal(valor) {
    valor = valor / 100;
    valor = Math.ceil(valor);

    return valor;
}

function multiplica(nome) {
    var id = "#" + nome;
    var data = "input[data-destino = '" + nome + "']";
    var total = 1;
    var i;

    $(data).each(function() {
        if ($(this).val() != "") {
            i = removeMascara2($(this).val());
            //alert(i);
        }
        else {
            i = 0;
        }
        total = total * i;
    });

    total = trataDecimal(total);

    $(id).val(total);
    $(id).priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });

    totaliza($(id).data("destino"));

}

function totaliza(nome) {
    //alert(nome);
    var id = "#" + nome;
    var data = "input[data-destino = '" + nome + "']";
    var total = 0;
    var i;
    $(data).each(function() {
        if ($(this).val() != "")
            i = removeMascara2($(this).val());
        else
            i = 0;
        total = total + (i * 1);

    });
    $(id).val(total);
    $(id).priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });
    //alert($(id).data("destino"));
    if ($(id).data("destino") != undefined) {
        totaliza($(id).data("destino"));
    }
}

function calculaTotal(nome) {

    var id = "#" + nome + "_total";
    var classe = "." + nome + "_subTotal";
    var total = 0;
    var i;
    $(classe).each(function() {
        //alert($(this).val());
        //alert(removeMascara2($(this).val()));
        if ($(this).val() != "")
            i = removeMascara2($(this).val());
        else
            i = 0;
        total = total + (i * 1);

    });
    $(id).val(total);
    $(id).priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });

}

$(function() {

    $(document).on('blur', ".formTotalizador", function() {
        var id = $(this).attr('id');
        var ids = id.split("_");
        var prefixo = ids[0];
        calculaTotal(prefixo);
    });

    $(document).on('blur', ".formMultiplicadorAB", function() {

        var id = $(this).attr('id');
        var ids = id.split("--");
        var prefixo = "#" + ids[0];
        var id_a = prefixo + "--a";
        var id_b = prefixo + "--b";
        var id_c = prefixo + "--c";
        var a, b;
        if ($(id_a).val() != "")
            a = removeMascara2($(id_a).val());
        else
            a = 0;
        if ($(id_b).val() != "")
            b = removeMascara2($(id_b).val());
        else
            b = 0;
        c = (a * b) / 100;
        c = Math.ceil(c);
        $(id_c).val(c);
        $(id_c).priceFormat({
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.'
        });
        prefixo = ids[0].split("_");
        calculaTotal(prefixo[0]);
    });

    $(document).on('blur', ".formAdicionadorAB", function() {

        var id = $(this).attr('id');
        var ids = id.split("--");
        var prefixo = "#" + ids[0];
        var id_a = prefixo + "--a";
        var id_b = prefixo + "--b";
        var id_c = prefixo + "--c";
        var a, b;
        if ($(id_a).val() != "")
            a = removeMascara2($(id_a).val());
        else
            a = 0;
        if ($(id_b).val() != "")
            b = removeMascara2($(id_b).val());
        else
            b = 0;
        c = (a + b);
        //alert(a + " " + b + " " + c);
        $(id_c).val(c);
        $(id_c).priceFormat({
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.'
        });
        prefixo = ids[0].split("_");
        calculaTotal(prefixo[0]);
    });

});