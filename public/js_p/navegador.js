var Navegador = {
    isChrome: function() {
        if (navigator.userAgent.indexOf("Chrome") > 0) {
            return true;
        }
        return false;
    },
    isFirefox: function() {
        if (navigator.userAgent.indexOf("Firefox") > 0) {
            return true;
        }
        return false;
    }, isMobile: function() {
        if (navigator.userAgent.match(/Android/i)
                || navigator.userAgent.match(/webOS/i)
                || navigator.userAgent.match(/iPhone/i)
                || navigator.userAgent.match(/iPad/i)
                || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i)
                || navigator.userAgent.match(/Windows Phone/i)
                ) {
            return true;
        }
        else {
            return false;
        }
    }
}

