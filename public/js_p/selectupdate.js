var SelectUpdate = {
    unidadePicker: 1,
    init: function () {

        $(document).on('change', '.selectUpdate2', $.debounce(250, SelectUpdate.selectUpdate2));
        $(document).on('change', '.selectUpdate2Double', $.debounce(250, SelectUpdate.selectUpdate2Double));
        $(document).on('change', '.selectUpdate2Contas', $.debounce(250, SelectUpdate.selectUpdate2Contas));
        $(document).on("click", ".unidadePicker", SelectUpdate.unidadePickerModal);
        //$("#selecionar-unidadePicker").click(SelectUpdate.selecionarUnidadePicker);
        $(document).on('change', ".selectUpdateDouble", SelectUpdate.selectUpdateDouble);

    },
    selectUpdate2: function () {

        //console.log("aqui");

        var url = $(this).data("url");
        var destino = "#" + $(this).data("destino");

//        console.log("entrou:",$(this).attr("id"));
//        console.log("html:",$(this).html());
//        console.log("destino:",destino);

        if ($(this).html() == '<option value="">---</option><option value="">Não há registros</option>' || $(this).html() == '') {

//            console.log ("caiu no if");
            $(this).addClass("hide");
            if (destino != "#undefined") {
                $(destino).html("");
                $(destino).addClass("hide");
                $(destino).trigger("change");
//                console.log ("trigou");
            }

        } else {
//            console.log ("caiu no else");
            $(this).removeClass("hide");
            //$(destino).after('<img class="selectUpdate2_loader offset4" src="img_p/selectUpdate2-loader.gif" />');
            $.getJSON(url + "&id=" + $(this).val(), function (j) {
                var options = "";
                for (var i = 0; i < j.length; i++) {
                    options += '<option value="' + j[i].id + '">' + j[i].nome + '</option>';
                }

                if (destino != "#undefined") {
                    if (j[0] != undefined) {
                        $(destino).html(options);
                        if ($(destino).html() != '<option value="">---</option><option value="">Não há registros</option>' && $(destino).html() != '') {
                            $(destino).removeClass("hide");
                        } else {
                            $(destino).addClass("hide");
                        }
                    } else {
                        $(destino).html("");
                        $(destino).addClass("hide");
                    }
                    $(destino).trigger("change");
//                    console.log ("trigou");
                }
            });
        }
        $(".selectUpdate2_loader").remove();
    },
    selectUpdate2Double: function () {

        var url1 = $(this).data("url1");
        var destino1 = "#" + $(this).data("destino1");
        var url2 = $(this).data("url2");
        var destino2 = "#" + $(this).data("destino2");

        if ($(this).html() == '<option value="">---</option><option value="">Não há registros</option>' || $(this).html() == '') {

//            console.log ("caiu no if");
            $(this).addClass("hide");
            if (destino1 != "#undefined") {
                $(destino1).html("");
                $(destino1).addClass("hide");
                $(destino1).trigger("change");
//                console.log ("trigou");
            }
            if (destino2 != "#undefined") {
                $(destino2).html("");
                $(destino2).addClass("hide");
                $(destino2).trigger("change");
//                console.log ("trigou");
            }

        } else {
//            console.log ("caiu no else");
            $(this).removeClass("hide");
            //$(destino).after('<img class="selectUpdate2_loader offset4" src="img_p/selectUpdate2-loader.gif" />');
            $.getJSON(url1 + "&id=" + $(this).val(), function (j) {
                var options = "";
                for (var i = 0; i < j.length; i++) {
                    options += '<option value="' + j[i].id + '">' + j[i].nome + '</option>';
                }

                if (destino1 != "#undefined") {
                    if (j[0] != undefined) {
                        $(destino1).html(options);
                        if ($(destino1).html() != '<option value="">---</option><option value="">Não há registros</option>' && $(destino1).html() != '') {
                            $(destino1).removeClass("hide");
                        } else {
                            $(destino1).addClass("hide");
                        }
                    } else {
                        $(destino1).html("");
                        $(destino1).addClass("hide");
                    }
                    $(destino1).trigger("change");
//                    console.log ("trigou");
                }
            });
            $.getJSON(url2 + "&id=" + $(this).val(), function (j) {
                var options = "";
                for (var i = 0; i < j.length; i++) {
                    options += '<option value="' + j[i].id + '">' + j[i].nome + '</option>';
                }

                if (destino2 != "#undefined") {
                    if (j[0] != undefined) {
                        $(destino2).html(options);
                        if ($(destino2).html() != '<option value="">---</option><option value="">Não há registros</option>' && $(destino2).html() != '') {
                            $(destino2).removeClass("hide");
                        } else {
                            $(destino2).addClass("hide");
                        }
                    } else {
                        $(destino2).html("");
                        $(destino2).addClass("hide");
                    }
                    $(destino2).trigger("change");
//                    console.log ("trigou");
                }
            });
        }
        $(".selectUpdate2_loader").remove();
    },
    selectUpdate2Contas: function () {

        var url1 = $(this).data("url1");
        var destino1 = "#" + $(this).data("destino1");
        var url2 = $(this).data("url2");
        var destino2 = "#" + $(this).data("destino2");
        var subEtapa_id = $("#nova_subEtapa_id").val();

        if ($(this).html() == '<option value="">---</option><option value="">Não há registros</option>' || $(this).html() == '') {

            $(this).addClass("hide");
            if (destino1 != "#undefined") {
                $(destino1).html("");
                $(destino1).addClass("hide");
            }
            if (destino2 != "#undefined") {
                $(destino2).html("");
                $(destino2).addClass("hide");
            }

        } else {
            $(this).removeClass("hide");
            $.getJSON(url1 + "&id=" + $(this).val() + "&subEtapa_id=" + subEtapa_id, function (j) {
                var options = "";
                for (var i = 0; i < j.length; i++) {
                    options += '<option value="' + j[i].id + '">' + j[i].nome + '</option>';
                }

                if (destino1 != "#undefined") {
                    
                    if (j[0] != undefined) {
                        $(destino1).html(options);
                        if ($(destino1).html() != '<option value="">---</option><option value="">Não há registros</option>' && $(destino1).html() != '') {
                            $(destino1).removeClass("hide");
                            $(destino1).trigger("change");
                        } else {
                            $(destino1).addClass("hide");
                        }
                    } else {
                        $(destino1).html("");
                        $(destino1).addClass("hide");
                    }
                }
            });
            $.getJSON(url2 + "&id=" + $(this).val() + "&subEtapa_id=" + subEtapa_id, function (j) {
                var options = "";
                for (var i = 0; i < j.length; i++) {
                    options += '<option value="' + j[i].id + '">' + j[i].nome + '</option>';
                }

                if (destino2 != "#undefined") {
                    if (j[0] != undefined) {
                        $(destino2).html(options);
                        if ($(destino2).html() != '<option value="">---</option><option value="">Não há registros</option>' && $(destino2).html() != '') {
                            $(destino2).removeClass("hide");
                        } else {
                            $(destino2).addClass("hide");
                        }
                    } else {
                        $(destino2).html("");
                        $(destino2).addClass("hide");
                    }
                }
            });
        }
        $(".selectUpdate2_loader").remove();
    },
    unidadePickerModal: function () {
        SelectUpdate.unidadePicker = $("#" + $(this).data("target"));
        $("#unidadePickerModal").modal('show');
    },
    selecionarUnidadePicker: function () {
        var unidade_id;
        var unidade;

        var un_nivel6 = $("#un_nivel6 option:selected");
        var un_nivel5 = $("#un_nivel5 option:selected");
        var un_nivel4 = $("#un_nivel4 option:selected");
        var un_nivel3 = $("#un_nivel3 option:selected");
        var un_nivel2 = $("#un_nivel2 option:selected");
        var un_nivel1 = $("#un_nivel1 option:selected");

        if (un_nivel6.val() != undefined && un_nivel6.val() != "") {
            unidade_id = un_nivel6.val();
            unidade = un_nivel6.text();
        } else if (un_nivel5.val() != undefined && un_nivel5.val() != "") {
            unidade_id = un_nivel5.val();
            unidade = un_nivel5.text();
        } else if (un_nivel4.val() != undefined && un_nivel4.val() != "") {
            unidade_id = un_nivel4.val();
            unidade = un_nivel4.text();
        } else if (un_nivel3.val() != undefined && un_nivel3.val() != "") {
            unidade_id = un_nivel3.val();
            unidade = un_nivel3.text();
        } else if (un_nivel2.val() != undefined && un_nivel2.val() != "") {
            unidade_id = un_nivel2.val();
            unidade = un_nivel2.text();
        } else if (un_nivel1.val() != undefined && un_nivel1.val() != "") {
            unidade_id = un_nivel1.val();
            unidade = un_nivel1.text();
        }

        var destino = "#" + SelectUpdate.unidadePicker.attr("id") + "_id";

        SelectUpdate.unidadePicker.val(unidade);
        $(destino).val(unidade_id).trigger("change");

        $("#unidadePickerModal").modal("hide");
    },
    selectUpdateDouble: function () {
        var id = $(this).attr('id');
        $.getJSON("", {m: $('#' + id + '_module').val(), c: $('#' + id + '_controller').val(), a: $('#' + id + '_action').val(), id: $(this).val(), ajax: 'true'}, function (j) {
            var options = '';
            for (var i = 0; i < j.length; i++) {
                options += '<option value="' + j[i].id + '">' + j[i].nome + '</option>';
            }
            var destino = '#' + $('#' + id + '_destino').val();
            $(destino).html(options);
            $(destino + '.selectUpdate').trigger("change");
        }),
                $.getJSON("", {m: $('#' + id + '_module2').val(), c: $('#' + id + '_controller2').val(), a: $('#' + id + '_action2').val(), id: $(this).val(), ajax: 'true'}, function (j) {
                    var options = '';
                    for (var i = 0; i < j.length; i++) {
                        options += '<option value="' + j[i].id + '">' + j[i].nome + '</option>';
                    }
                    var destino = '#' + $('#' + id + '_destino2').val();
                    $(destino).html(options);
                    $(destino + '.selectUpdate').trigger("change");
                });
    }
};