var Complementar = {
    init: function () {

        Autocomplete.init();
        SelectUpdate.init();
        //SmartSession.init();
        Ajax.init();

        $(document).on("click", ".filtro-botao", Complementar.filtro);
        $(document).on("click", "ul.nav li", Complementar.tabs);
//        $('a[data-toggle="tab"]').on("shown.bs.tab", Complementar.tabs);
        $(document).on("click", ".panel-heading .fa-th", Complementar.fullScreen);
        $(document).on("change.bs.fileinput", ".fileinput", Complementar.maxSize);
        $(document).on('focus', 'input, textarea', Complementar.initMascaras);
        $(document).on('click', '.remove-arquivo', Complementar.removeArquivoModal);
        $(document).on('click', '.btn-removeArquivo', Complementar.removeArquivo);
        Complementar.initExtensoes();

        //previne que quando o enter seja pressionado o formulário seja enviado
        document.onkeypress = Complementar.stopRKey;

        //dar alerta quando o usuario tenta fechar a tab ou atualizar
        $(window).bind('beforeunload', function (event) {
            return 'Atenção!\nVocê está prestes a fechar esta janela. Caso não tenha salvo as alterações serão perdidas.';
            stopSmartSession = 1;
        });

        //$(document).on("blur", ".obrigatorio", Complementar.obrigatorio.checkIndividual);
        $(document).on("click", ".erase-select2", Complementar.eraseSelect2);

    },
    eraseSelect2: function () {

        var id = $(this).data("destino");
        $("#" + id).val(null).trigger("change");
    },
    initMascaras: function () {
        $('input.valor,textarea.valor').priceFormat({
            allowNegative: true,
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.'
        });
        $(".cnpj").mask("99.999.999/9999-99");
        $(".cep").mask("99999-999");
        $(".cpf").mask("999.999.999-99");
        $(".pis").mask("999.99999.99-9");
        $(".dataCampo").mask("99/99/9999");
        $(".data").each(function () {
            $(this).datepicker({dateFormat: 'dd/mm/yy'});
            $(this).attr("readonly", true);
            $(this).attr("placeholder", "calendário");
            $(this).addClass("readonly-clickable");
        }); // 10/01/1989
        $(".datafp").each(function () {
            var min = ($(this).data('min') != undefined) ? $(this).data('min') : "-30";
            var max = ($(this).data('max') != undefined) ? $(this).data('max') : "30";
            $(this).datepicker({dateFormat: 'dd/mm/yy', minDate: min, maxDate: max});
            $(this).attr("readonly", true);
            $(this).attr("placeholder", "calendário");
            $(this).addClass("readonly-clickable");
        });
        $(".hora").mask("99:99");
        $(".tel").mask("99999999?9");
        $(".telddd").mask("(99)99999999?9");
        $(".numeroContrato").mask("999999999999-*");
        $(".elastic").elastic();

        $("input:text").attr('autocomplete', 'off');
        $("input.autocomplete").attr('autocomplete', 'on');
        //$("input.valor").attr('autocomplete', 'off');
    },
    initExtensoes: function () {
        $(".tooltips").tooltip({'placement': 'bottom', 'trigger': 'hover'});
        $(".popovers").popover({'placement': 'bottom', 'trigger': 'hover'});



        Autocomplete.init();
        Complementar.initMascaras();
    },
    stopRKey: function (evt) {
        var evt = (evt) ? evt : ((event) ? event : null);
        var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
        if ((evt.keyCode == 13) && ((node.type == "text") || (node.type == "password"))) {
            return false;
        }
    },
    tr: {
        addTr: function ($tbody, $lastItemMarker) {
            var lastItem = $lastItemMarker.val();
            lastItem = lastItem * 1;
            var newItem = lastItem + 1;

            var $tr = $tbody.find("tr:last-child").clone();
            $tr.find("select, input, button").each(function (i, el) {
                el = $(el);
                el.val("");
                $.each(this.attributes, function () {
                    if (this.specified) {
                        if (this.name == "name" || this.name == "id" || this.name == "data-target" || this.name == "data-destino") {
                            this.value = this.value.replace(/\d+/g, newItem);
                        }
                    }
                });
            });

            $tbody.append($tr);
            $lastItemMarker.val(newItem);

            Complementar.tr.updateTrIndex($tbody);
        },
        removeTr: function ($trigger) {
            var $tbody = $trigger.closest("tbody");
            if ($tbody.find("tr").length > 1) {
                $trigger.closest("tr").remove();
                Complementar.tr.updateTrIndex($tbody);
            }
        },
        updateTrIndex: function ($tbody) {
            $tbody.find("td:first-child").each(function (i, el) {
                $(el).empty().append(i + 1);
            });
        }
    },
    filtro: function () {
        $(".filtro-opcoes").toggleClass("hide");
        Autocomplete.init();
    },
    tabs: function () {
        Autocomplete.init();
    },
    fullScreen: function () {
        $("#menu-desktop").toggleClass("hide");
        $("#conteudoMacro").toggleClass("col-lg-10", "col-lg-12").toggleClass("col-md-9", "col-md-12");
    },
    maxSize: function () {

        var inputFile = $(this).find("input:file");
        var max = inputFile.data("maxsize");
        if (max == undefined) {
            max = 2;
        }
        if (inputFile.get(0).files[0].size > 1024 * 1024 * max) {
            $(this).fileinput('clear');
            alert('arquivo grande');
        }
    },
    obrigatorio: {
        checkIndividual: function () {
            if ($(this).hasClass('data') === false && $(this).hasClass('data') === false) {
                if ($(this).val() != "") {
                    $(this).removeClass("obrigatorio-alert");
                } else {
                    if (!$(this).hasClass("obrigatorio-alert")) {
                        $(this).addClass("obrigatorio-alert");
                    }
                    alert("Este campo não pode permanecer em branco");
                }
            }
        },
        checkForm: function ($form) {

            //testa se há obrigatorios vazios
            if ($form.find('.obrigatorio').filter(function () {
                return $(this).val() == "";
            }).length != 0) {
                //e.preventDefault();
                $form.find('.obrigatorio').filter(function () {
                    return $(this).val() == "";
                }).each(function (i, el) {
                    el = $(el);
                    el.addClass("obrigatorio-alert");
                    $('#obrigatorio').modal();
                });
                return false;
            } else {
                return true;
            }

        }
    },
    clickLink: function (e, link) {

        if (link.hasClass("confirmar")) {

            e.preventDefault();
            var href = link.attr("href");
            var texto = link.attr("name");
            $("#confirmar-btn").prop("href", href);
            $("#confirmarBody").html(texto);
            $('#confirmar').modal();

        } else {
            $(window).unbind('beforeunload');

            if (link.attr("target") != "_blank") {
                $('#carregando').modal({
                    backdrop: 'static'
                });
            }

        }
    },
    rgb2hex: function (rgb) {
        rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
        function hex(x) {
            return ("0" + parseInt(x).toString(16)).slice(-2);
        }
        return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
    },
    exibePorcentagem: function (num) {
        num = parseFloat(num);
        num = num.toFixed(2);
        num = num * 100;
        num = num.toString();
        num = num + "%";
        return num;
    },
    removeArquivoModal: function () {

        console.log("arq " + $(this).attr('data-arquivo-id'));
        /*$.post("?m=Adm&c=Arquivo&a=DesativarModal&id=" + $(this).attr('data-arquivo-id'), {}, function (retorno) {
         $("#modalExcluirArquivo").html(retorno);
         $("#modalExcluirArquivo").modal('show');
         }, "html");*/
        $("#modalExcluirArquivo .arquivo-id").val($(this).attr('data-arquivo-id'));
        $("#modalExcluirArquivo").modal('show');

    },
    removeArquivo: function () {
        console.log("here");
        var arquivo_id = $("#modalExcluirArquivo .arquivo-id").val();
        console.log("arq_id " + arquivo_id);
        $.post("?m=Adm&c=Arquivo&a=Desativar&id=" + arquivo_id, {}, function (retorno) {
            console.log("ret " + retorno);
            if (retorno == 1) {
                //$(this).parent().parent().parent().addClass("hide");
                $("#td-" + arquivo_id).addClass("hide");
                $("#modalExcluirArquivo").modal('hide');
            }
        }, "html");
    }

};
$(function () {
    Complementar.init();
});
