var Complementar = {
    init: function () {

        Autocomplete.init();
        SelectUpdate.init();
        SmartSession.init();
        //Ajax.init();
    
        $(document).on("click", ".filtro-botao", Complementar.filtro);
        $(document).on("click", ".panel-heading .fa-th", Complementar.fullScreen);
        $(document).on("change", "input:file", Complementar.maxSize);
        $(document).on('focus', 'input,textarea', Complementar.initMascaras);

        //previne que quando o enter seja pressionado o formulário seja enviado
        document.onkeypress = Complementar.stopRKey;

        //dar alerta quando o usuario tenta fechar a tab ou atualizar
        $(window).bind('beforeunload', function (event) {
            return 'Atenção!\nVocê está prestes a fechar esta janela. Caso não tenha salvo as alterações serão perdidas.';
            stopSmartSession = 1;
        });

        $("input:text").attr('autocomplete', 'off');
        //$("input.valor").attr('autocomplete', 'off');

        $(".tooltips").tooltip({'placement': 'bottom', 'trigger': 'hover'});
        $(".popovers").popover({'placement': 'bottom', 'trigger': 'hover'});

        $(".elastic").elastic();
        $("select.select2").select2();

        $(document).on("keyup", ".obrigatorio", Complementar.obrigatorio.checkIndividual);
        $(document).on('click', 'input[type="submit"], button[type="submit"]', function (e) {
            Complementar.obrigatorio.checkForm(e, $(this));
        });
        $(document).on('click', 'a[href^="?"]', function (e) {
            Complementar.clickLink(e, $(this));
        });

    },
    initMascaras: function () {
        $('input.valor,textarea.valor').priceFormat({
            allowNegative: true,
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.'
        });
        $(".cnpj").mask("99.999.999/9999-99");
        $(".cep").mask("99999-999");
        $(".cpf").mask("999.999.999-99");
        $(".data").mask("99/99/9999");
        $(".hora").mask("99:99");
        $(".tel").mask("9999-9999");
        $(".telddd").mask("(99)9999-9999");
        $(".numeroContrato").mask("999999999999-*");
    },
    stopRKey: function (evt) {
        var evt = (evt) ? evt : ((event) ? event : null);
        var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
        if ((evt.keyCode == 13) && ((node.type == "text") || (node.type == "password"))) {
            return false;
        }
    },
    tr: {
        addTr: function ($tbody, $lastItemMarker) {
            var lastItem = $lastItemMarker.val();
            lastItem = lastItem * 1;
            var newItem = lastItem + 1;

            var $tr = $tbody.find("tr:last-child").clone();
            $tr.find("select, input, button").each(function (i, el) {
                el = $(el);
                el.val("");
                $.each(this.attributes, function () {
                    if (this.specified) {
                        if (this.name == "name" || this.name == "id" || this.name == "data-target" || this.name == "data-destino") {
                            this.value = this.value.replace(/\d+/g, newItem);
                        }
                    }
                });
            });

            $tbody.append($tr);
            $lastItemMarker.val(newItem);

            Complementar.tr.updateTrIndex($tbody);
        },
        removeTr: function ($trigger) {
            var $tbody = $trigger.closest("tbody");
            if ($tbody.find("tr").length > 1) {
                $trigger.closest("tr").remove();
                Complementar.tr.updateTrIndex($tbody);
            }
        },
        updateTrIndex: function ($tbody) {
            $tbody.find("td:first-child").each(function (i, el) {
                $(el).empty().append(i + 1);
            });
        }
    },
    filtro: function () {
        $(".filtro-opcoes").toggleClass("hide");

        //Seta o tamanho do select2 ao tamanho do select original
        $(".filtro-opcoes").find("select.select2").each(function () {
            var tamanho = $(this).width();
            $(this).select2({width: tamanho});
        });

        Autocomplete.pessoa.init();

    },
    fullScreen: function () {
        $("#menu-desktop").toggleClass("hide");
        $("#conteudoMacro").toggleClass("col-lg-10", "col-lg-12").toggleClass("col-md-9", "col-md-12");
    },
    maxSize: function () {
        var max = $(this).data("maxsize");
        if (max == undefined) {
            max = 2;
        }
        if ($(this).get(0).files[0].size > 1024 * 1024 * max) {
            $(this).val("");
            alert('arquivo grande');
        }
    },
    obrigatorio: {
        checkIndividual: function () {
            if ($(this).hasClass("autocomplete") || $(this).hasClass("autocompletePessoa")) {
                if ($(this).find(':input:first').val() != "") {
                    if ($(this).val() == "") {
                        $(this).find(':input:first').val("");
                        $(this).addClass("obrigatorio-alert");
                    } else {
                        $(this).removeClass("obrigatorio-alert");
                    }
                } else {
                    if (!$(this).hasClass("obrigatorio-alert")) {
                        $(this).addClass("obrigatorio-alert");
                    }
                }
            } else {
                if ($(this).val() != "") {
                    $(this).removeClass("obrigatorio-alert");
                } else {
                    if (!$(this).hasClass("obrigatorio-alert")) {
                        $(this).addClass("obrigatorio-alert");
                    }
                }
            }
        },
        checkForm: function (e, input) {
            e.preventDefault();
            //testa se há obrigatorios vazios
            if (input.closest("form").find('.obrigatorio').filter(function () {
                return $(this).val() == "";
            }).length != 0) {
                //e.preventDefault();
                input.closest("form").find('.obrigatorio').filter(function () {
                    return $(this).val() == "";
                }).each(function (i, el) {
                    el = $(el);
                    var elAnterior = el.prev("input:text");
                    if (elAnterior.hasClass("autocomplete") || elAnterior.hasClass("autocompletePessoa")) {
                        elAnterior.addClass("obrigatorio-alert");
                    } else {
                        el.addClass("obrigatorio-alert");
                    }
                    $('#obrigatorio').modal();
                });
            } else {
                SmartSession.testaSessao(input.closest("form"));
            }

        }
    },
    clickLink: function (e, link) {
        SmartSession.stopSmartSession = 1;

        if (link.hasClass("confirmar")) {

            e.preventDefault();
            var href = link.attr("href");
            var texto = link.attr("name");
            $("#confirmar-btn").prop("href", href);
            $("#confirmarBody").html(texto);
            $('#confirmar').modal();

        } else {
            $(window).unbind('beforeunload');

            if (link.attr("target") != "_blank") {
                $('#carregando').modal({
                    backdrop: 'static'
                });
            } else {
                SmartSession.stopSmartSession = 0;
            }

        }
    }
};
$(function () {
    Complementar.init();
});
