SmartSession = {
    statusSessao: 1,
    testeSession: '',
    stopSmartSession: '',
    init: function() {

        //funcao que roda de tempos em tempo
        setInterval(function() {
            SmartSession.testaSessao();
        }, 10000);
        $("#semconexao").addClass('hide');
        

        $('#sessionSmartModalConexao').modal({
            keyboard: false,
            //backdrop: false,
            show: false
        });
        $('#sessionSmartModalLogin').modal({
            keyboard: false,
            //backdrop: false,
            backdrop: 'static',
            show: false
        });
        $("#semconexao").click(function() {
            $("#sessionSmartModalConexao").modal('show');
        });

        //funcao que faz o login do cara
        $("#btn-loginJson").click(function() {
            //console.log("conectando");
            //$("#sessionSmartModalLogin").modal('hide');
            $("#carregando").modal('show');

            var url = '?m=Index&c=Index&a=Login';
            //console.log(url);
            $.post(url, {json: 1, login: $("#loginJson #login").val(), senha: $("#loginJson #senha").val()}, function(data) {
                //console.log(data);
                if (data === "ok") {
                    $("#carregando").modal('hide');
                    $("#sessionSmartModalLogin").modal('hide');
                    SmartSession.statusSessao = 1;
                } else {
                    $("#carregando").modal('hide');
                    //$("#sessionSmartModalLogin").modal('show');
                    $("#modalLogin-span").text("Login ou senha inválidos");
                    $("#modalLogin-alert").removeClass('hide');
                }

            }).fail(function(jqXHR, exception) {
                //alert(jqXHR.status);     
                $("#carregando").modal('hide');
                //$("#sessionSmartModalLogin").modal('show');
                $("#modalLogin-span").text("Falha de Conexão");
                $("#modalLogin-alert").removeClass('hide');

            });

        });

    },
    submitForm: function(form) {
        SmartSession.testaSessao(form);
    },
    testaSessao: function(form) {

        SmartSession.atualizaIndices();

        //if brownser não é o chrome, diz que tá ok e submita o form q vinher
        if (!Navegador.isChrome() && !Navegador.isFirefox()) {

            SmartSession.statusSessao = 1;

            if (form != undefined) {

                if (form.data("target") != "blank") {
                    $('#carregando').modal({
                        backdrop: 'static'
                    });
                }
                $(window).unbind('beforeunload');
                form.submit();
            }

        } else {

            SmartSession.testeSession = $.get("?m=Index&c=Index&a=TestaSessao", function(data) {

                if (data == 2) {
                    //alert("voce ainda esta logado (fará nada)");
                    //alert("esconde a tela de conect perdida");
                    //if (statusSessao=1) {
                    $("#semconexao").addClass('hide');
                    $("#sessionSmartModalLogin").modal('hide');
                    //}
                    SmartSession.statusSessao = 1;
                    //console.log("entrou 1.1");

                    //alert ("beleza");

                    if (form != undefined) {
                        if (form.data("target") != "blank") {
                            $('#carregando').modal({
                                backdrop: 'static'
                            });
                        }
                        $(window).unbind('beforeunload');
                        form.submit();
                    }

                } else if (data == 1) {
                    if (SmartSession.statusSessao != 2) {
                        SmartSession.exibeTelaLogin();
                    }
                    SmartSession.statusSessao = 2;
                    $("#semconexao").addClass('hide');
                    //console.log("entrou 1.2");
                } else {
                    //alert ("estranho");
                }
            }).fail(function(jqXHR, exception) {
                //alert(jqXHR.status);     
                if (SmartSession.stopSmartSession != 1 && SmartSession.statusSessao != 3) {

                    SmartSession.exibeTelaConexao();
                }
                SmartSession.statusSessao = 3;

            });
        } //fim do else do navegador
    },
    exibeTelaLogin: function() {
        //alert("faça o seu login");

        $("#carregando").modal('hide');
        $("#sessionSmartModalLogin").modal('show');
        $("#loginJson #login").val("");
        $("#loginJson #senha").val("");

    },
    exibeTelaConexao: function() {
        //alert("aguarde um momento, a conexão expirou");
        $("#sessionSmartModalLogin").modal('hide');
        $("#carregando").modal('hide');
        $("#semconexao").removeClass('hide');

    },
    atualizaIndices: function() {
        $.ajax({
            type: "GET",
            url: "?m=Acao&c=Acao&a=SetQtdAcoes",
            dataType: 'json',
            success: function(data) {
                //minhas
                $(".myAll").fadeOut(function() {
                    $(this).html(data.MyAll).fadeIn();
                });
                $(".myNova").fadeOut(function() {
                    $(this).html(data.MyNova).fadeIn();
                });
                $(".myAndamento").fadeOut(function() {
                    $(this).html(data.MyAndamento).fadeIn();
                });
                $(".myConcluida").fadeOut(function() {
                    $(this).html(data.MyConcluida).fadeIn();
                });
                //delegadas
                $(".delAll").fadeOut(function() {
                    $(this).html(data.DelAll).fadeIn();
                });
                $(".delAceitacao").fadeOut(function() {
                    $(this).html(data.DelAceitacao).fadeIn();
                });
                $(".delPendente").fadeOut(function() {
                    $(this).html(data.DelPendente).fadeIn();
                });
                $(".delRejeitada").fadeOut(function() {
                    $(this).html(data.DelRejeitada).fadeIn();
                });
                $(".delConfirmacao").fadeOut(function() {
                    $(this).html(data.DelConfirmacao).fadeIn();
                });
                //emitidas
                $(".emAll").fadeOut(function() {
                    $(this).html(data.EmAll).fadeIn();
                });
                $(".emAceitacao").fadeOut(function() {
                    $(this).html(data.EmAceitacao).fadeIn();
                });
                $(".emPendente").fadeOut(function() {
                    $(this).html(data.EmPendente).fadeIn();
                });
                $(".emRejeitada").fadeOut(function() {
                    $(this).html(data.EmRejeitada).fadeIn();
                });
                $(".emConfirmacao").fadeOut(function() {
                    $(this).html(data.EmConfirmacao).fadeIn();
                });
            }
        });
    }
}