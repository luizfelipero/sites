Util = {
    trataEntradas: function (arg) {
        arg = arg.split("&");
        var urlArray = new Array();
        arg.forEach(function (link) {
            link = link.split("=");
            link[1] = link[1].replace("+", " ");
            urlArray[link[0]] = link[1];
        });
        return urlArray;
    },
    addZeros: function (num, max) {
        num = num.toString();

        while (num.length < max) {
            num = "0" + num;
        }
        return num;
    },
    exibeNumero: function (num) {
        if (num == "") {
            num = 0;
        }
        var numFloat = Util.transformaNumero(num, 2);
        num = numFloat.toString();
        num = num.replace(".", ",");
        return num;
    },
    exibeDias: function (dias) {
        if (dias == "") {
            dias = 0;
        }
        var diasFloat = Util.transformaNumero(dias, 1);
        dias = diasFloat.toString();
        dias = dias.replace(".", ",");
        return dias;
    },
    formataNumero: function (num) {

        if (num == "" || num === undefined) {
            num = 0;
        } else {
            num = num.replace(".", "");
            num = num.replace(".", "");
            num = num.replace(".", "");
            num = num.replace(".", "");
            num = num.replace("%2C", ".");
            num = num.replace(",", ".");
        }
        num = parseFloat(num, 2);
        return num;
    },
    formataHora: function (hora) {
        if (hora == "") {
            hora = '00:00';
        }

        hora = hora.replace("%3A", ":");
        hora = hora.split(":");

        var novaHora = Util.addZeros(hora[0], 2);
        var novoMinuto = Util.addZeros(hora[1], 2);
        var horaF = novaHora + ":" + novoMinuto;

        return horaF;
    },
    exibePorcentagem: function (num) {
        num = num.replace(",", ".");
        var numFloat = parseFloat(num);
        numFloat = numFloat.toFixed(2);
        num = numFloat.toString();
        num = num * 100;
        num = num + "%";
        return num;
    },
    exibeData: function (data) {
        if (data != undefined && data != "0000-00-00") {
            var ano = data.substr(0, 4);
            var mes = data.substr(5, 2);
            var dia = data.substr(8, 2);
            var datar = dia + "/" + mes + "/" + ano;
        }
        return datar;
    },
    exibeDataDiaMes: function (data) {
        if (data != "" && data != "0000-00-00") {
            var mes = data.substr(5, 2);
            var dia = data.substr(8, 2);
            var datar = dia + "/" + mes;
        }
        return datar;
    },
    exibeHora: function (hora) {

        if (hora != "" && hora != "00:00:00") {
            var horas = hora.substr(0, 2);
            var minutos = hora.substr(3, 2);
            var segundos = hora.substr(5, 2);
            var horar = horas + ":" + minutos;
        }
        return horar;
    },
    exibeDataHora: function (data) {
        if (data != "" && data != "0000-00-00 00:00:00") {
            var ano = data.substr(0, 4);
            var mes = data.substr(5, 2);
            var dia = data.substr(8, 2);
            var hora = data.substr(11, 2);
            var minuto = data.substr(14, 2);
            var datar = dia + "/" + mes + "/" + ano + " às " + hora + ":" + minuto;
        }
        return datar;
    },
    formataData: function (data) {
        data = data.replace("%2F", "/");
        data = data.replace("%2F", "/");

        var teste = data.substr(2, 1);
        if (teste == "-" || teste == "/") {

            data = data.replace("_", "");
            data = data.replace("/", "");
            data = data.replace("/", "");
            data = data.replace("-", "");
            if (data != "") {
                var ano = data.substr(4, 4);
                var mes = data.substr(2, 2);
                var dia = data.substr(0, 2);
                var datar = ano + "-" + mes + "-" + dia;
            }
            return datar;
        } else {
            return data;
        }
    },
    alert: function (msg) {
//        if (FrontController.server == "mobile") {
//            navigator.notification.alert(
//                    msg,
//                    function () {
//                    },
//                    'Aviso',
//                    'Ok'
//                    );
//        } else {
        alert(msg);
//        }
    },
    confirmacao: function (msg) {
        if (FrontController.server == "mobile") {
            navigator.notification.alert(
                    msg,
                    function () {
                    },
                    'Aviso',
                    'Ok'
                    );
            navigator.notification.confirm(
                    'Deseja executar a ação?',
                    Util.respostaSim,
                    'Pergunta',
                    ['sim', 'não']
                    );
        } else {
            return confirm(msg);
        }
    },
    respostaSim: function (botao) {
        if (botao == 1)
            navigator.notification.alert('Ação executada!', function () {
            }, 'sucesso', 'fechar');
    },
    makeLists: function (objs, extra) {

        var comp = "";
        var retorno = "";
        if (extra == undefined) {
            extra = "";
        }
        var seletores = ["selecione", "none", "tg", "t", "g"];
        var seletoresValores = ["", "", "tg", "t", "g"];
        var seletoresRotulos = ["--Selecione--", "---", "Todos e Geral", "Todos", "Geral"];
        //Se tiver seletor, coloca ele como 'selected'
        if (extra['seletores'] instanceof Array) {
            for (var i = 0; i < seletores.length; i++) {
                if ($.inArray(seletores[i], extra['seletores']) > -1) {
                    comp = '<option value="' + seletoresValores[i] + '"';
                    if (seletores[i] == extra['default']) {
                        comp += ' selected = "selected"';
                    }
                    comp += '>' + seletoresRotulos[i] + '</option>';
                    retorno = retorno + comp;
                }
            }
        }

        //Coloca 'selected' nos objects que tão como default
        objs.forEach(function (obj) {
            comp = '<option value="' + obj.id + '"';
            if (obj.id == extra['default']) {
                comp += ' selected = "selected"';
            }
            comp += '>';
            comp += obj.nome;
            comp += '</option>';
            if (extra['ord'] == 'c' || extra['ord'] == undefined) {
                retorno += comp;
            } else if (extra['ord'] == 'd') {
                retorno = comp + retorno;
            }
        });
//        $(".makeLists").html(retorno);
        return retorno;
    },
    link: function (m, c, a, id, etc) {
        if (etc == undefined) {
            etc = "";
        }
        var ret;

        if (m == undefined && c == undefined && a == undefined) {
            ret = "";
        } else {
            ret = "?m=" + m + "&c=" + c + "&a=" + a;
        }
        if (id != undefined) {
            ret += "&id=" + id;
        }
        ret += etc;

        return ret;
    },
    linkHideForm: function (m, c, a) {
        var ret = "";
        if (m == undefined && c == undefined && a == undefined) {
            ret = "";
        } else {
            ret = "<input type=\"hidden\" name=\"m\" value=\"" + m + "\">";
            ret += "<input type=\"hidden\" name=\"c\" value=\"" + c + "\">";
            ret += "<input type=\"hidden\" name=\"a\" value=\"" + a + "\">";
        }
        var element = document.getElementsByTagName("form");
        return element.innerHTML = ret;
    },
    paginacao: function (arg, extras) {
        if (arg['index'] == undefined) {
            arg['index'] = "";
        }
        var numArg = arg['index'].split(',').map(Number);
        var ext = "";
        var ret = "";
        var key = "";
        var val = "";
        var val2 = "";
        if (extras != undefined) {
            if (extras instanceof Array) {
//                reset(extras); verificar
                var objExtras = $.extend({}, extras);
                $.each(objExtras, function (key, val) {
                    if (val instanceof Array) {
                        $.each(objExtras, function (key2, val2) {
                            ext += '&' + key + '%5B%5D=' + val2;
                        });
                    } else {
                        ext += '&' + key + '=' + val;
                    }
                });
            }
        }
        var link = arg['link'] + ext;
        var paginas = Math.ceil(arg['total'] / arg['limit']);
        ret += '<div style="text-align: center">';
        ret += '<ul class="pagination">';
        if (arg['index'] > 0) {
            ret += '<li><a href="' + link + '&index=0"><i class="fa fa-backward icon-red"></i></a></li>';
            ret += '<li><a href="' + link + '&index=' + (numArg[0] - 1) + '"><i class="fa fa-chevron-left icon-red"></i></a></li>';
        }

        //get limites superior e inferior
        var lSuperior = ((paginas - arg['index']) > 5) ? (numArg[0] + 5) : (paginas - 1);
        var lInferior = ((arg['index']) < 5) ? 0 : (numArg[0] - 5);
        for (var i = lInferior; i <= lSuperior; i++) {
            ret += '<li';
            ret += (arg['index'] == i) ? ' class="active" ' : "";
            ret += '><a href="' + link + '&index=' + i + '">' + (i + 1) + '</a></li>';
        }

        if (arg['index'] < (paginas - 1)) {
            ret += '<li><a href="' + link + '&index=' + (numArg[0] + 1) + '"><i class="fa fa-chevron-right icon-red"></i></a></li>';
            ret += '<li><a href="' + link + '&index=' + (paginas - 1) + '"><i class="fa fa-forward icon-red"></i></a></li>';
        }

        ret += '</ul>';
        ret += '</div>';
        $(document).ready(function () {
            $(".paginacao").html(ret);
        });
    },
    getDataAtual: function () {
        var data = new Date();
        var diaAtual = Util.addZeros(data.getDate(), 2);
        var mesAtual = Util.addZeros((data.getMonth() + 1), 2);
        var anoAtual = data.getFullYear();
        var dataAtual = anoAtual + "-" + mesAtual + "-" + diaAtual;

        return dataAtual;
    },
    getDataHoraAtual: function () {
        var data = new Date();
        var segundoAtual = Util.addZeros(data.getSeconds(), 2);
        var minutoAtual = Util.addZeros(data.getMinutes(), 2);
        var horaAtual = Util.addZeros(data.getHours(), 2);
        var diaAtual = Util.addZeros(data.getDate(), 2);
        var mesAtual = Util.addZeros((data.getMonth() + 1), 2);
        var anoAtual = data.getFullYear();
        var dataAtual = anoAtual + "-" + mesAtual + "-" + diaAtual + " " + horaAtual + ":" + minutoAtual + ":" + segundoAtual;

        return dataAtual;
    },
    getDataHoraAtualMin: function (min) {
        var data = new Date();
        data.setMinutes(data.getMinutes() + min);
        var segundoAtual = Util.addZeros(data.getSeconds(), 2);
        var minutoAtual = Util.addZeros(data.getMinutes(), 2);
        var horaAtual = Util.addZeros(data.getHours(), 2);
        var diaAtual = Util.addZeros(data.getDate(), 2);
        var mesAtual = Util.addZeros((data.getMonth() + 1), 2);
        var anoAtual = data.getFullYear();
        var dataAtual = anoAtual + "-" + mesAtual + "-" + diaAtual + " " + horaAtual + ":" + minutoAtual + ":" + segundoAtual;

        return dataAtual;
    },
    somaDias: function (data, dias) {
        var data = data.split("-");

        var ano = data[0];
        var mes = (data[1] - 1);
        var dia = data[2];

        var novaData = new Date(ano, mes, dia);

        novaData.setDate(novaData.getDate() + (dias));
        var novoMes = novaData.getMonth() + 1;
        var dataAlterada = novaData.getFullYear() + "-" + (Util.addZeros(novoMes, 2)) + "-" + Util.addZeros(novaData.getDate(), 2);

        return dataAlterada;
    },
    transformaNumero: function (num, casasDec) {
        var numero = parseFloat(num);
        numero = numero.toFixed(casasDec);

        return numero;
    },
    mensagem: function (msg, tipo) {
        if (tipo != undefined) {
            tipo = "alert-" + tipo;
        } else {
            tipo = "alert-warning";
        }
        var retorno = '<div class="alert ' + tipo + '">\n\
                <button type=\"button\" class="close" data-dismiss="alert">&times;</button>\n\
                <strong>Aviso!</strong> ' + msg + ' </div>';

        return retorno;
    },
    getHoraAtual: function () {
        var data = new Date();
        var minuto = Util.addZeros(data.getMinutes(), 2);
        var hora = Util.addZeros(data.getHours(), 2);

        var horaAtual = hora + ":" + minuto;

        return horaAtual;
    },
    somaHoras: function (hora, soma) {
        hora = hora.split(":");

        var novaHora = hora[0] * 1;
        var minuto = '00';

        var horaSomada = (novaHora + soma) + ":" + minuto;

        return horaSomada;
    },
    getDifHoras: function (horaI, horaF) {
        horaI = horaI.split(":");
        horaF = horaF.split(":");

        var difHoras = (horaF[0] * 1) - (horaI[0] * 1);

        return difHoras;
    },
    getTurno: function (data, hora) {

        if (data === undefined) {
            data = Util.getDataAtual();
        }
        if (hora === undefined) {
            hora = Util.getHoraAtual();
        }

        var turno;

        if ((hora >= '06:00') && (hora <= '12:29')) {
            turno = 1;
        } else if ((hora >= '12:30') && (hora <= '18:00')) {
            turno = 2;
        }

        var dataTurno = [data, turno];

        return dataTurno;
    },
    getLimites: function (data, turno) {
        var turnoSistema;
        var dataSistema;
        if (data !== undefined) {
            dataSistema = data;
        }
        if (turno === undefined) {
            turnoSistema = Util.getTurno(dataSistema);
            turno = turnoSistema['1'];
        }

        var horaInicio;
        var horaTermino;
        if (turno === 1) {
            horaInicio = "06:00";
            horaTermino = "12:00";
        } else {
            horaInicio = "12:30";
            horaTermino = "18:00";
        }

        var limites = [data, horaInicio, horaTermino];

        return limites;
    },
    getProxTurno: function (data, turno) {
        var dataTurnoSistema;
        var dataSistema;
        if (data !== undefined) {
            dataSistema = data;
        }
        if (turno === undefined) {
            dataTurnoSistema = Util.getTurno(dataSistema);
            data = dataTurnoSistema['0'];
            turno = dataTurnoSistema['1'];
        }

        var proxTurno;
        var proxData;
        if (turno === 1) {
            proxTurno = turno + 1;
            proxData = data;
        } else {
            proxData = Util.somaDias(data, 1);
            proxTurno = 1;
        }
        var proximo = [proxData, proxTurno];

        return proximo;
    }


}