var Autocomplete = {
    init: function () {


        $("select.select2:visible").each(function () {
            var width;
            if ($(this).parent().is("div")) {
                width = "resolve";
            } else {
                width = $(this).parent().width();
            }
            $(this).select2({width: width});
        });

        Autocomplete.pessoa.init();
        Autocomplete.requisicao.init();
        Autocomplete.infra.init();
        Autocomplete.vendas.init();
        Autocomplete.maquina.init();
        Autocomplete.habilidade.init();
        Autocomplete.assistencia.init();
        Autocomplete.cargos.init();
    },
    
    autocompletePessoa2: function () {
        el = $(this);
        var url = el.data("url");
        var destinoId = el.data("destinoid");
        var destinoCargo = el.data("destinocargo");
        var destinoUnidade = el.data("destinounidade");
        el.typeahead({
            source: function (query, process) {

                $.getJSON(url, {term: query}, function (data) {
                    itens = [];
                    map = {};
                    $.each(data, function (i, item) {
                        map[item['nome']] = item;
                        itens.push(item['nome']);
                    });
                    process(itens);
                });
            },
            minLength: 3,
            updater: function (item) {
                var destino = $("#" + el.attr('id') + "_destino").val();
                var selected = map[item];
                $("#" + destinoId).val(selected['id']);
                if (destinoCargo != "") {
                    $("#" + destinoCargo).val(selected['cargo']);
                }
                if (destinoUnidade != "") {
                    $("#" + destinoUnidade).val(selected['unidade']);
                }
                return item;
            }
        });
    },
    autocomplete2: function () {
        el = $(this);
        var url = el.data("url");
        var destino = el.data("destino");
        el.typeahead({
            source: $.debounce(800, function (query, process) {
                $.getJSON(url, {term: query}, function (data) {
                    itens = [];
                    map = {};
                    $.each(data, function (i, item) {
                        map[item.nome] = item;
                        itens.push(item.nome);
                    });
                    process(itens);
                });
            }),
            updater: function (item) {
                var selected = map[item].id;
                $("#" + destino).val(selected);
                return item;
            }
        });
    },
    pessoa: {
        init: function () {
            $(".pessoa-select2:visible").each(function () {
                //if ($(this).val() == null || $(this).find("option.select2-default").length != 0 ) {
                if (!$(this).next().is("span")) {
                    $(this).select2(Autocomplete.pessoa.select2($(this)));
                }
            });
        },
        formatRepo: function (repo, $select) {

            var markup;
            var dadosUnidade = $select.data('unidade');
            var dadosCargo = $select.data('cargo');
            var cargoSaida = '';
            var unidadeSaida = '';
            if (dadosCargo != undefined && repo.cargo != undefined) {
                cargoSaida = " -> " + repo.cargo;
            }
            if (dadosUnidade != undefined && repo.unidade != undefined) {
                unidadeSaida = " -> " + repo.unidade;
            }

            if (repo.loading)
                return repo.text;
            markup = '<div class="clearfix">' +
                    '<div clas="col-sm-10">' +
                    '<div class="clearfix">' +
                    '<div class="col-sm-12">' + repo.nome + cargoSaida + unidadeSaida + '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
            return markup;
        },
        formatRepoSelection: function (repo, $select) {
            if ($select.data("cargo") != "" && repo.cargo != undefined) {
                if (repo.cargo != "") {
                    $("#" + $select.data("cargo")).val(repo.cargo);
                }
            }
            if ($select.data("unidade") != "" && repo.unidade != undefined) {
                if (repo.unidade != "") {
                    $("#" + $select.data("unidade")).val(repo.unidade);
                }
            }


            return repo.nome || repo.text;
        },
        select2: function ($select, width) {

            if (width === undefined) {
                if ($select.parent().is("div")) {
                    width = "resolve";
                } else {
                    width = $select.parent().width();
                }
            }

            var obj = {ajax: {
                    url: $select.attr('data-url'),
                    dataType: 'json',
                    delay: 100,
                    data: function (params) {
                        return {
                            term: params.term, // search term
                            extra: $select.data("extra"),
                            page: params.page
                        };
                    },
                    processResults: function (data, page) {
                        // parse the results into the format expected by Select2.
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                width: width,
                escapeMarkup: function (markup) {
                    return markup;
                }, // let our custom formatter work
                minimumInputLength: 3,
                templateResult: function (repo) {
                    return Autocomplete.pessoa.formatRepo(repo, $select);
                }, // omitted for brevity, see the source of this page
                templateSelection: function (repo) {
                    return Autocomplete.pessoa.formatRepoSelection(repo, $select);
                } // omitted for brevity, see the source of this page
            }
            return obj;
        }
    },
    requisicao: {
        init: function () {
            $(".select2-requisicao").each(function () {
                if ($(this).val() == null || $(this).find("option.select2-default").length != 0) {
                    $(this).select2(Autocomplete.requisicao.select2($(this)));
                }
            });
        },
        formatRepo: function (repo, $select) {

            var markup;

            if (repo.loading)
                return repo.text;
            markup = '<div class="clearfix">' +
                    '<div clas="col-sm-10">' +
                    '<div class="clearfix">' +
                    '<div class="col-sm-12">' + repo.nome + '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
            return markup;
        },
        formatRepoSelection: function (repo, $select) {

            if ($select.data("tipo") == "r") { //requisição
                if (RequisicaoJs.hasItem($select.data("conta"), repo.id, $select.data("subEtapa_id"), $select)) {
                    alert('Este item já foi selecionado');
                    $select.html("");
                    return "";
                } else {
                    console.log($select.data("sufixo"));
                    if (repo.totalOrcado != undefined) {
                        $("#item-totalOrcado-" + $select.data("sufixo")).val(repo.totalOrcado);
                    }
                    if (repo.precoOrcado != undefined) {
                        $("#item-precoOrcado-" + $select.data("sufixo")).val(repo.precoOrcado);
                    }
                    if (repo.qtdOrcado != undefined) {
                        $("#item-qtdOrcado-" + $select.data("sufixo")).val(repo.qtdOrcado);
                    }
                    if (repo.totalRequisitado != undefined) {
                        $("#item-totalRequisitado-" + $select.data("sufixo")).val(repo.totalRequisitado);
                    }
                    if (repo.medida != undefined) {
                        $("#item-medida-" + $select.data("sufixo")).val(repo.medida);
                    }
                    return repo.nome || repo.text;
                }
            } else if ($select.data("tipo") == "o") { //orçamento
                if (repo.padrao != undefined) {
                    $("#item-padrao-" + $select.data("sufixo")).val(repo.padrao);
                }
                if (repo.medida != undefined) {
                    $("#item-medida-" + $select.data("sufixo")).val(repo.medida);
                }

                return repo.nome || repo.text;
            } else if ($select.data("tipo") == "f") { //fornecedor
                return repo.nome || repo.text;
            }

        },
        select2: function ($select) {
            var width = $select.parent().width();
            var obj = {ajax: {
                    //url: '?m=Req&c=Especificacao&a=ListarNovoJson',
                    url: $select.attr('data-url'),
                    dataType: 'json',
                    delay: 100,
                    data: function (params) {
                        return {
                            term: params.term, // search term
                            conta_id: $select.data("conta"),
                            subEtapa_id: $select.attr("data-subEtapa_id"),
                            tipo: $select.data("tipo"),
                            page: params.page
                        };
                    },
                    processResults: function (data, page) {
                        // parse the results into the format expected by Select2.
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                width: width,
                escapeMarkup: function (markup) {
                    return markup;
                }, // let our custom formatter work
                minimumInputLength: 6,
                templateResult: function (repo) {
                    return Autocomplete.requisicao.formatRepo(repo, $select);
                }, // omitted for brevity, see the source of this page
                templateSelection: function (repo) {
                    return Autocomplete.requisicao.formatRepoSelection(repo, $select);
                } // omitted for brevity, see the source of this page
            }
            return obj;
        }
    },
    infra: {
        init: function () {
            $(".select2-infra:visible").each(function () {
                if ($(this).val() == null || $(this).find("option.select2-default").length != 0) {
                    $(this).select2(Autocomplete.infra.select2($(this)));
                }
            });
        },
        formatRepo: function (repo, $select) {

            var markup;

            if (repo.loading)
                return repo.text;
            markup = '<div class="clearfix">' +
                    '<div clas="col-sm-10">' +
                    '<div class="clearfix">' +
                    '<div class="col-sm-12">' + repo.nome + '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
            return markup;
        },
        formatRepoSelection: function (repo, $select) {

            if ($select.data("tipo") == "r") { //requisição

                var elemento_id = $select.parent().parent().data('id');
                var rateio_id = $select.parent().parent().data('rateio_id');

                if (requisicao.rateios[rateio_id].itens[elemento_id] != undefined) {
                    if (requisicao.rateios[rateio_id].hasItem(elemento_id, repo.id)) {
                        alert('Este item já foi selecionado');
                    } else {
                        requisicao.rateios[rateio_id].itens[elemento_id].autocomplete(repo);
                        return repo.nome || repo.text;
                    }
                }


            } else if ($select.data("tipo") == "o") { //orçamento

                if (InfraJs.orcamento.hasItem($select.data("rateio_id"), repo.id, $select)) {
                    alert('Este item já foi selecionado');
                    $select.html("");
                    return "";
                } else {
                    console.log($select.data("sufixo"));
                    if (repo.padrao != undefined) {
                        $("#item-padrao-" + $select.data("sufixo")).val(repo.padrao);
                    }
                    if (repo.medida != undefined) {
                        $("#item-medida-" + $select.data("sufixo")).val(repo.medida);
                    }
                    return repo.nome || repo.text;
                }
            } else if ($select.data("tipo") == "f") { //fornecedor

                var elemento_id = $select.parent().parent().data('id');

                requisicao.fornecedores[elemento_id].autocomplete(repo);

                return repo.nome || repo.text;
            } else if ($select.data("tipo") == "i") { //itens na aba de requisição

                var elemento_id = $select.parent().parent().data('id');
                if (repo.total != undefined) {
                    console.log(repo.total);
                    //var total = Util.formataNumero(repo.total);
                    //$("#fornecedor-" + elemento_id + " .totalParcelado").val(total);
                    requisicao.fornecedores[elemento_id].triggerEventos(repo.total);
                }


                //if (requisicao.fornecedores[elemento_id].autocomplete(repo) != undefined) {
                //    requisicao.fornecedores[elemento_id].autocomplete(repo);
                //    return repo.nome || repo.text;
                //}

                return repo.nome || repo.text;
            }

        },
        select2: function ($select) {
            console.log("select2");
            var width = $select.parent().width();
            var obj = {ajax: {
                    url: $select.attr('data-url'),
                    dataType: 'json',
                    delay: 1000,
                    data: function (params) {

                        var rateio_id = $select.parent().parent().data('rateio_id');
                        var rateio = undefined;
                        if (rateio_id != undefined) {
                            rateio = requisicao.findRateio(rateio_id);
                        }

                        return {
                            term: params.term, // search term
                            conta_id: (rateio != undefined) ? rateio.conta_id : "",
                            subConta_id: (rateio != undefined) ? rateio.subConta_id : "",
                            subEtapa_id: (rateio != undefined) ? rateio.subEtapa_id : "",
                            servico_id: (rateio != undefined) ? rateio.servico_id : "",
                            tipo: $select.data("tipo"),
                            page: params.page
                        };
                    },
                    processResults: function (data, page) {
                        // parse the results into the format expected by Select2.
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                width: width,
                escapeMarkup: function (markup) {
                    return markup;
                }, // let our custom formatter work
                minimumInputLength: 3,
                templateResult: function (repo) {
                    return Autocomplete.infra.formatRepo(repo, $select);
                }, // omitted for brevity, see the source of this page
                templateSelection: function (repo) {
                    return Autocomplete.infra.formatRepoSelection(repo, $select);
                } // omitted for brevity, see the source of this page
            }
            return obj;
        }
    },
    vendas: {
        init: function () {
            $(".vendas-select2").each(function () {
                if ($(this).val() == null || $(this).find("option.select2-default").length != 0) {
                    $(this).select2(Autocomplete.vendas.select2($(this)));
                }
            });
        },
        formatRepo: function (repo, $select) {

            var markup;

            if (repo.loading)
                return repo.text;
            markup = '<div class="clearfix">' +
                    '<div clas="col-sm-10">' +
                    '<div class="clearfix">' +
                    '<div class="col-sm-12">' + repo.nome + '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
            return markup;
        },
        formatRepoSelection: function (repo, $select) {
            return repo.nome || repo.text;

        },
        select2: function ($select) {
            var width = $select.parent().width();
            var obj = {ajax: {
                    url: '?m=Vnd&c=Cliente&a=ListarJson',
                    dataType: 'json',
                    delay: 100,
                    data: function (params) {
                        return {
                            term: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function (data, page) {
                        // parse the results into the format expected by Select2.
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                width: width,
                escapeMarkup: function (markup) {
                    return markup;
                }, // let our custom formatter work
                minimumInputLength: 3,
                templateResult: function (repo) {
                    return Autocomplete.vendas.formatRepo(repo, $select);
                }, // omitted for brevity, see the source of this page
                templateSelection: function (repo) {
                    return Autocomplete.vendas.formatRepoSelection(repo, $select);
                } // omitted for brevity, see the source of this page
            }
            return obj;
        }
    },
    maquina: {
        init: function () {
            $(".select2-maquina").each(function () {
                if ($(this).val() == null || $(this).find("option.select2-default").length != 0) {
                    $(this).select2(Autocomplete.maquina.select2($(this)));
                }
            });
        },
        formatRepo: function (repo, $select) {

            var markup;

            if (repo.loading)
                return repo.text;
            markup = '<div class="clearfix">' +
                    '<div clas="col-sm-10">' +
                    '<div class="clearfix">' +
                    '<div class="col-sm-12">' + repo.nome + '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
            return markup;
        },
        formatRepoSelection: function (repo, $select) {
            return repo.nome || repo.text;

        },
        select2: function ($select) {
            var width = $select.parent().width();
            var obj = {ajax: {
                    url: '?m=Aloc&c=Maquina&a=ListarJson',
                    dataType: 'json',
                    delay: 100,
                    data: function (params) {
                        return {
                            term: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function (data, page) {
                        // parse the results into the format expected by Select2.
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                width: width,
                escapeMarkup: function (markup) {
                    return markup;
                }, // let our custom formatter work
                minimumInputLength: 3,
                templateResult: function (repo) {
                    return Autocomplete.maquina.formatRepo(repo, $select);
                }, // omitted for brevity, see the source of this page
                templateSelection: function (repo) {
                    return Autocomplete.maquina.formatRepoSelection(repo, $select);
                } // omitted for brevity, see the source of this page
            }
            return obj;
        }
    },
    assistencia: {
        init: function () {
            $(".assistencia-select2").each(function () {
                if ($(this).val() == null || $(this).find("option.select2-default").length != 0) {
                    $(this).select2(Autocomplete.assistencia.select2($(this)));
                }
            });
        },
        formatRepo: function (repo, $select) {

            var markup;

            if (repo.loading)
                return repo.text;
            markup = '<div class="clearfix">' +
                    '<div clas="col-sm-10">' +
                    '<div class="clearfix">' +
                    '<div class="col-sm-12">' + repo.nome + '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
            return markup;
        },
        formatRepoSelection: function (repo, $select) {
            
            if (repo.medida != undefined) {
                if (repo.medida != "") {
                    $("#material-medida-" + $select.data("sufixo")).val(repo.medida);
                }
            }
            return repo.nome || repo.text;
        },
        select2: function ($select) {
            var width = $select.parent().width();
            var obj = {ajax: {
                    url: '?m=Supri&c=Item&a=ListarJson',
                    dataType: 'json',
                    delay: 100,
                    data: function (params) {
                        return {
                            term: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function (data, page) {
                        // parse the results into the format expected by Select2.
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                width: width,
                escapeMarkup: function (markup) {
                    return markup;
                }, // let our custom formatter work
                minimumInputLength: 3,
                templateResult: function (repo) {
                    return Autocomplete.assistencia.formatRepo(repo, $select);
                }, // omitted for brevity, see the source of this page
                templateSelection: function (repo) {
                    return Autocomplete.assistencia.formatRepoSelection(repo, $select);
                } // omitted for brevity, see the source of this page
            }
            return obj;
        }
    },
    cargos: {
        init: function () {
            $(".cargos-select2:visible").each(function () {
                if (!$(this).next().is("span")) {
                    $(this).select2(Autocomplete.cargos.select2($(this)));
                }
            });
        },
        formatRepo: function (repo) {

            var markup;
            if (repo.loading)
                return repo.text;
            markup = '<div class="clearfix">' +
                    '<div clas="col-sm-10">' +
                    '<div class="clearfix">' +
                    '<div class="col-sm-12">' + repo.nome + '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';

            return markup;
        },
        formatRepoSelection: function (repo) {
            return repo.nome || repo.text;
        },
        select2: function ($select, width) {

            if (width === undefined) {
                if ($select.parent().is("div")) {
                    width = "resolve";
                } else {
                    width = $select.parent().width();
                }
            }

            var obj = {ajax: {
                    url: '?m=Adm&c=Cargo&a=ListarJson',
                    dataType: 'json',
                    delay: 100,
                    data: function (params) {
                        return {
                            term: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function (data, page) {
                        // parse the results into the format expected by Select2.
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                width: width,
                escapeMarkup: function (markup) {
                    return markup;
                }, // let our custom formatter work
                minimumInputLength: 3,
                templateResult: function (repo) {
                    return Autocomplete.cargos.formatRepo(repo, $select);
                }, // omitted for brevity, see the source of this page
                templateSelection: function (repo) {
                    return Autocomplete.cargos.formatRepoSelection(repo, $select);
                } // omitted for brevity, see the source of this page
            };
            return obj;
        }
    },
    
    habilidade: {
        init: function () {
            $(".select2-habilidades").each(function () {
                if ($(this).val() == null || $(this).find("option.select2-default").length != 0) {
                    $(this).select2(Autocomplete.habilidade.select2($(this)));
                }
            });
        },
        formatRepo: function (repo, $select) {

            var markup;

            if (repo.loading)
                return repo.text;
            markup = '<div class="clearfix">' +
                    '<div clas="col-sm-10">' +
                    '<div class="clearfix">' +
                    '<div class="col-sm-12">' + repo.nome + '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
            return markup;
        },
        formatRepoSelection: function (repo, $select) {
            return repo.nome || repo.text;

        },
        select2: function ($select) {
            var width = $select.parent().width();
            var obj = {ajax: {
                    url: '?m=Mat&c=HabilidadeNome&a=ListarNomeJson',
                    dataType: 'json',
                    delay: 100,
                    data: function (params) {
                        return {
                            term: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function (data, page) {
                        // parse the results into the format expected by Select2.
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                width: width,
                escapeMarkup: function (markup) {
                    return markup;
                }, // let our custom formatter work
                minimumInputLength: 3,
                templateResult: function (repo) {
                    return Autocomplete.habilidade.formatRepo(repo, $select);
                }, // omitted for brevity, see the source of this page
                templateSelection: function (repo) {
                    return Autocomplete.habilidade.formatRepoSelection(repo, $select);
                } // omitted for brevity, see the source of this page
            }
            return obj;
        }
    },
}