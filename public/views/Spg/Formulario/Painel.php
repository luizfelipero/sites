<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2><i class="fa fa-th"></i><span class="break"></span>Solicitação de Produtos Gerais</h2>
            <div class="box-icon">

            </div>
        </div>
        <div class="panel-body" style="height: 200px">
            <div class="row">
                <?php foreach ($this->produtos as $produto) { ?>
                    <div class="col-sm-3">
                        <div style="background-color: #dddddd; border-radius: 5px; padding: 10px; text-align: center">
                            <h4 style="min-height: 60px"><?php echo $produto->getNome(); ?></h4>
                            <div class="btn-group" role="group" aria-label="...">
                                <?php if ($produto->getGestor_id() == $GLOBALS['user']->getId()) { ?>                
                                    <a class="btn btn-default" href="<?php echo Util_Link::link("Spg", "Formulario", "Pendencias", $produto->getId()); ?>"><span style="background-color: #d9534f;" class="badge pull-left"><?php echo $produto->getQtdPendencias(); ?></span>&nbsp; Pend.</a>
                                    <a class="btn btn-default" href="<?php echo Util_Link::link("Spg", "Formulario", "IndexGeral", $produto->getId()); ?>"><i class="fa fa-list"></i></a>
                                <?php } else { ?>
                                    <a class="btn btn-default" href="<?php echo Util_Link::link("Spg", "Formulario", "Pendencias", $produto->getId()); ?>"><span style="background-color: #d9534f;" class="badge pull-left"><?php echo $produto->getQtdPendencias(); ?></span>&nbsp; Pendências</a>
                                <?php } ?>
                                <a class="btn btn-default" href="<?php echo Util_Link::link("Spg", "Formulario", "Index", $produto->getId()); ?>"><i class="fa fa-filter"></i></a>
                                <a class="btn btn-default" href="<?php echo Util_Link::link("Spg", "Formulario", "Inserir", $produto->getId()); ?>"><i class="fa fa-plus"></i></a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>