<div class="panel panel-default">
    <div class="panel-heading">
        <h2><i class="fa fa-th"></i><span class="break"></span>Pendências</h2>
        <div class="box-icon">
            <span class="break"></span>
            <a href="<?php echo Util_Link::link("Spg", "Formulario", "Painel"); ?>"><i class="fa fa-plus"></i></a>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12 ">
                <div class="col-sm-10 col-sm-offset-1">
                    <table class="table table-striped table-bordered table-condensed table-hover">
                        <tr>
                            <th>Nº</th>
                            <th>Solicitante</th>
                            <th>Produto</th>
                            <th style="width: 15%">Data</th>
                            <th>Status</th>
                            <th>Situação</th>
                            <th style="width: 3%"></th>
                        </tr>
                        <?php
                        if ($this->formularios != "") {
                            $j = 1;
                            foreach ($this->formularios as $form) {
                                ?>
                                <tr>
                                    <td><?php echo $form->getId(); ?></td>
                                    <td><?php echo Adm_PessoaMapper::getInstance()->findById($form->getSolicitante_id())->getNome(); ?></td>
                                    <td><?php echo Spg_ProdutoMapper::getInstance()->findById($form->getProduto_id())->getNome(); ?></td>
                                    <td><?php echo Util_Utilidade::exibeData($form->getData()); ?></td>
                                    <td><?php echo $form->getNome(); ?></td>
                                    <td><?php echo $form->getNomeAndamento(); ?></td>
                                    <td>
                                        <a href="<?php echo Util_Link::link("Spg", "Formulario", "Visualizar", $form->getId()); ?>" class="btn btn-default btn-xs"><span class="fa fa-search"></span></a>
                                        <a href="<?php echo Util_Link::link("Spg", "Formulario", "Editar", $form->getId()); ?>" class="btn btn-default btn-xs"><span class="fa fa-pencil"></span></a>
                                    </td>
                                </tr>    
                                <?php
                                $j++;
                            }
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>