<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2><i class="fa fa-th"></i><span class="break"></span><?php echo $this->produto->getNome(); ?></h2>
            <div class="box-icon">
                <span class="break"></span>
                <a href="<?php echo Util_Link::link("Spg", "Formulario", "Painel"); ?>"><i class="fa fa-plus"></i></a>
            </div>
        </div>

        <div class="panel-body">            
            <form class="" action="<?php echo Util_Link::link("Spg", "Formulario", "Inserir"); ?>" method="POST">
                <div class="row">
                    <div class="col-sm-12 col-sm-offset-0">
                        <div class="alert alert-info" role="alert">
                            <label style="padding-left: 10px;"><strong>Instruções</strong></label>
                            <span><?php echo $this->produto->getInstrucoes(); ?></span>
                        </div>
                    </div>
                </div>
                <?php
                foreach ($this->produto->getComposicoes() as $comp) {

                    echo $comp->getHtml();
                }
                ?>
                <div class="row">
                    <div class="col-sm-12 col-sm-offset-0">

                        <label style="padding-left: 10px;"><strong><?= $this->produto->getTituloTextArea(); ?></strong></label>
                        <textarea name="obs" id="obs" class="form-control"></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-sm-offset-0">
                        <input type ="hidden" name="post" value="1" />
                        <input type ="hidden" name="produto_id" value="<?php echo $this->produto->getId(); ?>" />
                        <input id="submitform" class="btn btn-primary" name="submitform" type="submit" value="Salvar" />
                        <a href="<?php echo Util_Link::link("Spg", "Formulario", "Inserir"); ?>" style="padding-left: 15px">Cancelar</a>

                    </div>
                </div>
            </form>
        </div>

    </div>
</div>
<script src="js_p/spg.js?<?php echo $versaoJs; ?>"></script>
<script>
    SpgJs.init();
</script>