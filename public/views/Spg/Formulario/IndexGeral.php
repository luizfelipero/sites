<div class="panel panel-default">
    <div class="panel-heading">
        <h2><i class="fa fa-th"></i><span class="break"></span>Meus Formulários</h2>
        <div class="box-icon">
            <span class="break"></span>
            <a href="#"><i class="fa fa-filter filtro-botao"></i></a>
            <span class="break"></span>
            <a href="<?php echo Util_Link::link("Spg", "Formulario", "Painel"); ?>"><i class="fa fa-plus"></i></a>
        </div>
    </div>
    <div class="panel-body filtro-opcoes hide">
        <form method="GET" action="?">
            <?php echo Util_Link::linkHideForm("Spg", "Formulario", "Index"); ?>
            <label>Data Início</label>
            <input type="text" name="datai" id="datai" class="data form-control">
            <label>Data Fim</label>
            <input type="text" name="dataf" id="dataf" class="data form-control">
            <input id="subform" class="btn btn-primary" type="submit" value="filtrar" />
        </form>

    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2 ">
                <div class='table-responsive'>
                    <table class="table table-striped table-bordered table-condensed table-hover">
                        <tr>
                            <th style="width: 30px">ID</th>
                            <th>Tipo</th>
                            <th>Data</th>
                            <th style="width: 7%; text-align: center">Status</th>
                            <th style="width: 30px"></th>
                        </tr>
                        <?php
                        if ($this->formularios != "") {
                            foreach ($this->formularios as $form) {
                                ?>
                                <tr>
                                    <td><?php echo $form->getId(); ?></td>
                                    <td><?php echo $form->getProduto()->getNome(); ?></td>
                                    <td><?php echo Util_Utilidade::exibeData($form->getData()); ?></td>
                                    <td><?php echo $form->getNomeStatus()->getNome(); ?></td>
                                    <td>
                                        <a href="<?php echo Util_Link::link("Spg", "Formulario", "Visualizar", $form->getId()); ?>" class="btn btn-default btn-xs"><span class="fa fa-search"></span></a>
                                    </td>
                                </tr>    
                                <?php
                            }
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>
        <?php echo $this->paginacao->exibe(); ?>
    </div>
</div>