<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2><i class="fa fa-th"></i><span class="break"></span><?php echo $this->produto->getNome(); ?></h2>
            <div class="box-icon">
                <span class="break"></span>
                <a href="<?php echo Util_Link::link("Spg", "Formulario", "Painel"); ?>"><i class="fa fa-plus"></i></a>
            </div>
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-sm-12" >
                    <ul id="myTab" class="nav nav-tabs">
                        <li class="active"><a href="#formulario" data-toggle="tab">Formulário</a></li>
                        <li><a href="#historico" data-toggle="tab">Histórico</a></li>
                        <li><a href="#arquivos" data-toggle="tab">Arquivos</a></li>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane active" id="formulario">
                            <div class="row">
                                <div class="col-sm-4 col-sm-offset-0">
                                    <label> Solicitante:</label>
                                    <input type="text" name="solicitante" value=" <?php echo $this->solicitante->getNome(); ?>" class="form-control" disabled>
                                </div>
                                <div class="col-sm-4 col-sm-offset-0">
                                    <label> Status:</label>
                                    <input type="text" name="status" value=" <?php echo $this->formulario->getNomeStatus()->getNomeAndamento(); ?>" class="form-control" disabled>
                                </div>
                                <div class="col-sm-2 col-sm-offset-0">
                                    <label> UGB/Departamento:</label>
                                    <input type="text" name="solicitante" value=" <?php echo $this->solicitante->getUnidadeEspecifica()->getNome(); ?>" class="form-control" disabled>
                                </div>
                                <div class="col-sm-2 col-sm-offset-0">
                                    <label> Data:</label>
                                    <input type="text" name="data" value=" <?php echo Util_Utilidade::exibeData($this->formulario->getData()); ?>" class="form-control" disabled>
                                </div>
                                <span>&nbsp</span>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-sm-offset-0">

                                    <?php
                                    foreach ($this->produto->getComposicoes() as $comp) {

                                        echo $comp->getHtmlVisualizar($this->formulario);
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-sm-offset-0">

                                    <label style="padding-left: 10px;"><strong><?= $this->produto->getTituloTextArea(); ?></strong></label>
                                    <textarea name="obs" id="obs" class="form-control" disabled><?php echo $this->formulario->getObs(); ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="historico">
                            <?php if ($this->formulario->getHistorico()) { ?>
                                <div class="col-sm-10 col-sm-offset-1">
                                    <table class="table table-condensed table-hover table-striped table-bordered">
                                        <tr>
                                            <th>Status</th>
                                            <th>Quem</th>
                                            <th>Quando</th>
                                            <th>Obs</th>
                                        </tr>
                                        <?php foreach ($this->formulario->getHistorico() as $hist) { ?>
                                            <tr>
                                                <td><?php echo $hist->getNomeStatus()->getNome(); ?></td>
                                                <td><?php echo Adm_PessoaMapper::getInstance()->findById($hist->getResponsavel_id())->getNome(); ?></td>
                                                <td><?php echo Util_Utilidade::exibeData($hist->getData()); ?></td>
                                                <td><?php echo $hist->getObs(); ?></td>
                                            </tr>
                                        <?php } ?>
                                    </table>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="tab-pane" id="arquivos">
                            <div class="col-sm-6">            
                                <label>Arquivos</label> 
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover table-condensed table-bordered">
                                        <tr>
                                            <th>Nome</th>
                                            <th></th>                                                                                                                                   
                                        </tr>                                                

                                        <?php
                                        if ($this->formulario->getArquivos() != "") {
                                            foreach ($this->formulario->getArquivos() as $arquivo) {
                                                ?>  
                                                <tr>
                                                    <td>
                                                        <?php echo $arquivo->getRotulo() . " "; ?> <button class="btn btn-xs btn-default remove-arquivo-spg pull-right" data-arquivo-id="<?php echo $arquivo->getId(); ?>"><i class="fa fa-remove"></i> Remover</button>
                                                    </td>
                                                    <td style="width: 30px;">   
                                                        <a class="btn btn-xs btn-default" target="_blank" href="<?php echo Util_Link::link("Adm", "Arquivo", "Exibir", $arquivo->getId()); ?>"><i class="fa fa-arrow-down"></i> Baixar</a>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>      
                                    </table>   
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label>Adicionar Arquivos</label>
                                <span class="help-block">Formato de arquivo aceito: <?php echo Adm_ArquivoMapper::getInstance()->getLabel('spg'); ?> </span>
                                <form id="myform" action="<?php echo Util_Link::link("Spg", "Formulario", "InserirArquivo"); ?>" method="post" enctype="multipart/form-data" data-sync="1" >
                                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                        <div class="form-control" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                                        <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Selecione</span><span class="fileinput-exists">Trocar</span><input type="file" name="arquivo_1" data-maxsize="10"></span>

                                    </div>
                                    <input name="id" id="id" type="hidden" value="<?php echo $this->formulario->getId(); ?>" />
                                    <input name="post" id="post" type="hidden" value="1" />
                                    <input id="atualizar" name="atualizar" type="submit" value="Inserir Arquivo" class="btn btn-primary btn-submit" >
                                </form>
                            </div>
                        </div>
                    </div>
                    <form action="<?php echo Util_Link::link("Spg", "Formulario", "MudarStatus"); ?>" method="POST">
                        <?php if ($this->formulario->podeDelegar()) { ?>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Responsável:</label>
                                    <select class="pessoa-select2 form-control obrigatorio" name="responsavel_id" id="responsavel_id" data-extra="d" data-url="<?php echo Util_Link::link("Adm", "Pessoa", "ListarNovoJson"); ?>"></select>
                                </div>
                            </div>
                        <?php } else if ($this->formulario->getResponsavel_id() != 0) { ?>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Responsável:</label>
                                    <input class="form-control" type="text" value="<?= $this->formulario->getNomeResponsavel() ?>" disabled>
                                </div>
                            </div>
                        <?php } ?>

                        <div class="row">
                            <?php if ($this->formulario->podeAprovar()) { ?>
                                <div class="col-sm-6 col-sm-offset-0">
                                    <label>Observações:</label>
                                    <textarea name='obs' class="form-control"></textarea>
                                    <input name="id" id="id" type="hidden" value="<?php echo $this->formulario->getId(); ?>" />
                                    <button style="margin-left: 120px;" name="acao" value="2" class="btn btn-primary">Reprovar</button>
                                    <button style="margin-left: 100px;" name="acao" value="1" class="btn btn-primary">Aprovar</button>
                                </div>
                            <?php } ?>
                            <div class="col-sm-3 col-sm-offset-1 col-center">
                                <?php if ($this->formulario->podeEditar()) { ?>
                                    <a href="<?php echo Util_Link::link("Spg", "Formulario", "Editar", $this->formulario->getId()); ?>" class="btn btn-primary">Editar</a>

                                <?php } if ($this->formulario->podeAprovar() && $this->formulario->getNomeStatus()->getProximoStatus_id() != 0) { ?>
                                    <a href="<?php echo Util_Link::link("Spg", "Formulario", "Excluir", $this->formulario->getId()); ?>" class="btn btn-primary pull-right">Excluir</a>
                                <?php } ?>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(".remove-arquivo-spg").click(function () {
        console.log("aqr " + $(this).data("arquivo-id"));
        $.get("?m=Adm&c=Arquivo&a=Desativar&id=" + $(this).data("arquivo-id"));
        $(this).parent().parent().addClass("hide");
    });
</script>