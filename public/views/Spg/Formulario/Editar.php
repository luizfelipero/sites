,<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2><i class="fa fa-th"></i><span class="break"></span><?php echo $this->produto->getNome(); ?></h2>
            <div class="box-icon">
                <span class="break"></span>
                <a href="<?php echo Util_Link::link("Spg", "Formulario", "Painel"); ?>"><i class="fa fa-plus"></i></a>
            </div>
        </div>

        <div class="panel-body">            
            <form class="" action="<?php echo Util_Link::link("Spg", "Formulario", "Editar"); ?>" method="POST" data-sync = "1">
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-0">
                        <label> Solicitante:</label>
                        <input type="text" name="solicitante" value=" <?php echo $this->solicitante->getNome(); ?>" class="form-control" disabled>
                    </div>
                    <div class="col-sm-4 col-sm-offset-0">
                        <label> UGB/Departamento:</label>
                        <input type="text" name="solicitante" value=" <?php echo $this->solicitante->getUnidadeEspecifica()->getNome(); ?>" class="form-control" disabled>
                    </div>
                    <div class="col-sm-4 col-sm-offset-0">
                        <label> Data:</label>
                        <input type="text" name="data" value=" <?php echo Util_Utilidade::exibeData($this->formulario->getData()); ?>" class="form-control" disabled>
                    </div>
                    <span>&nbsp</span>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-sm-offset-0">

                        <?php
                        foreach ($this->produto->getComposicoes() as $comp) {

                            echo $comp->getHtmlVisualizar($this->formulario, 1);
                        }
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-sm-offset-0">

                        <label style="padding-left: 10px;"><strong><?= $this->produto->getTituloTextArea(); ?></strong></label>
                        <textarea name="obs" id="obs" class="form-control"><?php echo $this->formulario->getObs(); ?></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-sm-offset-5">
                        <input type ="hidden" name="post" value="1" />
                        <input type ="hidden" name="produto_id" value="<?php echo $this->produto->getId(); ?>" />
                        <input type ="hidden" name="id" value="<?php echo $this->formulario->getId(); ?>" />
                        <input id="submitform" class="btn btn-primary" name="submitform" type="submit" value="Salvar" />
                        
                    </div>
                </div>
            </form>
        </div>

    </div>
</div>

<script src="js_p/spg.js?<?php echo $versaoJs; ?>"></script>
<script>
    SpgJs.init();
</script>