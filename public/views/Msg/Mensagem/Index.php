<div class="span12 box">
    <div class="box-header" data-original-title>
        <h2><i class="icon-th"></i><span class="break"></span>Mensagens</h2>
        <div class="box-icon">
            <span class="break"></span>
            <a href="#"><i class="icon-filter filtro-botao"></i></a>
            <span class="break"></span>
            <a href="<?php echo Util_Link::link("Msg", "Mensagem", "Inserir"); ?>" class="btn-setting"><i class="icon-plus"></i></a>
        </div>
    </div>

    <div class="box-content hide filtro-opcoes">
        <div class="row-fluid">
            <div class="span6 offset3">
                <form action="" method="get">
                    <?php echo Util_Link::linkHideForm("Msg", "Mensagem", "Index"); ?>
                    <label>Remetente</label>
                    <input name="remetente" type="text" id="remetente" class="autocomplete">
                    <input name="remetente_id" id="remetente_id" type="hidden">
                    <input name="remetente_url" id="remetente_url" type="hidden" value="<?php echo Util_Link::link("Adm", "Pessoa", "ListarJson"); ?>" >
                    <input name="remetente_destino" id="remetente_destino" type="hidden" value="remetente_id">
                    <input type="submit" value="filtrar" class="btn btn-primary">
                </form>
            </div>
        </div>
    </div>

    <div class="box-content">
        <div class="row-fluid">
            <div class="span8 offset2">

                <table class="table table-striped table-hover table-condensed table-bordered">
                    <tr>
                        <th>Data</th>
                        <th>Enviado/Recebido</th>
                        <th>Mensagem</th>
                        <th></th>
                    </tr>

                    <tr>
                        <?php
                        if ($this->msgs != "") {
                            foreach ($this->msgs as $msg) {
                                ?>
                                <td><?php echo Util_Utilidade::exibeDataHora($msg->getData()); ?></td>
                                <td><?php
                                    echo $msg->getContato()->getNome();
                                    if ($msg->getTipo() == 1) {
                                        ?>
                                        <span class="label label-warning pull-right"><i class="icon-arrow-right icon-white"></i></span>
                                    <?php } else { ?>
                                        <span class="label label-important pull-right"><i class="icon-arrow-left icon-white"></i></span>
                                    <?php }
                                    ?>
                                </td>
                                <td><?php
                                    $string = $msg->getConteudo();
                                    $tamString = strlen($string);
                                    if ($tamString < 20) {
                                        echo $string;
                                    } else {
                                        echo substr($string, 0, 20) . "...";
                                    }
                                    if ($msg->getNova() == 1 && $msg->getTipo() == 2) {
                                        ?>
                                        <span class="label label-success pull-right">novo</span>
                                    <?php } ?>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <a class="btn btn-mini" href="<?php echo Util_Link::link("Msg", "Mensagem", "Visualizar", $msg->getId()); ?>"><i class="icon-search"></i></a>
                                    </div>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </table>
                <?php
                echo $this->paginacao->exibe();
                ?>
            </div>
        </div>
    </div>
</div>