<div class="span12 box">
    <div class="box-header" data-original-title>
        <h2><i class="icon-th"></i><span class="break"></span>Mensagem</h2>
        <div class="box-icon">
            <span class="break"></span>
            <a href="<?php echo Util_Link::link("Msg", "Mensagem", "Inserir"); ?>" class="btn-setting"><i class="icon-plus"></i></a>
        </div>
    </div>
    <div class="box-content">
        <div class="row-fluid">
            <div class="row-fluid">
                <div class="span5 offset3">
                    <label>Remetente</label>
                    <input type="text" class="span12" disabled="disabled" value="<?php echo $this->msgs->getRemetenteNome(); ?>">
                    <label>Mensagem</label>
                    <textarea rows="5" cols="5" class="span12" disabled="disabled"><?php echo $this->msgs->getConteudo(); ?></textarea>
                    <div>
                        <?php if (Util_Documento::existeArquivo("msg", $this->msgs->getNomeArquivo(), $this->msgs->getExtensao())) { ?>
                            <a class="btn" target="_blank" href="<?php echo Util_Documento::exibe("msg", $this->msgs->getNomeArquivo(), $this->msgs->getExtensao()); ?>"><i class="icon-arrow-down"></i> baixar anexo</a>
                        <?php } ?>
                            <a class="btn btn-primary" href="<?php echo Util_Link::link("Msg", "Mensagem", "Index"); ?>">voltar</a>
                    </div>
                    <div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>