<div class="span12 box">
    <div class="box-header" data-original-title>
        <h2><i class="icon-th"></i><span class="break"></span>Escrever Mensagem</h2>
        <div class="box-icon">
            <span class="break"></span>
            <a href="<?php echo Util_Link::link("Msg", "Mensagem", "Inserir"); ?>" class="btn-setting"><i class="icon-plus"></i></a>
        </div>
    </div>
    <div class="box-content">
        <div class="row-fluid">
            <div class="span5 offset3">
                <form action="<?php echo Util_Link::link("Msg", "Mensagem", "Inserir"); ?>" method="post" enctype="multipart/form-data" data-sync="1">

                    <label>Para</label>
                    <input name="destinatario" type="text" id="destinatario" class="autocomplete span12">
                    <input name="destinatario_id" id="destinatario_id" type="hidden">
                    <input name="destinatario_url" id="destinatario_url" type="hidden" value="<?php echo Util_Link::link("Adm", "Pessoa", "ListarJson"); ?>" >
                    <input name="destinatario_destino" id="destinatario_destino" type="hidden" value="destinatario_id">

                    <label>Mensagem</label>
                    <textarea rows="5" cols="5" name="conteudo" id="conteudo" class="span12"></textarea>

                    <div class="row-fluid">
                        <div class="span4">
                            <label>Inserir Anexo</label>
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="input-append">
                                    <div class="uneditable-input span4"><i class="icon-file fileupload-exists"></i> <span class="fileupload-preview"></span></div><span class="btn btn-file"><span class="fileupload-new">Selecione</span><span class="fileupload-exists">Alterar</span><input name="arquivo" type="file" /></span><a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remover</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <hr>
                    <div class="row-fluid">
                        <div class="center">
                            <input name="nova" id="post" type="hidden" value="1">
                            <input name="remetente_id" id="post" type="hidden" value="<?php echo $GLOBALS['user']->getId(); ?>">
                            <input name="post" id="post" type="hidden" value="1" >
                            <input class="btn btn-primary" type="submit" value="enviar">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>