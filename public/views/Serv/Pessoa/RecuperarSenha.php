<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Viana & Moura</title>

        <!-- start css -->
        <link href="bootstrap/css/bootstrap.min.css?v2" rel="stylesheet" media="all">
        <link href="css_p/ajuste2.css?v4" rel="stylesheet" media="screen">
        <link href="plugins/font-awesome-4.2.0/css/font-awesome.min.css?v2" rel="stylesheet">
        <link href="plugins/jasny-bootstrap/css/jasny-bootstrap.min.css?v2" rel="stylesheet">
        <!-- end css -->

        <!-- start js -->
        <script src="plugins/jquery-2.1.1.min.js?v2"></script>
        <script src="plugins/jquery-migrate-1.2.1.min.js?v2"></script>
        <script src="bootstrap/js/bootstrap.min.js?v2"></script>
        <script src="plugins/jquery.ba-throttle-debounce.js?v2"></script>
        <script src="plugins/jquery.knob.js?v2"></script>
        <script src="plugins/jasny-bootstrap/js/jasny-bootstrap.min.js?v2"></script>
        <script src="plugins/elastic.js?v2"></script>
        <script src="plugins/jquery_maskedinput.js?v2"></script>
        <!-- end js -->

        <style type="text/css">


            body {
                padding-top: 0px;
                padding-bottom: 40px;
                background-image: url("img_p/bg-light.jpg");
                overflow-y: scroll;
            }
        </style>
    </head>
    <body>
        <nav id="navbar-desktop" class="navbar navbar-default hidden-sm hidden-xs" role="navigation">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="fa fa-user"></span>
                    </button>
                    <a class="navbar-brand" href="#"><img src="img_p/logo.png"></a>
                </div>
            </div><!-- /.container-fluid -->
        </nav>
        <nav id="menu-mobile-nav" class="navbar navbar-default visible-xs visible-sm" role="navigation">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="#"><img src="img_p/logo.png"></a>
                </div>
            </div><!-- /.container-fluid -->
        </nav>
        <div class="container-fluid">
            <div class="row">
                <div id="conteudoMacro" class="col-lg-12 col-md-9">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2><i class="fa fa-th"></i><span class="break"></span>Acesso ao Sistema</h2>
                            </div>
                            <div class="panel-body">
                                <form action="<?php echo Util_Link::link("Serv", "Pessoa", "RecuperarSenha"); ?>" method="post">
                                    <?php echo Util_FlashMessage::read(); ?>
                                    <div class="row">
                                        <div class="col-sm-4 col-sm-offset-4">
                                            <label>Login</label>
                                            <input name="login" id="login" class="form-control" type="text" value="">
                                            <label>CPF</label>
                                            <input name="cpf" id="cpf" class="cpf form-control" type="text" value="">
                                            <input name="post" id="post" type="hidden" value="1" /><br>
                                            <input class="btn btn-primary" id="submitform" name="submitform" type="submit" value="salvar" />
                                        </div>
                                    </div>
                                </form>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer>
            <hr>
            <span>&nbsp;&copy; Viana & Moura Construções S/A</span>
            <span class="pull-right"><i class="fa fa-question"></i> Dúvidas e Sugestões: (81) 9103-9875<br>
            </span>
        </footer>
        <script>

            $("#login").mask("aaaa?aaaaaaaaaaaaaaaa", {placeholder: ""});
            $("#cpf").mask("999.999.999-99", {placeholder: "_"});

            $(function () {
                $("#botaoSenha").click(function () {
                    $("#botaoSenha").addClass('hide');
                    $("#botaoSenha").removeClass('show');

                    $("#camposSenha").addClass('show');
                    $("#camposSenha").removeClass('hide');
                })
            });

        </script>
    </body>
</html>