<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2><i class="fa fa-th"></i><span class="break"></span>Gerar Código - Modelo</h2>
        </div>
        <div class="panel-body">
            <form action="<?php echo Util_Link::link("Scaf", "Scaffold", "CriarCodigoModelo"); ?>" method="post" data-sync="1">
                <div class="row">
                    <div class="col-sm-12">
                        <label>O que deseja criar?</label>
                        <h4>
                            <input type="checkbox" name="model" id="model" value="1" checked> Model
                            <input type="checkbox" name="controller" id="controller" value="1" checked> Controller
                            <input type="checkbox" name="mapper" id="mapper" value="1" checked> Mapper
                            <input type="checkbox" name="repository" id="repository" value="1" checked> Repository
                            <input type="checkbox" name="index" id="index" value="1" checked> Index
                            <input type="checkbox" name="inserir" id="index" value="1" checked> Inserir
                            <input type="checkbox" name="editar" id="index" value="1" checked> Editar
                            <input type="checkbox" name="visualizar" id="index" value="1" checked> Visualizar
                        </h4>
                    </div>
                    <div class="col-sm-4">
                        <label>Nome Tabela</label>
                        <input type="text" id="nomeTabela" name="nomeTabela" class="form-control">
                    </div>
                    <div class="col-sm-4">
                        <label>Nome View</label>
                        <input type="text" id="nomeView" name="nomeView" class="form-control">
                    </div>
                    <div class="col-sm-4">
                        <label>Nome Model</label>
                        <input type="text" id="nomeModel" name="nomeModel" class="form-control">
                    </div>
                    <div class="col-sm-6">
                        <label>Necessário</label>
                        <textarea id="necessarios" name="necessarios" class="form-control"></textarea>
                    </div>
                    <div class="col-sm-6">
                        <label>Atributos</label>
                        <textarea id="atributos" name="atributos" class="form-control"></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-center">
                            <input type="submit" value="salvar" class="btn btn-primary btn-submit">                    
                            <input type="hidden" value="1" name="post">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>