<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2><i class="fa fa-th"></i><span class="break"></span>Gerar Código - Modelo</h2>
        </div>
        <?php if ($this->resultado) { ?>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-12">
                        <label>Resultado</label>
                        <textarea class="form-control" style="height: 150px;">
                            <?php echo $this->resultado; ?>
                        </textarea>
                    </div>
                </div>
            </div>
        <?php } ?>
        <div class="panel-body">
            <form action="<?php echo Util_Link::link("Scaf", "Scaffold", "FormCodigoObjeto"); ?>" method="post" data-sync="1">
                <div class="row">
                    <div class="col-sm-4">
                        <label>Nome Tabela (sem _id)</label>
                        <input type="text" id="nomeTabela" name="nomeTabela" class="form-control">
                    </div>
                    <div class="col-sm-4">
                        <label>Nome Model</label>
                        <input type="text" id="nomeModel" name="nomeModel" class="form-control">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-center">
                            <input type="submit" value="salvar" class="btn btn-primary btn-submit">                    
                            <input type="hidden" value="1" name="post">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>