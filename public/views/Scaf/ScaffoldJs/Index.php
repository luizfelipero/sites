<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2><i class="fa fa-th"></i><span class="break"></span>Scaffold SIG</h2>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="btn-group btn-group-justified">
                        <a href="<?php echo Util_Link::link("Scaf", "ScaffoldJs", "FormCodigoModelo"); ?>" class="btn btn-info">Módulos</a>
                        <a href="<?php echo Util_Link::link("Scaf", "ScaffoldJs", "FormCodigoObjeto"); ?>" class="btn btn-success">getModelo() c/ cache</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>