<div class="col-sm-12">
    <div class="row hide" id="msgChrome">
        <div class="col-lg-12">
            <div class="alert alert-danger">
                <strong>Atenção!</strong> Identificamos que você não está utilizando o Chrome. Para aproveitar melhor o sistema aconselhamos a instalação.<a href="http://www.google.com/chrome" target="_blank"><img src="img_p/chrome.png"></a>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- knobs -->
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="panel panel-default box-home">
                <div class="panel-heading">
                    <h2><i class="fa fa-th"></i><span class="break"></span>Contribuindo para a felicidade</h2>
                    <div class="box-icon">
                    </div>
                </div>
                <div class="panel-body" style="height: 260px; overflow-y: visible; overflow-x: visible">
                    <div class="row hide" id="knob">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators" style="margin-bottom: -20px;">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active" style="background-color: #cc0000;"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1" style="background-color: #cc0000;"></li>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                                <!--                                <div class="item active" style="height: 230px; padding: 0px; margin: 0px;">
                                                                    <img src="img_p/7milcasas.jpg" class="img-responsive">
                                                                </div>-->
                                <div class="item active" style="height: 200px; margin-top: 30px;">
                                    <div class="col-xs-6 col-center knob-div">
                                        <input class="knob" value="<?php echo $this->casasVendidasLast; ?>" data-min="0" data-max="8000" data-fgColor="#F89406" />
                                        <br>
                                        <span class="knobslabel" style="color: #F89406">Casas Vendidas</span>
                                        <br>
                                        <button type="button" id="casasVendidas-btn" class="btn btn-warning btn-sm" style="margin-top: 8px;"><i class="fa fa-plus"></i></button>
                                    </div>
                                    <div class="col-xs-6 col-center knob-div">
                                        <input class="knob" value="68" data-max="100" data-fgColor="#CC0000" />
                                        <br>
                                        <span class="knobslabel" style="color: #CC0000">NPS</span>
                                        <br>
                                        <div class="btn-group" role="group" aria-label="..." style="margin-top: 8px;">
                                            <a href="file/informes/nps-abril17.pptx" target="_blank" class="btn btn-default btn-sm">Abril</a>
                                            <a href="file/informes/nps-maio17.pptx" target="_blank" class="btn btn-default btn-sm">Maio</a>
                                            <a href="file/informes/nps-jun17.pptx" target="_blank" class="btn btn-danger btn-sm">Jun</a>
                                        </div>                                        
                                    </div>
                                </div>
                                <div class="item" style="height: 200px; margin-top: 30px;">
                                    <div class="col-xs-6 col-center knob-div">
                                        <input class="knob" value="34" data-max="34" data-fgColor="#337AB7" />
                                        <br>
                                        <span class="knobslabel" style="color: #337AB7">Mais Educação</span>
                                    </div>
                                    <div class="col-xs-6 col-center knob-div">
                                        <input class="knob" value="3002" data-max="3300" data-fgColor="#468847" />
                                        <br>
                                        <span class="knobslabel" style="color: #468847">Minha Vila Mais Verde<br>Árvores Plantadas</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /knobs -->
        <!-- gdh -->
        <div class="col-sm-6 col-lg-4">
            <div class="panel panel-default box-home-scroll">
                <div class="panel-heading">
                    <h2><i class="fa fa-th"></i><span class="break"></span>Notas GDH</h2>
                    <div class="box-icon">

                    </div>
                </div>
                <div class="panel-body" style="height: 260px;">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="btn-group btn-group-justified">
                                <div class="btn-group" role="group">
                                    <a class="btn dropdown-toggle btn-warning" data-toggle="dropdown" href="#" style="padding: 10px;">
                                        IMAT
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="?m=Ass&c=Imat&a=Index&mes=6&ano=2017" class="">IMAT Junho</a></li>
                                        <li><a href="?m=Ass&c=Imat&a=Index&semestre=1&ano=2017" class="">IMAT 2017.1 (concluido)</a></li>
                                    </ul>
                                </div>
                                <div class="btn-group" role="group">
                                    <a class="btn dropdown-toggle btn-success" data-toggle="dropdown" href="#" style="padding: 10px;">
                                        Avatar
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="https://docs.google.com/forms/d/e/1FAIpQLSffwN6iXiO7NOI7vQI7_9fsUtlyxxwu2b29nok12IyYOVG8KA/viewform" class="">Solicitação de participação<br>em curso Avatar</a></li>
                                    </ul>
                                </div>
                                <div class="btn-group" role="group">
                                    <a class="btn dropdown-toggle btn-info" data-toggle="dropdown" href="#" style="padding: 10px;">
                                        Calendários
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="file/calendarios/2017/EC.pdf" class="" target="_blank">Recife</a></li>
                                        <li><a href="file/calendarios/2017/BJ.pdf" class="" target="_blank">Belo Jardim</a></li>
                                        <li><a href="file/calendarios/2017/SC.pdf" class="" target="_blank">Santa Cruz</a></li>
                                        <li><a href="file/calendarios/2017/CA.pdf" class="" target="_blank">Caruaru</a></li>
                                        <li><a href="file/calendarios/2017/GA.pdf" class="" target="_blank">Garanhuns</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <hr style="margin: 5px;">
                            <a href="file/informes/vianews-e11.pdf" target="_blank">
                                <img class="noticia img-responsive" src="file/informes/vianews-e11-teaser.jpg" style="margin-bottom: 5px;">
                            </a>
                            <hr style="margin: 3px;">
                            <a href="file/informes/api_2017-1.pptx" class="btn btn-block btn-primary" target="_blank">API 2017.1</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /gdh -->
        <!-- eventos -->
        <div class="col-sm-6 col-lg-4">
            <div class="panel panel-default box-home-scroll">
                <div class="panel-heading">
                    <h2><i class="fa fa-th"></i><span class="break"></span>Calendário Eventos (90 dias)</h2>
                </div>
                <div class="panel-body" style="height: 260px;">
                    <div class="row">
                        <div class="col-sm-12" style="margin-top: -10px;">
                            <span class="label label-info">Informes</span>
                            <span class="label label-success">Qualidade</span>
                            <span class="label label-warning">Pessoas</span>
                            <div class="table-responsive" style="margin-top: 7px;">
                                <table   class="table table-bordered table-condensed table-hover table-striped">
                                    <thead style="border-radius: 10px 20px 30px 40px;">
                                        <tr>
                                            <th class="col-center">Data</th>
                                            <th class="col-center">Evento</th>                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($this->eventos as $evento) { ?>
                                            <tr>
                                                <td class="col-center"><label class="label label-<?php echo $evento->getTipo(); ?>"><?php echo $evento->getRotuloData(); ?></label></td>
                                                <td><?php echo $evento->getNome(); ?></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /eventos -->

        <!-- mudanças -->
        <?php if ($GLOBALS['user']->getTipo() < 90) { ?>
            <div class="col-sm-6 col-lg-4">
                <div class="panel panel-default box-home-scroll">
                    <div class="panel-heading">
                        <h2 style=""><i class="fa fa-th"></i><span class="break"></span>Últimas Mudanças</h2>
                        <div class="box-icon">
                            <span class="break"></span>
                            <a href="<?php echo Util_Link::link("Adm", "Pessoa", "Mudancas"); ?>"><i class="fa fa-search"></i></a>
                        </div>
                    </div>
                    <div class="panel-body" style="height: 350px; padding: 5px;">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel-group" id="mudancas" role="tablist" aria-multiselectable="false">
                                    <?php
                                    if ($this->ultimasMudancasQuadro != "") {
                                        foreach ($this->ultimasMudancasQuadro as $mudanca) {
                                            ?>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <a data-toggle="collapse" data-parent="#mudancas" href="#mudanca<?php echo $mudanca->getId(); ?>">
                                                        <?php echo Util_Utilidade::wrapTexto($mudanca->getColaborador()->getNome(), 20); ?>
                                                        <?php echo $mudanca->getLabelAbreviado(); ?>
                                                        <span class="pull-right"><?php echo Util_Utilidade::exibeData($mudanca->getData()); ?>&nbsp;</span>

                                                    </a>
                                                </div>
                                                <div id="mudanca<?php echo $mudanca->getId(); ?>" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <?php echo $mudanca->getLabel(); ?>
                                                        <?php echo $mudanca->getCargo()->getNome(); ?>
                                                        em <?php echo $mudanca->getNomeUnidade(); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <!-- /mudancas -->

        <!-- aniversarios -->
        <?php if ($GLOBALS['user']->getTipo() < 90) { ?>
            <div class="col-sm-6 col-lg-4">
                <div class="panel panel-default box-home-scroll">                 
                    <div class="panel-heading">
                        <h2><i class="fa fa-th"></i><span class="break"></span>Aniversariantes</h2>
                        <div class="box-icon">

                        </div>
                    </div>
                    <div class="panel-body" style="height: 350px; padding: 5px;">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel-group" id="aniversariantes" role="tablist">
                                    <?php
                                    if ($this->aniversariantes != "") {
                                        foreach ($this->aniversariantes as $aniversariante) {
                                            ?>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <a data-toggle="collapse" data-parent="#aniversariantes" href="#aniversariante<?php echo $aniversariante->getId(); ?>">
                                                        <?php echo Util_Utilidade::wrapTexto($aniversariante->getNome(), 20); ?>
                                                        <span class="label label-warning pull-right"><i class="fa fa-birthday-cake icon-white"></i></span>
                                                        <span class="pull-right"><?php echo $aniversariante->getDiaAniversario() . "/" . Util_Utilidade::addZeros($aniversariante->getMesAniversario(), 2); ?>&nbsp;</span>

                                                    </a>
                                                </div>
                                                <div id="aniversariante<?php echo $aniversariante->getId(); ?>" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <?php echo $aniversariante->getCargo()->getNome(); ?>
                                                        em <?php echo $aniversariante->getNomeUnidade(); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                                <div class="panel-group" id="aniversariantesEmpresa" role="tablist">
                                    <?php
                                    if ($this->aniversariantesEmpresa != "") {
                                        foreach ($this->aniversariantesEmpresa as $aniversarianteEmpresa) {
                                            ?>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <a class="accordion" data-toggle="collapse" data-parent="#aniversariantesEmpresa" href="#aniversarianteEmpresa<?php echo $aniversarianteEmpresa->getId(); ?>">
                                                        <?php echo Util_Utilidade::wrapTexto($aniversarianteEmpresa->getNome(), 25); ?>
                                                        <span class="label label-info pull-right"><i class="fa fa-home icon-white"></i> <?php echo $aniversarianteEmpresa->getTempoEmpresaACompletar(); ?></span>
                                                        <span class="pull-right"><?php echo $aniversarianteEmpresa->getAniversarioEmpresa(); ?>&nbsp;</span>
                                                    </a>
                                                </div>
                                                <div id="aniversarianteEmpresa<?php echo $aniversarianteEmpresa->getId(); ?>" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <?php echo $aniversarianteEmpresa->getCargo()->getNome(); ?>
                                                        em <?php echo $aniversarianteEmpresa->getNomeUnidade(); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <!-- /aniversarios -->

        <!-- ausencias -->
        <?php if ($GLOBALS['user']->getTipo() < 90) { ?>
            <div class="col-sm-6 col-lg-4">
                <div class="panel panel-default box-home-scroll">
                    <div class="panel-heading">
                        <h2 style=""><i class="fa fa-th"></i><span class="break"></span>Ausências</h2>
                        <div class="box-icon">
                            <span class="break"></span>
                            <a href="<?php echo Util_Link::link("Adm", "Ausencia", "Inserir"); ?>"><i class="fa fa-plus"></i></a>
                        </div>
                    </div>
                    <div class="panel-body" style="height: 350px; padding: 5px;">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel-group" id="mudancas" role="tablist" aria-multiselectable="false">
                                    <?php
                                    if ($this->ausencias != "") {
                                        foreach ($this->ausencias as $ausencia) {
                                            ?>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <a data-toggle="collapse" data-parent="#mudancas" href="#mudanca<?php echo $ausencia->getId(); ?>">
                                                        <?php echo Util_Utilidade::wrapTexto($ausencia->getNomeColaborador(), 20); ?>
                                                        <span class="pull-right"><?php echo substr(Util_Utilidade::exibeData($ausencia->getDataInicio()), 0, 5) . " até " . substr(Util_Utilidade::exibeData($ausencia->getDataFim()), 0, 5); ?>&nbsp;</span>

                                                    </a>
                                                </div>
                                                <div id="mudanca<?php echo $ausencia->getId(); ?>" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <?php
                                                        echo $ausencia->getTipo();
                                                        if ($ausencia->getTexto() != "") {
                                                            echo " > " . $ausencia->getTexto();
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<?php } ?>
        <!-- /mudancas -->
    </div>
</div>

<!-- modals -->

<div id="casasVendidas-modal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Casas Vendidas</h4>
            </div>
            <div class="modal-body">
                <div id="casasVendidas-chart" style="width: 780px; height: 350px;" >
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <h5>Total</h5>
                        <table class="table table-bordered table-condensed">
                            <tr>
                                <?php
                                //porcent é para a divisão das celulas
                                $porcent = floor(100 / count($this->casasVendidas));
                                foreach ($this->casasVendidas as $casasVendidas) {
                                    ?>
                                    <th style="width: <?php echo $porcent; ?>%"><?php echo $casasVendidas->getNomeMes(); ?></th>
<?php } ?>
                            </tr>
                            <tr>
                                <?php foreach ($this->casasVendidas as $casasVendidas) { ?>
                                    <td class="right"><?php echo $casasVendidas->getTotal(); ?></td>
<?php } ?>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div id="maiseducacao-modal" class="modal fade" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" >
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Programa Tsuru - Projeto +Educação</h3>
            </div>
            <div class="modal-body">
                <div class="col-sm-10 col-sm-offset-1">
                    <p>Até agora 72 colaboradores voltaram a estudar na Viana & Moura</p>
                </div>
            </div>
            <div class="modal-body" id="maiseducacao-chart" style="height: 300px;" >

            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<div id="modal-aedes" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Combate ao Aedes Aegypti começa com Informação</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <h4>Veja o que podemos fazer para combater o mosquito e se proteger de Dengue, Zika e Chikungunya.<br>A informação é uma arma valiosa.</h4>
                        <br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div style="min-height: 300px;">
                            <p class="text-center" style="background-color: #F89406; padding: 10px;">
                                <i class="fa fa-commenting fa-inverse fa-5x"></i>
                            </p>
                            <h4>Avise se encontrar focos nas obras, imobiliárias e escritório</h4>
                            <p>Você pode entrar em contato com o responsável ou com o setor de sustentabilidade</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div style="min-height: 300px;">
                            <p class="text-center" style="background-color: #468847; padding: 10px;">
                                <i class="fa fa-bullhorn fa-inverse fa-5x"></i>
                            </p>
                            <h4>Avise se encontrar focos nas ruas</h4>
                            <p>Você pode entrar em contato com as autoridades através:</p>
                            <ul>
                                <li>Das secretarias de saúde;</li>
                                <li>Número 136 do Ministério da Saúde;</li>
                                <li>Whatsapp Globo NE - (81) 98181-2222.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div style="min-height: 300px;">
                            <p class="text-center" style="background-color: #cc0000; padding: 10px;">
                                <i class="fa fa-street-view fa-inverse fa-5x"></i>
                            </p>
                            <h4>Proteja-se</h4>
                            <p>Ao ir trabalhar use repelente (atenção ao período de reposição) e roupas de manga longa</p>
                            <a href="http://epoca.globo.com/vida/noticia/2015/12/como-se-proteger-do-zika-virus.html" class="btn btn-sm btn-danger btn-block" target="_blank">Dicas Repelentes</a></li>

                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div style="min-height: 300px;">
                            <p class="text-center" style="background-color: #0000ff; padding: 10px;">
                                <i class="fa fa-tint fa-inverse fa-5x"></i>
                            </p>
                            <h4>Combate ao mosquito</h4>
                            <p>O principal foco ainda é o combate ao mosquito. Elimine os locais onde o mosquito pode depositar suas larvas. A construção de armadilhas também pode ser um aliado.</p>
                            <a href="https://youtu.be/nAu4tvPzGTo" class="btn btn-sm btn-info btn-block" target="_blank">Aprenda a fazer as Armadilhas</a></li>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<script>

    //NOTA: google chart não pode ficar dentro do "onload" do jquery
//    var novas = <?php // echo ($this->acoes['MyNova'] == "") ? 0 : $this->acoes['MyNova'];            ?>;
//    var emDia = <?php // echo ($this->acoes['MyEmDia'] == "") ? 0 : $this->acoes['MyEmDia'];            ?>;
//    var atrasadas = <?php // echo ($this->acoes['MyAtrasada'] == "") ? 0 : $this->acoes['MyAtrasada'];            ?>;
    var chartAcoes;

    function drawChart() {
//        //grafico de ações
//        var data = google.visualization.arrayToDataTable([
//            ['Ações', 'Minhas ações'],
//            ['Novas', novas],
//            ['Em dia', emDia],
//            ['Atrasadas', atrasadas]
//        ]);
//
//        var options = {
//            backgroundColor: 'none',
//            legend: 'none',
//            colors: ['#109618', '#3366CC', '#DC3912'],
//            chartArea: {width: "90%", height: "92%"},
//            fontSize: 16,
//            pieStartAngle: 45,
//            pieSliceTextStyle: {color: "#333333", fontName: "Arial", fontSize: "15"},
//            tooltip: {textStyle: {fontSize: 12}}};
//
//        chartAcoes = new google.visualization.PieChart(document.getElementById('grafico'));
//        chartAcoes.draw(data, options);
//
//        google.visualization.events.addListener(chartAcoes, 'select', selectHandler);
//
//        $("#grafico").css('visibility', 'visible');
//        $("#indicador").show(100);
//        $("#carregando").modal('hide');
//
//        function selectHandler() {
//
//            //            alert(chartAcoes.getSelection()[0].row);
//            $(window).unbind('beforeunload');
//            if (chartAcoes.getSelection()[0].row == 0) {
//                window.location = "?m=Acao&c=Acao&a=MyNova";
//            } else {
//                window.location = "?m=Acao&c=Acao&a=MyAndamento";
//            }
//        }

        //grafico de casas Vendidas
        var data = google.visualization.arrayToDataTable([
            ['ugb', 'BJ', 'SC', 'SL', 'CA', 'GA', {role: 'annotation'}],
<?php foreach ($this->casasVendidas as $casasVendidas) { ?>
                ['<?php echo $casasVendidas->getNomeMes(); ?>', <?php echo $casasVendidas->getUgb01(); ?>, <?php echo $casasVendidas->getUgb02(); ?>, <?php echo $casasVendidas->getUgb03(); ?>, <?php echo $casasVendidas->getUgb04(); ?>, <?php echo $casasVendidas->getUgb05(); ?>, ''],
<?php } ?>
        ]);

        var options = {
            legend: 'none',
            width: 780,
            height: 300,
            bar: {groupWidth: '75%'},
            isStacked: true,
        };

        var chartCasas = new google.visualization.ColumnChart(document.getElementById('casasVendidas-chart'));
        chartCasas.draw(data, options);



        //grafico +educacao
        var data = google.visualization.arrayToDataTable([
            ['Genre', 'Diretos', 'Indiretos', {role: 'annotation'}],
            ['UGB01', 33, 10, ''],
            ['UGB02', 0, 4, ''],
            ['UGB04', 9, 7, ''],
            ['UGB05', 8, 1, ''],
        ]);

        var options = {
            width: 780,
            height: 300,
            legend: {position: 'right', maxLines: 3},
            //legend: 'none',
            bar: {groupWidth: '75%'},
        };

        var chartEducacao = new google.visualization.ColumnChart(document.getElementById('maiseducacao-chart'));
        chartEducacao.draw(data, options);


    }




    $(function () {

        $(".knob").knob({
            width: "100",
            height: "100",
            bgColor: "#cccccc",
            skin: "tron",
            angleOffset: -125,
            angleArc: 250,
            readOnly: true

        });

        $("#knob").removeClass('hide');

//        $(".noticia").click(function () {
//            $("#noticias-modal").modal('show');
//        });
        $("#maiseducacao-btn").click(function () {
            $("#maiseducacao-modal").modal('show');
        });
        $("#casasVendidas-btn").click(function () {
            $("#casasVendidas-modal").modal('show');
        });
        if (!Navegador.isChrome() && !Navegador.isMobile()) {
            $("#msgChrome").removeClass("hide");
        }

        //$("#modal-aedes").modal('show');

        drawChart();

    });

</script>