<div class="span12 box">
    <div class="box-header" data-original-title>
        <h2><i class="icon-th"></i><span class="break"></span>V&M Apps</h2>
        <div class="box-icon">
            <?php if ($GLOBALS['user']->getDepartamento_id() == 76) { ?>
                <span class="break"></span>
                <a href="<?php echo Util_Link::link("Adm", "App", "Inserir"); ?>" class="btn-setting"><i class="icon-plus"></i></a>
            <?php } ?>
        </div>
    </div>
    <div class="box-content">
        <div class="row-fluid">
            <?php
            if ($this->apps != "") {
                foreach ($this->apps as $app) {
                    ?>
                    <div class="span6 well">
                        <div class="row-fluid">
                            <div class="span9">
                                <img src="<?php echo $app->getIcon(); ?>" width="50">
                                <?php echo $app->getNome(); ?><br><br>
                                <?php echo $app->getDescricao(); ?><br>
                                <a href="<?php echo $app->getLink(); ?>" class="btn"><i class="icon-download"></i> Download</a>
                                <a href="<?php echo Util_Link::link("Adm", "App", "Ajuda"); ?>" class="btn"><i class="icon-question-sign"></i> Ajuda</a>
                            </div>
                            <div class="span3">
                                <img src="<?php echo $app->getQrCode(); ?>" width="100" class="img-polaroid pull-right">
                            </div>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
        <?php echo $this->paginacao->exibe(); ?>
    </div>
</div>