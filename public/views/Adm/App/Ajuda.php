<div class="span12 box">
    <div class="box-header" data-original-title>
        <h2><i class="icon-th"></i><span class="break"></span> Ajuda</h2>
    </div>
    <div class="box-content">
        <div class="row-fluid">
            <div class="span10 offset1">
                <h3>1. Verifique se seu celular é suportado</h3>
                <p>
                    No aparelho vá em <strong>Configurações > Sobre o telefone</strong><br>
                    Deve-se verificar se o campo <strong>Versão do Android</strong> é 2.2 ou superior.
                </p>
                <h3>2. Permita os aplicativos V&M no seu aparelho</h3>
                <p>
                    No aparelho vá em <strong>Configurações > Aplicativos | Segurança</strong><br>
                    Se desmarcado, marque o campo <strong>Fontes desconhecidas</strong>.<br>
                </p>
                <h3>3. Instale o QR Droid em seu aparelho</h3>
                <h5>Agora volte aos aplicativos e, usando o QR Droid, escaneie os códigos.</h5>
            </div>
        </div>
    </div>
</div>