<div class="panel panel-default">
    <div class="panel-heading">
        <h2><i class="fa fa-th"></i><span class="break"></span>Inserir Pessoa</h2>
    </div>
    <div class="panel-body">
        <form action="<?php echo Util_Link::link("Adm", "Pessoa", "Inserir"); ?>" enctype="multipart/form-data" data-sync="1" method="post" >
            <div class="row">
                <div class="col-sm-3">
                    <label>Nome</label>
                    <input name="nome" type="text" class="form-control">
                    <label>Apelido</label>
                    <input type="text" name="apelido" class="form-control"> 
                    <label>CPF</label>
                    <input name="cpf" type="text" class="cpf form-control">
                    <label>Aniversário</label>
                    <div class="row">
                        <div class="col-xs-4">
                            <input name="diaAniversario" class="form-control" type="text" maxlength="2" >
                        </div>
                        <div class="col-xs-1">/</div>
                        <div class="col-xs-4">
                            <input name="mesAniversario" class="form-control" type="text" maxlength="2" >
                        </div>
                    </div>
                    <label>Data Admissão</label>
                    <input type="text" name="data" class="form-control data">
                </div>
                <div class="col-sm-2 center">
                    <label>Foto</label>
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 90px; height: 120px;"><img src="img_p/foto_default.jpg"></div>
                        <div>
                            <span class="btn btn-default btn-file"><span class="fileinput-new">Selecionar</span><span class="fileinput-exists">Substituir</span><input type="file" name="arquivo_1"></span>
                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <label>E-mail Pessoal</label>
                    <input name="emailPessoal" type="text" class="form-control" >
                    <label>Skype</label>
                    <input name="skype" type="text" class="form-control" >
                    <label>Telefone</label>
                    <input name="telefone" type="text" class="telddd form-control" >
                    <label>Telefone Pessoal</label>
                    <input name="telefonePessoal" type="text" class="telddd form-control" >
                </div>
                <div class="col-sm-3">
                    <label>Tipo</label>
                    <select name="tipo" id="tipo" class="form-control select2">
                        <option value="0">--Selecione--</option>
                        <option value="1">Indireto</option>
                        <option value="2">Direto</option>
                        <option value="3">Direto Infra</option>
                    </select>
                    <div>
                        <label>Chapa</label>
                        <input name="chapa" type="text" class="form-control" >
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="aviso" value="1" checked> Avisar na Home
                        </label>
                    </div>
                    <div id="tipo1" class="realoc hide">
                        <label>Cargo</label>
                        <select name="cargo_id_1" class="form-control select2">
                            <?php echo Util_Form::makeLists($this->indiretos['cargos'], array("seletores" => array("selecione"))); ?>
                        </select>
                        <?php echo Util_Form::makeSelectUnidadesInserir(2); ?>
                    </div>
                    <div id="tipo2" class="realoc hide">
                        <label>Cargo</label>
                        <select name="cargo_id_2" class="form-control select2">
                            <?php echo Util_Form::makeLists($this->diretos['cargos'], array("seletores" => array("selecione"), "default" => $default)); ?>
                        </select>
                        <label>Unidade</label>
                        <select name="unidade_id_2" id="unidade_id_2" class="form-control select2">
                            <?php echo Util_Form::makeLists($this->diretos['unidades'], array("seletores" => array("selecione"))); ?>
                        </select>
                    </div>
                    <div id="tipo3" class="realoc hide">
                        <label>Cargo</label>
                        <select name="cargo_id_3" class="form-control select2">
                            <?php echo Util_Form::makeLists($this->diretosInfra['cargos'], array("seletores" => array("selecione"))); ?>
                        </select>
                        <label>Unidade</label>
                        <select name="unidade_id_3" id="unidade_id_3" class="form-control select2">
                            <?php echo Util_Form::makeLists($this->diretos['unidades'], array("seletores" => array("selecione"))); ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 col-sm-offset-4">
                    <input name="post" id="post" type="hidden" value="1" />
                    <input type="hidden" name="operacao" value="1">
                    <input type="hidden" name="sig" value="1">
                    <input class="btn btn-primary" id="submitform" name="submitform" type="submit" value="salvar" />
                </div>
            </div>
        </form>
    </div>
</div>

<script>

    $("#tipo").change(function () {

        $(".realoc").addClass('hide');
        var id = "#tipo" + $(this).val();
        $(id).removeClass('hide');

    });



</script>