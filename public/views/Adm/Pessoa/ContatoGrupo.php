<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2><i class="fa fa-th"></i><span class="break"></span>Contatos para Grupos</h2>            
        </div>
        <div class="panel-body">

            <div class="row">
                <div class="col-sm-12 ">
                    <label>GRUPO A a F (<?php echo count($this->emailsAF); ?>)</label>
                    <textarea class="form-control"><?php echo implode(";",$this->emailsAF); ?></textarea>
                    <label>GRUPO G a L (<?php echo count($this->emailsGL); ?>)</label>
                    <textarea class="form-control"><?php echo implode($this->emailsGL,";"); ?></textarea>
                    <label>GRUPO M a R (<?php echo count($this->emailsMR); ?>)</label>
                    <textarea class="form-control"><?php echo implode($this->emailsMR,";"); ?></textarea>
                    <label>GRUPO S a Z (<?php echo count($this->emailsSZ); ?>)</label>
                    <textarea class="form-control"><?php echo implode($this->emailsSZ,";"); ?></textarea>
                </div>
            </div>

        </div>
    </div>
</div>
<script>

    $(function () {

        $("#cartoes-btn").click(function () {
            $("#cartao").removeClass('hide');
            $("#lista").addClass('hide');
            $("#exibicao").val("c");
            $("#cartoes-btn").addClass("btn-primary");
            $("#cartoes-btn").removeClass("btn-default");
            $("#lista-btn").removeClass("btn-primary");
            $("#lista-btn").addClass("btn-default");
        });
        $("#lista-btn").click(function () {
            $("#cartao").addClass('hide');
            $("#lista").removeClass('hide');
            $("#exibicao").val("l");
            $("#lista-btn").addClass("btn-primary");
            $("#lista-btn").removeClass("btn-default");
            $("#cartoes-btn").removeClass("btn-primary");
            $("#cartoes-btn").addClass("btn-default");
        });


    });

</script>