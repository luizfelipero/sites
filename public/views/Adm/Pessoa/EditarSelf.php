<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2><i class="fa fa-th"></i><span class="break"></span>Editar Dados Pessoais</h2>
        </div>
        <div class="panel-body">
            <form action="<?php echo Util_Link::link("Adm", "Pessoa", "EditarSelf"); ?>" enctype="multipart/form-data" data-sync="1" method="post">
                <div class="row">
                    <div class="col-sm-3">
                        <legend>Informações do Sistema</legend>
                        <label>Nome Completo</label>
                        <input name="nome" type="text" class="form-control obrigatorio" value="<?php echo $this->pessoa->getNomeCompleto(); ?>" >
                        <label>Nome Reduzido (1 nome e 1 sobrenome)</label>
                        <input type="text" name="apelido" class="form-control" value="<?php echo $this->pessoa->getApelido(); ?>" > 
                        <label>Aniversário</label>
                        <div class="row">
                            <div class="col-sm-4">
                                <input name="diaAniversario" class="form-control obrigatorio" value="<?php echo $this->pessoa->getDiaAniversario(); ?>" type="text" maxlength="2" >
                            </div>
                            <div class="col-sm-1">
                                /
                            </div>
                            <div class="col-sm-4">
                                <input name="mesAniversario" class="form-control obrigatorio" value="<?php echo $this->pessoa->getMesAniversario(); ?>" type="text" maxlength="2" >
                            </div>
                        </div>
                        <span id="helpBlock" class="help-block">A senha deve ter no mínimo 8 caracteres, com ao menos 1 letra e 1 número</span>
                        <label>Senha</label>
                        <input type="password" name="senha" class="form-control">
                        <label>Confirme</label>
                        <input type="password" name="csenha" class="form-control">
                    </div>
                    <div class="col-sm-3">
                        <legend>Foto</legend>
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                <?php echo $this->pessoa->getImg(); ?>
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                            <div>
                                <span class="btn btn-default btn-file"><span class="fileinput-new">selecionar</span><span class="fileinput-exists">alterar</span><input type="file" name="arquivo_1"></span>
                                <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">remover</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <legend>Informações de Contato</legend>
                        <label>E-mail Pessoal</label>
                        <input name="emailPessoal" type="text" class="form-control" id="emailPessoal" value="<?php echo $this->pessoa->getEmailPessoal(); ?>">
                        <label>Skype</label>
                        <input name="skype" type="text" class="form-control" value="<?php echo $this->pessoa->getSkype(); ?>">
                        <label>Telefone</label>
                        <input name="telefone" type="text" id="telefone" class="form-control telddd" value="<?php echo Util_Utilidade::exibeTelefone($this->pessoa->getTelefone()); ?>">
                        <label>Telefone Pessoal</label>
                        <input name="telefonePessoal" type="text" id="telefonePessoal" class="form-control telddd" value="<?php echo Util_Utilidade::exibeTelefone($this->pessoa->getTelefonePessoal()); ?>">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="wapp" <?php echo ($this->pessoa->getWapp() == 1) ? "checked" : ""; ?> > Whats app
                            </label>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-4">
                        <input name="id" id="post" type="hidden" value="<?php echo $this->pessoa->getId(); ?>" />
                        <input name="post" id="post" type="hidden" value="1" />
                        <input class="btn btn-primary" id="editar" name="submitform" type="submit" value="salvar" />
                    </div>
                </div>
            </form>
        </div>  
    </div>
</div>