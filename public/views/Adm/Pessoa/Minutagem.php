<div class="span12 box">
    <div class="box-header" data-original-title>
        <h2><i class="icon-th"></i><span class="break"></span>Minutagem</h2>
        <div class="box-icon">
            <span class="break"></span>
            <a href="#"><i class="icon-filter filtro-botao"></i></a>
        </div>
    </div>
    <div class="box-content hide filtro-opcoes">
        <form class="" action="" method="GET">
            <div class="row-fluid">
                <?php echo Util_Link::linkHideForm("Adm", "Pessoa", "Contato"); ?>
                <div class="span3 offset0">
                    <label>Unidade</label>
                    <select name="unidade_id" id="unidade_id" class="selectUpdate select2">
                        <?php echo Util_Form::makeLists($this->unidades, array("seletores" => array("none"))); ?>
                    </select>
                    <input type="hidden" id="unidade_id_module" value="Adm">
                    <input type="hidden" id="unidade_id_controller" value="Unidade">
                    <input type="hidden" id="unidade_id_action" value="ListarFilhasJson">
                    <input type="hidden" id="unidade_id_destino" value="departamento_id">
                </div>
                <div class="span3">
                    <label>Departamento</label>
                    <select name="departamento_id" id="departamento_id" class="selectUpdate select2">  
                    </select>
                    <input type="hidden" id="departamento_id_module" value="Adm">
                    <input type="hidden" id="departamento_id_controller" value="Unidade">
                    <input type="hidden" id="departamento_id_action" value="ListarFilhasJson">
                    <input type="hidden" id="departamento_id_destino" value="setor_id">
                </div>
                <div class="span3">
                    <label>Setor</label>
                    <select name="setor_id" id="setor_id" class="select2">  
                    </select>
                </div>
                <div class="span3">
                    <label>Cargo</label>
                    <select name="cargo_id" id="cargo_id" class="select2">
                        <?php echo Util_Form::makeLists($this->cargos, array("seletores" => array("none"))); ?>
                    </select>
                </div>
                <div class="span3">
                    <label>Inicial</label>
                    <select name="inicial" id="inicial" class="select2">
                        <?php echo Util_Form::makeListLetras(); ?>
                    </select>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span2 offset5">
                    <input id="submitform" class="btn btn-primary" name="submitform" type="submit" value="filtrar" />
                </div>
            </div>
        </form>
    </div>
    <div class="box-content">
        <div class="row-fluid">
            <div class="span12 offset0">
                <?php echo $this->paginacaoLetras->exibe(); ?>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12 offset0">
                <table class="table table-striped table-bordered table-condensed table-hover">
                    <tr>
                        <th>Nome</th>
                        <th>Cargo</th>
                        <th>Unidade</th>
                        <th>Telefone</th>
                        <th>Minutagem</th>
                        <th>Ações</th>
                    </tr>
                    <?php
                    if ($this->pessoas != "") {
                        foreach ($this->pessoas as $pessoa) {
                            ?>
                            <tr>
                                <td><?php echo ($pessoa->getNome() != "") ? $pessoa->getNome() : "none"; ?></td>
                                <td><?php echo $pessoa->getCargo()->getNome(); ?></td>
                                <td><?php echo $pessoa->getNomeUnidade(); ?></td>
                                <td><?php echo Util_Utilidade::exibeTelefone($pessoa->getTelefone()); ?></td>
                                <td><?php echo $pessoa->getMinutagem(); ?></td>
                                <td>
                                    <div class="btn-group">
                                        <a class="btn btn-mini" href="<?php echo Util_Link::link("Adm", "Pessoa", "EditarMinutagem", $pessoa->getId()); ?>"><i class="icon-pencil"></i></a>
                                    </div>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </table>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12 offset0">
                <?php echo $this->paginacao->exibe(); ?>
            </div>
        </div>
    </div>
</div>
</div>