<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2><i class="fa fa-th"></i><span class="break"></span>Editar Pessoa</h2>
            <div class="box-icon">
                <span class="break"></span>
                <a href="<?php echo Util_Link::link("Adm", "Pessoa", "Inserir"); ?>" class="btn-setting"><i class="fa fa-plus"></i></a>
            </div>
        </div>
        <div class="panel-body">
            <form action="<?php echo Util_Link::link("Adm", "Pessoa", "Editar"); ?>" enctype="multipart/form-data" data-sync="1" method="post">
                <div class="row">
                    <div class="col-sm-3">
                        <legend>Informações</legend>
                        <label>Nome Completo</label>
                        <input name="nome" type="text" class="form-control" value="<?php echo $this->pessoa->getNomeCompleto(); ?>" >
                        <label>Nome Reduzido</label>
                        <input type="text" name="apelido" class="form-control" value="<?php echo $this->pessoa->getApelido(); ?>" > 
                        <label>CPF</label>
                        <input name="cpf" type="text" class="cpf form-control" value="<?php echo $this->pessoa->getCpf(); ?>" >
                        <label>Chapa</label>
                        <input name="chapa" type="text" class="form-control" value="<?php echo ($this->pessoa->getChapa()) ? $this->pessoa->getChapa() : ""; ?>" >
                        <label>Aniversário</label>
                        <div class="row">
                            <div class="col-sm-4">
                                <input name="diaAniversario" class="form-control" value="<?php echo $this->pessoa->getDiaAniversario(); ?>" type="text" maxlength="2" >
                            </div>
                            <div class="col-sm-1">
                                /
                            </div>
                            <div class="col-sm-4">
                                <input name="mesAniversario" class="form-control" value="<?php echo $this->pessoa->getMesAniversario(); ?>" type="text" maxlength="2" >
                            </div>
                        </div>
                        <label>Data Admissão</label>
                        <input type="text" name="dataAdmissao" class="form-control data" value="<?php echo ($this->pessoa->getDataAdmissao() == "0000-00-00") ? "Não informada" : Util_Utilidade::exibeData($this->pessoa->getDataAdmissao()); ?>" >

                    </div>
                    <div class="col-sm-2">
                        <legend>Foto</legend>
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 150px; height: 100px;">
                                <?php echo $this->pessoa->getImg(); ?>
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                            <div>
                                <span class="btn btn-default btn-file"><span class="fileinput-new">selecionar</span><span class="fileinput-exists">alterar</span><input type="file" name="arquivo_1"></span>
                                <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">remover</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <legend>Informações de Contato</legend>
                        <?php if ($GLOBALS['user']->getDepartamento_id() == 76) { ?>
                            <label>E-mail</label>
                            <input name="email" type="text" value="<?php echo $this->pessoa->getEmail(); ?>" class="form-control" >
                        <?php } ?>
                        <label>E-mail Pessoal</label>
                        <input name="emailPessoal" type="text" value="<?php echo $this->pessoa->getEmailPessoal(); ?>" class="form-control" >
                        <label>Telefone</label>
                        <input name="telefone" type="text" value="<?php echo Util_Utilidade::exibeTelefone($this->pessoa->getTelefone()); ?>" class="telddd form-control" >
                        <label>Telefone Pessoal</label>
                        <input name="telefonePessoal" type="text" value="<?php echo Util_Utilidade::exibeTelefone($this->pessoa->getTelefonePessoal()); ?>" class="telddd form-control" >
                        <label>Skype</label>
                        <input name="skype" type="text" class="form-control" value="<?php echo $this->pessoa->getSkype(); ?>">
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3 center">
                        <input name="id" id="post" type="hidden" value="<?php echo $this->pessoa->getId(); ?>" />
                        <input name="post" id="post" type="hidden" value="1" />
                        <input class="btn btn-primary" id="submitform" name="submitform" type="submit" value="salvar" />
                    </div>
                </div>
            </form>
        </div>  
    </div>
</div>