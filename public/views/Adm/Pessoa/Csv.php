<?php

if ($this->pessoas != "") {


//OUPUT HEADERS
    $now = gmdate("D, d M Y H:i:s");
    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
    header("Last-Modified: {$now} GMT");

    // force download  
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");

    // disposition / encoding on response body
    header("Content-Disposition: attachment;filename=\"$this->nome.csv\"");
    header("Content-Transfer-Encoding: binary");


    if ($_GET['tipo'] == "t") {
        echo '"Cargo";'
        . '"Nome";'
        . '"Segundo nome";'
        . '"Sobrenome";'
        . '"Sufixo";'
        . '"Cargo";'
        . '"Empresa";'
        . '"Aniversario";'
        . '"Endereco SIP";'
        . '"Push-to-talk";'
        . '"Comp. exibicao";'
        . '"ID de usuario";'
        . '"Notas";'
        . '"Celular geral";'
        . '"Telefone geral";'
        . '"E-mail geral";'
        . '"Fax geral";'
        . '"Chamada de video geral";'
        . '"Endereco Web geral";'
        . '"Endereco VOIP geral";'
        . '"Caixa postal geral";'
        . '"Ramal geral";'
        . '"Endereco geral";'
        . '"CEP geral";'
        . '"Cidade geral";'
        . '"Estado geral";'
        . '"Pais geral";'
        . '"Celular pessoal";'
        . '"Telefone residencial";'
        . '"E-mail pessoal";'
        . '"Fax residencial";'
        . '"Chamada de video pessoal";'
        . '"Endereco Web residencial";'
        . '"Endereco VOIP residencial";'
        . '"Caixa postal residencial";'
        . '"Ramal residencial";'
        . '"Endereço residencial";'
        . '"CEP residencial";'
        . '"Cidade de residencia";'
        . '"Estado de residencia";'
        . '"Pais de residencia";'
        . '"Celular comercial";'
        . '"Telefone comercial";'
        . '"E-mail comercial";'
        . '"Fax comercial";'
        . '"Chamada de video comercial";'
        . '"Endereco Web comercial";'
        . '"Endereco VOIP comercial";'
        . '"Caixa postal comercial";'
        . '"Ramal comercial";'
        . '"Endereco comercial";'
        . '"CEP comercial";'
        . '"Cidade comercial";'
        . '"Estado comercial";'
        . '"Pais comercial"' . "\r\n";


        foreach ($this->pessoas as $pessoa) {

            echo '"";';
            echo '"' . utf8_decode($pessoa->getNome()) . '";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            if ($pessoa->getTelefone() != "") {
                echo '"021' . $pessoa->getTelefone() . '";';
            }
            echo '"' . $pessoa->getEmail() . '";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '"";';
            echo '""';
            echo "\r\n";
        }
    } else if ($_GET['tipo'] == "e") {


        ob_start();
        $df = fopen("php://output", 'w');
        //fputcsv($df, array_keys(reset($array)));
        fputcsv($df, array("E-mail", "Nome"));
        foreach ($this->pessoas as $pessoa) {
            fputcsv($df, array($pessoa->getEmail(), $pessoa->getNome()));
        }
        fclose($df);
        echo ob_get_clean();
        
    }
}
?>