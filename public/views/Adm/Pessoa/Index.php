<div class="panel panel-default">
    <div class="panel-heading">
        <h2><i class="fa fa-th"></i><span class="break"></span>Pessoas</h2>
        <div class="box-icon">
            <span class="break"></span>
            <a href="#"><i class="fa fa-filter filtro-botao"></i></a>
            <?php if ($GLOBALS['user']->getDepartamento_id() == 76) { ?>
                <span class="break"></span>
                <a href="<?php echo Util_Link::link("Adm", "Pessoa", "Pendencias"); ?>" class="btn-setting"><i class="fa fa-user"></i> <?php if ($this->pendencias != "") { ?><label class="label label-danger"><?php echo $this->pendencias; ?></label><?php } ?></a>
            <?php } ?>
            <span class="break"></span>
            <a href="<?php echo Util_Link::link("Adm", "Pessoa", "Mudancas"); ?>" class="btn-setting"><i class="fa fa-refresh"></i></a>
<!--            <span class="break"></span>
            <a href="<?php echo Util_Link::link("Adm", "Pessoa", "ChecarCpf"); ?>" class="btn-setting"><i class="fa fa-tag"></i></a>-->
            <span class="break"></span>
            <a href="<?php echo Util_Link::link("Adm", "Pessoa", "Inserir"); ?>" class="btn-setting"><i class="fa fa-plus"></i></a>
        </div>
    </div>
    <div class="panel-body filtro-opcoes hide">
        <form class="" action="" method="GET">
            <div class="row">
                <?php echo Util_Link::linkHideForm("Adm", "Pessoa", "Index"); ?>
                <div class="col-sm-3 ">
                    <?php echo Util_Form::makeSelectUnidadesInserir(1); ?>
                </div>
                <div class="col-sm-3">
                    <label>Cargo</label>
                    <select name="cargo_id" id="cargo_id" class="form-control select2">
                        <?php echo Util_Form::makeLists($this->cargos, array("seletores" => array("none"))); ?>
                    </select>
                </div>
                <div class="col-sm-3">
                    <label>Nome (pode utilizar %)</label>
                    <input type="text" name="nome" id="nome" class="form-control">
                    <input type="hidden" name="pass" value="<?php echo date("His"); ?>">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-2 col-sm-offset-5">
                    <input id="subform" class="btn btn-primary" type="submit" value="filtrar" />
                </div>
            </div>
        </form>
    </div>
    <div class="panel-body">
        <div id="pessoas" >
            <div class="row">
                <div class="col-sm-12 ">
                    <?php echo $this->paginacaoLetras->exibe(); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 ">
                    <span>Sua pesquisa retornou <strong><?php echo $this->total; ?></strong> pessoa(s).</span>
                    <div class='table-responsive'>
                        <table class="table table-striped table-bordered table-condensed table-hover">
                            <tr>
                                <th>Chapa</th>
                                <th>Login</th>
                                <th>Nome</th>
                                <th>Cargo</th>
                                <th>Unidade</th>
                                <th>Ações</th>
                            </tr>
                            <?php
                            if ($this->pessoas != "") {
                                foreach ($this->pessoas as $pessoa) {
                                    ?>
                                    <tr>
                                        <td><?php echo ($pessoa->getChapa() != "") ? $pessoa->getChapa() : "Falta"; ?></td>
                                        <td><?php echo $pessoa->getLogin(); ?></td>
                                        <td><?php echo $pessoa->getNomeCompleto(); ?><span class="label label-info pull-right"><i class="fa fa-home icon-white"></i> <?php echo $pessoa->getTempoEmpresa(); ?></span></td>
                                        <td><?php echo ($pessoa->getCargo()) ? $pessoa->getCargo()->getNome() : "-"; ?></td>
                                        <td><?php echo ($pessoa->getUnidadeEspecifica_id()) ? $pessoa->getNomeUnidade(" > ") : ""; ?></td>
                                        <td>
                                            <div class="btn-group">
                                                <a class="btn btn-default btn-xs" href="<?php echo Util_Link::link("Adm", "Pessoa", "Visualizar", $pessoa->getId()); ?>"><i class="fa fa-search"></i></a>
                                                <a class="btn btn-xs btn-default" target="_blank" href="<?php echo Util_Link::link("Adm", "Pessoa", "Cracha", $pessoa->getId()); ?>"><i class="fa fa-tag"></i></a>
                                            </div>
                                        </td>
                                    </tr>    
                                    <?php
                                }
                            }
                            ?>
                        </table>
                    </div>
                </div>
            </div>
            <?php echo $this->paginacao->exibe(); ?>
        </div>
    </div>
</div>