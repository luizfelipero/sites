<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2><i class="fa fa-th"></i><span class="break"></span>Inserir Itens CSV</h2>
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <label>Arquivos</label>
                    <form action="<?php echo Util_Link::link("Adm", "Pessoa", "InsereChapa"); ?>" method="post" enctype="multipart/form-data" data-sync="1" >
                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                            <div class="form-control" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                            <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Selecione</span><span class="fileinput-exists">Trocar</span><input type="file" name="arquivo_1"></span>
                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
                        </div>
                        <input name="post" id="post" type="hidden" value="1" />
                        <input id="atualizar" name="atualizar" type="submit" value="inserir" class="btn btn-primary">
                    </form>     
                </div>
            </div>
        </div>                        
    </div>
</div>