<div class="span12 box">
    <div class="box-header" data-original-title>
        <h2><i class="icon-th"></i><span class="break"></span>Editar Pessoa</h2>
        <div class="box-icon">
            <span class="break"></span>
            <a href="<?php echo Util_Link::link("Adm", "Pessoa", "Inserir"); ?>" class="btn-setting"><i class="icon-plus"></i></a>
        </div>
    </div>
    <div class="box-content">
        <form action="<?php echo Util_Link::link("Adm", "Pessoa", "EditarMinutagem"); ?>" method="post">
            <div class="row-fluid">
                <div class="span12">
                    <legend>Alterar Minutagem</legend>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span3">
                    <label>Telefone</label>
                    <input name="telefone" type="text" class="telddd" id="telefone" value="<?php echo Util_Utilidade::exibeTelefone($this->pessoa->getTelefone()); ?>">
                </div>
                <div class="span3">
                    <label>Minutagem</label>
                    <input name="minutagem" type="text" value="<?php echo $this->pessoa->getMinutagem(); ?>">
                </div>
            </div>
            <hr>
            <div class="row-fluid">
                <div class="span4 offset4 center">
                    <input name="id" id="post" type="hidden" value="<?php echo $this->pessoa->getId(); ?>" />
                    <input name="post" id="post" type="hidden" value="1" />
                    <input class="btn btn-primary" id="submitform" name="submitform" type="submit" value="salvar" />
                </div>
            </div>
        </form>
    </div>  
</div>