<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2><i class="fa fa-th"></i><span class="break"></span>Visualizar Pessoa</h2>
            <div class="box-icon">
                <span class="break"></span>
                <a href="<?php echo Util_Link::link("Adm", "Pessoa", "Inserir"); ?>" class="btn-setting"><i class="fa fa-plus"></i></a>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="nav nav-tabs" id="myTab">
                        <li class="active"><a href="#info" data-toggle="tab">Informações</a></li>
                        <li><a href="#historico" data-toggle="tab">Histórico</a></li>
                        <li><a href="#arquivos" data-toggle="tab">Arquivos</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="info">
                            <div class="row">
                                <div class="col-sm-3">
                                    <legend>Informações</legend>
                                    <label>Nome Completo</label>
                                    <input name="nome" type="text" class="form-control" value="<?php echo $this->pessoa->getNomeCompleto(); ?>" disabled>
                                    <label>Nome Reduzido</label>
                                    <input type="text" name="apelido" class="form-control" value="<?php echo $this->pessoa->getApelido(); ?>" disabled> 
                                    <label>CPF</label>
                                    <input name="cpf" type="text" class="cpf form-control" value="<?php echo $this->pessoa->getCpf(); ?>" disabled>
                                    <label>Chapa</label>
                                    <input name="chapa" type="text" class="form-control" value="<?php echo $this->pessoa->getChapa(); ?>" disabled >
                                    <label>Aniversário</label>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <input name="diaAniversario" class="form-control" value="<?php echo $this->pessoa->getDiaAniversario(); ?>" type="text" maxlength="2" disabled>
                                        </div>
                                        <div class="col-sm-1">
                                            /
                                        </div>
                                        <div class="col-sm-4">
                                            <input name="mesAniversario" class="form-control" value="<?php echo $this->pessoa->getMesAniversario(); ?>" type="text" maxlength="2" disabled>
                                        </div>
                                    </div>
                                    <label>Login</label>
                                    <input type="text" name="login" class="form-control" value="<?php echo $this->pessoa->getLogin(); ?>" disabled>
                                    <label>Data Admissão <span class="label label-info pull-right"><i class="fa fa-home icon-white"></i> <?php echo $this->pessoa->getTempoEmpresa(); ?></span></label>
                                    <input type="text" name="dataAdmissao" class="form-control data" value="<?php echo ($this->pessoa->getDataAdmissao() == "0000-00-00") ? "Não informada" : Util_Utilidade::exibeData($this->pessoa->getDataAdmissao()); ?>" disabled>

                                </div>
                                <div class="col-sm-2">
                                    <legend>Foto</legend>
                                    <div class="thumbnail" style="max-width: 90px; max-height: 120px;"><?php echo $this->pessoa->getImg(); ?></div>    
                                </div>
                                <div class="col-sm-3">
                                    <legend>Alocação</legend>
                                    <label>Tipo</label>
                                    <input type="text" class="form-control" value="<?php echo $this->pessoa->getTipoNome(); ?>" disabled>
                                    <label>Cargo</label>
                                    <input type="text" class="form-control" value="<?php echo $this->pessoa->getCargo()->getNome(); ?>" disabled>
                                    <label>Unidade</label>
                                    <?php echo ($this->pessoa->getUnidadeEspecifica_id()) ? $this->pessoa->getUnidadeEspecifica()->getNomeCompleto() : "-"; ?>
                                </div>
                                <div class="col-sm-3">
                                    <legend>Informações de Contato</legend>
                                    <label>E-mail</label>
                                    <input name="email" type="text" value="<?php echo $this->pessoa->getEmail(); ?>" class="form-control" disabled>
                                    <label>E-mail Pessoal</label>
                                    <input name="emailPessoal" type="text" value="<?php echo $this->pessoa->getEmailPessoal(); ?>" class="form-control" disabled>
                                    <label>Telefone</label>
                                    <input name="telefone" type="text" value="<?php echo Util_Utilidade::exibeTelefone($this->pessoa->getTelefone()); ?>" class="telddd form-control" disabled>
                                    <label>Telefone Pessoal</label>
                                    <input name="telefonePessoal" type="text" value="<?php echo Util_Utilidade::exibeTelefone($this->pessoa->getTelefonePessoal()); ?>" class="telddd form-control" disabled>
                                    <label>Skype</label>
                                    <input name="skype" type="text" value="<?php echo $this->pessoa->getSkype(); ?>" class="form-control" disabled>
                                </div>
                            </div>
                            <hr>
                            <?php if ($this->pessoa->getTipo() < 90) { ?>
                                <div class="row">
                                    <div class="col-sm-10 col-sm-offset-1 center">
                                        <a href="<?php echo Util_Link::link("Adm", "Pessoa", "Editar", $this->pessoa->getId()); ?>" class="btn btn-primary">Editar dados cadastrais</a>
                                        <a href="<?php echo Util_Link::link("Adm", "Pessoa", "Realocar", $this->pessoa->getId()); ?>" class="btn btn-primary">Realocar/Desligar</a>
                                        <a class="btn btn-primary" target="_blank" href="<?php echo Util_Link::link("Adm", "Pessoa", "Cracha", $this->pessoa->getId()); ?>">Imprimir Crachá</a>
                                        <?php if ($GLOBALS['user']->getDepartamento_id() == 76 || $GLOBALS['user']->getDepartamento_id() == 74) { ?>
                                            <a href="<?php echo Util_Link::link("Adm", "Pessoa", "Email", $this->pessoa->getId()); ?>" class="btn btn-primary">E-mail</a>
                                            <a href="<?php echo Util_Link::link("Adm", "Pessoa", "Acesso", $this->pessoa->getId()); ?>" class="btn btn-primary">Alterar login</a>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="tab-pane" id="historico">
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-1">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped table-hover table-condensed">
                                            <tr>
                                                <th>Mov</th>
                                                <th>Data Movimentação</th>
                                                <th>Data Sistema</th>
                                                <th>Cargo</th>
                                                <th>Unidade</th>
                                                <th>Usuário</th>
                                                <th></th>
                                            </tr>
                                            <?php
                                            if ($this->pessoa->getCargosHistoricos() != "") {
                                                foreach ($this->pessoa->getCargosHistoricos() as $mudanca) {
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $mudanca->getLabelAbreviado(true); ?></td>
                                                        <td><?php echo Util_Utilidade::exibeData($mudanca->getData()); ?></td>
                                                        <td><?php echo Util_Utilidade::exibeData($mudanca->getDataCadastro()); ?></td>
                                                        <td><?php echo $mudanca->getCargo()->getNome(); ?></td>
                                                        <td><?php echo $mudanca->getNomeUnidade(); ?></td>
                                                        <td><?php echo ($mudanca->getUsuario()) ? Util_Utilidade::wrapTexto($mudanca->getUsuario()->getNome(), 20) : "-"; ?></td>
                                                        <td></td>
                                                    </tr>   
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <a href="<?php
                                    $urlExtra = "&mes=" . date('m') . "&ano=" . date("Y") . "&colaborador_id=" . $this->pessoa->getId();
                                    echo Util_Link::link("Aloc", "Relatorio", "Colaborador", $urlExtra);
                                    ?>" class="btn btn-default">Ver Alocações</a>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="arquivos">
                            <div class="col-sm-6">            
                                <label>Arquivos</label> 
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover table-condensed table-bordered">
                                        <tr>
                                            <th>Tipo</th>
                                            <th>Nome</th>
                                            <th></th>                                                                                                                                   
                                        </tr>                                                

                                        <?php
                                        if ($this->pessoa->getDocumentos() != "") {
                                            foreach ($this->pessoa->getDocumentos() as $documento) {
                                                $arquivo = $documento->getArquivo();
                                                ?>  
                                                <tr>
                                                    <td>
                                                        <?php echo $documento->getTipo()->getNome(); ?>                                                        
                                                    </td>
                                                    <td>
                                                        <?php echo $arquivo->getRotulo(); ?>                                                        
                                                    </td>
                                                    <td style="width: 30px;">   
                                                        <a class="btn btn-xs btn-default" target="_blank" href="<?php echo Util_Link::link("Adm", "Arquivo", "Exibir", $arquivo->getId()); ?>"><i class="fa fa-arrow-down"></i> Baixar</a>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>      
                                    </table>   
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <form id="myform" action="<?php echo Util_Link::link("Adm", "PessoaDocumento", "InserirDocumento"); ?>" method="post" enctype="multipart/form-data" data-sync="1" >
                                    <label>Tipo do Documento</label>
                                    <select name="tipo_id" class="form-control select2">
                                        <?php echo Util_Form::makeLists($this->tiposDocumento); ?>
                                    </select>
                                    <label>Adicionar Arquivo</label>
                                    <span class="help-block">Formato de arquivo aceito: <?php echo Adm_ArquivoMapper::getInstance()->getLabel('pessoaDoc'); ?> </span>
                                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                        <div class="form-control" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                                        <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Selecione</span><span class="fileinput-exists">Trocar</span><input type="file" name="arquivo_1" data-maxsize="10"></span>

                                    </div>
                                    <input name="pessoa_id" id="id" type="hidden" value="<?php echo $this->pessoa->getId(); ?>" />
                                    <input name="post" id="post" type="hidden" value="1" />
                                    <input id="atualizar" name="atualizar" type="submit" value="Inserir Arquivo" class="btn btn-primary btn-submit" >
                                </form>
                            </div>
                        </div><!-- arquivos -->
                    </div>
                </div>
            </div>
        </div>  
    </div>
</div>