<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2><i class="fa fa-th"></i><span class="break"></span>Grupos de E-Mail</h2>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-12 col-sm-offset-0" style="height: 500px">
                    <iframe src="file/gruposEmail/GruposEmail_v5.pdf" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>