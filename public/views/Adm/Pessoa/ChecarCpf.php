<div class="span12 box">
    <div class="box-header" data-original-title>
        <h2><i class="icon-th"></i><span class="break"></span>Checar CPF's</h2>
    </div>
    <div class="box-content">
        <div class="row-fluid">
            <div class="span12">
                <table class="table table-condensed table-bordered table-striped table-hover">
                    <tr>
                        <th>id</th>
                        <th>nome</th>
                        <th>cargo</th>
                        <th>unidade</th>
                        <th class="span3">cpf</th>
                        <th></th>
                    </tr>
                    <?php foreach ($this->pessoas as $pessoa) {
                        
                    if ($pessoa->getCpf()=="" || !Util_Utilidade::cpfValido($pessoa->getCpf()))  {  
                    ?>
                    <tr>
                        <td><?php echo $pessoa->getId(); ?></td>
                        <td><?php echo $pessoa->getNomeCompleto(); ?></td>
                        <td><?php echo $pessoa->getCargo()->getNome(); ?></td>
                        <td><?php echo $pessoa->getNomeUnidade(); ?></td>
                        <td><?php 
                                if ($pessoa->getCpf()=="") echo "<span class=\"badge badge-warning\">vazio</span>";
                                else if (Util_Utilidade::cpfValido($pessoa->getCpf())) echo "<span class=\"badge badge-info\">ok</span>";
                                else echo "<span class=\"badge badge-important\">erro</span> ".Util_Utilidade::exibeCpf($pessoa->getCpf()); ?></td>
                        <td><a href="<?php echo Util_Link::link("Adm", "Pessoa", "Editar",$pessoa->getId()); ?>" class="btn btn-small"><i class="icon-pencil"></i></a></td>
                    </tr>
                    <?php } } ?>
                </table>
            </div>
        </div>
    </div>
</div>