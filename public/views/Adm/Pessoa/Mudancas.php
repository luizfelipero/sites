<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2><i class="fa fa-th"></i><span class="break"></span>Última Mudanças - Pessoas</h2>
            <div class="box-icon">
                <span class="break"></span>
                <a href="#"><i class="fa fa-filter filtro-botao"></i></a>
            </div>
        </div>
        <div class="panel-body hide filtro-opcoes">
            <form class="" action="" method="GET">
                <div class="row">
                    <?php echo Util_Link::linkHideForm("Adm", "Pessoa", "Mudancas"); ?>
                    <div class="col-sm-4 col-sm-offset-4">
                        <label>Unidade</label>
                        <select name="unidade_id" id="unidade_id" class="select2 form-control select2" >
                            <?php echo Util_Form::makeListUnidades(array("selecione" => "selecione")); ?>
                        </select>
                        <label>Colaborador</label>
                        <select class="pessoa-select2 form-control" name="colaborador_id" id="colaborador_id" data-cargo='1' data-unidade="1" data-extra="i" data-url="<?php echo Util_Link::link("Adm", "Pessoa", "ListarNovoJson"); ?>"></select>
                        <label>Tipo</label>
                        <select name="tipo" id="tipo" class="form-control select2">
                            <option value="">Todos</option>
                            <option value="1">Indireto</option>
                            <option value="2">Direto</option>
                            <option value="3">Direto Infra</option>
                        </select>
                        <label>Pendencia</label>
                        <select name="pendencia" id="pendencia" class="form-control select2">
                            <option value="">Todos</option>
                            <option value="1">Com Pendencia</option>
                            <option value="2">Sem Pendencia</option>
                        </select>
                        <input type="submit" value="filtrar" class="btn btn-primary">
                    </div>
                </div>
            </form>
        </div>
        <div class="panel-body">
            <div id="pessoas" >
                <div class="row">
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-condensed table-hover">
                                <tr>
                                    <th>Tipo</th>
                                    <th>Nome</th>
                                    <th>Data Mudança</th>
                                    <th>Data Sistema</th>
                                    <th>Status</th>
                                    <th>Cargo</th>
                                    <th>Responsável DP</th>
                                </tr>
                                <?php
                                if ($this->mudancas != "") {
                                    foreach ($this->mudancas as $mudanca) {
                                        ?>
                                        <tr>
                                            <td><?php echo $mudanca->getLabelAbreviado(true); ?></td>
                                            <td><?php echo Util_Utilidade::wrapTexto($mudanca->getColaborador()->getNomeCompleto(), 25); ?></td>
                                            <td><?php echo Util_Utilidade::exibeData($mudanca->getData()); ?></td>
                                            <td><?php echo Util_Utilidade::exibeData($mudanca->getDataCadastro()); ?></td>
                                            <td><?php echo $mudanca->getStatusLabel(); ?></td>
                                            <td><?php echo $mudanca->getCargo()->getNome() . " em " . $mudanca->getNomeUnidade(); ?></td>
                                            <td><?php echo ($mudanca->getUsuario_id() != 0) ? Util_Utilidade::wrapTexto($mudanca->getUsuario()->getNome(), 10) : "não informado"; ?></td>
                                        </tr>    
                                        <?php
                                    }
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                </div>
                <?php echo $this->paginacao->exibe(); ?>
            </div>
        </div>
    </div>
</div>