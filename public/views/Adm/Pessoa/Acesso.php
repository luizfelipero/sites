<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2><i class="fa fa-th"></i><span class="break"></span>Acesso ao Sistema</h2>
        </div>
        <div class="panel-body">
            <form action="<?php echo Util_Link::link("Adm", "Pessoa", "Acesso"); ?>" method="post">
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-4">
                        <label>Nome</label>
                        <input type="text" value="<?php echo $this->pessoa->getNomeCompleto(); ?>" class="form-control" disabled>
                        <label>Login</label>
                        <input name="login" id="login" class="form-control" type="text" value="<?php echo $this->pessoa->getLogin(); ?>">
                        <button id="botaoSenha" class="btn btn-default" type="button">Quero especificar a senha</button><br>
                        <div id="camposSenha" class="hide">
                            <label>Senha</label>
                            <input name="senha" type="password" id="senha" class="form-control">
                            <label>Confirmação</label>
                            <input name="csenha" type="password" id="csenha" class="form-control">
                        </div>
                        <input name="id" id="post" type="hidden" value="<?php echo $this->pessoa->getId(); ?>" />
                        <input name="post" id="post" type="hidden" value="1" /><br>
                            <input class="btn btn-primary" id="submitform" name="submitform" type="submit" value="salvar" />
                    </div>
                </div>
            </form>
        </div>  
    </div>
</div>
<script>

    $("#login").mask("aaaa?aaaaaaaaaaaaaaaa", {placeholder: ""});

    $(function() {
        $("#botaoSenha").click(function() {
            $("#botaoSenha").addClass('hide');
            $("#botaoSenha").removeClass('show');

            $("#camposSenha").addClass('show');
            $("#camposSenha").removeClass('hide');
        })
    });

</script>