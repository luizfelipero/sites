<html>        
    <head>
        <title>Gerador Crachá</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width">
        <style>
            .conteudo-cracha {
                display: block;
                clear: left;
            }
            .crachap1, .crachap2 {
                width: 326px; /*1cm = 36px*/
                padding: 8px;
                height: 229px; /*1cm = 36px*/
                float: left;
                border: 1px dashed #000;
            }
            .crachap1 {
                background-color: #fff;
                border-right: 1px dotted #000;
            }
            .crachap2 {
                background-color: #fff;
                border-left: 1px dotted #000;

            }
            .logo {
                width: 110px; 
                float: left;
            }
            .dados {
                clear: left;
                font-family: verdana;
                padding-top: 5px;
            }
            .crachap2 .dados {
                margin-top: 94px;
            }
            .dados span {
                display: block;
                padding: 2px;
            }
            .dados .rotulo {
                color: #972A27;
                font-size: 12px;
                padding-left: 10px;
            }
            .dados .info {
                color: #000;
                font-size: 15px;
                border: 1px solid #972a27;
                border-radius: 6px;
            }
        </style>
    </head>
    <body>
        <?php if ($this->colaborador != "") { ?>
            <div class="conteudo-cracha">
                <div class="crachap1">
                    <div class="logo">
                        <img src="img_p/logovert.jpg" width="110" height="94">
                    </div>
                    <div class="dados">
                        <span class="rotulo">Nome</span>
                        <span class="info"><?php echo $this->colaborador->getNome(); ?></span>
                        <span class="rotulo">Unidade</span>
                        <span class="info"><?php echo $this->colaborador->getUnidadeEspecifica()->getNome(); ?></span>
                        <span class="rotulo">Função</span>
                        <span class="info"><?php echo $this->colaborador->getCargo()->getNome(); ?></span>
                    </div>                
                </div>
                <div class="crachap2">
                    <div class="dados">
                        <div style="text-align: center">
                            <?php
                            $generator = new Vendor_Barcode_BarcodeGeneratorPNG();
                            echo '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($this->colaborador->getChapa() . "1", $generator::TYPE_CODE_128)) . '" style="height: 35px;">';
                            ?>
                        </div> 
                        <br>
                        <br>
                        <span class="rotulo">Chapa</span>
                        <span class="info"><?php echo $this->colaborador->getChapa(); ?></span>
                    </div> 
                </div>            
            </div>
        <?php } ?>
    </body>

</html>