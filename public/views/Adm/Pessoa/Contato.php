<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2><i class="fa fa-th"></i><span class="break"></span>Contatos</h2>
            <div class="box-icon">
                <?php //if ($GLOBALS['user']->getDepartamento_id() == 55 || $GLOBALS['user']->getDepartamento_id() == 76) { ?>
                <?php if (1 == 0) { ?>
                    <span class="break"></span>
                    <a href="<?php echo Util_Link::link("Adm", "Pessoa", "Minutagem"); ?>"><i class="fa fa-phone"></i></a>
                <?php } ?>
                <span class="break"></span>
                <a href="<?php echo Util_Link::link("Adm", "Pessoa", "Csv", "", "&tipo=t"); ?>" target="_blank" class="tooltips" data-original-title="Baixar CSV Telefones"><i class="fa fa-download"></i> <span class="label label-default">Tel</span></a>
                <span class="break"></span>
                <a href="<?php echo Util_Link::link("Adm", "Pessoa", "Csv", "", "&tipo=e"); ?>" target="_blank" class="tooltips" data-original-title="Baixar CSV E-mail"><i class="fa fa-download"></i> <span class="label label-default">E-mail</span></a>
                <span class="break"></span>
                <a href="#"><i class="fa fa-filter filtro-botao"></i></a>
            </div>
        </div>
        <div class="panel-body hide filtro-opcoes">
            <form class="" action="" method="GET">
                <div class="row">
                    <?php echo Util_Link::linkHideForm("Adm", "Pessoa", "Contato"); ?>
                    <div class="col-sm-3">
                        <?php echo Util_Form::makeSelectUnidadesInserir(1); ?>
                    </div>
                    <div class="col-sm-3">
                        <label>Cargo</label>
                        <select name="cargo_id" id="cargo_id" class="form-control select2">
                            <?php echo Util_Form::makeLists($this->cargos, array("seletores" => array("none"))); ?>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <label>Nome (pode utilizar %)</label>
                        <input type="text" name="nome" id="nome" class="form-control">
                        <input type="hidden" name="pass" value="<?php echo date("His"); ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2 offset5">
                        <input id="submitform" class="btn btn-primary btn-submit" name="submitform" type="submit" value="filtrar" />
                        <input type="hidden" name="exibicao" id="exibicao" value="<?php echo $this->exibicao; ?>">
                    </div>
                </div>
            </form>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-4 col-sm-offset-4">
                    <div class="btn-group" role="group">
                        <button type="button" id="lista-btn" class="btn <?php echo ($this->exibicao == "l") ? "btn-primary" : "btn-default"; ?>">Lista</button>
                        <button type="button" id="cartoes-btn" class="btn <?php echo ($this->exibicao == "c") ? "btn-primary" : "btn-default"; ?>">Cartões de Visita</button>
                    </div>
                </div>
            </div>
            <div id="lista" class="<?php if ($this->exibicao == "c") echo "hide"; ?>">
                <div class="row">
                    <div class="col-sm-12 ">
                        <?php echo $this->paginacaoListaLetras->exibe(); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 ">
                        <span>Sua pesquisa retornou <strong><?php echo $this->total; ?></strong> pessoa(s).</span>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-condensed table-hover">
                                <tr>
                                    <th>Nome</th>
                                    <th>Cargo</th>
                                    <th>Unidade</th>
                                    <th style="width: 160px;">Telefones</th>
                                    <th>E-mails</th>
                                    <th>Skype</th>
                                </tr>
                                <?php
                                if ($this->pessoas != "") {
                                    foreach ($this->pessoas as $pessoa) {
                                        ?>
                                        <tr>
                                            <td><?php echo $pessoa->getNomeCompleto(); ?></td>
                                            <td><?php echo ($pessoa->getCargo()) ? $pessoa->getCargo()->getNome() : "-"; ?></td>
                                            <td><?php echo ($pessoa->getUnidadeEspecifica_id()) ? $pessoa->getNomeUnidade() : "-"; ?></td>
                                            <td><?php echo $pessoa->getTelefones(); ?></td>
                                            <td><?php echo $pessoa->getEmail(); ?></td>
                                            <td><?php echo $pessoa->getSkype(); ?></td>
                                        </tr>    
                                        <?php
                                    }
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 ">
                        <?php echo $this->paginacaoLista->exibe(); ?>
                    </div>
                </div>

            </div>
            <div id="cartao" class="<?php if ($this->exibicao == "l") echo "hide"; ?>">
                <div class="row">
                    <div class="col-sm-12 ">
                        <?php echo $this->paginacaoCartaoLetras->exibe(); ?>
                    </div>
                </div>
                <div class="row">
                    <?php
                    $i = 0;
                    if ($this->pessoas != "") {
                        foreach ($this->pessoas as $pessoa) {
                            ?><div class="col-sm-4" style="margin-bottom: 15px" >
                                <div style="height: 130px">
                                    <div class="row">
                                        <div class="col-sm-4 thumbnail" >                                        
                                            <?php echo $pessoa->getImg(); ?>
                                        </div>
                                        <div class="col-sm-8" style="font-size: 12px; word-wrap: break-word">
                                            <h5><?php echo $pessoa->getNome(); ?></h5>
                                            <?php echo $pessoa->getNomeUnidade(); ?>
                                            <?php
                                            if ($pessoa->getTelefones() != "") {
                                                echo "<br>" . $pessoa->getTelefones();
                                            }
                                            ?>
                                            <br><?php echo $pessoa->getEmail(); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php if ($i % 3 == 2) { ?>
                            </div>
                            <div class="row">
                                <?php
                            }
                            $i++;
                        }
                    }
                    ?>
                </div>
                <div class="row">
                    <div class="col-sm-12 ">
                        <?php echo $this->paginacaoCartao->exibe(); ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script>

    $(function () {

        $("#cartoes-btn").click(function () {
            $("#cartao").removeClass('hide');
            $("#lista").addClass('hide');
            $("#exibicao").val("c");
            $("#cartoes-btn").addClass("btn-primary");
            $("#cartoes-btn").removeClass("btn-default");
            $("#lista-btn").removeClass("btn-primary");
            $("#lista-btn").addClass("btn-default");
        });
        $("#lista-btn").click(function () {
            $("#cartao").addClass('hide');
            $("#lista").removeClass('hide');
            $("#exibicao").val("l");
            $("#lista-btn").addClass("btn-primary");
            $("#lista-btn").removeClass("btn-default");
            $("#cartoes-btn").removeClass("btn-primary");
            $("#cartoes-btn").addClass("btn-default");
        });


    });

</script>