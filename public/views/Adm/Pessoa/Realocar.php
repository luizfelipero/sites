<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2><i class="fa fa-th"></i><span class="break"></span>Realocar Pessoa</h2>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-3">
                    <legend>Colaborador</legend>
                    <input type="text" disabled class="form-control" value="<?php echo $this->pessoa->getNome(); ?>">
                </div>
                <?php if   ($GLOBALS['user']->getDepartamento_id() == 75 || 
                            $GLOBALS['user']->getDepartamento_id() == 76 ||
                            (($GLOBALS['user']->getUgb_id() == 7 ||
                            $GLOBALS['user']->getUgb_id() == 4) && ($this->pessoa->getUgb_id() == 7 || $this->pessoa->getUgb_id() == 4))
                        ) { ?>
                    <div class="col-sm-6">
                        <legend>Realocação</legend>
                        <div class="row">
                            <form action="<?php echo Util_Link::link("Adm", "Pessoa", "Realocar"); ?>" method="post">
                                <div class="col-sm-6">
                                    <label>Tipo</label>
                                    <select name="tipo" id="tipo" class="form-control select2">
                                        <option value="1">Indireto</option>
                                        <option value="2">Direto</option>
                                        <option value="3">Direto Infra</option>
                                    </select>
                                    <label>Data Mudança de Cargo <span class="label label-danger">Atenção aos limites</span></label>
                                    <input id="dataR" name="data" class="data form-control" value="<?php
                                    if ($this->pessoa->getUltimoCargoHistorico() != "") {
                                        //echo Util_Utilidade::exibeData($this->pessoa->getUltimoCargoHistorico()->getData());
                                    }
                                    ?>" type="text">
                                    <label>Tipo</label>
                                    <select name="operacao" class="form-control select2">
                                        <option value="">--Selecione--</option>
                                        <option value="1">Admissão</option>
                                        <option value="2">Efetivação</option>
                                        <option value="3">Promoção</option>
                                        <option value="4">Transferência</option>
                                    </select>
                                    <input name="id" id="post" type="hidden" value="<?php echo $this->pessoa->getId(); ?>" />
                                    <input name="post" id="post" type="hidden" value="1" />
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="aviso" value="1" checked> Avisar na Home
                                        </label>
                                    </div>
                                    <input type="hidden" name="sig" value="1">
                                    <?php if ($GLOBALS['user']->getDepartamento_id() == 76) { ?>                                    
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="oculto" value="1"> Oculta
                                            </label>
                                        </div>
                                    <?php } ?>
                                    <input class="btn btn-primary" id="submitform" name="submitform" type="submit" value="salvar realocação" />
                                </div>
                                <div class="col-sm-6">
                                    <div id="tipo1" class="realoc hide">
                                        <label>Cargo</label>
                                        <select name="cargo_id_1" class="form-control select2">
                                            <?php
                                            $default = $this->pessoa->getCargo_id();
                                            echo Util_Form::makeLists($this->indiretos['cargos'], array("seletores" => array("selecione"), "default" => $default));
                                            ?>
                                        </select>
                                        <?php echo Util_Form::makeSelectUnidadesInserir(2); ?>
                                    </div>
                                    <div id="tipo2" class="realoc hide">
                                        <label>Cargo</label>
                                        <select name="cargo_id_2" class="form-control select2">
                                            <?php echo Util_Form::makeLists($this->diretos['cargos'], array("seletores" => array("selecione"), "default" => $default)); ?>
                                        </select>
                                        <label>Unidade</label>
                                        <select name="unidade_id_2" id="unidade_id_2" class="form-control select2">
                                            <?php echo Util_Form::makeLists($this->diretos['unidades'], array("seletores" => array("selecione"))); ?>
                                        </select>
                                    </div>
                                    <div id="tipo3" class="realoc hide">
                                        <label>Cargo</label>
                                        <select name="cargo_id_3" class="form-control select2">
                                            <?php echo Util_Form::makeLists($this->diretosInfra['cargos'], array("seletores" => array("selecione"), "default" => $default)); ?>
                                        </select>
                                        <label>Unidade</label>
                                        <select name="unidade_id_3" id="unidade_id_3" class="form-control select2">
                                            <?php echo Util_Form::makeLists($this->diretosInfra['unidades'], array("seletores" => array("selecione"))); ?>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                <?php } ?>
                <div class="col-sm-3">
                    <form action="<?php echo Util_Link::link("Adm", "Pessoa", "Realocar"); ?>" method="post">
                        <legend>Desligamento</legend>
                        <label>Data <button id="btn-help-desligamento" type="button" class="btn btn-danger btn-xs" data-container="body" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Serão permitidas alocações até o momento da demissão"><i class="fa fa-warning"></i></button></label>
                        <input id="dataD" name="data" class="datafp form-control" type="text">
                        <label>Momento da Demissão</label>
                        <select name="turno" class="form-control select2">
                            <option value="0">Logo Cedo</option>
                            <option value="1">Ao meio-dia</option>
                            <option value="2">Final da tarde</option>
                        </select>
                        <label>Classificação</label>
                        <select name="motivoDemissao" class="form-control select2">
                            <option value="0">--Selecione--</option>
                            <option value="1">Pedido</option>
                            <option value="2">Término Contrato</option>
                            <option value="3">Empresa</option>
                            <option value="4">Justa Causa</option>
                        </select>
                        <input name="id" id="post" type="hidden" value="<?php echo $this->pessoa->getId(); ?>" />
                        <input name="post" id="post" type="hidden" value="1" />
                        <input name="tipo" type="hidden" value="<?php echo $this->pessoa->getTipo(); ?>">
                        <input name="cargo_id" type="hidden" value="<?php echo $this->pessoa->getCargo_id(); ?>">
                        <input name="filial_id" type="hidden" value="<?php echo $this->pessoa->getFilial_id(); ?>">
                        <input name="centro_id" type="hidden" value="<?php echo $this->pessoa->getCentro_id(); ?>">
                        <input name="diretoria_id" type="hidden" value="<?php echo $this->pessoa->getDiretoria_id(); ?>">
                        <input name="ugb_id" type="hidden" value="<?php echo $this->pessoa->getUgb_id(); ?>">
                        <input name="departamento_id" type="hidden" value="<?php echo $this->pessoa->getDepartamento_id(); ?>">
                        <input name="setor_id" type="hidden" value="<?php echo $this->pessoa->getSetor_id(); ?>">

                        <input type="hidden" name="operacao" value="5">
                        <input type="hidden" name="sig" value="1">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="aviso" value="1" checked> Avisar na Home
                            </label>
                        </div>
                        <input class="btn btn-primary" id="submitform" name="submitform" type="submit" value="salvar desligamento" />
                    </form>
                </div>
            </div>
        </div>  
    </div>
</div>

<script>

    $(function () {

        $("#tipo").val(<?php echo $this->pessoa->getTipo(); ?>);

        var id = "#tipo<?php echo $this->pessoa->getTipo(); ?>";
        $(id).removeClass('hide');
        
        $("#btn-help-desligamento").popover();
    });

    $("#tipo").change(function () {

        $(".realoc").addClass('hide');
        var id = "#tipo" + $(this).val();
        $(id).removeClass('hide');

    });

</script>