<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2><i class="fa fa-th"></i><span class="break"></span>Alterar e Informar E-mail</h2>
        </div>
        <div class="panel-body">
            <form action="<?php echo Util_Link::link("Adm", "Pessoa", "Email"); ?>" method="post">
                <div class="row-fluid">
                    <div class="col-sm-4 col-sm-offset-4">
                        <label>Nome</label>
                        <input type="text" class="form-control" value="<?php echo $this->pessoa->getNomeCompleto(); ?>" disabled>
                        <label>Login</label>
                        <input type="text" class="form-control" value="<?php echo $this->pessoa->getLogin(); ?>" disabled>
                        <label>E-mail Pessoal</label>
                        <input type="text" class="form-control" name="emailPessoal" value="<?php echo $this->pessoa->getEmailPessoal(); ?>">
                        <label>E-mail</label>
                        <input type="text" class="form-control" name="email" value="<?php echo ($this->pessoa->getEmail() != "") ? $this->pessoa->getEmail() : "@vianaemoura.com.br"; ?>">
                        <label>Senha</label>
                        <input name="senha" class="form-control" type="text" id="senha" value="<?php echo Adm_PessoaMapper::getInstance()->geraSenha(); ?>">
                        <input name="id" id="post" type="hidden" value="<?php echo $this->pessoa->getId(); ?>" />
                        <br>
                        <input name="post" id="post" type="hidden" value="1" />
                        <input class="btn btn-primary" id="submitform2" name="submitform2" type="submit" value="salvar" />
                    </div>
                </div>
            </form>
        </div>  
    </div>
</div>