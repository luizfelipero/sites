<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2><i class="fa fa-th"></i><span class="break"></span>Contatos</h2>
            <div class="box-icon">

            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
<!--                    <p>
                        <label class="label label-danger">falta</label>
                        Informe a SIG o telefone que está faltando.
                    </p>-->
                    <table class="table table-striped table-bordered table-condensed table-hover">
                        <tr>
                            <th>Unidade</th>
                            <th>Telefone</th>
                        </tr>
                        <tr>
                            <td>Escritório Recife</td>
                            <td>(81) 3325-1131</td>
                        </tr>
                        <tr>
                            <td>Imobiliária Belo Jardim</td>
                            <td>(81) 3726-3413</td>
                        </tr>
                        <tr>
                            <td>Imobiliária Santa Cruz</td>
                            <td>(81) 3731-4878</td>
                        </tr>
                        <tr>
                            <td>Imobiliária Caruaru</td>
                            <td>(81) 3721-1569</td>
                        </tr>
                        <tr>
                            <td>Imobiliária Garanhuns</td>
                            <td>(87) 3763-6657</td>
                        </tr>
                    </table>
                    
                    <table class="table table-striped table-bordered table-condensed table-hover">
                        <tr>
                            <th>Projetos - Escritório Central</th>
                            <th>Telefone</th>
                        </tr>
                        <tr>
                            <td>Infra</td>
                            <td>(81) 9239-4768</td>
                        </tr>
                        <tr>
                            <td>Urbanização</td>
                            <td>(81) 9292-6713</td>
                        </tr>
                        <tr>
                            <td>Legalização</td>
                            <td>(81) 9292-7776</td>
                        </tr>
                        <tr>
                            <td>Padronização</td>
                            <td>(81) 8908-6577</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>