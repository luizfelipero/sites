<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2><i class="fa fa-th"></i><span class="break"></span>Última Mudanças - Pendências Pessoas</h2>
        </div>
        <div class="panel-body">
            <div id="pessoas" >
                <div class="row">
                    <div class="col-sm-12 col-sm-offset-0">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-condensed table-hover">
                                <tr>
                                    <th>Tipo</th>
                                    <th>Nome</th>
                                    <th>Data</th>
                                    <th>Passou a ser</th>
                                    <th>E-mail Pessoal</th>
                                    <th>Falta</th>
                                    <th class="col-sm-2">Ações</th>
                                </tr>
                                <?php
                                if ($this->mudancas != "") {
                                    foreach ($this->mudancas as $mudanca) {
                                        ?>
                                        <tr>
                                            <td><?php echo $mudanca->getLabelAbreviado(); ?></td>
                                            <td><?php echo Util_Utilidade::wrapTexto($mudanca->getColaborador()->getNome(), 25); ?></td>
                                            <td><?php echo Util_Utilidade::exibeData($mudanca->getData()); ?></td>
                                            <td><?php echo $mudanca->getCargo()->getNome() . " em " . $mudanca->getNomeUnidade(); ?></td>
                                            <td><?php if ($mudanca->getOperacao() == 5 && $mudanca->getColaborador()->getEmail() != "") { ?>
                                                    <span class="label label-warning">Del</span>
                                                    <?php
                                                    echo $mudanca->getColaborador()->getEmail();
                                                } else {
                                                    echo $mudanca->getColaborador()->getEmailPessoal();
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?php if ($mudanca->getColaborador()->getLogin() == "") { ?>
                                                    <span class="label label-danger">Login</span>
                                                <?php } if ($mudanca->getColaborador()->getEmail() == "") { ?>
                                                    <span class="label label-danger">E-mail</span>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <div class="btn-group">
                                                    <a class="btn btn-sm btn-default" href="<?php echo Util_Link::link("Adm", "Pessoa", "Visualizar", $mudanca->getColaborador_Id()); ?>"><i class="fa fa-search"></i></a>
                                                    <a class="btn btn-sm btn-default" href="<?php echo Util_Link::link("Adm", "Pessoa", "Email", $mudanca->getColaborador_Id()); ?>"><i class="fa fa-envelope"></i></a>
                                                    <a class="btn btn-sm btn-default" href="<?php echo Util_Link::link("Adm", "Pessoa", "Acesso", $mudanca->getColaborador_Id()); ?>"><i class="fa fa-user"></i></a>
                                                    <a class="btn btn-sm btn-default" href="<?php echo Util_Link::link("Adm", "Pessoa", "CheckPendencia", $mudanca->getId()); ?>"><i class="fa fa-check"></i></a>
                                                </div>
                                            </td>
                                        </tr>    
                                        <?php
                                    }
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>