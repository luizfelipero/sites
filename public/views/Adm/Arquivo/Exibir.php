<?php

$extensao = $this->arquivo->getExtensao();


if ($extensao == ".pdf") {
    header('Content-type: application/pdf');
} else if ($extensao == ".xls") {
    header('Content-type: application/vnd.ms-excel');
    header('Content-type: application/force-download');
    header('Content-Disposition: attachment; filename="' . $this->arquivo->getRotulo() . '"');
} else if ($extensao == ".xlsx") {
    header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-type: application/force-download');
    header('Content-Disposition: attachment; filename="' . $this->arquivo->getRotulo() . '"');
} else if ($extensao == ".xlsm") {
    header('Content-type: application/vnd.ms-excel.sheet.macroEnabled.12');
    header('Content-type: application/force-download');
    header('Content-Disposition: attachment; filename="' . $this->arquivo->getRotulo() . '"');
} else if ($extensao == ".doc") {
    header('Content-type: application/msword');
    header('Content-type: application/force-download');
    header('Content-Disposition: attachment; filename="' . $this->arquivo->getRotulo() . '"');
} else if ($extensao == ".docx") {
    header('Content-type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
    header('Content-type: application/force-download');
    header('Content-Disposition: attachment; filename="' . $this->arquivo->getRotulo() . '"');
} else if ($extensao == ".png") {
    header('Content-type: image/png');
} else if ($extensao == ".jpg" || $extensao == ".jpeg") {
    header('Content-type: image/jpeg');
}

echo file_get_contents($this->arquivo->getUrl());
//readfile($this->arquivo->getUrl());


?>