<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2><i class="fa fa-th"></i><span class="break"></span>Permissões</h2>
            <div class="box-icon">
                <span class="break"></span>
                <a href="#"><i class="fa fa-filter filtro-botao"></i></a>
            </div>
        </div>
        <div class="panel-body filtro-opcoes">
            <div class="row">

            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-12 ">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-condensed table-freeze">
                            <thead>
                                <tr>
                                    <th class="cell-150">id</th>
                                    <th class="cell-150">Module</th>
                                    <th class="cell-150">Controller</th>
                                    <th class="cell-150">Action</th>
                                    <th class="cell-150">Unidade</th>
                                    <th class="cell-150">Cargo</th>
                                    <th class="cell-100">Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($this->permissoes != "") {
                                    foreach ($this->permissoes as $permissao) {
                                        ?>
                                        <tr <?php echo ($permissao->getTipo() == 1) ? "class=\"danger\"" : "class=\"success\""; ?>>
                                            <td class="cell-150"><?php echo $permissao->getId(); ?></td>
                                            <td class="cell-150"><?php echo ($permissao->getModule() == "_") ? "todos" : $permissao->getModule(); ?></td>
                                            <td class="cell-150"><?php echo ($permissao->getController() == "_") ? "todos" : $permissao->getController(); ?></td>
                                            <td class="cell-150"><?php echo ($permissao->getAction() == "_") ? "todos" : $permissao->getAction(); ?></td>
                                            <td class="cell-150"><?php echo ($permissao->getUnidade_id() != 0) ? $permissao->getUnidade()->getNomeCompleto() : "todos"; ?></td>
                                            <td class="cell-150"><?php echo ($permissao->getCargo_id() != 0) ? $permissao->getCargo()->getNome() : "todos"; ?></td>
                                            <td class="cell-100">
                                                <div class="btn-group">
                                                    <a href="<?php echo Util_Link::link("Adm", "Permissao", "Remover", $permissao->getId()); ?>" class="btn btn-default btn-sm"><i class="fa fa-remove"></i></a>
                                                    <a href="<?php ?>" class="btn btn-default btn-sm"><i class="fa fa-repeat"></i></a>
                                                </div>
                                            </td>
                                        </tr>    
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-12">
                    <form action="<?php echo Util_Link::link("Adm", "Permissao", "Inserir"); ?>" method="post">
                        <h4>Inserir Novas Permissões</h4>
                        <table class="table table-striped table-condensed">
                            <tr>
                                <th>Module</th>
                                <th>Controller</th>
                                <th>Action</th>
                                <th>Unidade</th>
                                <th>Cargo</th>
                                <th>Tipo</th>
                            </tr>
                            <?php for ($i = 0; $i < 10; $i++) { ?>
                                <tr>
                                    <td>
                                        <input type="text" name="module_<?php echo $i; ?>" id="module_<?php echo $i; ?>" class="form-control">
                                    </td>
                                    <td>
                                        <input type="text" name="controller_<?php echo $i; ?>" id="controller_<?php echo $i; ?>" class="form-control">
                                    </td>
                                    <td>
                                        <input type="text" name="action_<?php echo $i; ?>" id="action_<?php echo $i; ?>" class="form-control">
                                    </td>
                                    <td>
                                        <?php echo Util_Form::makeUnidadePicker("unidade_" . $i); ?>
                                    </td>
                                    <td>
                                        <select name="cargo_<?php echo $i; ?>" id="cargo_<?php echo $i; ?>" class="form-control select2">
                                            <?php echo Util_Form::makeLists($this->cargos, array("seletores" => array("selecione", "t"))); ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="tipo_<?php echo $i; ?>" id="tipo_<?php echo $i; ?>" class="form-control select2">
                                            <option value="0">Autorizado</option>
                                            <option value="1">Proibido</option>
                                        </select>
                                    </td>
                                </tr>    
                            <?php } ?>
                        </table>
                        <input type="submit" value="salvar" class="btn btn-primary">
                    </form>
                </div>
            </div>
        </div>    
    </div>    
</div>