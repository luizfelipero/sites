<?php

function __autoload($class_name) {
//echo "<br>".$class_name;
    if ($class_name != "") {

        if (strpos($class_name, "_") !== false) {

            $class_name = explode("_", $class_name);

            $module = $class_name[0];
            $class = (isset($class_name[2])) ? $class_name[2] : $class_name[1];

            if ($module == "Util")
                $caminho = 'app/util/';
            else if (strpos($class, "Controller") !== false)
                $caminho = ('app/module/' . $module . '/controller/');
            else if (strpos($class, "Mapper") !== false)
                $caminho = ('app/module/' . $module . '/model/mapper/');
            else if (strpos($class, "Repository") !== false)
                $caminho = ('app/module/' . $module . '/model/repository/');
            else
                $caminho = ('app/module/' . $module . '/model/');

            $caminho .= $class . '.php';

            if ($module == "Vendor") {
                $caminho = ('app/vendor/' . $class_name[1] . '/' . $class . '.php');
            }
        } else {

            $class = $class_name;
            if (strpos($class, "View") !== false)
                $caminho = ('app/view/View.php');
            else if (strpos($class, "Abstract") !== false)
                $caminho = ('app/core/' . $class . ".php");
        }
//echo " > ".$caminho." ! ";
        if ($caminho != "") {
            require_once($caminho);
        }
    }
}

class FrontController {

    public $controller;

    public function __construct() {

        $tempoInicial = time();
        $post['dataAcesso'] = date("Y-m-d H:i:s");

        //recebe o controller e o action
        $d = (!isset($_GET['d'])) ? "Index" : $_GET['d'];
        $m = (!isset($_GET['m'])) ? "Index" : $_GET['m'];
        $c = (!isset($_GET['c'])) ? "Index" : $_GET['c'];
        $a = (!isset($_GET['a'])) ? "Index" : $_GET['a'];

        $GLOBALS['ajax'] = (isset($_REQUEST['ajax'])) ? $_REQUEST['ajax'] : 0;
        session_start();

        //conecta com o DB
        $GLOBALS['server']['nome'] = "local";

        if ($GLOBALS['server']['nome'] == "local") {
            $GLOBALS['connection'] = mysqli_connect("localhost", "root", "", "db_sites");
            $GLOBALS['server']['label'] = "label-success";
        } else if ($GLOBALS['server']['nome'] == "aws") {
            $GLOBALS['connection'] = mysqli_connect("meubanco.cpledtfecfkm.sa-east-1.rds.amazonaws.com", "admin", "AwsSig16", "db_extranet");
            $GLOBALS['server']['label'] = "label-info";
        }

        if (!$GLOBALS['connection']) {
            header("Location: telas/Erro.html");
            exit();
        } else {
            mysqli_set_charset($GLOBALS['connection'], 'utf8');
            mysqli_query($GLOBALS['connection'], "SET NAMES UTF8");
        }


        if (isset($_SESSION['user_id'])) {
            $GLOBALS['user'] = Adm_PessoaMapper::getInstance()->find(array(array("id", "=", $_SESSION['user_id'])));
            $GLOBALS['domain'] = $GLOBALS['user']->getDominio()->getNome();
            $d = $GLOBALS['domain'];
            if ($GLOBALS['user']->getDominio()->getNome() == "smanhotto") {
                header("Location: ../../smanhottoNovo/");
            }
        }
        //testa validacao
        $novo = $this->validacao($d, $m, $c, $a);
        $nomeDomain = $novo['d'] . "Domain";
        $nomeController = $novo['m'] . "_" . $novo['c'] . 'Controller';
        $nomeAction = $novo['a'] . 'Action';

        if (isset($_SESSION['user_id'])) {
            //testa se tem permissao 
            if (!Adm_PermissaoMapper::getInstance()->autorizado($novo['d'], $novo['m'], $novo['c'], $novo['a'])) {
                header("Location: ?m=Index");
                exit();
            }
        }

        //dados da view
        $view = new View($novo['d'], $novo['m'], $novo['c'], $novo['a']);
        $post['domain'] = $novo['d'];
        $post['module'] = $novo['m'];
        $post['controller'] = $novo['c'];
        $post['action'] = $novo['a'];
        $post['get'] = $_GET;
        $GLOBALS['GET'] = $_GET;

        try {
            //inicia controller
            $this->controller = new $nomeController($view);
            $this->controller->$nomeAction();
            $this->controller->view->render();

            $tempoFinal = time();
            $post['tempo'] = $tempoFinal - $tempoInicial;

            Adm_LogMapper::getInstance()->insert($post);
            mysqli_close();
        } catch (Exception $e) {
            mysqli_close();
            throw $e;
        }
    }

    public function validacao($d, $m, $c, $a) {
        $manutencao = 0;

        if ($manutencao == 1 && (!isset($_GET['hack']))) {
            header("Location: telas/Manutencao.html");
            exit();
        } else {

            //primeiro caso: está acessando alguma pagina livre
            if (($d == "Index" && $m == "Index" && $c == "Index" && $a != "Home") || $m == "Serv") {
                $novo['d'] = $d;
                $novo['m'] = $m;
                $novo['c'] = $c;
                $novo['a'] = $a;
            }
            //está acessando alguma bloqueada
            else {

                //agora ve se ele está logado
                if ($GLOBALS['user'] instanceof Adm_Pessoa) {

                    //testa se a sessao ainda é valida
                    if ($this->sessaoValida()) {
                        //testa se há permissao para o ação em questão
                        if (Adm_PermissaoMapper::getInstance()->autorizado($d, $m, $c, $a)) {
                            $novo = array("d" => $d, "m" => $m, "c" => $c, "a" => $a);
//                            $novo = array("d" => $d);
                        }
                        //bloqueia
                        else {
                            Util_FlashMessage::write("Acesso Negado");
                            $novo = array("d" => $d, "m" => "Index", "c" => "Index", "a" => "Home");
//                            $novo = array("d" => $d);
                        }
                    } else {
                        if ($GLOBALS['ajax'] == 1) {
                            echo "logoff";
                            die();
                        } else {
                            Util_FlashMessage::write("Sessão expirada");
                            $novo = array("d" => $d);
                        }
                    }
                }

                //se não estiver logado redireciona pra index
                else {

                    if ($GLOBALS['ajax'] == 1) {
                        echo "logoff";
                        die();
                    } else {
                        Util_FlashMessage::write("É necessário fazer login");
                        $novo = array("d" => $d, "m" => "Index", "c" => "Index", "a" => "Index");
                    }
                }
            }
        }

        return $novo;
    }

    public function sessaoValida() {

        if (isset($_SESSION['LAST_ACTIVITY'])) {
            if ((time() - $_SESSION['LAST_ACTIVITY']) > 18000) { //60*60*5 - 5 horas
                session_unset();
                $retorno = false;
            } else {
                $_SESSION['LAST_ACTIVITY'] = time();
                $retorno = true;
            }
        } else {
            $retorno = false;
        }

        return $retorno;
    }

}

?>