<?php

abstract class AbstractRepository {

    protected $nomeTb;
    protected $nomeView;
    protected $nomeBackup;
    protected $usualOrder;
    protected $columns;
    protected $forcaTable = 0;

    public function getNomeTb($view = 0) {
        if ($view == 1 && $this->nomeView != "" && $this->forcaTable == 0) {
            return $this->nomeView;
        } else {
            $this->setForcaTable(0);
            return $this->nomeTb;
        }
    }

    public function setForcaTable($val) {
        $this->forcaTable = $val;
    }

    public function getNomeTbBackup() {
        return $this->nomeBackup;
    }

    public function selectColumnDirect($col, $args, $extra = "", $indice = "") {

        $valores = $this->selectColumn($col, $args, $extra, $indice);
        if ($valores[0] != "") {
            return $valores[0];
        }
    }

    public function selectColumn($col, $args, $extra = "", $indice = "") {

        if ($indice != "") {
            $cols = $col . ", " . $indice;
        } else {
            $cols = $col;
        }
        $sql = "select " . $cols . " from " . $this->getNomeTb(1);
        $sql .= $this->montaWheres($args, $extra);
        if (isset($this->print) && $this->print == "selectColumn") {
            echo $sql . "<br>";
        }
        $query = mysqli_query($GLOBALS['connection'], $sql);
        $total = ($query) ? mysqli_num_rows($query) : 0;

        for ($i = 0; $i < $total; $i++) {

            if ($indice != "") {
                $resultRow = mysqli_fetch_assoc($query);
                $result[$resultRow[$indice]] = $resultRow[$col];
            } else {
                $resultRow = mysqli_fetch_assoc($query);
                $result[] = $resultRow[$col];
            }
        }
        return $result;
    }

    public function selectColumnLiteral($literal) {
        $sql = $literal;

        if (isset($this->print) && $this->print == "fetchAllLiteral") {
            echo $sql . "\n";
        }

        $query = mysqli_query($GLOBALS['connection'], $sql);
        $total = ($query) ? mysqli_num_rows($query) : 0;

        for ($i = 0; $i < $total; $i++) {

            if ($indice != "") {
                $resultRow = mysqli_fetch_assoc($query);
                $result[$resultRow[$indice]] = $resultRow[$col];
            } else {
                $resultRow = mysqli_fetch_assoc($query);
                $result[] = $resultRow[$col];
            }
        }

        return $result;
    }

    public function selectColumns($cols, $args, $extra = "") {

        $sql = "select " . $cols . " from " . $this->getNomeTb(1);
        $sql .= $this->montaWheres($args, $extra);
        if (isset($this->print) && $this->print == "selectColumns") {
            echo $sql . "\n";
        }
        $query = mysqli_query($GLOBALS['connection'], $sql);
        $total = ($query) ? mysqli_num_rows($query) : 0;
        for ($i = 0; $i < $total; $i++) {
            $result[] = mysqli_fetch_assoc($query);
        }
        return $result;
    }

    public function fetchAll($args, $extra, $view = 1) {
        $sql = "select";
        if (isset($this->cache) && $this->cache == 1)
            $sql .= " SQL_CACHE";
        $sql .= " * from ";
        $sql .= $this->getNomeTb($view);
        $sql .= $this->montaWheres($args, $extra);

        if (isset($this->print) && $this->print == "fetchAll") {
            echo $sql . "<br>";
        }

        $query = mysqli_query($GLOBALS['connection'], $sql);
        $total = ($query) ? mysqli_num_rows($query) : 0;
        for ($i = 0; $i < $total; $i++) {
            $result[] = mysqli_fetch_assoc($query);
        }
        return $result;
    }

    public function fetchAllLiteral($literal) {
        $sql = $literal;

        if (isset($this->print) && $this->print == "fetchAllLiteral") {
            echo $sql . "\n";
        }

        $query = mysqli_query($GLOBALS['connection'], $sql);
        $total = ($query) ? mysqli_num_rows($query) : 0;

        for ($i = 0; $i < $total; $i++) {
            $result[] = mysqli_fetch_assoc($query);
        }
        return $result;
    }

    public function find($args, $extra = "") {

        $extra['limit'] = 1;
        $ret = $this->fetchAll($args, $extra);
        return $ret[0];
    }

    public function findById($id) {

        $args[] = array("id", "=", $id);
        return $this->find($args);
    }

    public function count($args, $extra) {
        $sql = "select count(*) from " . $this->getNomeTb(1);
        $sql .= $this->montaWheres($args, $extra);
        if (isset($this->print) && $this->print == "count") {
            echo $sql . "\n";
        }
        $query = mysqli_query($GLOBALS['connection'], $sql);

        if ($query) {
            $resultRow = mysqli_fetch_array($query);
            $total = $resultRow[0];
        } else {
            $total = 0;
        }

        return $total;
    }

    public function sum($sum, $args, $extra) {
        $sql = "select sum(" . $sum . ") from " . $this->getNomeTb(1);
        $sql .= $this->montaWheres($args, $extra);
        if (isset($this->print) && $this->print == "sum") {
            echo $sql . "\n";
        }
        $query = mysqli_query($GLOBALS['connection'], $sql);

        if ($query) {
            $resultRow = mysqli_fetch_array($query);
            $total = $resultRow[0];
        } else {
            $total = 0;
        }

        return $total;
    }

    public function update($args, $where, $extra) {
        $sql = "update " . $this->getNomeTb() . " set ";
        $i = 0;

        $array_keys = array_keys($args);
        $columns = $this->getColumns();

        //s� altera as que vinherem no array
        foreach ($array_keys as $col) {
            if (in_array($col, $columns) && $col != "id") {
                if ($i != 0) {
                    $sql .= ", ";
                }
                $sql .= $col . "='" . $this->trataCaracteres($args[$col]) . "'";
                $i++;
            }
        }
        $i = 0;
        if ($where != "") {
            $sql .= $this->montaWheres($where, $extra);
        } else {
            $sql .= " where id = '" . $args['id'] . "'";
        }


        if (isset($this->print) && $this->print == "update") {
            echo $sql . "\n";
        }
        //echo $sql; die();
        $query = mysqli_query($GLOBALS['connection'], $sql);

        if ($this->nomeBackup != "") {
            $this->insertBackup($args);
        }
    }

    public function shiftColumn($col, $where) {

        $sql = "update " . $this->getNomeTb() . " set ";
        $sql .= $col . "=" . $col . " + 1 ";
        if ($where != "") {
            $sql .= $this->montaWheres($where);
        }


        if (isset($this->print) && $this->print == "shiftColumn") {
            echo $sql . "\n";
        }
        $query = mysqli_query($GLOBALS['connection'], $sql);
    }

    public function insert($args) {

        $sql = "insert into " . $this->getNomeTb() . " ";
        $i = 0;
        $columns = $this->getColumns();
        //print_r($columns);
        foreach ($columns as $col) {
            if ($col != "id" || $args['id'] != "") {
                if ($i != 0) {
                    $parte1 .= ", ";
                    $parte2 .= ", ";
                }
                $parte1 .= "`" . $col . "`";
                $parte2 .= "'" . $this->trataCaracteres($args[$col]) . "'";
                $i++;
            }
        }

        $sql .= " (" . $parte1 . ") values (" . $parte2 . ") ";
        //echo $sql."<br>";
        if (isset($this->print) && $this->print == "insert") {
            echo $sql . "\n";
        }
        $query = mysqli_query($GLOBALS['connection'], $sql);
        $result = mysqli_insert_id($GLOBALS['connection']);
        return $result;
    }

    public function insertBackup($args) {

        $args['dataBackup'] = date("Y-m-d H:i:s");

        $sql = "insert into " . $this->getNomeTbBackup() . " ";
        $i = 0;
        $columns = $this->getColumns();
        //print_r($columns);
        foreach ($columns as $col) {
            if ($col != "id" || $args['id'] != "") {
                if (isset($args[$col])) {
                    if ($i != 0) {
                        $parte1 .= ", ";
                        $parte2 .= ", ";
                    }
                    $parte1 .= "`" . $col . "`";
                    $parte2 .= "'" . $this->trataCaracteres($args[$col]) . "'";
                    $i++;
                }
            }
        }

        $sql .= " (" . $parte1 . ") values (" . $parte2 . ") ";
        //echo $sql."<br>";
        if (isset($this->print) && $this->print == "insert") {
            echo $sql . "\n";
        }
        $query = mysqli_query($GLOBALS['connection'], $sql);
        $result = mysqli_insert_id($GLOBALS['connection']);
        return $result;
    }

    public function delete($id) {

        $obj = $this->findById($id);

        if ($obj != "") {
            $sql = "delete from " . $this->getNomeTb() . " where id=" . $id . " limit 1";
            mysqli_query($GLOBALS['connection'], $sql);
        }
        if (isset($this->print) && $this->print == "delete") {
            echo $sql . "\n";
        }
    }

    public function getPPV() {
        $sql = "select max(id) from " . $this->getNomeTb(1);
        $query = mysqli_query($GLOBALS['connection'], $sql);
        if (mysqli_num_rows($query) > 0) {
            $resultRow = mysqli_fetch_array($query);
            $num = $resultRow[0];
        }
        return ++$num;
    }

    public function getMax($campo, $args, $extra) {
        return $this->getMinMax("max", $campo, $args, $extra);
    }

    public function getMin($campo, $args, $extra) {
        return $this->getMinMax("min", $campo, $args, $extra);
    }

    public function getMinMax($limite, $campo, $args, $extra) {
        $sql = "select";
        $sql .= " " . $limite . "(" . $campo . ")";
        $sql .= " from ";
        $sql .= $this->getNomeTb(1);
        $sql .= $this->montaWheres($args, $extra);


        $query = mysqli_query($GLOBALS['connection'], $sql);

        if ($query) {
            $resultRow = mysqli_fetch_array($query);
            $result = $resultRow[0];
        } else {
            $result = 0;
        }

        return $result;
    }

    private function montaWheres($args, $extra) {

        $sql = "";

        if (count($args) > 0 && $args != "") {
            $sql .= " where";

            //print_r($parametros);
            foreach ($args as $p) {
                if (is_array($p[0])) { //monta OU
                    $sql .= " ( ";
                    foreach ($p as $pp) {
                        if (is_array($pp[0])) { //monta E
                            $sql .= " ( ";
                            foreach ($pp as $ppp) {
                                if (is_array($ppp[0])) {
                                    $sql .= " ( ";
                                    foreach ($ppp as $pppp) {
                                        if (is_array($pppp[0])) {
                                            $sql .= " ( ";
                                            foreach ($pppp as $ppppp) {
                                                $sql .= $this->montaWhere($ppppp) . " and";
                                            }
                                            $sql .= " 1=1";
                                            $sql .= " ) or ";
                                        } else {
                                            $sql .= $this->montaWhere($pppp) . " or";
                                        }
                                    }
                                    $sql .= " 1=0";
                                    $sql .= " ) and ";
                                } else {
                                    $sql .= $this->montaWhere($ppp) . " and";
                                }
                            }
                            $sql .= " 1=1";
                            $sql .= " ) or ";
                        } else {
                            $sql .= $this->montaWhere($pp) . " or";
                        }
                    }
                    $sql .= " 0=1";
                    $sql .= " ) and ";
                } else {
                    $sql .= $this->montaWhere($p) . " and";
                }
            }
            $sql .= " 1=1";
        }

        if ($extra != "none") {

            //tratar os extras order limit offset
            if (isset($extra['groupby']))
                $sql .= " group by " . $extra['groupby'];
            if (isset($extra['order']))
                $sql .= " order by " . $extra['order'];
            else
                $sql .= " order by " . $this->usualOrder;
            if (isset($extra['limit']))
                $sql .= " limit " . $extra['limit'];
            if (isset($extra['offset']))
                $sql .= " offset " . $extra['offset'];
        }

        return $sql;
    }

    private function montaWhere($p) {

        if (!is_array($p[2])) {
            $p[2] = $this->trataCaracteres($p[2]);
        }

        $sql = " " . $p[0];
        if ($p[1] == 'lk') {
            $sql .= " like '%" . $p[2] . "%'";
        } else if ($p[1] == 'lkd') {
            $sql .= " like '" . $p[2] . "'";
        } else if ($p[1] == 'nlk') {
            $sql .= " not like '" . $p[2] . "'";
        } else if ($p[1] == "null") {
            $sql .= " is null";
        } else if ($p[1] == "notnull") {
            $sql .= " is not null";
        } else if ($p[1] == "literal") {
            //nao faz nada
        } else if ($p[1] == "in") {
            if (is_array($p[2])) {
                $p[2] = implode(",", $p[2]);
            } if ($p[2] == "") {
                $p[2] = "0";
            }
            $sql .= " in (" . $p[2] . ")";
        } else if ($p[1] == "not in") {
            if (is_array($p[2]))
                $p[2] = implode(",", $p[2]);
            if ($p[2] == "")
                $p[2] = "0";
            $sql .= " not in (" . $p[2] . ")";
        } else {
            if ($p[1] == "lt")
                $sql .= "<";
            else if ($p[1] == "gt")
                $sql .= ">";
            else if ($p[1] == "lte")
                $sql .= "<=";
            else if ($p[1] == "gte")
                $sql .= ">=";
            else if ($p[1] == "!=")
                $sql .= "!=";
            else
                $sql .= "=";
            $sql .= "'" . $p[2] . "'";
        }
        return $sql;
    }

    private function getColumns() {

        if ($this->columns != "") {
            return $this->columns;
        } else {
            //echo "certo";
            $sql = "show columns from " . $this->getNomeTb();
            $query = mysqli_query($GLOBALS['connection'], $sql);
            $total = ($query) ? mysqli_num_rows($query) : 0;
            for ($i = 0; $i < $total; $i++) {
                $row = mysqli_fetch_assoc($query);
                $this->columns[] = $row['Field'];
            }
            //print_r($this->columns);
            return $this->columns;
        }
    }

    private function trataCaracteres($val) {

        $val = addslashes($val);
        //permanece porque mesmo com a aspas escapada precisa substituir por &quot;
        $val = str_replace("\"", "&quot;", $val);
        $val = trim($val);
        return $val;
    }

}
