<?php

class AbstractMapper {

    protected $modelName;
    protected $repository;
    protected $necessary;

    public function fetchAll($args = "", $extra = "") {
        $results = $this->repository->fetchAll($args, $extra);
        if ($results != "") {
            foreach ($results as $result) {
                $return[] = new $this->modelName($result);
            }
        }
        return $return;
    }

    public function fetchAllTable($args = "", $extra = "") {

        $this->repository->setForcaTable(1);
        return $this->fetchAll($args, $extra);
    }

    public function fetchAllJson($args = "", $extra = "", $tipo = "") {
        $results = $this->repository->fetchAll($args, $extra, 0);
        if ($results != "") {
            foreach ($results as $result) {
                if ($tipo == "casa" || $tipo == "distribuicao") {
                    $result['extranet_id'] = $result['id'];
                    unset($result['id']);
                }
                $resultsNovo[] = $result;
            }
            return $resultsNovo;
        } else {
            return "empty";
        }
    }

    public function fetchAllLiteral($literal) {
        $results = $this->repository->fetchAllLiteral($literal);
        if ($results != "") {
            foreach ($results as $result) {
                $return[] = new $this->modelName($result);
            }
        }
        return $return;
    }

    public function selectColumnLiteral($literal) {

        return $this->repository->selectColumnLiteral($literal);
    }

    public function fetchAllAtivos($args = "", $extras = "") {
        $args[] = array("ativo", "=", 1);
        return $this->fetchAll($args, $extras);
    }

    public function find($args = "", $extra = "") {
        $ret = $this->repository->find($args, $extra);

        if ($ret != "") {
            $ret = new $this->modelName($ret);
        }
        return $ret;
    }

    public function findTable($args = "", $extra = "") {

        $this->repository->setForcaTable(1);
        return $this->find($args, $extra);
    }

    public function findById($id) {
        $ret = $this->repository->findById($id);
        if ($ret != "") {
            $ret = new $this->modelName($ret);
        }
        return $ret;
    }

    public function findByIdTable($id) {
        $this->repository->setForcaTable(1);
        return $this->findById($id);
    }

    public function count($args = "", $ext = "") {
        return $this->repository->count($args, $ext);
    }

    public function countTable($args = "", $ext = "") {
        $this->repository->setForcaTable(1);
        return $this->count($args, $ext);
    }

    public function sum($sum = "", $args = "", $extra = "") {
        return $this->repository->sum($sum, $args, $extra);
    }

    public function sumTable($args = "", $ext = "") {
        $this->repository->setForcaTable(1);
        return $this->sum($args, $ext);
    }

    public function insert($post) {

        foreach ($this->necessary as $n) {
            if ($post[$n] == "") {
                $nome = str_replace("_id", "", $n);
                $erro[] = "Campo " . $nome . " deve ser preenchido";
            }
        }
        if ($erro == "") {
            $post['lastModify'] = date("Y-m-d H:i:s");
            $ret = $this->repository->insert($post);
            return $ret;
        } else {
            throw new Exception(implode($erro, "+"));
        }
    }

    public function update($post, $where = "", $extra = "") {


        $array_keys = array_keys($post);

        foreach ($array_keys as $n) {
            if ($post[$n] == "" && in_array($n, $this->necessary)) {
                $nome = str_replace("_id", "", $n);
                $erro[] = "Campo " . $nome . " deve ser preenchido";
            }
        }
        if ($erro == "") {
            if ($this instanceof Prod_CasaMapper) {
                $lastModifyCasa = 0;
                $cols = array("ugb_id", "vila_id", "quadra_id", "inicioPrev", "inicioReal", "tipoProjeto_id", "tipoCasa_id", "cor", "chapa");
                foreach ($cols as $col) {
                    if (in_array($col, array_keys($post))) {
                        $lastModifyCasa = 1;
                    }
                }
                if ($lastModifyCasa == 1) {
                    $post['lastModify'] = date("Y-m-d H:i:s");
                }
//                else {
//                    unset($post['lastModify']);
//                }
            } else {
                if (!isset($post['lastModify'])) {
                    $post['lastModify'] = date("Y-m-d H:i:s");
                }
//                else {
//                    unset($post['lastModify']);
//                }
            }
            $ret = $this->repository->update($post, $where, $extra);
            return $ret;
        } else {
            throw new Exception(implode($erro, "+"));
        }
    }

    public function shiftColumn($col, $where) {
        $this->repository->shiftColumn($col, $where);
    }

    public function delete($id) {
        if ($this->findById($id) == "") {
            throw new Exception("Não encontrado");
        } else {
            $this->repository->delete($id);
        }
    }

    public function deleteVarios($objs) {

        foreach ($objs as $obj) {
            if ($obj != "") {
                $this->repository->delete($obj->getId());
            }
        }
    }

    public function inativa($id) {
        if ($this->findById($id) == "") {
            throw new Exception("Não encontrado");
        } else {
            $post['id'] = $id;
            $post['ativo'] = 0;
            $this->repository->update($post);
        }
    }

    public function inativaVarios($objs, $extra) {
        
//        if (!$objs[0] instanceof $this->modelName) {
//        //if (is_object($objs[0])) {
//            echo "sou um array<br>";
//            $results = $this->repository->fetchAll($objs, $extra);
//            unset($objs);
//            if ($results != "") {
//                foreach ($results as $result) {
//                    $objs[] = new $this->modelName($result);
//                    print_r($objs);
//                }
//            }
//        }

        foreach ($objs as $obj) {
            if ($obj != "") {
                $post['id'] = $obj->getId();
                $post['ativo'] = 0;
                $this->repository->update($post);
            }
        }
    }

    public function selectColumnDirect($col, $args, $extra = "", $indice = "") {

        return $this->repository->selectColumnDirect($col, $args, $extra, $indice);
    }

    public function selectColumnDirectTable($col, $args, $extra = "", $indice = "") {

        $this->repository->setForcaTable(1);
        return $this->selectColumnDirect($col, $args, $extra, $indice);
    }

    public function selectColumn($col, $args, $extra = "", $indice = "") {

        return $this->repository->selectColumn($col, $args, $extra, $indice);
    }

    public function selectColumnTable($col, $args, $extra = "", $indice = "") {

        $this->repository->setForcaTable(1);
        return $this->selectColumn($col, $args, $extra, $indice);
    }

    public function selectColumns($cols, $args, $extra = "") {

        return $this->repository->selectColumns($cols, $args, $extra);
    }

    public function selectColumnsTable($cols, $args, $extra = "") {

        $this->repository->setForcaTable(1);
        return $this->selectColumns($cols, $args, $extra);
    }

    public function getMax($campo, $args, $extra = "") {

        return $this->repository->getMax($campo, $args, $extra);
    }

    public function getMaxTable($campo, $args, $extra = "") {

        $this->repository->setForcaTable(1);
        return $this->repository->getMax($campo, $args, $extra);
    }

    public function getMinTable($campo, $args, $extra = "") {

        $this->repository->setForcaTable(1);
        return $this->repository->getMin($campo, $args, $extra);
    }

    public function getMin($campo, $args, $extra = "") {

        return $this->repository->getMin($campo, $args, $extra);
    }

}

?>