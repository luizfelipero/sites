<?php

abstract class AbstractController {

    public $view;

    public function __construct($view) {

        $this->view = $view;
    }

}

?>