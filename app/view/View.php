<?php

class View {

    public $locationView;
    public $locationLayout;
    public $nomeView;
    public $nomeLayout;
    public $domain;
    public $module;

    public function __construct($d, $m, $c, $a, $url = "") {

        $this->domain = $d;
        $this->module = $m;
        $this->controller = $c;
        $this->action = $a;

//        $this->locationView = "views/" . $m . "/" . $c . "/";
        $this->locationView = "views/" . $d . "/" . $m . "/" . $c . "/";
        $this->locationLayout = "layout/";
        $this->nomeView = $a;
        $this->url = $url;
//        if (($m == "Index" && $c == "Index" && $a != "Home") && ($m == "Index" && $c == "Index" && $a != "HomeTerceirizado") && ($m == "Index" && $c == "Index" && $a != "HomeGv"))
//            $this->nomeLayout = "none";
//        else if (strpos($a, "Json") !== false)
//            $this->nomeLayout = "json";
//        else if ($a == "Imagem" || strpos($a, "Html") !== false)
//            $this->nomeLayout = "none";
//        else if ($m == "Pdca" && $c == "Apresentacao" && $a == "Visualizar")
//            $this->nomeLayout = "apresentacao";
//        else if ($c == "Grafico" && $a == "Exibe")
//            $this->nomeLayout = "chart";
//        else if (strpos($a, "Imprimir") !== false)
//            $this->nomeLayout = "impressao";
//        else if ($GLOBALS['ajax'] == 1)
//            $this->nomeLayout = "internaAjax";
//        else if ($GLOBALS['user'] != "" && $GLOBALS['user']->getTipo() == 99)
//            $this->nomeLayout = "internaGv";
//        else
        $this->nomeLayout = "interna";
    }

    public function setLayout($layout) {
        if ($layout == "interna" && $GLOBALS['ajax'] == 1) {
            $this->nomeLayout = "internaAjax";
        } else {
            $this->nomeLayout = $layout;
        }
    }

    public function setView($view) {

        $this->nomeView = $view;
    }

    public function render() {

        include ($this->locationLayout . $this->nomeLayout . ".php");
    }

    public function renderView() {
        if (file_exists($this->locationView . $this->nomeView . ".php")) {
            include ($this->locationView . $this->nomeView . ".php");
        }
    }

}

?>