<?php


/**
 * A ReCaptchaResponse is returned from recaptcha_check_answer()
 */
class Vendor_Captcha_ReCaptchaResponse {
    public $is_valid;
    public $error;
}
?>
