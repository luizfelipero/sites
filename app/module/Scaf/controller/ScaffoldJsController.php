<?php

class Scaf_ScaffoldJsController extends AbstractController {

    public function IndexAction() {
        
    }

    public function FormCodigoModeloAction() {
        
    }

    public function FormCodigoObjetoAction() {

        if (isset($_POST['nomeTabela'])) {
            $nomeTabela = $_POST['nomeTabela'];
            $nomeModel = $_POST['nomeModel'];

            $nomeArquivoModel = "get " . $nomeModelo;

            $this->view->resultado = $this->CriarGetObjeto($nomeTabela, $nomeModel, $nomeArquivoModel);
        }
    }

    public function CriarCodigoModeloAction() {
        try {
            $nomeTabela = $_POST['nomeTabela'];
            $nomeModel = $_POST['nomeModel'];
            $atributos = $_POST['atributos'];
            $atributos = str_replace(" ", "", $atributos);
            $atributos = explode(",", $atributos);
            $atributos[] = 'lastModify';
            $atributos[] = 'extranet_id';
            $nome = explode("_", $nomeModel);
            $nomeModelo = $nome[1];
            $nomeModulo = $nome[0];

            $necessarios = str_replace(" ", "", $_POST['necessarios']);
            $necessarios = explode(",", $necessarios);

            $nomeArquivoModel = $nomeModelo;
            $nomeArquivoMapper = $nomeModelo . "Mapper";
            $nomeArquivoController = $nomeModelo . "Controller";

            if ($_POST['model'] == 1) {
                $this->CriarModel($nomeModulo, $nomeModelo, $nomeArquivoModel, $atributos);
            }
            if ($_POST['controller'] == 1) {
                $this->CriarController($nomeModulo, $nomeModelo, $nomeArquivoController);
            }
            if ($_POST['mapper'] == 1) {
                $this->CriarMapper($nomeModulo, $nomeModelo, $nomeTabela, $atributos, $nomeArquivoMapper, $necessarios);
            }
            Util_FlashMessage::write("Arquivos gerados com sucesso!", "success");
            Util_Link::redirect("Scaf", "ScaffoldJs", "FormCodigoModelo");
        } catch (Exception $e) {
            Util_FlashMessage::write(explode("+", $e->getMessage()), "danger");
            Util_Link::redirect("Scaf", "ScaffoldJs", "FormCodigoModelo");
        }
    }

    public function CriarCodigoObjetoAction() {
        try {

            $this->view->setLayout("none");

            $nomeTabela = $_POST['nomeTabela'];
            $nomeModel = $_POST['nomeModel'];

            $nomeArquivoModel = "get " . $nomeModelo;

            $this->CriarGetObjeto($nomeTabela, $nomeModel, $nomeArquivoModel);

            Util_FlashMessage::write("Arquivos gerados com sucesso!", "success");
        } catch (Exception $e) {
            Util_FlashMessage::write(explode("+", $e->getMessage()), "danger");
            Util_Link::redirect("Scaf", "Scaffold", "FormCodigoObjeto");
        }
    }

    //creators

    public function CriarGetObjeto($nomeTabela, $nomeModel, $nomeArquivoModel) {

        $nomeMetodo = ucfirst($nomeTabela);

        $dados = 'public function get' . $nomeMetodo . '() { 
                if (!$this->' . $nomeTabela . ') {
                $this->' . $nomeTabela . " = " . $nomeModel . 'Mapper::getInstance()->findById($this->' . $nomeTabela . '_id);
                }
                return $this->' . $nomeTabela . ';
                 }';

        return $dados;
    }

    public function CriarModel($nomeModulo, $nomeModelo, $nomeArquivoModel, $atributos) {
        $model = fopen("../app/module/" . $nomeModulo . "/model/" . $nomeArquivoModel . ".js", "w");
        $dados = "function " . $nomeModulo . "_" . $nomeModelo . " (arg) { \n\n";
        foreach ($atributos as $atributo) {
            $dados .= "this." . $atributo . " = arg['" . $atributo . "'];\n";
        }
        $dados .= "\n";
        $dados .= "    //Regular getters\n";
        $dados .= "\n";

        //Getters
        foreach ($atributos as $atributo) {
            $dados .= "this.get" . ucfirst($atributo) . " = function () {\n";
            $dados .= "return this." . $atributo . ";\n";
            $dados .= "};\n";
            $dados .= "\n";
        }

        $dados .= "}";

        fwrite($model, $dados);
    }

    public function CriarMapper($nomeModulo, $nomeModelo, $nomeTabela, $atributos, $nomeArquivoMapper, $necessarios) {
        $mapper = fopen("../app/module/" . $nomeModulo . "/model/mapper/" . $nomeArquivoMapper . ".js", "w");
        $totalNecessarios = count($necessarios);
        for ($i = 0; $i < $totalNecessarios; $i++) {
            if ($i != ($totalNecessarios - 1)) {
                $necessarioFormatado .= "\"" . $necessarios[$i] . "\",";
            } else {
                $necessarioFormatado .= "\"" . $necessarios[$i] . "\"";
            }
        }
        if ($necessarioFormatado == "\"\"") {
            $necessarioFormatado = "";
        }
        $dados = "var " . $nomeModulo . "_" . $nomeModelo . "Mapper = new AbstractMapper();\n";
        $dados .= $nomeModulo . "_" . $nomeModelo . "Mapper.nomeTb = \"" . $nomeTabela . "\";\n";
        $dados .= $nomeModulo . "_" . $nomeModelo . "Mapper.columns = [";

        $j = 0;
        foreach ($atributos as $atributo) {
            $j++;
            if (count($atributos) == $j) {
                $dados .= "'" . $atributo . "'";
            } else {
                $dados .= "'" . $atributo . "',";
            }
        }
        $dados .= "];\n";

        $dados .= $nomeModulo . "_" . $nomeModelo . "Mapper.createTable = \"CREATE TABLE IF NOT EXISTS " . $nomeTabela . "(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT);\";\n";
        $dados .= $nomeModulo . "_" . $nomeModelo . "Mapper.usualOrder = \"id asc\";\n";
        $dados .= $nomeModulo . "_" . $nomeModelo . "Mapper.print = \"\";\n";
        $dados .= $nomeModulo . "_" . $nomeModelo . "Mapper.modelName = \"" . $nomeModulo . "_" . $nomeModelo . "\";\n";
        $dados .= $nomeModulo . "_" . $nomeModelo . "Mapper.necessary = [" . $necessarioFormatado . "];";

        fwrite($mapper, $dados);
    }

    public function CriarController($nomeModulo, $nomeModelo, $nomeArquivoController) {
        $controller = fopen("../app/module/" . $nomeModulo . "/controller/" . $nomeArquivoController . ".js", "w");

        $dados = "var " . $nomeModulo . "_" . $nomeModelo . "Controller = {\n\n";
        $dados .= '};';

        fwrite($controller, $dados);
    }

}

?>
