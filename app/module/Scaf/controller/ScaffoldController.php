<?php

class Scaf_ScaffoldController extends AbstractController {

    public function IndexAction() {
        
    }

    public function FormCodigoModeloAction() {
        
    }

    public function FormCodigoObjetoAction() {

        if (isset($_POST['nomeTabela'])) {
            $nomeTabela = $_POST['nomeTabela'];
            $nomeModel = $_POST['nomeModel'];

            $nomeArquivoModel = "get " . $nomeModelo;

            $this->view->resultado = $this->CriarGetObjeto($nomeTabela, $nomeModel, $nomeArquivoModel);
        }
    }

    public function CriarCodigoModeloAction() {
        try {
            $nomeTabela = $_POST['nomeTabela'];
            $nomeView = $_POST['nomeView'];
            $nomeModel = $_POST['nomeModel'];
            $atributos = $_POST['atributos'];
            $atributos = str_replace(" ", "", $atributos);
            $atributos = explode(",", $atributos);
            $nome = explode("_", $nomeModel);
            $nomeModelo = $nome[1];
            $nomeModulo = $nome[0];

            $necessarios = str_replace(" ", "", $_POST['necessarios']);
            $necessarios = explode(",", $necessarios);

            $nomeArquivoModel = $nomeModelo;
            $nomeArquivoMapper = $nomeModelo . "Mapper";
            $nomeArquivoRepository = $nomeModelo . "Repository";
            $nomeArquivoController = $nomeModelo . "Controller";

            if ($_POST['model'] == 1) {
                $this->CriarModel($nomeModulo, $nomeModelo, $nomeArquivoModel, $atributos);
            }
            if ($_POST['controller'] == 1) {
                $this->CriarController($nomeModulo, $nomeModelo, $nomeArquivoController);
            }
            if ($_POST['mapper'] == 1) {
                $this->CriarMapper($nomeModulo, $nomeModelo, $nomeArquivoMapper, $necessarios);
            }
            if ($_POST['repository'] == 1) {
                $this->CriarRepository($nomeModulo, $nomeModelo, $nomeTabela, $nomeView, $nomeArquivoRepository);
            }
            if ($_POST['index'] == 1) {
                $this->CriarIndexAction($nomeModulo, $nomeModelo);
            }
            if ($_POST['Editar'] == 1) {
                $this->CriarEditarAction($nomeModulo, $nomeModelo);
            }
            if ($_POST['Inserir'] == 1) {
                $this->CriarInserirAction($nomeModulo, $nomeModelo);
            }
            if ($_POST['Visualizar'] == 1) {
                $this->CriarVisualizarAction($nomeModulo, $nomeModelo);
            }

            Util_FlashMessage::write("Arquivos gerados com sucesso!", "success");
            Util_Link::redirect("Scaf", "Scaffold", "FormCodigoModelo");
        } catch (Exception $e) {
            Util_FlashMessage::write(explode("+", $e->getMessage()), "danger");
            Util_Link::redirect("Scaf", "Scaffold", "FormCodigoModelo");
        }
    }

    public function CriarCodigoObjetoAction() {
        try {

            $this->view->setLayout("none");

            $nomeTabela = $_POST['nomeTabela'];
            $nomeModel = $_POST['nomeModel'];

            $nomeArquivoModel = "get " . $nomeModelo;

            $this->CriarGetObjeto($nomeTabela, $nomeModel, $nomeArquivoModel);

            Util_FlashMessage::write("Arquivos gerados com sucesso!", "success");
        } catch (Exception $e) {
            Util_FlashMessage::write(explode("+", $e->getMessage()), "danger");
            Util_Link::redirect("Scaf", "Scaffold", "FormCodigoObjeto");
        }
    }

    //creators

    public function CriarGetObjeto($nomeTabela, $nomeModel, $nomeArquivoModel) {

        $nomeMetodo = ucfirst($nomeTabela);

        $dados = 'public function get' . $nomeMetodo . '() { 
                if (!$this->' . $nomeTabela . ') {
                $this->' . $nomeTabela . " = " . $nomeModel . 'Mapper::getInstance()->findById($this->' . $nomeTabela . '_id);
                }
                return $this->' . $nomeTabela . ';
                 }';

        return $dados;
    }

    public function CriarModel($nomeModulo, $nomeModelo, $nomeArquivoModel, $atributos) {



        $model = fopen("../app/module/" . $nomeModulo . "/model/" . $nomeArquivoModel . ".php", "w");

        $dados = "<?php    
                class " . $nomeModulo . "_" . $nomeModelo . " { ";
        foreach ($atributos as $atributo) {
            $dados .= "private $" . $atributo . ";";
        }

        $dados .= ' function __construct($arg) {';
        foreach ($atributos as $atributo) {
            $dados .= '$this->' . $atributo . '= $arg[\'' . $atributo . "'];";
        }
        $dados .= "}
            }
            ?>";

        fwrite($model, $dados);
    }

    public function CriarMapper($nomeModulo, $nomeModelo, $nomeArquivoMapper, $necessarios) {
        $mapper = fopen("../app/module/" . $nomeModulo . "/model/mapper/" . $nomeArquivoMapper . ".php", "w");
        $totalNecessarios = count($necessarios);
        for ($i = 0; $i < $totalNecessarios; $i++) {
            if ($i != ($totalNecessarios - 1)) {
                $necessarioFormatado .= "\"" . $necessarios[$i] . "\",";
            } else {
                $necessarioFormatado .= "\"" . $necessarios[$i] . "\"";
            }
        }
        if ($necessarioFormatado == "\"\"") {
            $necessarioFormatado = "";
        }
        $dados = "<?php    
                class " . $nomeModulo . "_" . $nomeModelo . 'Mapper extends AbstractMapper {  
                        private static $_instance = NULL;
                            final public static function getInstance() {
                            if (self::$_instance === null) {
                                self::$_instance = new self();
                                    }
                                    return self::$_instance;
                                        }
                                        public function __construct() {
        $this->repository = new ' . $nomeModulo . "_" . $nomeModelo . 'Repository();
        $this->necessary = array(' . $necessarioFormatado . ');
        $this->modelName = "' . $nomeModulo . "_" . $nomeModelo . '";
                    }
                    }
            ?>';

        fwrite($mapper, $dados);
    }

    public function CriarRepository($nomeModulo, $nomeModelo, $nomeTabela, $nomeView, $nomeArquivoRepository) {
        $repository = fopen("../app/module/" . $nomeModulo . "/model/repository/" . $nomeArquivoRepository . ".php", "w");

        $dados = "<?php    
                class " . $nomeModulo . "_" . $nomeModelo . "Repository extends AbstractRepository { ";

        $dados .= 'public function __construct() { 
            $this->nomeTb = "' . $nomeTabela . '";';

        if ($nomeView != "") {
            $dados .= '$this->nomeView = "' . $nomeView . '";';
        }

        $dados .= '$this->usualOrder = "id asc";
            $this->print="";
            
            }
            }
            ?>';

        fwrite($repository, $dados);
    }

    public function CriarController($nomeModulo, $nomeModelo, $nomeArquivoController) {
        $controller = fopen("../app/module/" . $nomeModulo . "/controller/" . $nomeArquivoController . ".php", "w");

        $dados = "<?php    
                class " . $nomeModulo . "_" . $nomeModelo . "Controller extends AbstractController { ";

        $dados .= '
            public function IndexAction() {
                if ($_GET["id"] != "") {
                    $arg[] = array("id", "=", $_GET["id"]);
                    $extras["id"] = $_GET["id"];
                }        
        
                $limit = 10;

                $ext["limit"] = $limit;
                $ext["offset"] = $_REQUEST["index"] * $limit;
                $ext["order"] = "id asc";

                $pag["total"] = ' . $nomeModulo . '_' . $nomeModelo . 'Mapper::getInstance()->count($arg);
                $pag["limit"] = $limit;
                $pag["link"] = Util_Link::link("' . $nomeModulo . '", "' . $nomeModelo . '", "Index");
                $pag["index"] = $_REQUEST["index"];

                $this->view->' . strtolower($nomeModelo) . 's = ' . $nomeModulo . '_' . $nomeModelo . 'Mapper::getInstance()->fetchAll($arg, $ext);
                $this->view->paginacao = new Util_Paginacao($pag, $extras);
            
            }
            
            public function InserirAction() {
                if ($_POST["post"] == "1") {
                    try {
                        $id = ' . $nomeModulo . '_' . $nomeModelo . 'Mapper::getInstance()->insert($_POST);
                        Util_FlashMessage::write("Inserido com sucesso", "success");
                        Util_Link::redirect("' . $nomeModulo . '", "' . $nomeModelo . '", "Visualizar", $id);
                    } catch (Exception $e) {
                        Util_FlashMessage::write(explode("+", $e->getMessage()), "danger");
                        Util_Link::redirect("' . $nomeModulo . '", "' . $nomeModelo . '", "Inserir");
                    }
                }
            }            
            
            public function EditarAction() {
                if ($_POST["post"] == "1") {
                    try {
                        $id = ' . $nomeModulo . '_' . $nomeModelo . 'Mapper::getInstance()->update($_POST);
                        Util_FlashMessage::write("Alterado com sucesso", "success");
                        Util_Link::redirect("' . $nomeModulo . '", "' . $nomeModelo . '", "Visualizar", $id);
                    } catch (Exception $e) {
                        Util_FlashMessage::write(explode("+", $e->getMessage()), "danger");
                        Util_Link::redirect("' . $nomeModulo . '", "' . $nomeModelo . '", "Editar", $id);
                    }
                } else {
                    $this->view->' . strtolower($nomeModelo) . ' = ' . $nomeModulo . '_' . $nomeModelo . 'Mapper::getInstance()->findById($_GET["id"]);
                }            
            }
            
            public function VisualizarAction() {
                $this->view->' . strtolower($nomeModelo) . ' = ' . $nomeModulo . '_' . $nomeModelo . 'Mapper::getInstance()->findById($_GET["id"]);
            }
            }
            ?>';

        fwrite($controller, $dados);
    }

    public function CriarVisualizarAction($nomeModulo, $nomeModelo) {

        $index = fopen("../public/views/" . $nomeModulo . "/" . $nomeModelo . "/Visualizar.php", "w");

        $dados = '<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2><i class="fa fa-th"></i><span class="break"></span>Visualizar</h2>
            <div class="box-icon">
                <span class="break"></span>
                <a href="#"><i class="fa fa-filter filtro-botao"></i></a>
                <span class="break"></span>
                <a href="?m=' . $nomeModulo . '&c=' . $nomeModelo . '&a=Inserir" class="btn-setting"><i class="fa fa-plus"></i></a>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="row">
                    <div class="col-sm-3">
                        <label>Nome</label>
                        <input type="text" name="nome" id="nome" value=" <?php echo $this->ITEM->getNome(); ?>" class="form-control">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
        fwrite($index, $dados);
    }

    public function CriarInserirAction($nomeModulo, $nomeModelo) {

        $index = fopen("../public/views/" . $nomeModulo . "/" . $nomeModelo . "/Inserir.php", "w");

        $dados = '<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2><i class="fa fa-th"></i><span class="break"></span>Inserir</h2>
            <div class="box-icon">
                <span class="break"></span>
                <a href="?m=' . $nomeModulo . '&c=' . $nomeModelo . '&a=Inserir" class="btn-setting"><i class="fa fa-plus"></i></a>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <form class="" action="<?php echo Util_Link::link("' . $nomeModulo . '", "' . $nomeModelo . '", "Index"); ?>" method="POST">

                        <div class="row">
                            <div class="col-sm-3">
                                <label>Nome</label>
                                <input type="text" name="nome" id="nome" class="form-control">
                            </div>
                        </div>

                        <input type ="hidden" name="post" value="1" />
                        <input id="submitform" class="btn btn-primary" name="submitform" type="submit" value="Salvar" />
                        <a href="<?php echo Util_Link::link("' . $nomeModulo . '", "' . $nomeModelo . '", "Index"); ?>" style="padding-left: 15px">Cancelar</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>';
        fwrite($index, $dados);
    }

    public function CriarEditarAction($nomeModulo, $nomeModelo) {

        $index = fopen("../public/views/" . $nomeModulo . "/" . $nomeModelo . "/Editar.php", "w");

        $dados = '<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2><i class="fa fa-th"></i><span class="break"></span>Editar</h2>
            <div class="box-icon">
                <span class="break"></span>
                <a href="#"><i class="fa fa-filter filtro-botao"></i></a>
                <span class="break"></span>
                <a href="?m=' . $nomeModulo . '&c=' . $nomeModelo . '&a=Inserir" class="btn-setting"><i class="fa fa-plus"></i></a>
            </div>
        </div>
        <div class="panel-body hide filtro-opcoes">
            <form class="" action="" method="GET">
                <?php echo Util_Link::linkHideForm("' . $nomeModulo . '", "' . $nomeModelo . '", "Index"); ?>
                
                <div class="row">
                    <div class="col-sm-3">
                        <label>Nome</label>
                        <input type="text" name="nome" id="nome" value=" <?php echo $this->ITEM->getNome(); ?>" class="form-control">
                    </div>
                </div>

                <input type ="hidden" name="post" value="1" />
                <input type ="hidden" name="id" value="<?php echo $this->ITEM->getId(); ?>" />
                <input id="submitform" class="btn btn-primary" name="submitform" type="submit" value="Salvar" />
                <a href="<?php echo Util_Link::link("' . $nomeModulo . '", "' . $nomeModelo . '", "Index"); ?>" style="padding-left: 15px">Cancelar</a>
            </form>
        </div>
    </div>
</div>';
        fwrite($index, $dados);
    }

    public function CriarIndexAction($nomeModulo, $nomeModelo) {
        print_r($nomeModulo);
        echo "---";
        print_r($nomeModelo);
        $index = fopen("../public/views/" . $nomeModulo . "/" . $nomeModelo . "/Index.php", "w");

        $dados = '<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2><i class="fa fa-th"></i><span class="break"></span>Index</h2>
            <div class="box-icon">
                <span class="break"></span>
                <a href="#"><i class="fa fa-filter filtro-botao"></i></a>
                <span class="break"></span>
                <a href="?m=' . $nomeModulo . '&c=' . $nomeModelo . '&a=Inserir" class="btn-setting"><i class="fa fa-plus"></i></a>
            </div>
        </div>
        <div class="panel-body hide filtro-opcoes">
            <form class="" action="" method="GET">
                <?php echo Util_Link::linkHideForm("' . $nomeModulo . '", "' . $nomeModelo . '", "Index"); ?>
                <div class="row">
                    <div class="col-sm-1 col-sm-offset-1">
                        <label>Nº</label>
                        <input name="id" id="id" type="text" class="form-control">
                    </div>
                    <div class="col-sm-1 col-sm-offset-1">
                        <label>Nome</label>
                        <input name="nome" id="nome" type="text" class="form-control">
                    </div>
                </div>
            </form>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-12">
                    <?php
                    if (!$this->ITENS) {
                        echo Util_Alert::exibe("Nenhum ITEM encontrado!");
                    } else {
                        ?>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-condensed table-hover">
                                <tr>
                                    <th>Nº</th>
                                    <th>Nome</th>
                                    <th>&nbsp;</th><!-- lupa -->
                                </tr>
                                <?php
                                foreach ($this->ITENS as $ITEM) {
                                    ?>
                                    <tr>
                                        <td><?php echo $ITEM->getId(); ?></td>
                                        <td><?php echo $ITEM->getNomeTipo(); ?></td>
                                        <td><!-- lupa -->
                                            <div class="btn-group">
                                                <a class="btn btn-xs btn-default" href="<?php echo Util_Link::link("' . $nomeModulo . '", "' . $nomeModelo . '", "Visualizar", $ITEM->getId()); ?>"><i class="fa fa-search"></i></a>
                                            </div>
                                        </td><!-- lupa -->
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </table>
                    </div>
                </div>
            </div>
            <?php echo $this->paginacao->exibe(); ?>
        </div>
    </div>
</div>';
        fwrite($index, $dados);
    }

}
?>
