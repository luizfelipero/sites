<?php

class Serv_PessoaController extends AbstractController {

    public function RecuperarSenhaAction() {
        $this->view->setLayout("none");
        if ($_POST['post'] == 1) {
            try {
                Adm_PessoaMapper::getInstance()->recuperarSenha($_POST);
                Util_FlashMessage::write("Senha alterada com sucesso e enviada para seu e-mail", "success");
                Util_Link::redirect("Index", "Index", "Index");
            } catch (Exception $e) {
                Util_FlashMessage::write(explode("+", $e->getMessage()), "danger");
                Util_Link::redirect("Serv", "Pessoa", "RecuperarSenha");
            }
        }
    }

}

?>
