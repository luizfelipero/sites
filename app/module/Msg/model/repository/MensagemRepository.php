<?php

class Msg_MensagemRepository extends AbstractRepository {

    public function __construct() {

        $this->nomeTb = "msg_mensagem";
        $this->nomeView = "v_mensagem_acao";
        $this->usualOrder = "data desc";
    }

}

?>