<?php

class Msg_Mensagem {

    private $id;
    private $conteudo;
    private $data;
    private $nova;
    private $acao_id;
    private $destinatario_id;
    private $remetente_id;
    private $extensao;

    function __construct($arg) {
        $this->id = $arg['id'];
        $this->conteudo = $arg['conteudo'];
        $this->data = $arg['data'];
        $this->nova = $arg['nova'];
        $this->acao_id = $arg['acao_id'];
        $this->remetente_id = $arg['remetente_id'];
        $this->destinatario_id = $arg['destinatario_id'];
        $this->extensao = $arg['extensao'];
    }
    
    public function getNomeArquivo() {
        return "msg_" . $this->id;
    }
    
    public function getAcao(){
        return Acao_AcaoMapper::getInstance()->findById($this->acao_id);
    }
    
    public function getRemetente() {
        return Adm_PessoaMapper::getInstance()->findById($this->remetente_id);
    }
    
    public function getDestinatario() {
        return Adm_PessoaMapper::getInstance()->findById($this->destinatario_id);
    }
    
    public function getRemetenteNome(){
        return $this->getRemetente()->getNome();
    }
    
    public function getDestinatarioNome(){
        return $this->getDestinatario()->getNome();
    }
    
    public function getTipo(){
        //retorna 1 se eu sou o remetente e 2 se eu sou o destinatario
        if ($GLOBALS['user']->getId() == $this->remetente_id) {
            //envia
            return 1;
        } else {
            //recebe
            return 2;
        }
        
    }
    
    public function getContato(){
        //com base no tipo retorna o objeto da Pessoa que não sou eu na msg
        
        $tipo = $this->getTipo();
        
        if ($tipo == 1) {
            return Adm_PessoaMapper::getInstance()->findById($this->destinatario_id);
        } else if($tipo == 2) {
            return Adm_PessoaMapper::getInstance()->findById($this->remetente_id);
        }
        
    }
    
    //Regular getters
    
    public function getId() {
        return $this->id;
    }

    public function getConteudo() {
        return $this->conteudo;
    }

    public function getData() {
        return $this->data;
    }

    public function getNova() {
        return $this->nova;
    }

    public function getAcao_id() {
        return $this->acao_id;
    }

    public function getDestinatario_id() {
        return $this->destinatario_id;
    }

    public function getRemetente_id() {
        return $this->remetente_id;
    }
    
    public function getExtensao() {
        return $this->extensao;
    }




}

?>