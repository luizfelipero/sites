<?php

class Msg_MensagemMapper extends AbstractMapper {

    private static $_instance = NULL;

    final public static function getInstance() {

        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function __construct() {
        $this->repository = new Msg_MensagemRepository();
        $this->necessary = array("conteudo");
        $this->modelName = "Msg_Mensagem";
    }

    public function insert($post, $file) {

        $post['data'] = date("Y-m-d H:i:s");
        $post['remetente_id'] = $GLOBALS['user']->getId();
//        print_r($file);
//        die();
        try {
            $post['extensao'] = strrchr($file['name'], '.');

            if ($file != "") {
                if ($file['size'] <= 2000000) {
                    $id = parent::insert($post);
                    Util_Documento::salvaArquivo($file, "msg", "msg_" . $id);
                } else {
                    throw new Exception("O arquivo possui tamanho maior do que o permitido.");
                }
            }
            parent::insert($post);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function lidaMsg($id) {

        try {
            $msg = $this->findById($id);
            if ($msg->getDestinatario_id() == $GLOBALS['user']->getId()) {
                //msg lida
                $arg['id'] = $id;
                $arg['nova'] = 0;
                parent::update($arg);
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getQtdNovas() {
        $arg[] = array("destinatario_id", "=", $GLOBALS['user']->getId());
        $arg[] = array("nova", "=", 1);
        return $this->count($arg);
    }

}

?>