<?php

class Msg_MensagemController extends AbstractController {

    public function IndexAction() {

        $sqlExtra['limit'] = 10;
        $sqlExtra['offset'] = $sqlExtra['limit'] * $_GET['index'];
        
        if ($_GET['remetente_id'] != "") {
            $arg[] = array("remetente_id", "=", $_GET['remetente_id']);
            $pagExtra['remetente_id'] = $_GET['remetente_id'];
        }
        
        $arg[] = array(
            array("destinatario_id", "=", $GLOBALS['user']->getId()),
            array("remetente_id", "=", $GLOBALS['user']->getId())
        );

        $pagArg['total'] = Msg_MensagemMapper::getInstance()->count($arg);
        $pagArg['limit'] = $sqlExtra['limit'];
        $pagArg['link'] = Util_Link::link("Msg", "Mensagem", "Index");
        $pagArg['index'] = $_GET['index'];

        $this->view->msgs = Msg_MensagemMapper::getInstance()->fetchAll($arg);
        $this->view->paginacao = new Util_Paginacao($pagArg, $pagExtra);
    }

    public function InserirAction() {

        if ($_POST['post'] == 1) {
            try {
                Msg_MensagemMapper::getInstance()->insert($_POST, $_FILES['arquivo']);
                Util_FlashMessage::write("Mensagem enviada com sucesso", "success");
                Util_Link::redirect("Msg", "Mensagem", "Index");
            } catch (Exception $e) {
                Util_FlashMessage::write(explode("+", $e->getMessage()), "danger");
                Util_Link::redirect("Msg", "Mensagem", "Inserir");
            }
        }
    }

    public function VisualizarAction() {
        Msg_MensagemMapper::getInstance()->lidaMsg($_GET['id']);
        $this->view->msgs = Msg_MensagemMapper::getInstance()->findById($_GET['id']);
    }

}

?>