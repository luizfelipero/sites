<?php

class Adm_AppController extends AbstractController {

    public function IndexAction() {

        $limit = 12;

        $sql_extra['limit'] = $limit;
        $sql_extra['offset'] = $_REQUEST['index'] * $limit;
        $sql_extra['order'] = "nome asc";

        $sql_arg[] = array("ativo", "=", 1);

        $pag_arg['total'] = Adm_AppMapper::getInstance()->count($sql_arg, "");
        $pag_arg['limit'] = $limit;
        $pag_arg['link'] = Util_Link::link("Adm", "App", "Index");
        $pag_arg['index'] = $_REQUEST['index'];

        $this->view->apps = Adm_AppMapper::getInstance()->fetchAll($sql_arg, $sql_extra);
        $this->view->paginacao = new Util_Paginacao($pag_arg);
    }
    
    public function AjudaAction(){
        
    }

}

?>
