<?php

class Adm_PessoaController extends AbstractController {

    public function IndexAction() {

        $limit = 12;

        $sql_extra['limit'] = $limit;
        $sql_extra['offset'] = $_REQUEST['index'] * $limit;
        $sql_extra['order'] = "nome asc";

        $sql_arg[] = array("ativo", "=", 1);

        $sql_arg[] = array("tipo", "!=", "99");
        $sql_arg[] = array("tipo", "!=", "98");

        if ($_GET['unidade_id'] != "") { //se vem de algum link de paginacao
            $unidade_id = $_GET['unidade_id'];
        } else { //se vem de algum filtro
            $unidade_id = Util_Utilidade::getUnidadeEspecifica($_GET['un_nivel1'], $_GET['un_nivel2'], $_GET['un_nivel3'], $_GET['un_nivel4'], $_GET['un_nivel5'], $_GET['un_nivel6']);
        }
        if ($unidade_id != "") {
            $sql_arg[] = array(
                array("filial_id", "=", $unidade_id),
                array("centro_id", "=", $unidade_id),
                array("diretoria_id", "=", $unidade_id),
                array("ugb_id", "=", $unidade_id),
                array("departamento_id", "=", $unidade_id),
                array("setor_id", "=", $unidade_id),
            );
            $pag_extra['unidade_id'] = $unidade_id;
        }

        if ($_GET['cargo_id'] != "" && $_GET['cargo_id'] != 0) {
            $sql_arg[] = array("cargo_id", "=", $_GET['cargo_id']);
            $pag_extra['cargo_id'] = $_GET['cargo_id'];
        }

        if ($_GET['nome'] != "") {
            $sql_arg[] = array("nome", "lk", $_GET['nome']);
            $pag_extra['nome'] = $_GET['nome'];
        }

        if ($_GET['inicial'] != "") {
            $nome = $_GET['inicial'] . "%";
            $sql_arg[] = array("nome", "lkd", $nome);
            $pag_extra['inicial'] = $_GET['inicial'];
        }

        $pag_arg['total'] = Adm_PessoaMapper::getInstance()->count($sql_arg, "");
        $this->view->total = $pag_arg['total'];
        $pag_arg['limit'] = $limit;
        $pag_arg['link'] = Util_Link::link("Adm", "Pessoa", "Index");
        $pag_arg['index'] = $_REQUEST['index'];

        $this->view->pessoas = Adm_PessoaMapper::getInstance()->fetchAll($sql_arg, $sql_extra);
        $this->view->paginacao = new Util_Paginacao($pag_arg, $pag_extra);
        $this->view->paginacaoLetras = new Util_PaginacaoLetras($pag_arg, $pag_extra);

        $this->view->pendencias = Adm_CargoHistoricoMapper::getInstance()->getNumeroPendencias();

        $this->view->cargos = Adm_CargoMapper::getInstance()->fetchAll();
    }

    public function CsvAction() {

        $this->view->setLayout("none");

        if ($_GET['tipo'] == "t") {
            $this->view->pessoas = Adm_PessoaMapper::getInstance()->fetchAll(array(
                array("telefone", "!=", ""),
                array("telefone", "!=", "-"),
                array("ativo", "=", 1),
                array("tipo", "!=", 98),
                array("tipo", "!=", 99)
            ));
            $this->view->nome = "Telefones";
        } else if ($_GET['tipo'] == "e") {
            $this->view->pessoas = Adm_PessoaMapper::getInstance()->fetchAll(array(
                array("email", "!=", ""),
                array("ativo", "=", 1),
                array("tipo", "!=", 98),
                array("tipo", "!=", 99)
            ));
            $this->view->nome = "Emails";
        }
    }

    public function MudancasAction() {

        $limit = 12;

        $sql_extra['limit'] = $limit;
        $sql_extra['offset'] = $_REQUEST['index'] * $limit;
        $sql_extra['order'] = "data desc";

        //$sql_arg[] = array("ativo", "=", 1);
        if ($_GET['unidade_id'] != "") {
            $sql_arg[] = array(
                array("filial_id", "=", $_GET['unidade_id']),
                array("centro_id", "=", $_GET['unidade_id']),
                array("diretoria_id", "=", $_GET['unidade_id']),
                array("ugb_id", "=", $_GET['unidade_id']),
                array("departamento_id", "=", $_GET['unidade_id']),
                array("setor_id", "=", $_GET['unidade_id'])
            );
            $pag_extra['unidade_id'] = $_GET['unidade_id'];
        }
        if ($_GET['colaborador_id'] != "") {
            $sql_arg[] = array("colaborador_id", "=", $_GET['colaborador_id']);
            $pag_extra['colaborador_id'] = $_GET['colaborador_id'];
        }
        if ($_GET['tipo'] != "") {
            $sql_arg[] = array("tipo", "=", $_GET['tipo']);
            $pag_extra['tipo'] = $_GET['tipo'];
        }
        if ($_GET['pendencia'] != "") {
            $sql_arg[] = array("sig", "=", $_GET['pendencia']);
            $pag_extra['pendencia'] = $_GET['pendencia'];
        }

        $pag_arg['total'] = Adm_CargoHistoricoMapper::getInstance()->count($sql_arg, "");
        $pag_arg['limit'] = $limit;
        $pag_arg['link'] = Util_Link::link("Adm", "Pessoa", "Mudancas");
        $pag_arg['index'] = $_GET['index'];

        $this->view->mudancas = Adm_CargoHistoricoMapper::getInstance()->fetchAll($sql_arg, $sql_extra);
        $this->view->paginacao = new Util_Paginacao($pag_arg, $pag_extra);
    }

    public function PendenciasAction() {

        $this->view->mudancas = Adm_CargoHistoricoMapper::getInstance()->getPendencias();
    }

    public function ContatoAction() {

        $limit = 12;

        $sql_extra['limit'] = $limit;
        $sql_extra['offset'] = $_REQUEST['index'] * $limit;
        $sql_extra['order'] = "nome asc";

        $sql_arg[] = array("ativo", "=", 1);
        $sql_arg[] = array("tipo", "in", "1,97");

        $this->view->exibicao = (isset($_GET['exibicao'])) ? $_GET['exibicao'] : "l";

        if ($_GET['unidade_id'] != "") { //se vem de algum link de paginacao
            $unidade_id = $_GET['unidade_id'];
        } else { //se vem de algum filtro
            $unidade_id = Util_Utilidade::getUnidadeEspecifica($_GET['un_nivel1'], $_GET['un_nivel2'], $_GET['un_nivel3'], $_GET['un_nivel4'], $_GET['un_nivel5'], $_GET['un_nivel6']);
        }
        if ($unidade_id != "") {
            $sql_arg[] = array(
                array("filial_id", "=", $unidade_id),
                array("centro_id", "=", $unidade_id),
                array("diretoria_id", "=", $unidade_id),
                array("ugb_id", "=", $unidade_id),
                array("departamento_id", "=", $unidade_id),
                array("setor_id", "=", $unidade_id),
            );
            $pag_extra['unidade_id'] = $unidade_id;
        }

        if ($_GET['cargo_id'] != "" && $_GET['cargo_id'] != 0) {
            $sql_arg[] = array("cargo_id", "=", $_GET['cargo_id']);
            $pag_extra['cargo_id'] = $_GET['cargo_id'];
        }
        if ($_GET['inicial'] != "") {
            $nome = $_GET['inicial'] . "%";
            $sql_arg[] = array("nome", "lkd", $nome);
            $pag_extra['inicial'] = $_GET['inicial'];
        }
        if ($_GET['nome'] != "") {
            $sql_arg[] = array("nome", "lk", $_GET['nome']);
            $pag_extra['nome'] = $_GET['nome'];
        }

        $pag_arg['total'] = Adm_PessoaMapper::getInstance()->count($sql_arg, "");
        $this->view->total = $pag_arg['total'];
        $pag_arg['limit'] = $limit;
        $pag_arg['link'] = Util_Link::link("Adm", "Pessoa", "Contato");
        $pag_arg['index'] = $_REQUEST['index'];

        $this->view->pessoas = Adm_PessoaMapper::getInstance()->fetchAll($sql_arg, $sql_extra);

        $pag_extra['exibicao'] = "l";
        $this->view->paginacaoLista = new Util_Paginacao($pag_arg, $pag_extra);
        $pag_extra['exibicao'] = "c";
        $this->view->paginacaoCartao = new Util_Paginacao($pag_arg, $pag_extra);

        $pag_extra['exibicao'] = "l";
        $this->view->paginacaoListaLetras = new Util_PaginacaoLetras($pag_arg, $pag_extra);
        $pag_extra['exibicao'] = "c";
        $this->view->paginacaoCartaoLetras = new Util_PaginacaoLetras($pag_arg, $pag_extra);

        $this->view->cargos = Adm_CargoMapper::getInstance()->fetchAll();

        $this->view->todasPessoas = Adm_PessoaMapper::getInstance()->fetchAll();
    }

    public function ContatoGrupoAction() {

        $this->view->emailsAF = Adm_PessoaMapper::getInstance()->selectColumn("email", array(
            array("ativo", "=", 1),
            array("email", "!=", ""),
            array("email", "gt", "a"),
            array("email", "lt", "g"),
        ));
        $this->view->emailsGL = Adm_PessoaMapper::getInstance()->selectColumn("email", array(
            array("ativo", "=", 1),
            array("email", "!=", ""),
            array("email", "gt", "g"),
            array("email", "lt", "m"),
        ));
        $this->view->emailsMR = Adm_PessoaMapper::getInstance()->selectColumn("email", array(
            array("ativo", "=", 1),
            array("email", "!=", ""),
            array("email", "gt", "m"),
            array("email", "lt", "s"),
        ));
        $this->view->emailsSZ = Adm_PessoaMapper::getInstance()->selectColumn("email", array(
            array("ativo", "=", 1),
            array("email", "!=", ""),
            array("email", "gt", "s")
        ));
    }

    public function UnidadesAction() {
        
    }

    public function VisualizarAction() {

        $this->view->pessoa = Adm_PessoaMapper::getInstance()->findById($_GET['id']);
        $this->view->tiposDocumento = Adm_PessoaDocumentoTipoMapper::getInstance()->fetchAll();
    }

    public function RealocarAction() {

        if ($_POST['post'] == 1) {
            try {
                Adm_PessoaMapper::getInstance()->realocar($_POST);
                Util_FlashMessage::write("Pessoa realocada com sucesso", "success");
                Util_Link::redirect("Adm", "Pessoa", "Visualizar", $_POST['id']);
                //Util_Link::redirect("Adm","Pessoa","Editar",$_POST['id']+1);
            } catch (Exception $e) {
                Util_FlashMessage::write(explode("+", $e->getMessage()), "danger");
                Util_Link::redirect("Adm", "Pessoa", "Realocar", $_POST['id']);
            }
        } else {
            $pessoa = Adm_PessoaMapper::getInstance()->findById($_GET['id']);
            $this->view->pessoa = $pessoa;

            //para diretos
            $this->view->diretos['unidades'] = Adm_UnidadeMapper::getInstance()->fetchAll(array(array("tipo_id", "=", 4), array("obra", "=", 1)));
            $this->view->diretos['cargos'] = Adm_CargoMapper::getInstance()->fetchAll(array(array("tipo", "=", 2), array("ativo", "=", 1)));

            //para indiretos
            //$this->view->indiretos['unidades'] = Adm_UnidadeMapper::getInstance()->fetchAll(array(array("nivel", "=", 1)));
            $this->view->indiretos['cargos'] = Adm_CargoMapper::getInstance()->fetchAll(array(array("tipo", "=", 1), array("ativo", "=", 1)));

            //diretos infra
            $this->view->diretosInfra['unidades'] = Adm_UnidadeMapper::getInstance()->fetchAll(array(array("tipo_id", "=", 4), array("obra", "=", 1), array("id", "!=", 7)));
            $this->view->diretosInfra['departamentos'] = Adm_UnidadeMapper::getInstance()->fetchAll(array(array(array("superior_id", "=", 116), array("superior_id", "=", 117))));
            $this->view->diretosInfra['cargos'] = Adm_CargoMapper::getInstance()->fetchAll(array(array("tipo", "=", 3), array("ativo", "=", 1)));
        }
    }

    public function DesligarAction() {

        try {
            Adm_PessoaMapper::getInstance()->realocar($_POST);
            Util_FlashMessage::write("Pessoa desligada com sucesso", "success");
            Util_Link::redirect("Adm", "Pessoa", "Visualizar", $_POST['id']);
            //Util_Link::redirect("Adm","Pessoa","Editar",$_POST['id']+1);
        } catch (Exception $e) {
            Util_FlashMessage::write(explode("+", $e->getMessage()), "danger");
            Util_Link::redirect("Adm", "Pessoa", "Realocar", $_POST['id']);
        }
    }

    public function EditarAction() {

        if ($_POST['post'] == 1) {
            try {
                Adm_PessoaMapper::getInstance()->alterar($_POST, $_FILES);
                Util_FlashMessage::write("Pessoa alterado com sucesso", "success");
                Util_Link::redirect("Adm", "Pessoa", "Visualizar", $_POST['id']);
                //Util_Link::redirect("Adm","Pessoa","Editar",$_POST['id']+1);
            } catch (Exception $e) {
                Util_FlashMessage::write(explode("+", $e->getMessage()), "danger");
                Util_Link::redirect("Adm", "Pessoa", "Editar", $_POST['id']);
            }
        } else {

            $this->view->pessoa = Adm_PessoaMapper::getInstance()->findById($_GET['id']);
        }
    }

    public function AcessoAction() {

        if ($_POST['post'] == 1) {
            try {
                Adm_PessoaMapper::getInstance()->acesso($_POST);
                Util_FlashMessage::write("Login criado/alterado com sucesso", "success");
                Util_Link::redirect("Adm", "Pessoa", "Pendencias");
            } catch (Exception $e) {
                Util_FlashMessage::write(explode("+", $e->getMessage()), "danger");

                Util_Link::redirect("Adm", "Pessoa", "Pendencias");
            }
        } else {
            $this->view->pessoa = Adm_PessoaMapper::getInstance()->findById($_GET['id']);
        }
    }

    public function EmailAction() {

        if ($_POST['post'] == 1) {
            try {
                Adm_PessoaMapper::getInstance()->email($_POST);
                Util_FlashMessage::write("Email salvo e informado com sucesso", "success");
                Util_Link::redirect("Adm", "Pessoa", "Pendencias");
            } catch (Exception $e) {
                Util_FlashMessage::write(explode("+", $e->getMessage()), "danger");
                Util_Link::redirect("Adm", "Pessoa", "Pendencias");
            }
        } else {
            $this->view->pessoa = Adm_PessoaMapper::getInstance()->findById($_GET['id']);
        }
    }

    public function CheckPendenciaAction() {

        try {
            Adm_CargoHistoricoMapper::getInstance()->checkPendencia($_GET['id']);
            Util_FlashMessage::write("Pendencia concluida com sucesso", "success");
            Util_Link::redirect("Adm", "Pessoa", "Pendencias");
        } catch (Exception $e) {
            Util_FlashMessage::write(explode("+", $e->getMessage()), "danger");
            Util_Link::redirect("Adm", "Pessoa", "Pendencias");
        }
    }

    public function CheckPendenciaSustAction() {

        try {
            Adm_CargoHistoricoMapper::getInstance()->checkPendenciaSust($_GET['id']);
            Util_FlashMessage::write("Pendencia concluida com sucesso", "success");
            Util_Link::redirect("Sust", "Exame", "PendenciaNovato");
        } catch (Exception $e) {
            Util_FlashMessage::write(explode("+", $e->getMessage()), "danger");
            Util_Link::redirect("Sust", "Exame", "PendenciaNovato");
        }
    }

    public function EditarSelfAction() {

        $id = $GLOBALS['user']->getId();

        if ($_POST['post'] == 1) {
            try {
                Adm_PessoaMapper::getInstance()->alterarSelf($_POST, $_FILES);
                Util_FlashMessage::write("Dados alterados com sucesso", "success");
                Util_Link::redirect("Index", "Index", "Home");
                //Util_Link::redirect("Adm","Pessoa","Editar",$_POST['id']+1);
            } catch (Exception $e) {
                Util_FlashMessage::write(explode("+", $e->getMessage()), "danger");
                Util_Link::redirect("Adm", "Pessoa", "EditarSelf");
            }
        } else {

            $this->view->pessoa = Adm_PessoaMapper::getInstance()->findById($id);
        }
    }

    public function MinutagemAction() {

        $limit = 12;

        $sql_extra['limit'] = $limit;
        $sql_extra['offset'] = $_REQUEST['index'] * $limit;
        $sql_extra['order'] = "nome asc";

        $sql_arg[] = array("ativo", "=", 1);
        $sql_arg[] = array("tipo", "=", 1);
        //$sql_arg[] = array("telefone", "!=", "");

        if ($_GET['unidade_id'] != "" && $_GET['unidade_id'] != 0) {
            $sql_arg[] = array("unidade_id", "=", $_GET['unidade_id']);
            $pag_extra['unidade_id'] = $_GET['unidade_id'];
        }
        if ($_GET['departamento_id'] != "" && $_GET['departamento_id'] != 0) {
            $sql_arg[] = array("departamento_id", "=", $_GET['departamento_id']);
            $pag_extra['departamento_id'] = $_GET['departamento_id'];
        }
        if ($_GET['setor_id'] != "" && $_GET['setor_id'] != 0) {
            $sql_arg[] = array("setor_id", "=", $_GET['setor_id']);
            $pag_extra['setor_id'] = $_GET['setor_id'];
        }
        if ($_GET['cargo_id'] != "" && $_GET['cargo_id'] != 0) {
            $sql_arg[] = array("cargo_id", "=", $_GET['cargo_id']);
            $pag_extra['cargo_id'] = $_GET['cargo_id'];
        }
        if ($_GET['inicial'] != "") {
            $nome = $_GET['inicial'] . "%";
            $sql_arg[] = array("nome", "lkd", $nome);
            $pag_extra['inicial'] = $_GET['inicial'];
        }

        $pag_arg['total'] = Adm_PessoaMapper::getInstance()->count($sql_arg, "");
        $pag_arg['limit'] = $limit;
        $pag_arg['link'] = Util_Link::link("Adm", "Pessoa", "Minutagem");
        $pag_arg['index'] = $_REQUEST['index'];

        $this->view->pessoas = Adm_PessoaMapper::getInstance()->fetchAll($sql_arg, $sql_extra);

        $this->view->paginacao = new Util_Paginacao($pag_arg, $pag_extra);
        $this->view->paginacaoLetras = new Util_PaginacaoLetras($pag_arg, $pag_extra);

        $this->view->unidades = Adm_UnidadeMapper::getInstance()->fetchAll(array(array("nivel", "=", 1)));
        $this->view->cargos = Adm_CargoMapper::getInstance()->fetchAll();
    }

    public function EditarMinutagemAction() {

        $id = $_REQUEST['id'];

        if ($_POST['post'] == 1) {
            try {
                Adm_PessoaMapper::getInstance()->save($_POST);
                Util_FlashMessage::write("Pessoa alterado com sucesso", "success");
                Util_Link::redirect("Adm", "Pessoa", "Minutagem");
            } catch (Exception $e) {
                Util_FlashMessage::write(explode("+", $e->getMessage()), "danger");
                Util_Link::redirect("Adm", "Pessoa", "Minutagem");
            }
        } else {

            $this->view->pessoa = Adm_PessoaMapper::getInstance()->findById($id);
        }
    }

    public function InserirAction() {

        if ($_POST['post'] == 1) {
            try {
                Adm_PessoaMapper::getInstance()->inserir($_POST, $_FILES);
                Util_FlashMessage::write("Pessoa inserida com sucesso", "success");
                Util_Link::redirect("Adm", "Pessoa", "Index");
            } catch (Exception $e) {
                Util_FlashMessage::write(explode("+", $e->getMessage()), "danger");
                Util_Link::redirect("Adm", "Pessoa", "Inserir");
            }
        } else {

            //para diretos
            $this->view->diretos['unidades'] = Adm_UnidadeMapper::getInstance()->fetchAll(array(array("tipo_id", "=", 4), array("obra", "=", 1)));
            $this->view->diretos['cargos'] = Adm_CargoMapper::getInstance()->fetchAll(array(array("tipo", "=", 2), array("ativo", "=", 1)));

            //para indiretos
            //$this->view->indiretos['unidades'] = Adm_UnidadeMapper::getInstance()->fetchAll(array(array("nivel", "=", 1)));
            $this->view->indiretos['cargos'] = Adm_CargoMapper::getInstance()->fetchAll(array(array("tipo", "=", 1), array("ativo", "=", 1)));

            //diretos infra
            $this->view->diretosInfra['cargos'] = Adm_CargoMapper::getInstance()->fetchAll(array(array("tipo", "=", 3), array("ativo", "=", 1)));
        }
    }

    public function ArquivarAction() {

        try {
            Adm_PessoaMapper::getInstance()->arquivar($_GET);
            Util_FlashMessage::write("Pessoa arquivada com sucesso", "success");
            Util_Link::redirect("Adm", "Pessoa", "Index");
        } catch (Exception $e) {
            Util_FlashMessage::write(explode("+", $e->getMessage()), "danger");
            Util_Link::redirect("Adm", "Pessoa", "Index");
        }
    }

    public function ListarJsonAction() {

        $arg[] = array("ativo", "=", 1);
        $arg[] = array("tipo", "=", 1);

        $term = rawurldecode($_GET['term']);
        if ($term != "")
            $arg[] = array("nome", "lk", $term);

        switch ($_GET['extra']) {
            case "lider":
                $lideres = Adm_CargoMapper::getInstance()->selectColumn("id", array(array("nivel", "gte", 6)));
                $lideres = implode(",", $lideres);
                $arg[] = array(array("cargo_id", "in", $lideres));
                break;
            case "outros":
                $arg[] = array("id", "!=", $GLOBALS['user']->getId());
                break;
        }

        $this->view->json = new Util_Json(Adm_PessoaMapper::getInstance()->fetchAll($arg));
    }

    public function ListarGeralJsonAction() {

        $arg[] = array("ativo", "=", 1);
        $arg[] = array("tipo", "!=", 9);

        $term = rawurldecode($_GET['term']);
        if ($term != "") {
            $arg[] = array("nome", "lk", $term);
        }

        $this->view->json = new Util_Json(Adm_PessoaMapper::getInstance()->fetchAll($arg));
    }

    public function ListarGeralInativoJsonAction() {

        $arg[] = array(
            array("ativo", "=", 1),
            array("dataSaida", "!=", "0000-00-00"),
            array("tipo", "!=", 9)
        );
        $term = rawurldecode($_GET['term']);
        if ($term != "") {
            $arg[] = array("nome", "lk", $term);
        }

        $this->view->json = new Util_Json(Adm_PessoaMapper::getInstance()->fetchAll($arg));
    }

    public function ListarInfoJsonAction() {

        $arg[] = array("ativo", "=", 1);
        $arg[] = array("tipo", "!=", 9);

        $term = rawurldecode($_GET['term']);
        if ($term != "") {
            $arg[] = array(
                array("nome", "lk", $term),
                array("apelido", "lk", $term)
            );
        }
        $pessoas = Adm_PessoaMapper::getInstance()->fetchAll($arg);
        if ($pessoas != "") {
            foreach ($pessoas as $pessoa) {
                $ret['id'] = $pessoa->getId();
                $ret['nome'] = $pessoa->getNome();
                $ret['cargo'] = $pessoa->getCargo()->getNome();
                $ret['unidade'] = $pessoa->getUnidadeEspecifica()->getNomeCompleto();

                $retorno[] = $ret;
            }
        } else {
            $retorno = array();
        }

        $this->view->json = json_encode($retorno);
    }

    public function ListarSubordinadosJsonAction() {
        $args[] = array("ativo", "=", 1);
        $args[] = array("tipo", "=", 1);

        $args[] = array("diretoria_id", "=", $GLOBALS['user']->getDiretoria_id());
        $args[] = array("nivelCargo", "lt", $GLOBALS['user']->getNivelCargo());

        $term = rawurldecode($_GET['term']);
        if ($term != "") {
            $args[] = array("nome", "lk", $term);
        }

        $this->view->json = new Util_Json(Adm_PessoaMapper::getInstance()->fetchAll($args));
    }

    public function checarCpfAction() {

        $arg[] = array("ativo", "=", "1");
        $arg[] = array("tipo", "!=", 9);
        $arg[] = array("departamento_id", "!=", 81);
        $this->view->pessoas = Adm_PessoaMapper::getInstance()->fetchAll($arg, array("order" => "unidade_id, nome"));
    }

    public function ListarNovoJsonAction() {
        $extra = explode(",", $_GET['extra']);

        //forçado, nunca parceiros e TV
        $args[] = array("tipo", "!=", "99");
        $args[] = array("tipo", "!=", "98");

        //Verifica se é inativo
        if (in_array("i", $extra)) {
            $args[] = array(
                array("ativo", "=", 1),
                array("dataSaida", "gte", Prod_CalendarioDiaMapper::getInstance()->removeMes(date("Y-m-d"), 2))
            );
        } else {
            $args[] = array("ativo", "=", 1);
        }
        //verifica se é subordinado
        if (in_array("s", $extra)) {
            $user = $GLOBALS['user'];
            $subordinados = $user->getSubordinadosIds();
            $args[] = array("id", "in", $subordinados);
        }

        //verifica se é indireto e parceiro
        if (in_array("d", $extra)) {
            $args[] = array(
                array("tipo", "in", "1,97"),
                array("login", "!=", "")
            );
        }

        //verifica se é self
        if (in_array("n", $extra)) {
            $user_id = $GLOBALS['user']->getId();
            $args[] = array("id", "!=", $user_id);
        }
        //verifica se é lider
        if (in_array("l", $extra)) {
            $lideres = Adm_CargoMapper::getInstance()->selectColumn("id", array(array("nivel", "gte", 6)));
            $lideres = implode(",", $lideres);
            $args[] = array(array("cargo_id", "in", $lideres));
        }

        //verifica se é da mesma unidade
        if (in_array("u", $extra)) {
            if ($GLOBALS['user']->getUnidadeEspecifica_id() == 4 || $GLOBALS['user']->getUnidadeEspecifica_id() == 7) {
                $unidadeEspecifica = array(4, 7);
            } else {
                $unidadeEspecifica = $GLOBALS['user']->getUnidadeEspecifica_id();
            }
            $args[] = array(
                array("centro_id", "in", $unidadeEspecifica),
                array("diretoria_id", "in", $unidadeEspecifica),
                array("ugb_id", "in", $unidadeEspecifica),
                array("departamento_id", "in", $unidadeEspecifica),
                array("setor_id", "in", $unidadeEspecifica)
            );
        }

        $term = rawurldecode($_GET['term']);
        if ($term != "") {
            $args[] = array("nome", "lk", $term);
        }
        $this->view->json = new Util_Json(Adm_PessoaMapper::getInstance()->fetchAll($args));
    }

    public function ListarSupervisorJsonAction() {
        if ($_GET['id'] != "") {
            if ($_GET['id'] == 4 || $_GET['id'] == 7) {
                $arg[] = array("ugb_id", "in", "4,7");
            } else {
                $arg[] = array("ugb_id", "=", $_GET['id']);
            }
            $arg[] = array("cargo_id", "=", 11);
            $arg[] = array("ativo", "=", 1);
        }
        $extra["order"] = "nome";
        $this->view->json = new Util_Json(Adm_PessoaMapper::getInstance()->fetchAll($arg, $extra), array("none" => 1));
    }

    public function GruposEmailAction() {
        
    }

    public function ShowSubordinadosAction() {

        set_time_limit(0);

        //allan version
        $pessoas = Adm_PessoaMapper::getInstance()->fetchAll(array(array("id", "in", "109,158,855,1238,1481,1524,1730,1666")));
        //diego version
        //$pessoas = Adm_PessoaMapper::getInstance()->fetchAll(array(array("id", "in", "195,247,924,943,1763")));

        foreach ($pessoas as $pessoa) {
            printf("<br>Para %s cargo(%s) nivel(%s) :<br>", $pessoa->getNome(), $pessoa->getCargo_id(), $pessoa->getNivelCargo());
            $subordinados = $pessoa->getSubordinados();

            foreach ($subordinados as $subordinado) {
                printf(" - [%s] %s<br>", $subordinado->getId(), $subordinado->getNome());
            }
        }

        die("<br><br>fim");
    }

    public function InsereChapaAction() {

        set_time_limit(0);

        if ($_POST['post'] == 1) {
            try {
                $erros = Adm_PessoaMapper::getInstance()->carregaDados();
                Util_FlashMessage::write("Arquivo processado com sucesso<br>" . $erros, "success");
                Util_Link::redirect("Adm", "Pessoa", "Index");
            } catch (Exception $e) {
                Util_FlashMessage::write(explode("+", $e->getMessage()), "danger");
                Util_Link::redirect("Adm", "Pessoa", "Index");
            }
        }
    }

    public function CrachaAction() {

        $this->view->setLayout("none");
        $this->view->colaborador = Adm_PessoaMapper::getInstance()->findById($_GET['id']);
    }

    public function TesteLiderAction() {
        $pessoa = Adm_PessoaMapper::getInstance()->findById($GLOBALS['user']->getId());
        $pessoa->getLider();
    }

    public function DesativaAction() {
//        $pessoas = Adm_PessoaMapper::getInstance()->fetchAll(array(
//            array("nome", "lk", "y")
//                ), array("limit" => 10));
//        Adm_PessoaMapper::getInstance()->inativaVarios($pessoas);
        //die();
        Adm_PessoaMapper::getInstance()->inativaVarios(array(
            array("nome", "lk", "y%")
                ), array("limit" => 10));
    }

}

?>
