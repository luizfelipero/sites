<?php

class Adm_PermissaoController extends AbstractController {

    public function IndexAction() {

        $this->view->permissoes = Adm_PermissaoMapper::getInstance()->fetchAll("", array("order" => "module asc, controller asc, action asc, unidade_id asc, cargo_id asc"));
        $this->view->unidades = Adm_UnidadeMapper::getInstance()->fetchAll(array(array("nivel", "=", 1)));
        $this->view->cargos = Adm_CargoMapper::getInstance()->fetchAll();
    }

    public function InserirAction() {

        try {
            Adm_PermissaoMapper::getInstance()->insert($_POST);
            Util_FlashMessage::write("Permissões inseridas com sucesso", "success");
            Util_Link::redirect("Adm", "Permissao", "Index");
        } catch (Exception $e) {
            Util_FlashMessage::write(explode("+", $e->getMessage()), "danger");
            Util_Link::redirect("Adm", "Permissao", "Index");
        }
    }

    public function ListarFilhasJsonAction() {

        $un = $_GET['id'];
        if ($un != "" && $un != "tg" && $un != "g") {
            $unidade = Adm_UnidadeMapper::getInstance()->findById($un);
            $subordinadas = $unidade->getSubordinadas();
            $this->view->json = new Util_Json($subordinadas, array("selecione" => 0, "none" => 0, "tg" => 0, "t" => 1));
        } else {
            $this->view->json = new Util_Json($subordinadas, array("selecione" => 0, "none" => 0, "tg" => 0, "t" => 1));
        }
    }

    public function RemoverAction() {

        try {

            Adm_PermissaoMapper::getInstance()->delete($_GET['id']);
            Util_FlashMessage::write("Permissão removida com sucesso", "success");
            Util_Link::redirect("Adm", "Permissao", "Index");
        } catch (Exception $e) {
            Util_FlashMessage::write(explode("+", $e->getMessage()), "danger");
            Util_Link::redirect("Adm", "Permissao", "Index");
        }
    }

}

?>
