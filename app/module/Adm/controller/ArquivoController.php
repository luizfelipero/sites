<?php

class Adm_ArquivoController extends AbstractController {

    public function ExibirAction() {

        $this->view->setLayout("none");
        $this->view->arquivo = Adm_ArquivoMapper::getInstance()->findById($_GET['id']);
    }

    public function DesativarModalAction() {
        $this->view->setLayout("none");

        $this->view->id = $_GET['id'];
    }

    public function DesativarAction() {
                
        $this->view->setLayout("none");
        //if ($_POST['post'] == 1) {
        try {
            Adm_ArquivoMapper::getInstance()->desativar($_GET['id']);
            echo "1";
        } catch (Exception $e) {
            echo "2";
        }
        //} else {
        //    $this->view->id = $_GET['id'];
        //}
    }

}

?>