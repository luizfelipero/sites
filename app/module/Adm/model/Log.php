<?php

class Adm_Log {

    private $id;
    private $module;
    private $controller;
    private $action;
    private $extras;
    private $usuario_id;
    private $dataAcesso;

    function __construct($arg) {
        $this->id = $arg['id'];
        $this->module = $arg['module'];
        $this->controller = $arg['controller'];
        $this->action = $arg['action'];
        $this->extras = $arg['extras'];
        $this->usuario_id = $arg['usuario_id'];
        $this->dataAcesso = $arg['dataAcesso'];
    }
    
    //Regular getters
    
    public function getId() {
        return $this->id;
    }

    public function getModule() {
        return $this->module;
    }

    public function getController() {
        return $this->controller;
    }

    public function getAction() {
        return $this->action;
    }

    public function getExtras() {
        return $this->extras;
    }

    public function getUsuario_id() {
        return $this->usuario_id;
    }

    public function getDataAcesso() {
        return $this->dataAcesso;
    }



}

?>