<?php

class Adm_Ausencia {

    private $id;
    private $colaborador_id;
    private $tipo;
    private $dataInicio;
    private $dataFim;
    private $texto;

    function __construct($arg) {
        $this->id = $arg['id'];
        $this->colaborador_id = $arg['colaborador_id'];
        $this->tipo = $arg['tipo'];
        $this->dataInicio = $arg['dataInicio'];
        $this->dataFim = $arg['dataFim'];
        $this->texto = $arg['texto'];
    }

    public function getNomeColaborador() {
        return Adm_PessoaMapper::getInstance()->findById($this->colaborador_id)->getNome();
    }

    //REGULAR GETTERS

    function getId() {
        return $this->id;
    }

    function getColaborador_id() {
        return $this->colaborador_id;
    }

    function getDataInicio() {
        return $this->dataInicio;
    }

    function getDataFim() {
        return $this->dataFim;
    }

    function getTexto() {
        return $this->texto;
    }

    function getTipo() {
        if ($this->tipo == 1) {
            return "Férias";
        } else if ($this->tipo == 2) {
            return "Avatar";
        } else if ($this->tipo == 3) {
            return "Folga";
        } else if ($this->tipo == 4) {
            return "Curso";
        } else if ($this->tipo == 5) {
            return "Atestado";
        } else if ($this->tipo == 6) {
            return "Outros";
        }else {
            return "Default";
        }
    }

}

?>