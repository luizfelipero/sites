<?php

class Adm_PessoaRepository extends AbstractRepository {

    public function __construct() {
        $this->nomeTb = "ger_pessoa";
        $this->nomeView = "v_ger_pessoa";
        $this->usualOrder = "nome asc";
        $this->print = "fetchAll";
    }

}
?>