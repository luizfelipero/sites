<?php

class Adm_Acesso {

    private $id;
    private $login;
    private $data;
    private $status;
    private $sig;

    function __construct($arg) {
        $this->id = $arg['id'];
        $this->login = $arg['login'];
        $this->data = $arg['data'];
        $this->status = $arg['status'];
        $this->sig = $arg['sig'];
    }
    
    public function getUsuario(){
        $arg[] = array("login", "=", $this->login);
        return Adm_PessoaMapper::getInstance()->find($arg);
    }

    public function getId() {
        return $this->id;
    }
    
    public function getLogin() {
        return $this->login;
    }

    public function getData() {
        return $this->data;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getSig() {
        return $this->sig;
    }

}

?>