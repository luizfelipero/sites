<?php

class Adm_Email {
    
    /*
     * Tipo
     * 1. Vendas
     * 2. Assistência
     */

    private $id;
    private $destinatario;
    private $assunto;
    private $mensagem;
    private $enviada;
    private $prioridade;
    private $dataAdd;
    private $replyto;
    private $dataEnvio;
    private $tipo;
    private $chave_id;

    function __construct($arg) {
        $this->id = $arg['id'];
        $this->destinatario = $arg['destinatario'];
        $this->assunto = $arg['assunto'];
        $this->mensagem = $arg['mensagem'];
        $this->enviada = $arg['enviada'];
        $this->prioridade = $arg['prioridade'];
        $this->dataAdd = $arg['dataAdd'];
        $this->replyto = $arg['replyto'];
        $this->dataEnvio = $arg['dataEnvio'];
        $this->tipo = $arg['dataEnvio'];
        $this->chave_id = $arg['dataEnvio'];
    }
    
    //Regular getters
    
    public function getId() {
        return $this->id;
    }

    public function getDestinatario() {
        return $this->destinatario;
    }

    public function getAssunto() {
        return $this->assunto;
    }

    public function getMensagem() {
        
        $mensagem = str_replace("&quot;", '"', $this->mensagem);
        
        return $mensagem;
    }

    public function getEnviada() {
        return $this->enviada;
    }

    public function getPrioridade() {
        return $this->prioridade;
    }

    public function getDataAdd() {
        return $this->dataAdd;
    }
    
    public function getReplyto() {
        return $this->replyto;
    }
    
    public function getDataEnvio() {
        return $this->dataEnvio;
    }
    
    function getTipo() {
        return $this->tipo;
    }

    function getChave_id() {
        return $this->chave_id;
    }

}

?>