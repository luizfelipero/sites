<?php

class Adm_Domain {

    private $id;
    private $nome;
    private $ativo;
    private $dataAdd;

    function __construct($arg) {

        $this->id = $arg['id'];
        $this->nome = $arg['nome'];
        $this->ativo = $arg['ativo'];
        $this->dataAdd = $arg['dataAdd'];
    }

    function getId() {
        return $this->id;
    }

    function getNome() {
        return $this->nome;
    }

    function getAtivo() {
        return $this->ativo;
    }

    function getDataAdd() {
        return $this->dataAdd;
    }

}

?>