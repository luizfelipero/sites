<?php

class Adm_Arquivo {

    private $id;
    private $tipo;
    private $chave_id;
    private $extensao;
    private $ativo;
    private $rotulo;
    private $data;
    private $base64;

    public function __construct($arg) {

        $this->id = $arg['id'];
        $this->tipo = $arg['tipo'];
        $this->chave_id = $arg['chave_id'];
        $this->extensao = $arg['extensao'];
        $this->ativo = $arg['ativo'];
        $this->rotulo = $arg['rotulo'];
        $this->data = $arg['data'];
        $this->base64 = $arg['base64'];
    }

    public function getUrl() {

        $id = $this->id;
        $tipo = $this->tipo;
        $chave_id = $this->chave_id;
        $extensao = $this->extensao;
        $url = "https://s3-sa-east-1.amazonaws.com/";
        $bucket = $GLOBALS['server']['bucket']; //setado no frontcontroller
        $dir = "/" . $tipo . "/";
        $caminho = $url . $bucket . $dir . $tipo . '_' . $chave_id . '_' . $id . $extensao;

        return $caminho;
    }

    public function getEmbed() {
        $embed = '<embed src="' . $this->getUrl() . '#toolbar=0&navpanes=0&scrollbar=0&zoom=scale" style="width:100%; height:1200px;"></embed>';
        return $embed;
    }

    public function getImg($style) {
        if (in_array($this->getExtensao(), array(".png", ".jpg", ".jpeg"))) {

            $img = '<img ' . $style . ' src="' . $this->getUrl() . '" />';
        }
        return $img;
    }

    public function getEndereco() {

        $exibir = array(".pdf", ".jpg", ".jpeg", ".png");
        
        if (in_array($this->extensao, $exibir)) {
            return Util_Link::link("Adm", "Arquivo", "Exibir", $this->id);
        } else {
            return $this->getUrl();
        }
    }

    //Regular getters

    public function getId() {
        return $this->id;
    }

    public function getTipo() {
        return $this->tipo;
    }

    public function getChave_id() {
        return $this->chave_id;
    }

    public function getExtensao() {
        return $this->extensao;
    }

    public function getAtivo() {
        return $this->ativo;
    }

    public function getRotulo() {
        return $this->rotulo;
    }

    public function getData() {
        return $this->data;
    }
    
    function getBase64() {
        return $this->base64;
    }

}

?>