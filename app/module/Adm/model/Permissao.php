<?php

class Adm_Permissao {

    private $id;
    private $tipoPessoa;
    private $unidade_id;
    private $module;
    private $controller;
    private $action;
    private $cargo_id;
    private $tipo;

    function __construct($arg) {
        $this->id = $arg['id'];
        $this->tipoPessoa = $arg['tipoPessoa'];
        $this->unidade_id = $arg['unidade_id'];
        $this->module = $arg['module'];
        $this->controller = $arg['controller'];
        $this->action = $arg['action'];
        $this->cargo_id = $arg['cargo_id'];
        $this->tipo = $arg['tipo'];
    }

    public function getUnidade() {
        return Adm_UnidadeMapper::getInstance()->findById($this->unidade_id);
    }

    public function getCargo() {
        return Adm_CargoMapper::getInstance()->findById($this->cargo_id);
    }

    public function getId() {
        return $this->id;
    }

    function getTipoPessoa() {
        return $this->tipoPessoa;
    }
    
    public function getUnidade_id() {
        return $this->unidade_id;
    }

    public function getModule() {
        return $this->module;
    }

    public function getController() {
        return $this->controller;
    }

    public function getAction() {
        return $this->action;
    }

    public function getCargo_id() {
        return $this->cargo_id;
    }

    public function getTipo() {
        return $this->tipo;
    }

}

?>
