<?php

class Adm_Arquivo {

    private $id;
    private $tipo;
    private $chave_id;
    private $extensao;
    private $ativo;
    private $rotulo;
    private $data;

    public function __construct($arg) {

        $this->id = $arg['id'];
        $this->tipo = $arg['tipo'];
        $this->chave_id = $arg['chave_id'];
        $this->extensao = $arg['extensao'];
        $this->ativo = $arg['ativo'];
        $this->rotulo = $arg['rotulo'];
        $this->data = $arg['data'];
    }

    public function getUrl() {

        $caminho = Adm_ArquivoMapper::getInstance()->getPasta($this->tipo);
        $caminho .= "/" . $this->tipo . "_" . $this->chave_id . "_" . $this->id . $this->extensao;
        return $caminho;
    }

    public function getUrlAction() {

        $caminho = '?m=Adm&c=Arquivo&a=Exibir&id=' . $this->id;
        return $caminho;
    }

    public function getUrlS3() {

        $id = $this->id;
        $tipo = $this->tipo;
        $chave_id = $this->chave_id;
        $extensao = $this->extensao;
//        print_r($extensao);die;
        $url = "https://s3.amazonaws.com/";
        $bucket = "vianaemourateste2";
        if (Adm_ArquivoMapper::getInstance()->getPastaS3($this->tipo) == "public/" . $tipo . "/") {
            $dir = "/public/" . $tipo . "//";
            $caminho = $url . $bucket . $dir . $tipo . '_' . $chave_id . '_' . $id . $extensao;
        } else {
            $dir = "/private/" . $tipo . "//";
            $caminho = $url . $bucket . $dir . $tipo . '_' . $chave_id . '_' . $id . $extensao;
        }

        return $caminho;
    }

    public function getEmbed() {

        $embed = '<embed src="?m=Adm&c=Arquivo&a=Exibir&id=' . $this->id . '#toolbar=0&navpanes=0&scrollbar=0&zoom=scale" style="width:100%; height:1200px;"></embed>';
        return $embed;
    }

    public function getEmbedS3() {
        $embed = '<embed src="' . $this->getUrlS3() . '#toolbar=0&navpanes=0&scrollbar=0&zoom=scale" style="width:100%; height:1200px;"></embed>';
        return $embed;
    }

    public function getImg() {
        if (in_array($this->getExtensao(), array(".png", ".jpg", ".jpeg"))) {
            //$img = '<img src="?m=Adm&c=Arquivo&a=Exibir&id=' . $this->id . '"  />';
            $img = '<img src="' . $this->getUrl() . '" />';
        }
        return $img;
    }

    public function getImgS3() {
        if (in_array($this->getExtensao(), array(".png", ".jpg", ".jpeg"))) {
            //$img = '<img src="?m=Adm&c=Arquivo&a=Exibir&id=' . $this->id . '"  />';
            $img = '<img src="' . $this->getUrlS3() . '" />';
        }
        return $img;
    }

    public function getId() {
        return $this->id;
    }

    public function getTipo() {
        return $this->tipo;
    }

    public function getChave_id() {
        return $this->chave_id;
    }

    public function getExtensao() {
        return $this->extensao;
    }

    public function getAtivo() {
        return $this->ativo;
    }

    public function getRotulo() {
        return $this->rotulo;
    }

    public function getData() {
        return $this->data;
    }

}

?>