<?php

class Adm_LogMapper extends AbstractMapper {

    private static $_instance = NULL;

    final public static function getInstance() {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function __construct() {
        $this->repository = new Adm_LogRepository();
        $this->necessary = array();
        $this->modelName = "Adm_Log";
    }

    public function insert($post) {
        if ($post['module'] != "Index" && $post['action'] != "SetQtdAcoes" && $post['action'] != "UltimaPagina") {
            if ($post['module'] != "Serv") {
                $post['usuario_id'] = $GLOBALS['user']->getId();
            }

            $extras = "";

            foreach ($post['get'] as $chave => $valor) {
                //se for diferente de mca faz if
                if ($chave != "m" && $chave != "c" & $chave != "a") {
                    $extras .= "&" . $chave . "=" . $valor;
                }
            }
            $post['extras'] = $extras;

            parent::insert($post);
        }
    }

    public function getUltimaPagina() {
        $usuario_id = $GLOBALS['user']->getId();

        $arg[] = array("usuario_id", "=", $usuario_id);
        $arg[] = array("action", "!=", "UltimaPagina");
        $extra['limit'] = 1;
        $extra['offset'] = 1;
        $extra['order'] = "id desc";
        $ultimo_id = $this->selectColumnDirect("id", $arg, $extra);

        //print_r($ultimo_id); die();

        $ultimaPagina = $this->findById($ultimo_id);

        //print_r($ultimaPagina);
        //die();

        return $ultimaPagina;
    }

}

?>