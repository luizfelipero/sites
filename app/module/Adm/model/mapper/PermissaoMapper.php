<?php

class Adm_PermissaoMapper extends AbstractMapper {

    private static $_instance = NULL;

    final public static function getInstance() {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function __construct() {
        $this->repository = new Adm_PermissaoRepository();
        $this->necessary = array();
        $this->modelName = "Adm_Permissao";
    }

    public function autorizado($domain, $module, $controller, $action) {

        //se o modulo for index retorna true pq index não tem restrição
        if (($GLOBALS['user'] instanceof Adm_Pessoa && $GLOBALS['user']->getDepartamento_id() == 76) || ($module == "Index" || $module == "Serv") || (strpos($action, "Json") !== false) || (strpos($action, "Html") !== false)) {

            return true;
        } else if ($GLOBALS['user']->getTipo() > 90) {

            $arg[0][] = array("domain", "=", $domain);
            $arg[0][] = array("domain", "=", "_");
            $arg[1][] = array("module", "=", $module);
            $arg[1][] = array("module", "=", "_");
            $arg[2][] = array("controller", "=", $controller);
            $arg[2][] = array("controller", "=", "_");
            $arg[3][] = array("action", "=", $action);
            $arg[3][] = array("action", "=", "_");
            $arg[4][] = array("tipoPessoa", "=", $GLOBALS['user']->getTipo());

            //procura a permissao mais especifica
            $permissao = parent::find($arg, array("order" => "domain asc, module asc, controller asc, action asc, tipo asc"));

            //não existe permissao?
            if ($permissao == "") {
                return false;
                //permissao proibe?
            } else if ($permissao->getTipo() == 1) {
                return false;
                //permissao autoriza?
            } else if ($permissao->getTipo() == 0) {
                return true;
                //previne erros
            } else {
                return false;
            }
        } else { //regular users
            $arg[0][] = array("domain", "=", $domain);
            $arg[0][] = array("domain", "=", "_");
            $arg[1][] = array("module", "=", $module);
            $arg[1][] = array("module", "=", "_");
            $arg[2][] = array("controller", "=", $controller);
            $arg[2][] = array("controller", "=", "_");
            $arg[3][] = array("action", "=", $action);
            $arg[3][] = array("action", "=", "_");
            $arg[4][] = array("tipoPessoa", "lt", 90);

            //procura a permissao mais especifica
            $permissao = parent::find($arg, array("order" => "module asc, controller asc, action asc, tipo asc"));

            //não existe permissao?
            if ($permissao == "")
                return false;
            //permissao proibe?
            else if ($permissao->getTipo() == 1)
                return false;
            //permissao autoriza?
            else if ($permissao->getTipo() == 0)
                return true;
            //previne erros
            else
                return false;
        }
    }

    public function insert($post) {

        try {
            for ($i = 0; $i < 10; $i++) {

//                if ($post["module_" . $i] != "") {
                if ($post["domain_" . $i] != "") {
                    $arg['domain'] = $post["domain_" . $i];
                    $arg['module'] = $post["module_" . $i];
                    $arg['controller'] = $post["controller_" . $i];
                    $arg['action'] = $post["action_" . $i];
                    $arg['tipo'] = $post["tipo_" . $i];

                    parent::insert($arg);
                }
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

}

?>