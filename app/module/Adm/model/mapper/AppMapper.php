<?php

class Adm_AppMapper extends AbstractMapper {

    private static $_instance = NULL;

    final public static function getInstance() {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function __construct() {
        $this->repository = new Adm_AppRepository();
        $this->necessary = array("nome", "versao", "versaoConteudo");
        $this->modelName = "Adm_App";
    }

    public function save($post) {

        try {
            if ($post['id'] != "") {
                parent::update($post);
            } else {
                parent::insert($post);
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

}

?>