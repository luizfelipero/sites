<?php

class Adm_AcessoMapper extends AbstractMapper {

    private static $_instance = NULL;

    final public static function getInstance() {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function __construct() {
        $this->repository = new Adm_AcessoRepository();
        $this->necessary = array();
        $this->modelName = "Adm_Acesso";
    }

    public function insereSucesso($login, $sig = 0) {
        $arg['login'] = $login;
        $arg['data'] = date("Y-m-d H:i:s");
        $arg['status'] = 1;
        $arg['sig'] = $sig;

        $this->insert($arg);
    }

    public function insereFalha($login) {

        $login = trim($login);
        if ($login != "") {
            $arg['login'] = $login;
            $arg['data'] = date("Y-m-d H:i:s");
            $arg['status'] = 2;

            $this->insert($arg);
        }
    }

    public function insereBloqueio($login) {
        $login = trim($login);
        if ($login != "") {
            $arg['login'] = $login;
            $arg['data'] = date("Y-m-d H:i:s");
            $arg['status'] = 3;

            if ($this->getQtdBloqueio($login) == 2) {
                $this->insert($arg);
                $arg['status'] = 4;
                $this->insert($arg);
            } else {
                $this->insert($arg);
            }
        }
    }

    public function getQtdFalhas($login) {
        $arg = array(
            array("login", "=", $login),
            array(
                array("status", "=", 1),
                array("status", "=", 3)
            )
        );
        $ultimoId = $this->getMax("id", $arg);

        $arg = array(
            array("login", "=", $login),
            array("status", "=", 2),
            array("id", "gt", $ultimoId)
        );
        return $this->count($arg);
    }

    public function getQtdBloqueio($login) {
        $dataLimite = time() - (24 * 60 * 60);
        $dataLimite = date("Y-m-d H:i:s", $dataLimite);

        $arg = array(
            array("login", "=", $login),
            array("status", "=", 3),
            array("data", "gt", $dataLimite)
        );

        return $this->count($arg);
    }

    public function estaBloqueado($login) {
        $dataLimite = time() - (3 * 60);
        $dataLimite = date("Y-m-d H:i:s", $dataLimite);

        $arg = array(
            array("login", "=", $login),
            array("status", "=", 3),
            array("data", "gt", $dataLimite)
        );
        if ($this->count($arg) == 0) {
            return false;
        } else {
            return true;
        }
    }

    public function estaSuperBloqueado($login) {
        $arg = array(
            array("login", "=", $login),
            array("status", "=", 4)
        );
        if ($this->count($arg) == 0) {
            return false;
        } else {
            return true;
        }
    }

}

?>