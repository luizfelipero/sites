<?php

class Adm_PessoaMapper extends AbstractMapper {

    private static $_instance = NULL;

    final public static function getInstance() {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function __construct() {
        $this->repository = new Adm_PessoaRepository();
        $this->necessary = array("nome");
        $this->modelName = "Adm_Pessoa";
    }

    public function geraSenha() {

        $ret = chr(rand(97, 122));
        $ret .= chr(rand(97, 122));
        $ret .= rand(0, 9);
        $ret .= rand(0, 9);
        $ret .= chr(rand(97, 122));
        $ret .= chr(rand(97, 122));
        $ret .= rand(0, 9);
        $ret .= rand(0, 9);

        return $ret;
    }

    public function acesso($post) {
//necessario: id, login

        $pessoa = $this->findById($post['id']);
//não necessario porque o funcionario pode nao ter email pessoal
//if ($pessoa->getEmailPessoal() == "")
//    throw new Exception("Primeiro preencha um e-mail pessoal");

        $post['login'] = trim($post['login']);
        $post['login'] = strtolower($post['login']);
        if ($post['login'] == "")
            throw new Exception("Escolha um login");

        if ($this->count(array(array("login", "=", $post['login']), array("id", "!=", $post['id']), array("ativo", "=", 1))) > 0)
            throw new Exception("Login já está em uso");


        if ($post['senha'] != "") {
            $this->senhaValida($post['senha'], $post['csenha'], $post['id']);
            $senha = $post['senha'];
        } else {
            $senha = $this->geraSenha();
        }

        $arg['senha'] = md5($senha);
        $arg['mudancaSenha'] = date("Y-m-d");

        $arg['id'] = $post['id'];
        $arg['login'] = $post['login'];

        $this->update($arg);

//        if ($pessoa->getEmail() != "")
//            $arg2['destinatario'] = $pessoa->getEmail();
//        else

        $arg2['destinatario'] = $pessoa->getEmailPessoal();

        $arg2['mensagem'] = "Seja bem vindo à Viana & Moura!<br><br>Login: " . $post['login'] . "<br>Senha: " . $senha . "<br><br>Acesse extranet.vianaemoura.com.br<br><b>É aconselhável mudar sua senha para maior segurança.</b>";
        $arg2['assunto'] = "Viana & Moura - Extranet";
        $arg2['prioridade'] = 3;
        Adm_EmailMapper::getInstance()->insert($arg2);
    }

    public function senhaValida($senha, $csenha, $colaborador_id) {

        if ($senha != $csenha) {
            throw new Exception("As senhas devem ser iguais");
        }
        $senhaAntiga = Adm_PessoaMapper::getInstance()->selectColumnDirect("senha", array(array("id", "=", $colaborador_id)));
        if (md5($senha) == $senhaAntiga) {
            throw new Exception("A senha não pode ser igual a anterior");
        }
        if ((!preg_match('/[a-z]+/', $senha) && !preg_match('/[A-Z]+/', $senha)) || !preg_match('/[0-9]+/', $senha)) {
            throw new Exception("A senha deve ter no mínimo 8 caracteres, com ao menos 1 letra e 1 número");
        }
        if (strlen($senha) < 8) {
            throw new Exception("A senha deve ter no mínimo 8 caracteres, com ao menos 1 letra e 1 número");
        }
    }

    public function realocar($post) {

        try {

            if ($post['operacao'] == "")
                throw new Exception("Selecione um tipo de Operação");
            if ($post['data'] == "")
                throw new Exception("Preencha a data");
//            if ($post['operacao'] == 3) {
//                if (Prod_CalendarioDiaMapper::getInstance()->getDiferencaUteis(date("Y-m-d"), Util_Utilidade::formataData($post['data'])) > 35) {
//                    throw new Exception("Data não pode ser antes de 35 dias de hoje para casos de Promoção");
//                }
//            }
            //normal 5 dias
//            if ($post['operacao'] != 3 && Prod_CalendarioDiaMapper::getInstance()->getDiferencaUteis(date("Y-m-d"), Util_Utilidade::formataData($post['data'])) > 5)
//                throw new Exception("Data não pode ser antes de 5 dias de hoje");
            //exceção 10 dias
//            if ($post['operacao'] != 3 && Prod_CalendarioDiaMapper::getInstance()->getDiferencaUteis(date("Y-m-d"), Util_Utilidade::formataData($post['data'])) > 10)
//                throw new Exception("Data não pode ser antes de 10 dias de hoje");
            if (Adm_CargoHistoricoMapper::getInstance()->checaImpedimentoAlocacao($post['data']))
                throw new Exception("Ocorreu um erro, a data que você quer fazer a mudança já está com o mês fechado para o custo. Confira a data ou entre em contato com SIG");
            if (Adm_CargoHistoricoMapper::getInstance()->checaImpedimento($post['id'], Util_Utilidade::formataData($post['data'])))
                throw new Exception("Ocorreu um erro, esse colaborador já possui uma mudança realizada nesta data ou após. Confira a data ou entre em contato com SIG");


            if ($post['operacao'] == 5) { //demissao
//coloca no historico a demissao
                $argCargo['colaborador_id'] = $post['id'];
                $argCargo['operacao'] = 5;
                $argCargo['data'] = Util_Utilidade::formataData($post['data']);
                $argCargo['dataCadastro'] = date("Y-m-d");
                $argCargo['aviso'] = $post['aviso'];
                $argCargo['turno'] = $post['turno'];
                $argCargo['tipo'] = $post['tipo'];
                $argCargo['cargo_id'] = $post['cargo_id'];
                $argCargo['motivoDemissao'] = $post['motivoDemissao'];
                $argCargo['filial_id'] = $post['filial_id'];
                $argCargo['centro_id'] = $post['centro_id'];
                $argCargo['diretoria_id'] = $post['diretoria_id'];
                $argCargo['ugb_id'] = $post['ugb_id'];
                $argCargo['departamento_id'] = $post['departamento_id'];
                $argCargo['setor_id'] = $post['setor_id'];


                Adm_CargoHistoricoMapper::getInstance()->save($argCargo);

//altera a pessoa
                $arg['dataSaida'] = Util_Utilidade::formataData($post['data']);
                $arg['turnoSaida'] = $post['turno'];
                $arg['ativo'] = 0;
                $arg['id'] = $post['id'];

                $this->update($arg);

                if ($post['motivoDemissao'] == 4) {
                    Ppr_PoupancaMapper::getInstance()->justaCausa($post['id'], $post['data'], $post['tipo']);
                }

//remove as alocações feitas nessa pessoa depois da data do desligamento
//                $aloc[] = array("colaborador_id", "=", $post['id']);
//                $aloc[] = array("dataTurno", "gt", ($argCargo['data'] . "-2"));
//                Aloc_AlocacaoColaboradorMapper::getInstance()->deleteVarios(
//                        Aloc_AlocacaoColaboradorMapper::getInstance()->fetchAll($aloc)
//                );
            } else {
//cadastro ou ateração
//args comuns de CargoHistorico e de Pessoa
                $arg['tipo'] = $post['tipo'];
                $arg['cargo_id'] = $post[$indice];
//args só de CargoHistorico
                $arg['data'] = Util_Utilidade::formataData($post['data']);
                $arg['dataCadastro'] = date("Y-m-d");
                $arg['colaborador_id'] = $post['id'];
                $arg['operacao'] = $post['operacao'];
                $arg['aviso'] = $post['aviso'];

                if ($arg['tipo'] == 1) {  //indireto
                    $arg['cargo_id'] = $post['cargo_id_1'];
                    $arg['filial_id'] = 5;
                    $arg['centro_id'] = $post['un_nivel2'];
                    if ($arg['centro_id'] == 1) { //EC
                        if ($post['un_nivel3'] == 50 || $post['un_nivel3'] == 111) { //nivel 3 é uma diretoria
                            $arg['diretoria_id'] = $post['un_nivel3'];
                            $arg['ugb_id'] = 0;
                            $arg['departamento_id'] = $post['un_nivel4'];
                            $arg['setor_id'] = $post['un_nivel5'];
                        } else {
                            $arg['diretoria_id'] = 0;
                            $arg['ugb_id'] = 0;
                            $arg['departamento_id'] = $post['un_nivel3'];
                            $arg['setor_id'] = $post['un_nivel4'];
                        }
                    } else if ($arg['centro_id'] == 134) { //Obra
                        $arg['diretoria_id'] = $post['un_nivel3'];
                        $arg['ugb_id'] = $post['un_nivel4'];
                        $arg['departamento_id'] = $post['un_nivel5'];
                        $arg['setor_id'] = $post['un_nivel6'];
                    }
                    if ($arg['centro_id'] != 257) {
                        if ($arg['diretoria_id'] == 0 && $arg['departamento_id'] == 0) {
                            throw new Exception("Você deve especificar pelo menos a diretoria ou o departamento de um Indireto");
                        }
                    }
                } else if ($arg['tipo'] == 2) { //direto obra
                    $arg['cargo_id'] = $post['cargo_id_2'];
                    $arg['filial_id'] = 5;
                    $arg['centro_id'] = 134;
                    $arg['diretoria_id'] = 112;
                    $arg['ugb_id'] = $post['unidade_id_2'];
                    $arg['departamento_id'] = 0;
                    $arg['setor_id'] = 0;
                    if ($arg['ugb_id'] == "" || $arg['ugb_id'] == 0) {
                        throw new Exception("Você deve especificar a UGB de um Direto");
                    }
                } else if ($arg['tipo'] == 3) { //direto infra
                    $arg['cargo_id'] = $post['cargo_id_3'];
                    $arg['filial_id'] = 5;
                    $arg['centro_id'] = 134;
                    $arg['diretoria_id'] = 112;
                    $arg['ugb_id'] = $post['unidade_id_3'];
//                    $arg['departamento_id'] = Adm_UnidadeMapper::getInstance()->findById($post['setor_id_3'])->getSuperior_id();
//                    $arg['setor_id'] = $post['setor_id_3'];
                }

                if ($arg['cargo_id'] == "" || $arg['cargo_id'] == 0) {
                    throw new Exception("Você deve especificar o cargo");
                }

                $arg['prazoExperiencia'] = $this->getPrazoExperiencia($arg['data'], $arg['operacao'], $arg['cargo_id']);

                if ($arg['operacao'] == 1) {
                    $postTermino['inicio'] = $post['data'];
                    $postTermino['colaborador_id'] = $arg['colaborador_id'];
                    $postTermino['cargo_id'] = $arg['cargo_id'];
                    $postTermino['operacao'] = $arg['operacao'];
                    $postTermino['pendente'] = 1;
                    Mat_TerminoExperienciaMapper::getInstance()->insert($postTermino);
                }

                if ($post['oculto'] != 1) {
                    Adm_CargoHistoricoMapper::getInstance()->save($arg);
                }

                unset($arg['colaborador_id']);
                unset($arg['data']);
                unset($arg['dataCadastro']);
                unset($arg['periodoExperiencia']);
                unset($arg['operacao']);
                unset($arg['aviso']);

                //args só de Pessoa
                $arg['id'] = $post['id'];

                $this->update($arg);
                Mat_TerminoExperienciaMapper::getInstance()->updateUnidade($arg['id']);
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    protected function getDepartamentoObra_id($dep, $ugb) {
        $arg[] = array("nome", "lk", $dep);
        $arg[] = array("superior_id", "=", $ugb);
        $arg[] = array("tipo_id", "=", 5);
        $obj = Adm_UnidadeMapper::getInstance()->find($arg);
        if ($obj != "") {
            return $obj->getId();
        }
    }

    public function getPrazoExperiencia($data, $operacao, $cargo_id, $ordem = "") {

        //ordem é o tipo de término, primeiro ou segundo
        //se for o primeiro, é metade do prazo do segundo
        //o prazo tá (x - 1) pra poder contar com o dia de hoje

        $cargo = Adm_CargoMapper::getInstance()->findById($cargo_id);

        if ($operacao == 1 || $operacao == 2) { //admissao ou efetivacao
            if ($cargo->getPai_id() == 72 || $cargo->getPai_id() == 73) { //semi-qualificados
                if ($ordem == 1) {
                    $prazo = 30 - 1;
                } else {
                    $prazo = 60 - 1;
                }
            } else {
                if ($ordem == 1) {
                    $prazo = 45 - 1;
                } else {
                    $prazo = 90 - 1;
                }
            }
        } else if ($operacao == 3 || $operacao == 4) { //transferencia ou promoção
            if ($cargo != "") {
                if ($cargo->getPai_id() == 72 || $cargo->getPai_id() == 73) { //semi-qualificados
                    if ($ordem == 1) {
                        $prazo = 15 - 1;
                    } else {
                        $prazo = 30 - 1;
                    }
                } else {
                    if ($ordem == 1) {
                        $prazo = 22 - 1;
                    } else {
                        $prazo = 45 - 1;
                    }
                }
            } else {
                if ($ordem == 1) {
                    $prazo = 22 - 1;
                } else {
                    $prazo = 45 - 1;
                }
            }
        }

        return Prod_CalendarioDiaMapper::getInstance()->somaDiasCorrido($data, $prazo);
    }

    public function inserir($post, $files) {

        try {

            $arg['cpf'] = str_replace("-", "", $post['cpf']);
            $arg['cpf'] = str_replace(".", "", $arg['cpf']);

            if ($post['operacao'] != 3 && Prod_CalendarioDiaMapper::getInstance()->getDiferencaUteis(date("Y-m-d"), Util_Utilidade::formataData($post['data'])) > 5) {
                throw new Exception("Data não pode ser antes de 5 dias de hoje");
            }

//essa validação tb é feita na hora de realocar, mas é preciso ser feita antes também
//para garantir que o usuário só é inserido se seguir os criterios
            if ($post['tipo'] == 1) {
                if ($post['un_nivel3'] == "")
                    throw new Exception("É preciso selecionar uma unidade");
                if ($post['cargo_id_1'] == "")
                    throw new Exception("É preciso selecionar um cargo");
            } elseif ($post['tipo'] == 2) {
                if ($post['unidade_id_2'] == "")
                    throw new Exception("É preciso selecionar uma unidade");
                if ($post['cargo_id_2'] == "")
                    throw new Exception("É preciso selecionar um cargo");
            } elseif ($post['tipo'] == 3) {
                if ($post['unidade_id_3'] == "")
                    throw new Exception("É preciso selecionar uma unidade");
                if ($post['cargo_id_3'] == "")
                    throw new Exception("É preciso selecionar um cargo");
            }

            if ($arg['cpf'] == "")
                throw new Exception("Preencha um CPF");
            if (Adm_PessoaMapper::getInstance()->find(array(array("cpf", "=", $arg['cpf']), array("ativo", "=", 1))) != "")
                throw new Exception("Já existe uma pessoa ativa cadastrada com esse CPF");
            if (!Util_Utilidade::cpfValido($arg['cpf']))
                throw new Exception("CPF Inválido");

            if ($post['email'] != "")
                $arg['email'] = $post['email'];
            
            if ($post['skype'] != "")
                $arg['skype'] = $post['skype'];
            
            if ($post['chapa'] != "")
                $arg['chapa'] = $post['chapa'];

            $arg['emailPessoal'] = $post['emailPessoal'];


            $arg['nome'] = $post['nome'];
            $arg['apelido'] = $post['apelido'];
            $arg['email'] = $post['email'];

            $arg['telefone'] = Util_Utilidade::formataTelefone($post['telefone']);
            $arg['telefonePessoal'] = Util_Utilidade::formataTelefone($post['telefonePessoal']);
            $arg['ativo'] = 1;


            $arg['dataCadastro'] = date("Y-m-d");
            $arg['dataAdmissao'] = Util_Utilidade::formataData($post['data']);
            $arg['diaAniversario'] = $post['diaAniversario'];
            $arg['mesAniversario'] = $post['mesAniversario'];

            $post['id'] = $this->save($arg, $files);
            $post['data'] = $arg['dataAdmissao'];
            $this->realocar($post);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function alterar($post, $files) {

        try {
            if ($post['cpf'] != "") {
                $arg['cpf'] = str_replace("-", "", $post['cpf']);
                $arg['cpf'] = str_replace(".", "", $arg['cpf']);
            }

            if ($post['cpf'] == "")
                throw new Exception("Preencha um CPF");
            if ($post['dataAdmissao'] == "")
                throw new Exception("Preencha uma data de Admissão");

            if ($post['email'] != "")
                $arg['email'] = $post['email'];

            $arg['nome'] = $post['nome'];
            $arg['apelido'] = $post['apelido'];
            $arg['emailPessoal'] = $post['emailPessoal'];
            $arg['telefone'] = Util_Utilidade::formataTelefone($post['telefone']);
            $arg['telefonePessoal'] = Util_Utilidade::formataTelefone($post['telefonePessoal']);
            $arg['ativo'] = 1;
            $arg['diaAniversario'] = $post['diaAniversario'];
            $arg['mesAniversario'] = $post['mesAniversario'];
            $arg['skype'] = $post['skype'];
            $arg['chapa'] = $post['chapa'];

            if ($post['dataAdmissao'] != "") {
                $arg['dataAdmissao'] = Util_Utilidade::formataData($post['dataAdmissao']);
            }

            $arg['id'] = $post['id'];
            $this->save($arg, $files);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function alterarSelf($post, $files) {

        try {

            $arg['nome'] = $post['nome'];
            $arg['apelido'] = $post['apelido'];
            $arg['emailPessoal'] = $post['emailPessoal'];
            $arg['telefone'] = Util_Utilidade::formataTelefone($post['telefone']);
            $arg['telefonePessoal'] = Util_Utilidade::formataTelefone($post['telefonePessoal']);
            $arg['ativo'] = 1;
            $arg['diaAniversario'] = $post['diaAniversario'];
            $arg['mesAniversario'] = $post['mesAniversario'];
            if (isset($post['wapp'])) {
                $arg['wapp'] = $post['wapp'];
            } else {
                $arg['wapp'] = 0;
            }
            $arg['mesAniversario'] = $post['mesAniversario'];
            $arg['skype'] = $post['skype'];
            $arg['chapa'] = $post['chapa'];

            if ($post['senha'] != "") {
                $this->senhaValida($post['senha'], $post['csenha'], $post['id']);

                $arg['senha'] = md5($post['senha']);
                $arg['mudancaSenha'] = date("Y-m-d");
            }


            $arg['id'] = $post['id'];
            $this->save($arg, $files);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function email($post) {

        try {
            $arg['id'] = $post['id'];
            $arg['email'] = $post['email'];
            $arg['emailPessoal'] = $post['emailPessoal'];

            $this->update($arg);
            $pessoa = $this->findById($post['id']);
            $mensagem = "Seguem seus dados para o seu e-mail corporativo.<br><br>E-mail: " . $post['email'] . "<br>Senha: " . $post['senha'] . "<br><br>Acesse webmail.vianaemoura.com.br<br><b>É aconselhável mudar sua senha para maior segurança.</b>";
            $assunto = "Viana & Moura - E-mail";
            Adm_EmailMapper::getInstance()->insert(array(
                "destinatario" => $pessoa->getEmailPessoal(),
                "assunto" => $assunto,
                "mensagem" => $mensagem,
                "prioridade" => 3
            ));
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function save($post, $files) {

        try {
            if ($post['telefone'] == "-") {
                $post['telefone'] = "";
            }

            if ($post['id'] != "") {
                $pessoa = $this->findById($post['id']);
                $arquivo = $pessoa->getArquivo();

                parent::update($post);
                Adm_ArquivoMapper::getInstance()->insert("pessoa", $pessoa->getId(), $files, $arquivo);
            } else {
                $id = parent::insert($post);
                Adm_ArquivoMapper::getInstance()->insert("pessoa", $id, $files);
            }
        } catch (Exception $e) {
            throw $e;
        }

        return $id;
    }

//metodos para a home

    public function getAniversariantes() {

        return $this->fetchAll(array(
                    array("ativo", "=", 1),
                    array("tipo", "=", 1),
                    array("diaAniversario", "!=", ""),
                    array("mesAniversario", "!=", ""),
                    array(
                        array("mesAniversario", "gt", date("m")),
                        array(
                            array("mesAniversario", "=", date("m")),
                            array("diaAniversario", "gte", date("d"))
                        )
                    )
                        ), array("order" => "mesAniversario asc, diaAniversario asc", "limit" => 5)
        );
    }

    public function getAniversariantesEmpresa() {

        return $this->fetchAll(
                        array(
                    array("dataAdmissao", "!=", "0000-00-00"),
                    array("ativo", "=", 1),
                    array("tipo", "=", 1),
                    array("year(dataAdmissao)", "lt", date("Y")),
                    array(
                        array("month(dataAdmissao)", "gt", date("m")),
                        array(
                            array("month(dataAdmissao)", "=", date("m")),
                            array("dayofmonth(dataAdmissao)", "gte", date("d"))
                        )
                    )
                        ), array("order" => "month(dataAdmissao) asc, dayofmonth(dataAdmissao) asc", "limit" => 5)
        );
    }

    public function recuperarSenha($post) {
//necessario: cpf, login 

        if ($post['login'] == "") {
            throw new Exception("Escolha um login");
        }
        if ($post['cpf'] == "") {
            throw new Exception("Informe o CPF");
        }

        $arg2 = array(
            array("cpf", "=", Util_Utilidade::formataCpf($post['cpf'])),
            array("login", "=", $post['login'])
        );

        $pessoa = $this->find($arg2);

        if ($pessoa == "") {
            throw new Exception("CPF ou login incorretos");
        }

        $senha = $this->geraSenha();

        $arg['senha'] = md5($senha);
        $arg['id'] = $pessoa->getId();
        $arg['login'] = $post['login'];

        $this->update($arg);

        if ($pessoa->getEmail() != "") {
            $arg2['destinatario'] = $pessoa->getEmail();
        } else {
            $arg2['destinatario'] = $pessoa->getEmailPessoal();
        }

        $arg2['mensagem'] = "Seja bem vindo à Viana & Moura!<br><br>Login: " . $post['login'] . "<br>Senha: " . $senha . "<br><br>Acesse extranet.vianaemoura.com.br<br><b>É aconselhável mudar sua senha para maior segurança.</b>";
        $arg2['assunto'] = "Viana & Moura - Extranet";
        $arg2['prioridade'] = 3;
        Adm_EmailMapper::getInstance()->insert($arg2);
    }

    public function inserirUsuarioFornecedor($post) {

        try {

            if (Adm_PessoaMapper::getInstance()->find(array(array("login", "=", $post['login']), array("ativo", "=", 1))) != "")
                throw new Exception("Já existe uma pessoa ativa cadastrada com esse login");

            $arg['emailPessoal'] = $post['emailPessoal'];
            $arg['nome'] = $post['nome'];
            $arg['telefonePessoal'] = Util_Utilidade::formataTelefone($post['telefonePessoal']);
            $arg['dataCadastro'] = date("Y-m-d");
            $arg['mudancaSenha'] = date("Y-m-d");
            $arg['login'] = $post['login'];
            $arg['fornecedor_id'] = $post['fornecedor_id'];
            $arg['tipo'] = 98;
            $arg['ativo'] = 1;

            if ($post['senha'] != "") {
                if ($post['senha'] != $post['csenha']) {
                    throw new Exception("As senhas devem ser iguais");
                }
                $senha = $post['senha'];
            } else {
                throw new Exception("Informe uma senha");
            }

            $arg['senha'] = md5($senha);

            parent::insert($arg);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function editarUsuarioFornecedor($post) {

        try {
            $arg['id'] = $post['id'];
            $arg['emailPessoal'] = $post['emailPessoal'];
            $arg['nome'] = $post['nome'];
            $arg['telefonePessoal'] = Util_Utilidade::formataTelefone($post['telefonePessoal']);
            $arg['dataCadastro'] = date("Y-m-d");
            $arg['mudancaSenha'] = date("Y-m-d");
            $arg['login'] = $post['login'];
            $arg['fornecedor_id'] = $post['fornecedor_id'];
            $arg['tipo'] = 98;
            $arg['ativo'] = 1;

            if ($post['senha'] != "") {
                if ($post['senha'] != $post['csenha']) {
                    throw new Exception("As senhas devem ser iguais");
                }
                $senha = $post['senha'];
            } else {
                throw new Exception("Informe uma senha");
            }

            $arg['senha'] = md5($senha);

            parent::update($arg);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function carregaDados() {

        $arquivo = $_FILES['arquivo_1']['tmp_name'];

        if (file_exists($arquivo)) {
            try {
                $lines = file($arquivo, FILE_SKIP_EMPTY_LINES);

                array_shift($lines); //remove a primeira linha, pq tem cabeçalho

                foreach ($lines as $line) {
                    $cols = explode(";", trim($line));
                    $cols[1] = Util_Utilidade::formataCpf($cols[1]);
                    //com a coluna do cpf vai procurar a pessoa
                    $arg['cpf'] = array("cpf", "lk", $cols[1]);
                    $pessoa = Adm_PessoaMapper::getInstance()->find($arg);
                    if ($pessoa) {
                        //depois que achar a pessoa, vamos atualizar a chapa
                        $post['id'] = $pessoa->getId();
                        $post['chapa'] = $cols[0];
                        parent::update($post);
                    } else {
                        $erros .= "CPF " . $cols[1] . " não encontrado.";
                    }
                }
                
                return $erros;
            } catch (Exception $e) {
                throw $e;
            }
        }
    }
    
    public function inserirArquivo($post, $files) {
        try {
            Adm_ArquivoMapper::getInstance()->insert("spg", $post['id'], $files);
        } catch (Exception $e) {
            throw $e;
        }
    }

}

?>