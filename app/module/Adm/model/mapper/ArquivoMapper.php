<?php

class Adm_ArquivoMapper extends AbstractMapper {

    private static $_instance = NULL;

    final public static function getInstance() {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function __construct() {

        $this->repository = new Adm_ArquivoRepository();
        $this->necessary = array("chave_id", "extensao");
        $this->modelName = "Adm_Arquivo";
        $this->bucket = $GLOBALS['server']['bucket']; //setado no frontcontroller
        $this->inicializaPadroes();
        $this->conexaoS3();
    }

    private function conexaoS3() {
        // AWS access info
        if (!defined('awsAccessKey'))
            define('awsAccessKey', 'AKIAIWD2FGR2UD7SSZ3A');
        if (!defined('awsSecretKey'))
            define('awsSecretKey', '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');

// Check for CURL
        if (!extension_loaded('curl') && !@dl(PHP_SHLIB_SUFFIX == 'so' ? 'curl.so' : 'php_curl.dll'))
            exit("\nERROR: CURL extension not loaded\n\n");

        Vendor_S3::setAuth(awsAccessKey, awsSecretKey);
    }

    private function inicializaPadroes() {

        //possiveis configurações: extensoes, limiteQtd, limiteTam, pasta
        //pessoas
        $this->padroes['pessoa']['extensoes'] = array(".jpg", ".jpeg");
        $this->padroes['pessoa']['nomeExtensao'] = array("JPEG", "JPG");
        $this->padroes['pessoa']['limiteQtd'] = 1;

        //pa Plano de Ação
        $this->padroes['pa']['extensoes'] = array(".xls", ".xlsx", ".pdf", ".doc", ".docx", ".png");
        $this->padroes['pa']['nomeExtensao'] = array("EXCEL", "PDF", "WORD", "PNG");
        $this->padroes['pa']['limiteQtd'] = 10;
        $this->padroes['pa']['limiteTam'] = 10;

        //ra Relatorio de Anomalia
        $this->padroes['ra']['extensoes'] = array(".xls", ".xlsx", ".pdf", ".doc", ".docx", ".png");
        $this->padroes['ra']['nomeExtensao'] = array("EXCEL", "PDF", "WORD", "PNG");
        $this->padroes['ra']['limiteQtd'] = 10;
        $this->padroes['ra']['limiteTam'] = 10;

        //ar Ata de Reunião
        $this->padroes['ar']['extensoes'] = array(".xls", ".xlsx", ".pdf", ".doc", ".docx", ".png", ".zip", ".rar");
        $this->padroes['ar']['nomeExtensao'] = array("EXCEL", "PDF", "WORD", "PNG", "ZIP", "RAR");
        $this->padroes['ar']['limiteQtd'] = 10;
        $this->padroes['ar']['limiteTam'] = 10;

        //documento
        $this->padroes['documento']['extensoes'] = array(".pdf", ".jpeg", ".jpg");
        $this->padroes['documento']['nomeExtensao'] = array("PDF", "JPEG", "JPG");
        $this->padroes['documento']['limiteQtd'] = 1;
        $this->padroes['documento']['limiteTam'] = 10;

        //form
        $this->padroes['form']['extensoes'] = array(".xls", ".xlsx", ".xlsm", ".pdf", ".doc", ".docx", ".png");
        $this->padroes['form']['nomeExtensao'] = array("EXCEL", "PDF", "WORD", "PNG");
        $this->padroes['form']['limiteQtd'] = 1;
        $this->padroes['form']['limiteTam'] = 10;

        //exame
        $this->padroes['exame']['extensoes'] = array(".pdf", ".jpeg", ".jpg");
        $this->padroes['exame']['nomeExtensao'] = array("PDF", "JPEG", "JPG");
        $this->padroes['exame']['limiteQtd'] = 1;

        //requisição
        $this->padroes['req']['extensoes'] = array(".dwg", ".pdf", ".xls", ".xlsx");
        $this->padroes['req']['nomeExtensao'] = array("DWG", "PDF", "EXCEL");
        $this->padroes['req']['limiteQtd'] = 1;
        $this->padroes['req']['limiteTam'] = 4;

        //boletim
        $this->padroes['bol']['extensoes'] = array(".pdf");
        $this->padroes['bol']['nomeExtensao'] = array("PDF");
        $this->padroes['bol']['limiteQtd'] = 1;

        //pagamento vendas - boleto
        $this->padroes['vndBoleto']['extensoes'] = array(".pdf", ".jpeg", ".jpg", ".png");
        $this->padroes['vndBoleto']['nomeExtensao'] = array("PDF", "JPEG", "JPG", "PNG");
        $this->padroes['vndBoleto']['limiteQtd'] = 1;

        //pagamento vendas - comprovante
        $this->padroes['vndComp']['extensoes'] = array(".pdf", ".jpeg", ".jpg", ".png");
        $this->padroes['vndComp']['nomeExtensao'] = array("PDF", "JPEG", "JPG", "PNG");
        $this->padroes['vndComp']['limiteQtd'] = 1;

        //lista mestra
        $this->padroes['listaMestra']['extensoes'] = array(".pdf");
        $this->padroes['listaMestra']['nomeExtensao'] = array("PDF");
        $this->padroes['listaMestra']['limiteQtd'] = 1;
        $this->padroes['listaMestra']['limiteTam'] = 20;

        //lista mestra
        $this->padroes['listaLegalizacao']['extensoes'] = array(".pdf", ".jpg", ".jpeg", ".png", ".dwg", ".xls", ".xlsx", ".doc", ".docx", ".ppt", ".pptx", ".dxf", ".mpp", ".psd");
        $this->padroes['listaLegalizacao']['nomeExtensao'] = array("PDF", "JPG", "PNG", "DWG", "EXCEL", "WORD", "PPT", "DXF", "PROJECT", "PHOTOSHOP");
        $this->padroes['listaLegalizacao']['limiteQtd'] = 1;
        $this->padroes['listaLegalizacao']['limiteTam'] = 20;

        //ata de solicitação de projetos - solicitante
        $this->padroes['aspSolicitante']['extensoes'] = array(".pdf", ".jpg", ".jpeg", ".png", ".dwg", ".xls", ".xlsx", ".doc", ".docx", ".ppt", ".pptx", ".dxf", ".mpp", ".psd");
        $this->padroes['aspSolicitante']['nomeExtensao'] = array("PDF", "JPG", "PNG", "DWG", "EXCEL", "WORD", "PPT", "DXF", "PROJECT", "PHOTOSHOP");
        $this->padroes['aspSolicitante']['limiteQtd'] = 1;
        $this->padroes['aspSolicitante']['limiteTam'] = 20;

        //ata de solicitação de projetos - projetos
        $this->padroes['aspProj']['extensoes'] = array(".pdf", ".jpg", ".jpeg", ".png", ".dwg", ".xls", ".xlsx", ".doc", ".docx", ".ppt", ".pptx", ".dxf", ".mpp", ".psd");
        $this->padroes['aspProj']['nomeExtensao'] = array("PDF", "JPG", "PNG", "DWG", "EXCEL", "WORD", "PPT", "DXF", "PROJECT", "PHOTOSHOP");
        $this->padroes['aspProj']['limiteQtd'] = 1;
        $this->padroes['aspProj']['limiteTam'] = 25;

        //vendas - contratos
        $this->padroes['vndContrato']['extensoes'] = array(".pdf", ".jpeg", ".jpg", ".png", ".tiff", ".tif");
        $this->padroes['vndContrato']['nomeExtensao'] = array("PDF", "JPEG", "JPG", "PNG", "TIF", "TIFF");
        $this->padroes['vndContrato']['limiteQtd'] = 1;
        $this->padroes['vndContrato']['limiteTam'] = 12;

        //fornecedores
        $this->padroes['forn']['extensoes'] = array(".pdf", ".jpeg", ".jpg", ".png");
        $this->padroes['forn']['nomeExtensao'] = array("PDF", "JPEG", "JPG", "PNG");
        $this->padroes['forn']['limiteQtd'] = 1;

        //funcionario dos fornecedores
        $this->padroes['fornFuncionario']['extensoes'] = array(".pdf", ".jpeg", ".jpg", ".png");
        $this->padroes['fornFuncionario']['nomeExtensao'] = array("PDF", "JPEG", "JPG", "PNG");
        $this->padroes['fornFuncionario']['limiteQtd'] = 1;

        //assistência de infra
        $this->padroes['assInfra']['extensoes'] = array(".jpeg", ".jpg", ".png");
        $this->padroes['assInfra']['nomeExtensao'] = array("JPEG", "JPG", "PNG");
        $this->padroes['assInfra']['limiteQtd'] = 1;

        //Help Desk
        $this->padroes['help']['extensoes'] = array(".pdf", ".jpeg", ".jpg", ".png");
        $this->padroes['help']['nomeExtensao'] = array("PDF", "JPEG", "JPG", "PNG");
        $this->padroes['help']['limiteQtd'] = 1;

        //Gestao Virtual
        $this->padroes['gv']['extensoes'] = array(".pdf", ".jpeg", ".jpg", ".png");
        $this->padroes['gv']['nomeExtensao'] = array("PDF", "JPEG", "JPG", "PNG");
        $this->padroes['gv']['limiteQtd'] = 1;

        //Solicitação de produtos gerais SPG
        $this->padroes['spg']['extensoes'] = array(".jpeg", ".jpg", ".png", ".xls", ".xlsx", ".xlsm", ".pdf", ".doc", ".docx");
        $this->padroes['spg']['nomeExtensao'] = array("JPEG", "JPG", "PNG", "EXCEL", "PDF", "WORD");
        $this->padroes['spg']['limiteQtd'] = 1;

        //Solicitação de Pessoas
        $this->padroes['pessoaDoc']['extensoes'] = array(".jpeg", ".jpg", ".png", ".xls", ".xlsx", ".xlsm", ".pdf", ".doc", ".docx");
        $this->padroes['pessoaDoc']['nomeExtensao'] = array("JPEG", "JPG", "PNG", "EXCEL", "PDF", "WORD");
        $this->padroes['pessoaDoc']['limiteQtd'] = 1;

        //maturidade - término de experiência
        $this->padroes['matTermino']['extensoes'] = array(".xls", ".xlsx", ".xlsm", ".pdf", ".doc", ".docx", ".png");
        $this->padroes['matTermino']['nomeExtensao'] = array("EXCEL", "PDF", "WORD", "PNG");
        $this->padroes['matTermino']['limiteQtd'] = 1;

        //assistência
        $this->padroes['assistencia']['extensoes'] = array(".jpeg", ".jpg", ".png", ".pdf", ".mp3", ".wav", ".xls", ".xlsx", ".zip", ".rar");
        $this->padroes['assistencia']['nomeExtensao'] = array("JPEG", "JPG", "PNG", "MP3", "WAV", "ZIP", "RAR");
        $this->padroes['assistencia']['limiteQtd'] = 1;
        $this->padroes['assistencia']['limiteTam'] = 20;

        //sap
        $this->padroes['sap']['extensoes'] = array(".jpeg", ".jpg", ".png", ".pdf", ".xls", ".xlsx", ".zip", ".rar");
        $this->padroes['sap']['nomeExtensao'] = array("JPEG", "JPG", "PNG", "ZIP", "RAR");
        $this->padroes['sap']['limiteQtd'] = 1;
        $this->padroes['sap']['limiteTam'] = 20;
    }

    public function getExtensoes($tipo) {
        return $this->padroes[$tipo]['extensoes'];
    }

    public function getLimiteQtd($tipo) {
        if ($this->padroes[$tipo]['limitQtd'] != "") {
            return $this->padroes[$tipo]['limitQtd'];
        } else {
            return 100;
        }
    }

    public function getLimiteTam($tipo) {
        if ($this->padroes[$tipo]['limiteTam'] != "") {
            return $this->padroes[$tipo]['limiteTam'] * 1000000;
        } else {
            return 20000000; //20Mb
        }
    }

    public function getPasta($tipo) {

        return $tipo . "/";
    }

    public function getTotalInserido($tipo, $chave_id) {

        $arg[] = array("tipo", "=", $tipo);
        $arg[] = array("chave_id", "=", $chave_id);
        $arg[] = array("ativo", "=", 1);
        return $this->count($arg);
    }

    public function getMetaData($extensao) {
        if ($extensao == ".jpg" || $extensao == ".jpeg") {
            return "image/jpeg";
        }
        if ($extensao == ".png") {
            return "image/png";
        }
        if ($extensao == ".doc") {
            return "application/msword";
        }
        if ($extensao == ".docx") {
            return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
        }
        if ($extensao == ".xls") {
            return "application/vnd.ms-excel";
        }
        if ($extensao == ".xlsx") {
            return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        }
        if ($extensao == ".xlsm") {
            return "application/vnd.ms-excel.sheet.macroEnabled.12";
        }
        if ($extensao == ".pdf") {
            return "application/pdf";
        }
        if ($extensao == ".dwg") {
            return "image/x-dwg";
        }
        if ($extensao == ".tiff" || $extensao == ".tif") {
            return "image/tiff";
        }
        if ($extensao == ".ppt") {
            return "application/vnd.ms-powerpoint";
        }
        if ($extensao == ".pptx") {
            return "application/vnd.openxmlformats-officedocument.presentationml.presentation";
        }
        if ($extensao == ".dxf") {
            return "image/vnd.dwg";
        }
        if ($extensao == ".mpp") {
            return "application/vnd.ms-project";
        }
        if ($extensao == ".psd") {
            return "application/octet-stream";
        }
        if ($extensao == ".mp3") {
            return "audio/mpeg";
        }
        if ($extensao == ".wav") {
            return "audio/wav";
        }
    }

    public function insertDirect($post) {
        parent::insert($post);
    }

    public function insert($tipo, $chave_id, $files, $desativa = "") {

        $tamLimite = $this->getLimiteTam($tipo);
        $qtdLimite = $this->getLimiteQtd($tipo);
        $extensoes = $this->getExtensoes($tipo);
        $total = $this->getTotalInserido($tipo, $chave_id);

        $arg['tipo'] = $tipo;
        $pasta = $this->getPasta($arg['tipo']);  //Caminho do bucket ("PATH")

        $arg['chave_id'] = $chave_id;
        $arg['ativo'] = 1;
        $arg['data'] = date("Y-m-d H:i:s");

        $i = 1;

        while (isset($files["arquivo_" . $i])) {
            if ($total > $qtdLimite) {
                Util_FlashMessage::write("Já foi inserido a Quantidade Máxima de arquivos;");
            } else {

                $arquivo = $files["arquivo_" . $i];
                $arg['extensao'] = strtolower(strrchr($arquivo['name'], '.'));
                $arg['rotulo'] = $arquivo['name'];

                if ($arquivo['name'] != "") {
                    if ($arquivo['danger'] == 0) {
                        if ($arquivo['size'] <= $tamLimite) {
                            if (in_array($arg['extensao'], $extensoes)) {

                                //insere no BD
                                $id = parent::insert($arg);
                                //copia
                                $nomeArquivo = $pasta . $tipo . "_" . $arg['chave_id'] . "_" . $id . $arg['extensao'];

                                $mimeType = $this->getMetaData($arg['extensao']);
                                //move the file                                
                                $s3 = Vendor_S3::putObjectFile($arquivo['tmp_name'], $this->bucket, $nomeArquivo, Vendor_S3::ACL_PUBLIC_READ, "", $mimeType);

                                if ($desativa != "") {
                                    $this->desativar($desativa->getId());
                                }
                            } else {
                                Util_FlashMessage::write("Extensão inválida do arquivo '" . $arg['rotulo'] . "'", "danger");
                            }
                        } else {
                            Util_FlashMessage::write("Houve um erro no tamanho do arquivo '" . $arg['rotulo'] . "'", "danger");
                        }
                    } else {
                        Util_FlashMessage::write("Houve um erro no carregamento do arquivo '" . $arg['rotulo'] . "'", "danger");
                    }
                }
            }

            $i++;
            $total++;
        }
        return $id;
    }

    public function copiarArquivo($tipo, $chave_id, $extensao, $rotulo, $caminhoArquivoVelho, $desativa = "") {
        try {

            $arg['tipo'] = $tipo;
            $arg['chave_id'] = $chave_id;
            $arg['extensao'] = $extensao;
            $arg['rotulo'] = $rotulo;
            $arg['ativo'] = 1;
            $arg['data'] = date("Y-m-d H:i:s");

            if ($caminhoArquivoVelho != "") {
                //insere no BD
                $id = parent::insert($arg);

                $caminhoArquivoNovo = $arg['tipo'] . "/" . $arg['tipo'] . "_" . $arg['chave_id'] . "_" . $id . $arg['extensao'];

                $copia = Vendor_S3::copyObject($this->bucket, $caminhoArquivoVelho, $this->bucket, $caminhoArquivoNovo, Vendor_S3::ACL_PUBLIC_READ);
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function desativar($id) {

        $arg['ativo'] = 0;
        $arg['id'] = $id;
        parent::update($arg);
    }

    public function getLabel($tipo) {
        $extensoes = count($this->padroes[$tipo]['nomeExtensao']);
        for ($i = 0; $i < $extensoes; $i++) {
            echo ' ' . '<span class="label label-default"> ' . $this->padroes[$tipo]['nomeExtensao'][$i] . '</span>' . ' ';
        }
    }

}

?>