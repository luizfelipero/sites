<?php

class Adm_EmailMapper extends AbstractMapper {

    private static $_instance = NULL;

    final public static function getInstance() {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function __construct() {
        $this->repository = new Adm_EmailRepository();
        $this->necessary = array();
        $this->modelName = "Adm_Email";
    }

    public function insert($post) {

        $post['dataAdd'] = date("Y-m-d H:i:s");

        parent::insert($post);
    }

    public function cronJob() {

        $arg[] = array("enviada", "lte", 2); //menor igual a 2 pq 3 é desistente, 4 nada e 5 é enviada
        $order['order'] = "prioridade desc, enviada desc, dataAdd asc";  //prioridade desc pq se for prioritário é igual 1
        $order['limit'] = "32";

        $pendentes = $this->fetchAll($arg, $order);

        if ($pendentes != "") {
            foreach ($pendentes as $pendente) {
                $this->tentaEnviar($pendente);
                usleep(1500000); //1.5 segundos
            }
        }
    }

    public function tentaEnviar($pendente) {


        $post['id'] = $pendente->getId();

        $retorno = $this->envia($pendente);

        if ($retorno == 200) { //sucesso
            $post['enviada'] = 5;
            $post['dataEnvio'] = date("Y-m-d H:i:s");
        }

        if ($retorno == 400 || $retorno == 403) { //erros
            //procura na tabela de e-mail
            $enviada = $pendente->getEnviada();

            if ($enviada < 3) { //e incrementa a qtd de tentativas de envio, se for < 3
                $post['enviada'] = $enviada + 1;
            } else { //se não, desiste de reenviar
                $post['enviada'] = 3;
            }
        }

        parent::update($post);
    }

    public function envia($pendente) {

        $ses = new Vendor_SES_SimpleEmailService('AKIAIU7QENNDFONHHERA', '2WKpCfy9/dotNu72N2n/2qi5PucW9PltlIZjbYoj');

        $ses->enableVerifyPeer(false);
        $m = new Vendor_SES_SimpleEmailServiceMessage();
        $m->messagehtml = true;
        $m->addTo($pendente->getDestinatario());
        $m->setFrom('naoresponda@vianaemoura.com.br');
        $m->setSubject($pendente->getAssunto());
        //Repete pq o primeiro é só texto e o segundo é o próprio HTML
        $m->setMessageFromString($pendente->getMensagem(), $pendente->getMensagem());

        if ($pendente->getReplyto() != "") {
            $m->addReplyTo($pendente->getReplyto());
        }

        $send = $ses->sendEmail($m);

        $erro = $send['numErro']['code'];
        //echo $send['numErro']['code'];
        //200 - sucesso
        //400 - mts tipos de erro
        //403 - AWS access key ID errado

        return $erro;
    }

    //Métodos específicos que usam HTML

    public function insertEmailAssistencia($chamado, $corpo) {
        
        
        $mensagem = file_get_contents("file/email/assistencia.html");
        $mensagem = str_replace("--msg--", $corpo, $mensagem);

        $post['mensagem'] = $mensagem;
        $post['destinatario'] = $chamado->getSolicitanteEmail();
        $post['assunto'] = "Viana & Moura - Assistência Técnica";
        $post['replyto'] = "cliente@vianaemoura.com.br";
        $post['prioridade'] = 1;

        $this->insert($post);
    }
    
    public function insertEmailAssistenciaNovo($chamado, $corpo) {
        
        
        $mensagem = file_get_contents("file/email/assistencia2.html");
        $mensagem = str_replace("--msg--", $corpo, $mensagem);

        $post['mensagem'] = $mensagem;
        $post['destinatario'] = $chamado->getSolicitanteEmail();
        $post['assunto'] = "Viana & Moura - Assistência Técnica";
        $post['replyto'] = "cliente@vianaemoura.com.br";
        $post['prioridade'] = 1;
        $post['tipo'] = 2;
        $post['chave_id'] = $chamado->getId();

        $this->insert($post);
    }
    
    public function insertEmailVendas($cliente) {
        
        if ($cliente->getStatus_id() == 50) {
            $mensagem = file_get_contents("file/email/vendasDuplicado.inc");
        } else {
            $mensagem = file_get_contents("file/email/vendas.inc");
        }        
        
        $mensagem = str_replace("--nome--", $cliente->getNome(), $mensagem);
        $mensagem = str_replace("--data--", Util_Utilidade::exibeData($cliente->getDataAdd()), $mensagem);
        $mensagem = str_replace("--cidade--", $cliente->getCidadeCasa(), $mensagem);
        $mensagem = str_replace("--numero--", $cliente->getProtocolo(), $mensagem);
        $mensagem = str_replace("--nome--", $cliente->getNome(), $mensagem);
        $mensagem = str_replace("--cpf--", Util_Utilidade::exibeCpf($cliente->getCpf()), $mensagem);


        $post['mensagem'] = $mensagem;
        $post['destinatario'] = $cliente->getEmail();
        $post['assunto'] = "Viana & Moura - Vendas";
        $post['replyto'] = "cliente@vianaemoura.com.br";
        $post['prioridade'] = 0;
        $post['tipo'] = 1;
        $post['chave_id'] = $cliente->getId();
        
        $this->insert($post);
    }
    
    public function insertEmailVendasPersonalizado($post, $cliente_id) {
        
        $mensagem = file_get_contents("file/email/vendasPersonalizado.inc");
        $mensagem = str_replace("--mensagem--", $post['mensagem'], $mensagem);

        $post['mensagem'] = $mensagem;
        $post['destinatario'] = $post['destinatario'];
        $post['assunto'] = "Viana & Moura - Vendas";
        $post['replyto'] = "cliente@vianaemoura.com.br";
        $post['prioridade'] = 0;
        $post['tipo'] = 1;
        $post['chave_id'] = $cliente_id;
        
        $this->insert($post);
    }
    
    public function insertEmailClienteUsuario($email, $senha) {

        $mensagem = file_get_contents("file/email/clienteUsuario.inc");
        $mensagem = str_replace("--senha--", $senha, $mensagem);


        $post['mensagem'] = $mensagem;
        $post['destinatario'] = $email;
        $post['assunto'] = "Viana & Moura - Portal do Cliente";
        $post['replyto'] = "cliente@vianaemoura.com.br";
        $post['prioridade'] = 0;

        $this->insert($post);
    }

}

?>