<?php

class Adm_Pessoa {

    private $id;
    private $login;
    private $cpf;
    //private $senha;
    private $nome;
    private $email;
    private $telefone;
    private $telefonePessoal;
    private $ativo;
    private $last_login;
    private $dominio_id;
    private $tipo;
    private $dataInicial;
    private $dataCadastro;
    private $dataFinal;
    private $diaAniversario;
    private $mesAniversario;

    function __construct($arg) {

        $this->id = $arg['id'];
        $this->login = $arg['login'];
        $this->cpf = $arg['cpf'];
        $this->nome = $arg['nome'];
        $this->email = $arg['email'];
        $this->telefone = $arg['telefone'];
        $this->telefonePessoal = $arg['telefonePessoal'];
        $this->ativo = $arg['ativo'];
        $this->last_login = $arg['last_login'];
        $this->dominio_id = $arg['dominio_id'];
        $this->tipo = $arg['tipo'];
        $this->dataInicial = $arg['dataInicial'];
        $this->dataCadastro = $arg['dataCadastro'];
        $this->dataFinal = $arg['dataFinal'];
        $this->diaAniversario = $arg['diaAniversario'];
        $this->mesAniversario = $arg['mesAniversario'];
    }

    public function getDominio() {
        return Adm_DominioMapper::getInstance()->findById($this->dominio_id);
    }

    //regular getters
    function getId() {
        return $this->id;
    }

    function getLogin() {
        return $this->login;
    }

    function getCpf() {
        return $this->cpf;
    }

    function getNome() {
        return $this->nome;
    }

    function getEmail() {
        return $this->email;
    }

    function getTelefone() {
        return $this->telefone;
    }

    function getTelefonePessoal() {
        return $this->telefonePessoal;
    }

    function getAtivo() {
        return $this->ativo;
    }

    function getLast_login() {
        return $this->last_login;
    }

    function getDominio_id() {
        return $this->dominio_id;
    }

    function getTipo() {
        return $this->tipo;
    }

    function getDataInicial() {
        return $this->dataInicial;
    }

    function getDataCadastro() {
        return $this->dataCadastro;
    }

    function getDataFinal() {
        return $this->dataFinal;
    }

    function getDiaAniversario() {
        return $this->diaAniversario;
    }

    function getMesAniversario() {
        return $this->mesAniversario;
    }

}

?>