<?php

class Util_Json {

    public function __construct($results, $options = "") {

        $this->results = $results;
        $this->options = $options;
    }

    public function exibe() {


        if ($this->options['selecione'] == 1) {
            $ret[] = array("id" => "", "nome" => "--Selecione--");
        } if ($this->options['none'] == 1) {
            $ret[] = array("id" => "", "nome" => "---");
        } if ($this->options['tg'] == 1) {
            $ret[] = array("id" => "tg", "nome" => "Todos e Geral");
        } if ($this->options['t'] == 1) {
            $ret[] = array("id" => "t", "nome" => "Todos");
        } if ($this->options['g'] == 1) {
            $ret[] = array("id" => "g", "nome" => "Geral");
        } if ($this->options['acesso'] == 1) {
            $ret[] = array("id" => "a", "nome" => "Acesso");
        }
        if ($this->results != "") {
            foreach ($this->results as $result) {

                $r = "";
                $r['id'] = $result->getId();
                if ($result instanceof Infra_Conta || $result instanceof Infra_Servico) {
                    $r['nome'] = $result->getAbrev();
                } else {
                    $r['nome'] = $result->getNome();
                }

                if ($result instanceof Adm_Pessoa) {
                    if ($result->getTipo() < 90) {
                        $r['cargo'] = $result->getCargo()->getNome();
                        $r['unidade'] = $result->getUnidadeEspecifica()->getNome();
                    }
                }

                $ret[] = $r;
            }
        } else {
            $ret[] = array("id" => "", "nome" => "Não há registros");
        }


        return json_encode($ret);
    }

}

?>
