<?php

class Util_Cor {

    private $ponteiro;

    public function __construct() {
        $this->ponteiro = 0;
    }

    public function next($class = "td") {

        $ret = $this->ponteiro;
        $this->ponteiro = ($this->ponteiro + 1) % 2;
        if ($class == "")
            echo $ret;
        else
            echo " class=\"" . $class . $ret . "\" ";
    }

    public function get($class = "td") {

        $ret = $this->ponteiro;
        if ($class == "")
            echo $ret;
        else
            echo " class=\"" . $class . $ret . "\" ";
    }

    public function zerar() {
        $this->ponteiro = 0;
    }

    public function change() {
        $this->ponteiro = ($this->ponteiro + 1) % 2;
    }

}

?>