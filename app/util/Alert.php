<?php

class Util_Alert {

    //put your code here
    public static function exibe($msg, $tipo = "", $id = "") {
        if ($tipo != "") {
            $tipo = "alert-" . $tipo;
        } else {
            $tipo = "alert-warning";
        }
        if ($id != "") {
            $id = 'id="'.$id.'"';
        }
        $retorno = '<div '.$id.' class="alert ' . $tipo . '">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Aviso!</strong>  ' . $msg . '</div>';

        return $retorno;
    }

}

?>
