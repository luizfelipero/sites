<?php

class Util_Time {

    //FORMATA COMO TIMESTAMP
    public static function dataToTimestamp($data) {
        $ano = substr($data, 0, 4);
        $mes = substr($data, 5, 2);
        $dia = substr($data, 8, 2);
        return mktime(0, 0, 0, $mes, $dia, $ano);
    }

    public static function Feriados($ano, $posicao) {
        $dia = 86400;
        $datas = array();
        $datas['pascoa'] = easter_date($ano);
        $datas['sexta_santa'] = $datas['pascoa'] - (2 * $dia);
        $datas['carnaval1'] = $datas['pascoa'] - (48 * $dia); //segunda
        $datas['carnaval2'] = $datas['pascoa'] - (47 * $dia); //terça
        $datas['carnaval3'] = $datas['pascoa'] - (46 * $dia); //quarta
        $datas['corpus_cristi'] = $datas['pascoa'] + (60 * $dia);
        $feriados = array(
            '01-01',
            date('m-d', $datas['carnaval1']),
            date('m-d', $datas['carnaval2']),
            date('m-d', $datas['carnaval3']),
            date('m-d', $datas['sexta_santa']),
            '04-21',
            '05-01',
            '06-24',
            '07-16',
            '09-07',
            '10-12',
            '11-02',
            '11-15',
            '12-25',
            '12-31',
        );

        return $ano . "-" . $feriados[$posicao];
    }

    public static function Soma1dia($data) {
        $ano = substr($data, 0, 4);
        $mes = substr($data, 5, 2);
        $dia = substr($data, 8, 2);
        return date("Y-m-d", mktime(0, 0, 0, $mes, $dia + 1, $ano));
    }

    public static function somaDiasUteis($xDataInicial, $xSomarDias) {
        for ($ii = 1; $ii <= $xSomarDias; $ii++) {

            $xDataInicial = Util_Time::Soma1dia($xDataInicial); //SOMA DIA NORMAL
            //VERIFICANDO SE EH DIA DE TRABALHO
            if (date("w", Util_Time::dataToTimestamp($xDataInicial)) == "0") {
                //SE DIA FOR DOMINGO OU FERIADO, SOMA +1
                $xDataInicial = Util_Time::Soma1dia($xDataInicial);
            } else if (date("w", Util_Time::dataToTimestamp($xDataInicial)) == "6") {
                //SE DIA FOR SABADO, SOMA +2
                $xDataInicial = Util_Time::Soma1dia($xDataInicial);
                $xDataInicial = Util_Time::Soma1dia($xDataInicial);
            } else {
                //senaum vemos se este dia eh FERIADO
                for ($i = 0; $i <= 12; $i++) {
                    if ($xDataInicial == Util_Time::Feriados(date("Y"), $i)) {
                        $xDataInicial = Util_Time::Soma1dia($xDataInicial);
                    }
                }
            }
        }
        return $xDataInicial;
    }

    public static function somaDiasCorrido($date1, $dias) {

//        $date1 = date_parse($date1);
//        $date1 = gmmktime(0, 0, 0, $date1['month'], $date1['day'], $date1['year']);
//        $date2 = $date1 + ($dias * 3600 * 24);
//        echo date("Y-m-d", $date2);
//        return (date("Y-m-d", $date2));
        //echo date('Y-m-d', strtotime("+".$dias." days",strtotime($date1)));
        return date('Y-m-d', strtotime("+" . $dias . " days", strtotime($date1)));
    }

    public static function difDias($xDataInicial, $xDataFinal) {
        //considera s� dias uteis

        $xDataInicial = substr($xDataInicial, 0, 10);
        $xDataFinal = substr($xDataFinal, 0, 10);

        $xSomarDias = 0;
        $saco = 0;
        while ($xDataInicial < $xDataFinal && $saco < 1000) {
            //echo ">>".$xDataInicial;
            $xDataInicial = Util_Time::Soma1dia($xDataInicial); //SOMA DIA NORMAL
            //VERIFICANDO SE e DIA DE TRABALHO
            if (date("w", Util_Time::dataToTimestamp($xDataInicial)) == "0") {
                //SE DIA FOR DOMINGO OU FERIADO, SOMA +1
                $xDataInicial = Util_Time::Soma1dia($xDataInicial);
            } else if (date("w", Util_Time::dataToTimestamp($xDataInicial)) == "6") {
                //SE DIA FOR SABADO, SOMA +2
                $xDataInicial = Util_Time::Soma1dia($xDataInicial);
                $xDataInicial = Util_Time::Soma1dia($xDataInicial);
            } else {
                //senao vemos se este dia eh FERIADO
                for ($i = 0; $i <= 12; $i++) {
                    if ($xDataInicial == Util_Time::Feriados(date("Y"), $i)) {
                        $xDataInicial = Util_Time::Soma1dia($xDataInicial);
                    }
                }
            }
            $xSomarDias++;
            $saco++;
        }
        return $xSomarDias;
    }

    public static function difDiasCorrido($dataFinal, $dataInicial) {
        //date 1 > date2

        $dataFinal = date_parse($dataFinal);
        $dataInicial = date_parse($dataInicial);
        return ((gmmktime(0, 0, 0, $dataFinal['month'], $dataFinal['day'], $dataFinal['year']) - gmmktime(0, 0, 0, $dataInicial['month'], $dataInicial['day'], $dataInicial['year'])) / 3600 / 24);
    }

    public static function difTempo($date1, $date2) {
        //recebe datetime e retorna
        $date1 = date_parse($date1);
        $date2 = date_parse($date2);
        return (
                ( (gmmktime($date1['hour'], $date1['minute'], $date1['second'], $date1['month'], $date1['day'], $date1['year'])) -
                (gmmktime($date2['hour'], $date2['minute'], $date2['second'], $date2['month'], $date2['day'], $date2['year'])) ) / 60);
    }

    public static function getUltimoDiaMes($mes) {

        $mes = $mes + 0;

        $ultimosDias = array(30, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
        return $ultimosDias[$mes];
    }

    public static function getProximoMes($mes, $ano) {

        $retorno[0] = ($mes == 12) ? ++$ano : $ano;
        $retorno[1] = (($mes + 1) % 12);
        if ($retorno[1] == 0)
            $retorno[1] = 12;

        return $retorno;
    }

    public static function getMesAnterior($mes, $ano) {

        $retorno[0] = ($mes == 1) ? --$ano : $ano;
        $retorno[1] = (($mes - 1) % 12);
        if ($retorno[1] == 0)
            $retorno[1] = 12;

        return $retorno;
    }

    public static function formataHora($hora) {
        $hora = explode(":", $hora);
        $i = 1;
        $decimal = 0;
        foreach ($hora as $h) {
            $decimal += ($h / $i);
            $i*=60;
        }
        return $decimal;
    }

    public static function adicionaMes($data, $qtdMes) {
        $data = new DateTime($data);
        $data->add(new DateInterval('P' . $qtdMes . 'M'));
        return $data->format('Y-m-d');
    }

    public static function adicionaDia($data, $qtdDia) {
        $data = new DateTime($data);
        $data->add(new DateInterval('P' . $qtdDia . 'D'));
        return $data->format('Y-m-d');
    }

}

?>