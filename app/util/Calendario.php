<?php

class Util_Calendario {

    public static function getDiasSemana($primeiro_dia_mes, $numero_dias_mes) {

        $ret = '';

        $semanas = 'DSTQQSS';
        for ($i = $primeiro_dia_mes, $j = 0; $j < $numero_dias_mes; $j++) {
            $ret .= '<th>' . $semanas{$i} . '</th>';
            if ($i == 6) {
                $i = 0;
            } else {
                $i++;
            }
        }
        return $ret;
    }

    public static function getDias($numero_dias_mes, $mes, $ano) {

        $ret = '<tr>';
        for ($i = 1; $i <= $numero_dias_mes; $i++) {
            $dia = $i < 10 ? "0" . $i : $i;
            $ret .= '<td';
            if ($dia == date("d") && date("m") == $mes && date("Y") == $ano) {
                $ret .= ' class="dia dia_' . $ano . "_" . $mes . "_" . $dia . ' hoje"';
            } else {
                $ret .= ' class="dia dia_' . $ano . "_" . $mes . "_" . $dia . '"';
            }
            $ret .= '>' . $dia . '</td>';
        }
        $ret .= '</tr>';
        return $ret;
    }

    public static function getTurnos($numero_dias_mes, $mes, $ano, $turno, $ugb_id, $alocTipo) {

        $gasTipo = Aloc_AlocacaoMapper::getInstance()->getTipoGas($alocTipo);

        $num_turno = $turno == "M" ? 1 : 2;
        $ret = '<tr>';
        for ($i = 1; $i <= $numero_dias_mes; $i++) {
            $dia = $i < 10 ? "0" . $i : $i;
            $cor = "dia_" . $ano . "-" . $mes . "-" . $dia . "-" . $num_turno . " ";
            if ($ano . "-" . $mes . "-" . $dia > date("Y-m-d")) {
                $cor .= "btn-default";
            } else {
                $arg = array(
                    array("dataTurno", "=", $ano . "-" . $mes . "-" . $dia . "-" . $num_turno),
                    array("unidade_id", "=", $ugb_id),
                    array("tipo_ga", "in", $gasTipo)
                );
                $teve_alocacao = Aloc_AlocacaoProducaoMapper::getInstance()->find($arg) == "";
                $cor .= $teve_alocacao == false ? "btn-info" : "btn-danger";
            }
            $ret .= '<td><a class="btn btn-xs ' . $cor . '" href="' . Util_Link::link('Aloc', 'Alocacao', 'Alocar', '', '&dataBusca=' . $dia . '/' . $mes . '/' . $ano . '&turnoBusca=' . $num_turno . '&ugb_id=' . $ugb_id . '&alocTipo=' . $alocTipo) . '">' . $turno . '</a></td>';
        }
        $ret .= '</tr>';
        return $ret;
    }

    public static function getNumeroDias($mes, $ano) {

        $numero_dias = array(
            '01' => 31, '02' => 28, '03' => 31, '04' => 30, '05' => 31, '06' => 30,
            '07' => 31, '08' => 31, '09' => 30, '10' => 31, '11' => 30, '12' => 31
        );

        if ((($ano % 4) == 0 and ( $ano % 100) != 0) or ( $ano % 400) == 0) {
            $numero_dias['02'] = 29;
        }

        return $numero_dias[$mes];
    }

    public static function getNomeMes($mes) {

        $meses = array('01' => "Janeiro", '02' => "Fevereiro", '03' => "Março",
            '04' => "Abril", '05' => "Maio", '06' => "Junho",
            '07' => "Julho", '08' => "Agosto", '09' => "Setembro",
            '10' => "Outubro", '11' => "Novembro", '12' => "Dezembro"
        );

        if ($mes >= 01 && $mes <= 12) {
            return $meses[$mes];
        }

        return "Mês deconhecido";
    }

    public static function getCalendario($mes = "", $ano = "", $ugb_id, $alocTipo) {

        $ret = '';

        if ($ano == 2101) {
            $mes = "";
            $ano = "";
        }


        if ($ano == '') {
            $ano = date("Y");
        }
        if ($mes == '') {
            $mes = date("m");
        }
        $mes = Util_Utilidade::addZeros($mes, 2);

        $numero_dias_mes = Util_Calendario::getNumeroDias($mes, $ano); // retorna o número de dias que tem o mês desejado
        $nome_mes = Util_Calendario::getNomeMes($mes);

        $jd = gregoriantojd($mes, '01', $ano);
        $primeiro_dia_mes = jddayofweek($jd, 0);

        $mesCalendario = ($ano == 2101) ? date("m") : $mes;
        $anoCalendario = ($ano == 2101) ? date("Y") : $ano;

        $ret .= '<h5 class="center"><b>' . $nome_mes . ' / ' . $ano . '</b>';
        $ret .= '
                <input id="mesCalendario" type="hidden" value="' . $mesCalendario . '">
                <input id="anoCalendario" type="hidden" value="' . $anoCalendario . '">
                <div class="btn-group pull-right">
                    <button type="button" class="btn btn-default deslocaCalendario" data-acao="ant-ano"><i class="fa fa-backward"></i></button>                    
                    <button type="button" class="btn btn-default deslocaCalendario" data-acao="ant-mes"><i class="fa fa-chevron-left"></i></button>
                    <button type="button" class="btn btn-default deslocaCalendario" data-acao="pro-mes"><i class="fa fa-chevron-right"></i></button>
                    <button type="button" class="btn btn-default deslocaCalendario" data-acao="pro-ano"><i class="fa fa-forward"></i></button>
                </div></h5>';
        $ret .= '<table class="table table-condensed">';
        $ret .= Util_Calendario::getDiasSemana($primeiro_dia_mes, $numero_dias_mes);
        $ret .= Util_Calendario::getDias($numero_dias_mes, $mes, $ano);
        $ret .= Util_Calendario::getTurnos($numero_dias_mes, $mes, $ano, "M", $ugb_id, $alocTipo);
        $ret .= Util_Calendario::getTurnos($numero_dias_mes, $mes, $ano, "T", $ugb_id, $alocTipo);

        $ret .= "</table>";
        return $ret;
    }

    public static function getDiasMaquina($numero_dias_mes, $mes, $ano) {
        $ugb_id = $_GET['ugb_id'];
        $vilas_id = Prod_VilaMapper::getInstance()->selectColumn("id", array(array("ugb_id", "=", $ugb_id)));
        $ret = '<tr>';
        $arg[] = array("vila_id", "in", $vilas_id);
        for ($i = 1; $i <= $numero_dias_mes; $i++) {
            $dia = Util_Utilidade::addZeros($i, 2);
            $cor = "dia_" . $ano . "_" . $mes . "_" . $dia . " ";
            if ($ano . "-" . $mes . "-" . $dia > date("Y-m-d")) {
                $cor .= "btn-default";
            } else if ($ano . "-" . $mes . "-" . $dia == date("Y-m-d")) {
                $cor .= "btn-success";
            } else {
                $arg['data'] = array("data", "=", $ano . "-" . $mes . "-" . $dia);
                $alocacao = Aloc_AlocacaoMaquinaMapper::getInstance()->find($arg);
                $cor .= ($alocacao != "") ? "btn-info" : "btn-danger";
            }
            $ret .= '<td><a class="btn btn-xs ' . $cor . '" href="' . Util_Link::link('Aloc', 'AlocacaoMaquina', 'Alocar', '', '&dataBusca=' . $dia . '/' . $mes . '/' . $ano . '&ugb_id=' . $ugb_id) . '">' . $dia . '</a></td>';
        }
        $ret .= '</tr>';
        return $ret;
    }

    public static function getCalendarioMaquina($mes = "", $ano = "", $ugb_id) {

        $ret = '';

        if ($ano == 2101) {
            $mes = "";
            $ano = "";
        }


        if ($ano == '') {
            $ano = date("Y");
        }
        if ($mes == '') {
            $mes = date("m");
        }
        $mes = Util_Utilidade::addZeros($mes, 2);

        $numero_dias_mes = Util_Calendario::getNumeroDias($mes, $ano); // retorna o número de dias que tem o mês desejado
        $nome_mes = Util_Calendario::getNomeMes($mes);

        $jd = gregoriantojd($mes, '01', $ano);
        $primeiro_dia_mes = jddayofweek($jd, 0);

        $mesCalendario = ($ano == 2101) ? date("m") : $mes;
        $anoCalendario = ($ano == 2101) ? date("Y") : $ano;

        $ret .= '<h5 class="center"><b>' . $nome_mes . ' / ' . $ano . '</b>';
        $ret .= '
                <input id="mesCalendario" type="hidden" value="' . $mesCalendario . '">
                <input id="anoCalendario" type="hidden" value="' . $anoCalendario . '">
                <div class="btn-group pull-right">
                    <button type="button" class="btn btn-default deslocaCalendario" data-acao="ant-ano"><i class="fa fa-backward"></i></button>                    
                    <button type="button" class="btn btn-default deslocaCalendario" data-acao="ant-mes"><i class="fa fa-chevron-left"></i></button>
                    <button type="button" class="btn btn-default deslocaCalendario" data-acao="pro-mes"><i class="fa fa-chevron-right"></i></button>
                    <button type="button" class="btn btn-default deslocaCalendario" data-acao="pro-ano"><i class="fa fa-forward"></i></button>
                </div></h5>';
        $ret .= '<table class="table table-condensed">';
        $ret .= Util_Calendario::getSemanas($primeiro_dia_mes, $numero_dias_mes);
        for ($i = 1; $i <= 3; $i++) {
            switch ($i) {
                case 1:
                    $ret .= Util_Calendario::getDiasMaquina($numero_dias_mes, $mes, $ano);
                    break;
            }
        }
        $ret .= "</table>";
        return $ret;
    }

    //exibe o rótulo da semana
    public static function getDiasSemanaVendas($primeiro_dia_mes, $numero_dias_mes) {

        $ret = '';

        $semanas = 'DSTQQSS';
        $ret .= '<th></th>';
        for ($i = $primeiro_dia_mes, $j = 0; $j < $numero_dias_mes; $j++) {
            if ($i > 0 && $i < 6) {
                $ret .= '<th>' . $semanas{$i} . '</th>';
            }
            if ($i == 6) {
                $i = 0;
            } else {
                $i++;
            }
        }
        return $ret;
    }

    //exibe os dias em números
    public static function getDiasVendas($numero_dias_mes, $mes, $ano, $ugb_id, $visualizar) {

        $ret = '';
        $ret .= '<tr>';
        $ret .= '<td></td>';
        for ($i = 1; $i <= $numero_dias_mes; $i++) {
            $dia = $i < 10 ? "0" . $i : $i;
            $data = $ano . "-" . $mes . "-" . $dia;

            //weekday -> 0 = domingo, 1=segunda, .... 6 = sabado
            $dateTime = new DateTime($data);
            $fds = $dateTime->format('w');
            if ($fds > 0 && $fds < 6) {
                $ret .= '<td';
                $ret .= ' class="dia dia_' . $ano . "_" . $mes . "_" . $dia . '"';
                $ret .= '><a class="btn btn-primary btn-xs" target="_blank" href="?m=Vnd&c=Contratacao&a=PainelContratacoes&data='. $data . '">' . $dia . '</a></td>';
            }
        }
        $ret .= '</tr>';

        $status_id = array(51, 52, 54, 21, 22);
        $status = Vnd_NomeStatusMapper::getInstance()->fetchAll(array(
            array("ordemMeta", "!=", 0)
                ), array("order" => "ordemMeta asc"));

        $checks_id = array(10, 11);
        $checks = Vnd_ChecklistItemMapper::getInstance()->fetchAll(array(
            array("id", "in", $checks_id)
                ), array("order" => "id desc"));


        /* foreach ($checks as $check) {
          $ret .= '<tr>';
          $ret .= '<td>' . $check->getAbreviacao() . '</td>';

          for ($i = 1; $i <= $numero_dias_mes; $i++) {
          $dia = $i < 10 ? "0" . $i : $i;
          $data = $ano . "-" . $mes . "-" . $dia;

          //weekday -> 0 = domingo, 1=segunda, .... 6 = sabado
          $dateTime = new DateTime($data);
          $fds = $dateTime->format('w');
          if ($fds > 0 && $fds < 6) {
          $ret .= '<td';
          $ret .= ' class="dia dia_' . $ano . "_" . $mes . "_" . $dia . '"';
          if ($visualizar == true) {
          $ret .= '>' . Vnd_MetaMapper::getInstance()->getMeta($data, 2, $check->getId(), $ugb_id)
          . '<br>'
          . Vnd_MetaMapper::getInstance()->getReal($data, 2, $check->getId(), $ugb_id)
          . '</td>';
          } else {
          $ret .= '><input type="text" class="form-control" name="check_id-' . $check->getId() . '-' . $data . '"></td>';
          }
          }
          }

          $ret .= '</tr>';
          $ret .= '</tr>';
          } */

        foreach ($status as $st) {
            $ret .= '<tr>';
            $ret .='<td>' . $st->getNome() . '</td>';

            for ($i = 1; $i <= $numero_dias_mes; $i++) {
                $dia = $i < 10 ? "0" . $i : $i;
                $data = $ano . "-" . $mes . "-" . $dia;

                //weekday -> 0 = domingo, 1=segunda, .... 6 = sabado
                $dateTime = new DateTime($data);
                $fds = $dateTime->format('w');
                if ($fds > 0 && $fds < 6) {
                    $ret .= '<td';
                    $ret .= ' class="dia dia_' . $ano . "_" . $mes . "_" . $dia . '"';

                    if ($visualizar == true) {
                        $ret .= '>' . Vnd_MetaMapper::getInstance()->getMeta($data, $st->getId(), $ugb_id)
                                . '<br>'
                                . Vnd_MetaMapper::getInstance()->getReal($data, $st->getId(), $ugb_id)
                                . '</td>';
                    } else {
                        $ret .= '><input type="text" class="form-control" name="status_id-' . $st->getId() . '-' . $data . '"></td>';
                    }
                }
                //die();
            }

            $ret .= '</tr>';
            $ret .= '</tr>';
        }


        return $ret;
    }

    public static function getCalendarioVendas($mes = "", $ano = "", $ugb_id, $passador = true, $visualizar) {

        $ret = '';

        if ($ano == 2101) {
            $mes = "";
            $ano = "";
        }


        if ($ano == '') {
            $ano = date("Y");
        }
        if ($mes == '') {
            $mes = date("m");
        }
        $mes = Util_Utilidade::addZeros($mes, 2);

        $numero_dias_mes = Util_Calendario::getNumeroDias($mes, $ano); // retorna o número de dias que tem o mês desejado
        $nome_mes = Util_Calendario::getNomeMes($mes);

        $jd = gregoriantojd($mes, '01', $ano);
        $primeiro_dia_mes = jddayofweek($jd, 0);

        $mesCalendario = ($ano == 2101) ? date("m") : $mes;
        $anoCalendario = ($ano == 2101) ? date("Y") : $ano;

        $ret .= '<h5 class="center"><b>' . $nome_mes . ' / ' . $ano . '</b>';
        $ret .= '<input id="mesCalendario" type="hidden" value="' . $mesCalendario . '">
                <input id="anoCalendario" type="hidden" value="' . $anoCalendario . '">';
        if ($passador) {
            $ret .= '<div class="btn-group pull-right">
                    <button type="button" class="btn btn-default deslocaCalendario" data-acao="ant-ano"><i class="fa fa-backward"></i></button>                    
                    <button type="button" class="btn btn-default deslocaCalendario" data-acao="ant-mes"><i class="fa fa-chevron-left"></i></button>
                    <button type="button" class="btn btn-default deslocaCalendario" data-acao="pro-mes"><i class="fa fa-chevron-right"></i></button>
                    <button type="button" class="btn btn-default deslocaCalendario" data-acao="pro-ano"><i class="fa fa-forward"></i></button>
                </div>';
        }
        $ret .= '</h5>';
        $ret .= '<table class="table table-condensed">';
        $ret .= Util_Calendario::getDiasSemanaVendas($primeiro_dia_mes, $numero_dias_mes);
        $ret .= Util_Calendario::getDiasVendas($numero_dias_mes, $mes, $ano, $ugb_id, $visualizar);
        //$ret .= Util_Calendario::getCampos($numero_dias_mes, $mes, $ano);

        $ret .= "</table>";
        return $ret;
    }

}

?>
