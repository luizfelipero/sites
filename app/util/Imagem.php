<?php

class Util_Imagem {

    public static function exibe($id) {

        $imagem = "file/fotos/foto_" . $id . ".jpg";
        $ret = "<img src=\"";
        
        if (file_exists($imagem)) {
            $ret .= $imagem;
        } else {
            $ret .= "file/fotos/foto_default.jpg";
        }
        $ret .= "\">";
        
        return $ret;
    }

    public static function salvaImagem($file, $id) {

        $nome_arquivo = $file['name'];

        $tamanho_arquivo = $file['size'];
        $arquivo_temporario = $file['tmp_name'];

        $end_imagem = "file/fotos/foto_" . $id . ".jpg";

        $extensoes_validas = array(".jpg", ".jpeg", ".JPG");
        $ext = strrchr($nome_arquivo, '.');
        
        if (!in_array($ext, $extensoes_validas)) {
            throw new Exception("Extensao invalida");
        } else {

            //criando miniatura
            $img_origem = ImageCreateFromJPEG($arquivo_temporario);
            $old_w = ImagesX($img_origem);
            $old_h = ImagesY($img_origem);

            if (($old_w <= 90) && ($old_h <= 120)) {
                $novo_w = $old_w;
                $novo_h = $old_h;
            } else {
                //testando proporcao da altura
                $prop1 = ($old_h / 120);
                $prop2 = ($old_w / 90);
                if ($old_w / $prop1 <= 90)
                    $prop = $prop1;
                else
                    $prop = $prop2;

                $novo_w = $old_w / $prop;
                $novo_h = $old_h / $prop;
            }

            $thumb = imagecreatetruecolor($novo_w, $novo_h);

            imagecopyresampled($thumb, $img_origem, 0, 0, 0, 0, $novo_w, $novo_h, $old_w, $old_h);

            ImageJPEG($thumb, $end_imagem, 89);
            ImageDestroy($img_origem);
            ImageDestroy($thumb);
        }
    }

    public static function destroiImagem($id) {

        $endereco = "file/fotos/" . $id . ".jpg";
        if (file_exists($endereco)) {
            unlink($endereco);
        }
    }
    
    public static function alteraImagem($file, $id) {

        Util_Imagem::destroiImagem($id);
        Util_Imagem::salvaImagem($file, $id);
    }

}

?>
