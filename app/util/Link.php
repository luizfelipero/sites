<?php

class Util_Link {

    public static function link($d, $m, $c, $a, $id = "", $etc = "") {
        print_r($d);
        if ($d == "" && $m == "" && $c == "" && $a == "")
            $ret = "";
        else
            $ret = "?d=" . $d . "&m=" . $m . "&c=" . $c . "&a=" . $a;

        if ($id != "")
            $ret .= "&id=" . $id;
        $ret .= $etc;
        return $ret;
    }

    public static function menuNivel1($rotulo, $arg = "") {

        if ($arg != "") {
            if ($arg['badge'] != "" && $arg['badge'] != 0) {
                $badge = "<span class=\"label label-danger pull-right\" style=\"margin-right: 5px\">" . $arg['badge'] . "</span>";
            }
            if ($arg['novo'] != "") {
                $novo = " <img style=\"margin-bottom: 10px;\" src=\"img_p/novo.png\" width=\"20px\"/>";
            }
        }
        return "<a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">" . $rotulo . $novo . "<img class=\"pull-right\" style=\"margin-top: 6px\" src=\"img_p/seta.png\"/>" . $badge . "</a>";
    }

    public static function menuNivel1Direto($d, $m, $c, $a, $rotulo, $arg = "") {

        if (Adm_PermissaoMapper::getInstance()->autorizado($d, $m, $c, $a)) {
            if ($arg != "") {
                if ($arg['badge'] != "" && $arg['badge'] != 0) {
                    $badge = "<span class=\"label label-danger pull-right\">" . $arg['badge'] . "</span>";
                }
                if ($arg['extra'] != "") {
                    $extra = $arg['extra'];
                }
                if ($arg['novo'] != "") {
                    $novo = " <img style=\"margin-bottom: 10px;\" src=\"img_p/novo.png\" width=\"20px\"/>";
                }
            }
            $retorno = "<li><a href=\"" . Util_Link::link($d, $m, $c, $a, "", $extra) . "\">" . $rotulo . $novo . $badge . "</a></li>";
        }
        return $retorno;
    }

    public static function menuNivel2($d, $m, $c, $a, $rotulo, $arg = "") {

        if ($arg != "") {
            if ($arg['badge'] != "" && $arg['badge'] != 0) {
                $badge = "<span class=\"label label-danger pull-right\">" . $arg['badge'] . "</span>";
            }
            if ($arg['extra'] != "") {
                $extra = $arg['extra'];
            }
            if ($arg['novo'] != "") {
                $novo = " <img style=\"margin-bottom: 10px;\" src=\"img_p/novo.png\" width=\"20px\"/>";
            }
        }

        $retorno = "<li ";
        if (!Adm_PermissaoMapper::getInstance()->autorizado($d, $m, $c, $a))
            $retorno .= "class=\"disabled\"";
        $retorno .= " ><a href=\"" . Util_Link::link($d, $m, $c, $a, "", $extra) . "\">" . $rotulo . $novo . $badge . "</a></li>";

        return $retorno;
    }

    public static function linkHideForm($d, $m, $c, $a) {

        if ($d == "" && $m == "" && $c == "" && $a == "")
            $ret = "";
        else {
            $ret = "<input type=\"hidden\" name=\"d\" value=\"" . $d . "\">";
            $ret = "<input type=\"hidden\" name=\"m\" value=\"" . $m . "\">";
            $ret .= "<input type=\"hidden\" name=\"c\" value=\"" . $c . "\">";
            $ret .= "<input type=\"hidden\" name=\"a\" value=\"" . $a . "\">";
        }
        return $ret;
    }

    public static function redirect($d, $m, $c, $a, $id = "", $etc = "", $tipo = "") {

        if ($GLOBALS['ajax'] == 1) {
            if ($d == $_GET['d'] && $m == $_GET['m'] && $c == $_GET['c'] && $a == $_GET['a']) {
                Util_Link::redirectStay($d, $m, $c, $a, $id, $etc, $tipo);
            } else {
                Util_Link::redirectForced($d, $m, $c, $a, $id, $etc, $tipo);
            }
        } else {
            Util_Link::redirectForced($d, $m, $c, $a, $id, $etc, $tipo);
        }
    }

    public static function redirectForced($d, $m, $c, $a, $id = "", $etc = "") {

        if ($GLOBALS['ajax'] == 1) {
            $etc .= "&ajax=1";
        }
        $url = Util_Link::link($d, $m, $c, $a, $id, $etc);
        header("Location: " . $url);
        exit();
    }

    public static function redirectStay($d, $m, $c, $a, $id = "", $etc = "") {

        $url = Util_Link::link($d, $m, $c, $a, $id, $etc);

        if ($GLOBALS['ajax'] == 1) {
            echo Util_FlashMessage::readAssync();
            exit();
        } else {
            header("Location: " . $url);
            exit();
        }
    }

    public static function stringfyUrl() {
        $get = $GLOBALS['GET'];
        foreach ($get as $chave => $value) {
            if (is_array($value)) {
                foreach ($value as $chaveV => $valueV) {
                    $valueV = Util_Utilidade::trataCaracteres($valueV);
                    $getArray[] = $chave . "[]=" . $valueV;
                }
            } else {
                $value = Util_Utilidade::trataCaracteres($value);
                $getArray[] = $chave . "=" . $value;
            }
        }
        $url = "?" . implode("&", $getArray);
        return $url;
    }

}

?>
