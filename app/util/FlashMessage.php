<?php

class Util_FlashMessage {

    public static function write($msgs, $tipo = "") {

        if (!is_array($msgs))
            $mensagens[] = $msgs;
        else
            $mensagens = $msgs;

        foreach ($mensagens as $msg) {
            if ($_SESSION['flash-msg'] != "") {
                $_SESSION['flash-msg'] = $_SESSION['flash-msg'] . "+" . $msg;
                $_SESSION['flash-tipo'] = $_SESSION['flash-tipo'] . "+" . $tipo;
            } else {
                $_SESSION['flash-msg'] = $msg;
                $_SESSION['flash-tipo'] = $tipo;
            }
        }
    }

    public static function read() {
        $retorno = "";
        if (Util_FlashMessage::exists()) {
            $msgs = explode("+", $_SESSION['flash-msg']);
            $tipos = explode("+", $_SESSION['flash-tipo']);
            $total = count($msgs);
            for ($i = 0; $i < $total; $i++) {
                $retorno .= Util_FlashMessage::montaDivs($msgs[$i], $tipos[$i]);
            }
        } else {
            //$retorno .= Util_FlashMessage::montaDivs("nada a declarar", "info");
        }
        $_SESSION['flash-msg'] = "";
        return $retorno;
    }

    public static function montaDivs($msg, $tipo) {
        if ($tipo != "")
            $tipo = "alert-" . $tipo;
        else 
            $tipo = "alert-warning";
        $retorno = '<div class="alert ' . $tipo . ' alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Aviso!</strong>  ' . $msg . '</div>';
        
        return $retorno;
    }
    
    public static function readAssync() {
        $retorno = "";
        if (Util_FlashMessage::exists()) {
            $msgs = explode("+", $_SESSION['flash-msg']);
            $tipos = explode("+", $_SESSION['flash-tipo']);
            $total = count($msgs);
            for ($i = 0; $i < $total; $i++) {
                $retorno[] = array("tipo" => $tipos[$i], "texto" => $msgs[$i]);
            }
        } else {
            //$retorno .= Util_FlashMessage::montaDivs("nada a declarar", "info");
        }
        $_SESSION['flash-msg'] = "";
        $retorno = json_encode($retorno);
        return $retorno;
    }

    public static function exists() {

        if ($_SESSION['flash-msg'] != "")
            return true;
        else
            return false;
    }

}

?>