<?php

class Util_RelatorioFileira {

    private $nome;
    private $args;

    public function __construct($nome, $args) {
        $this->nome = $nome;
        $this->args = $args;
    }

    /**
     * @return the $nome
     */
    public function getNome() {
        return $this->nome;
    }

    /**
     * @return the $args
     */
    public function getArgs() {
        return $this->args;
    }

}

?>