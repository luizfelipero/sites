<?php

class Util_Token {

    public static function geraTokenUnico() {
        $token = md5(uniqid(microtime(), true));
        $_SESSION["token"] = $token;

        return $token;
    }

    public static function getTokenValido() {
        return $_SESSION['token'];
    }

}

?>