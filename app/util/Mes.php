<?php

class Util_Mes {

    private $ano;
    private $mes;

    function __construct($mes = "", $ano = "") {

        if ($mes == "") {

            $this->ano = date("Y");
            $this->mes = date("m");
        } else if ($mes instanceof Util_Mes) {
            $this->ano = $mes->getAno();
            $this->mes = $mes->getMes();
        } else if ($ano == "") {
            $partes = explode("-", $mes);
            $this->ano = $partes[0];
            $this->mes = $partes[1];
        } else {

            $this->ano = $ano;
            $this->mes = $mes;
        }
        $this->mes += 0;
    }

    function getUltimoDiaMes() {
        //mes 31
        if (in_array($this->mes, array(1, 3, 5, 7, 8, 10, 12))) {
            return 31;
        }
        //mes 30
        else if (in_array($this->mes, array(4, 6, 9, 11))) {
            return 30;
        }
        //fevereiro
        else if ($this->mes == 2) {
            if ($this->ano % 4 == 0) {
                return 29;
            } else {
                return 28;
            }
        }
    }

    function getDataFinal() {
        return $this->ano . "-" . Util_Utilidade::addZeros($this->mes, 2) . "-" . $this->getUltimoDiaMes();
    }

    function getDataInicio() {
        return $this->ano . "-" . Util_Utilidade::addZeros($this->mes, 2) . "-01";
    }

    function getProximoMes() {
        return $this->getMesDepois(1);
    }

    function getAnteriorMes() {
        return $this->getMesAntes(1);
    }

    function getMesDepois($quantos = 1) {

        $mes = $this->mes;
        $ano = $this->ano;

        if (($quantos + $mes) > 24) {
            $mes = ($quantos + $mes - 24);
            $ano = $ano + 2;
        } else if (($quantos + $mes) > 12) {
            $mes = ($quantos + $mes - 12);
            $ano++;
        } else {
            $mes = $mes + $quantos;
        }
        return new Util_Mes($mes, $ano);
    }

    function getMesAntes($quantos = 1) {

        if ($this->mesAnterior[$quantos] == "") {
            $mes = $this->mes;
            $ano = $this->ano;

            if (($mes - $quantos) < (-11)) {
                $mes = ($mes - $quantos + 24);
                $ano = $ano - 2;
            } else if (($mes - $quantos) < 1) {
                $mes = ($mes - $quantos + 12);
                $ano--;
            } else {
                $mes = $mes - $quantos;
            }
            $this->mesAnterior[$quantos] = new Util_Mes($mes, $ano);
        }
        return $this->mesAnterior[$quantos];
    }

    function getMesesDepois($quantos) {

        $mes = $this->mes;
        $ano = $this->ano;

        $meses[] = new Util_Mes($mes, $ano);

        for ($i = 1; $i < $quantos; $i++) {

            if (($mes + $i) > 12) {
                $n_mes = ($mes + $i - 12);
                $n_ano = $ano + 1;
            } else {
                $n_mes = $mes + $i;
                $n_ano = $ano;
            }
            $meses[] = new Util_Mes($n_mes, $n_ano);
        }

        return $meses;
    }

    function getMesesAntes($quantos) {

        $mes = $this->mes;
        $ano = $this->ano;

        $meses[] = new Util_Mes($mes, $ano);

        for ($i = 1; $i < $quantos; $i++) {

            if (($mes - $i) < 1) {
                $n_mes = ($mes - $i + 12);
                $n_ano = $ano - 1;
            } else {
                $n_mes = $mes - $i;
                $n_ano = $ano;
            }
            $meses[] = new Util_Mes($n_mes, $n_ano);
        }
        $meses = array_reverse($meses);
        return $meses;
    }

    function getMesesEntre(Mes $mesFim) {

        //condição de que $this->mes seja menor que $mesFim
        if (
                ($this->ano < $mesFim->getAno()) || (
                ($this->ano == $mesFim->getAno()) &&
                ($this->mes <= $mesFim->getMes())
                )
        ) {

            $mesPonteiro = new Util_Mes($this->mes, $this->ano);
            while ($mesPonteiro->getMesCompleto() != $mesFim->getMesCompleto()) {

                $meses[] = $mesPonteiro;
                $mesPonteiro = $mesPonteiro->getProximoMes();
            }
            $meses[] = $mesPonteiro;
        }

        return $meses;
    }

    public function getMesCompleto() {
        return $this->ano . "-" . Util_Utilidade::addZeros($this->mes, 2);
    }

    public function getNome() {
        $meses = array('jan', 'fev', 'mar', 'abr', 'mai', 'jun', 'jul', 'ago', 'set', 'out', 'nov', 'dez');
        $retorno = $meses[$this->mes - 1] . "-" . $this->ano;
        return $retorno;
    }

    public function getNomeResumido() {
        $meses = array('jan', 'fev', 'mar', 'abr', 'mai', 'jun', 'jul', 'ago', 'set', 'out', 'nov', 'dez');
        $retorno = $meses[$this->mes - 1] . "-" . $this->getAnoResumido();
        return $retorno;
    }

    public function getAnoResumido() {
        $anoResumido = substr($this->ano, 2);
        return $anoResumido;
    }

    public function getMesAddZeros() {
        return Util_Utilidade::addZeros($this->mes, 2);
    }

    //REGULAR GETTERS

    public function getAno() {
        return $this->ano;
    }

    public function getMes() {
        return $this->mes;
    }

}

?>
