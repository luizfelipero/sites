<?php

class Util_Relatorio {

    private $mapper;
    private $args;
    private $colunas;
    private $linhas;
    private $colunaTotal;
    private $colunaPorcentagem;
    private $linhaTotal;
    private $metodo;
    private $valores;

    public function __construct($mapper, $args, $opcoes) {

        $this->mapper = $mapper;

        $this->args = $args;

        $this->colunaTotal = ($opcoes['colTotal'] == "") ? 1 : $opcoes['colTotal'];
        $this->colunaPorcentagem = ($opcoes['colPerc'] == "") ? 0 : $opcoes['colPerc'];
        $this->linhaTotal = ($opcoes['linTotal'] == "") ? 1 : $opcoes['linTotal'];

        $this->metodo = ($opcoes['metodo'] == "") ? "s" : "c";
        if ($this->metodo == "s") {
            $this->sumCol = $opcoes['sumCol'];
        }
    }

    public function addColunas($mapper, $args, $argSecundario) {

        $results = $mapper->fetchAll($args, "");
        foreach ($results as $r) {
            $this->addColuna($r->getNome(), array(array($argSecundario, "=", $r->getId())));
        }
    }

    public function addLinhas($mapper, $args, $argSecundario) {

        $results = $mapper->fetchAll($args, "");
        foreach ($results as $r) {
            $this->addLinha($r->getNome(), array(array($argSecundario, "=", $r->getId())));
        }
    }

    public function addColunasMes() {
        //implementar depois
    }

    public function addColuna($nome, $args) {
        $this->colunas[] = new RelatorioFileira($nome, $args);
    }

    public function addLinha($nome, $args) {
        $this->linhas[] = new RelatorioFileira($nome, $args);
    }

    public function populate() {

        $linhas = count($this->linhas);
        $colunas = count($this->colunas);

        for ($a = 0; $a < $linhas; $a++) {
            for ($b = 0; $b < $colunas; $b++) {
                $this->getValor($a, $b);
            }
        }
    }

    public function getValor($a, $b) {

        $args = $this->args;
        if ($this->linhas[$a]->getArgs() != "")
            $args = array_merge($args, $this->linhas[$a]->getArgs());
        if ($this->colunas[$b]->getArgs() != "")
            $args = array_merge($args, $this->colunas[$b]->getArgs());

        if ($this->metodo == "c") {
            $this->valores[$a][$b] = $this->mapper->count($args, "");
        } else if ($this->metodo == "s") {
            $this->valores[$a][$b] = $this->mapper->sum($this->sumCol, $args, "");
        }
    }

    public function get() {
        return $this->valores;
    }

    public function getTotalLinha($l) {
        return array_sum($this->valores[$l]);
    }

    public function getPorcentagemLinha($l) {
        return ($this->getTotalLinha($l) / $this->getTotal());
    }

    public function getTotalColuna($c) {
        $total = 0;
        for ($l = 0; $l < (count($this->linhas)); $l++) {
            $total += $this->valores[$l][$c];
        }
        return $total;
    }

    public function getTotal() {
        $ret = 0;
        foreach ($this->valores as $array) {
            $ret+= array_sum($array);
        }
        return $ret;
    }

    public function exibe() {

        $colunas = 1 + count($this->colunas);
        if ($this->colunaTotal == 1)
            $colunas++;
        if ($this->colunaPorcentagem == 1)
            $colunas++;

        $linhas = 1 + count($this->linhas);
        if ($this->linhaTotal == 1)
            $linhas++;

        $r = "<table>";
        //cabecalho
        $r.= "<tr>";
        $r.= "<th class=\"blank\"></th>";
        foreach ($this->colunas as $b) {
            $r .= "<th>" . $b->getNome() . "</th>";
        } if ($this->colunaTotal == 1) {
            $r .= "<th>Total</th>";
        } if ($this->colunaPorcentagem == 1) {
            $r .= "<th>%</th>";
        }
        $r.= "</tr>";
        //corpo
        $a = 0;
        foreach ($this->linhas as $l) {
            $r.= "<tr>";
            $r.= "<th class=\"thl\">" . $l->getNome() . "</th>";
            $b = 0;
            foreach ($this->colunas as $c) {
                $r.= "<td class=\"tdC0\">" . $this->valores[$a][$b] . "</td>";
                $b++;
            }
            if ($this->colunaTotal == 1) {
                $r .= "<td class=\"tdC0\">" . $this->getTotalLinha($a) . "</td>";
            } if ($this->colunaPorcentagem == 1) {
                $r .= "<td class=\"tdC0\">" . Util_Utilidade::exibePorcentagem($this->getPorcentagemLinha($a)) . "</td>";
            }
            $a++;
        }
        //rodape
        if ($this->linhaTotal == 1) {
            $r.= "<tr>";
            $r.= "<th class=\"thl\">Total</th>";
            for ($c = 0; $c < count($this->colunas); $c++) {
                $r .= "<td class=\"tdC0\">" . $this->getTotalColuna($c) . "</td>";
            } if ($this->colunaTotal == 1) {
                $r .= "<td class=\"tdC0\">" . $this->getTotal() . "</td>";
            } if ($this->colunaPorcentagem == 1) {
                $r .= "<td class=\"tdC0\">100%</td>";
            }
            $r.= "</tr>";
        }



        return $r;
    }

}

?>