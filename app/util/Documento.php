<?php

class Util_Documento {

    public static function exibe($tipo, $nome, $ext) {

        if ($tipo == "aloc") {
            $destino = "file/" . $tipo . "/" . $nome . $ext;
        } if ($tipo == "msg") {
            $destino = "file/" . $tipo . "/" . $nome . $ext;
        } else if ($tipo == "plano" || $tipo == "ata" || $tipo == "relatorio") {
            $destino = "file/acao/" . $tipo . "/" . $nome . $ext;
        } else if ($tipo == "exame") { 
            $destino = "file/exame/" . $nome . $ext;
        } else {
            $destino = "file/doc/" . $tipo . "/" . $nome . $ext;
        }
        return $destino;
    }

    public static function validaArquivo($file, $tipo) {

        $nome_arquivo = $file['name'];

        $extensoes_validas['aloc'] = array(".txt", ".csv");
        $extensoes_validas['form'] = array(".xls", ".xlsx", ".xlsm", ".pdf", ".doc", ".docx", ".png");
        $extensoes_validas['doc'] = array(".pdf");
        $extensoes_validas['os'] = array(".doc", ".docx", ".pdf");
        $extensoes_validas['pop'] = array(".pdf");
        $extensoes_validas['proj'] = array(".pdf");
        $extensoes_validas['ps'] = array(".pdf");
        $extensoes_validas['dcf'] = array(".pdf");
        $extensoes_validas['msg'] = array(".xls", ".xlsx", ".pdf", ".doc", ".docx", ".zip", ".rar");
        $extensoes_validas['plano'] = array(".xls", ".xlsx", ".pdf", ".doc", ".docx", ".png", ".zip", ".rar");
        $extensoes_validas['ata'] = array(".xls", ".xlsx", ".pdf", ".doc", ".docx", ".png", ".zip", ".rar");
        $extensoes_validas['relatorio'] = array(".xls", ".xlsx", ".pdf", ".doc", ".docx", ".png", ".zip", ".rar");
        $extensoes_validas['exame'] = array(".pdf",".jpeg",".jpg");
        $ext = strtolower(strrchr($nome_arquivo, '.'));

        if (!in_array($ext, $extensoes_validas[$tipo])) {
            throw new Exception("Extensao invalida");
        } else {
            return true;
        }
    }

    public static function salvaArquivo($file, $tipo, $nome) {

        $nome_arquivo = $file['name'];
        $tamanho_arquivo = $file['size'];
        $arquivo_temporario = $file['tmp_name'];

        $extensoes_validas['aloc'] = array(".txt", ".csv");
        $extensoes_validas['form'] = array(".xls", ".xlsx", ".xlsm", ".pdf", ".doc", ".docx", ".png");
        $extensoes_validas['doc'] = array(".pdf");
        $extensoes_validas['os'] = array(".doc", ".docx", ".pdf");
        $extensoes_validas['pop'] = array(".pdf");
        $extensoes_validas['proj'] = array(".pdf");
        $extensoes_validas['ps'] = array(".pdf");
        $extensoes_validas['dcf'] = array(".pdf");
        $extensoes_validas['msg'] = array(".xls", ".xlsx", ".pdf", ".doc", ".docx", ".zip", ".rar");
        $extensoes_validas['plano'] = array(".xls", ".xlsx", ".pdf", ".doc", ".docx", ".png", ".zip", ".rar");
        $extensoes_validas['ata'] = array(".xls", ".xlsx", ".pdf", ".doc", ".docx", ".png", ".zip", ".rar");
        $extensoes_validas['relatorio'] = array(".xls", ".xlsx", ".pdf", ".doc", ".docx", ".png", ".zip", ".rar");
        $extensoes_validas['exame'] = array(".pdf",".jpeg",".jpg");
        $ext = strtolower(strrchr($nome_arquivo, '.'));

        if ($tipo == "aloc") {
            $end_arquivo = "file/" . $tipo . "/" . $nome . $ext;
        } else if ($tipo == "msg") {
            $end_arquivo = "file/" . $tipo . "/" . $nome . $ext;
        } else if ($tipo == "plano" || $tipo == "ata" || $tipo == "relatorio") {
            $end_arquivo = "file/acao/" . $tipo . "/" . $nome . $ext;
        } else if ($tipo == "exame") { 
            $end_arquivo = "file/exame/" . $nome . $ext;
        } else {
            $end_arquivo = "file/doc/" . $tipo . "/" . $nome . $ext;
        }
      

        if (!in_array($ext, $extensoes_validas[$tipo])) {
            throw new Exception("Extensao invalida");
        } else {
            move_uploaded_file($arquivo_temporario, $end_arquivo);
        }
    }

    public static function destroiArquivo($tipo, $nome, $ext) {

        
        
        if ($tipo == "aloc") {
            $end_arquivo = "file/" . $tipo . "/" . $nome . $ext;
        } else if ($tipo == "plano" || $tipo == "ata" || $tipo == "relatorio") {
            $end_arquivo = "file/acao/" . $tipo . "/" . $nome . $ext;
        } else if ($tipo == "exame") { 
            $end_arquivo = "file/exame/" . $nome . $ext;
        } else {
            $end_arquivo = "file/doc/" . $tipo . "/" . $nome . $ext;
        }

        if (file_exists($end_arquivo)) {
            unlink($end_arquivo);
        }
    }

    public static function alteraArquivo($file, $nome, $tipo, $ext) {
        
        Util_Documento::destroiArquivo($tipo, $nome, $ext);
        Util_Documento::salvaArquivo($file, $tipo, $nome);
    }

    public static function existeArquivo($tipo, $nome, $ext) {
        //$end_arquivo = "file/doc/" . $tipo . "/" . $tipo . "_" . $id . "." . $ext;
        
        if ($tipo == "aloc") {
            $destino = "file/" . $tipo . "/" . $nome . $ext;
        } if ($tipo == "msg") {
            $destino = "file/" . $tipo . "/" . $nome . $ext;
        } else if ($tipo == "plano" || $tipo == "ata" || $tipo == "relatorio") {
            $destino = "file/acao/" . $tipo . "/" . $nome . $ext;
        } else if ($tipo == "exame") { 
            $destino = "file/exame/" . $nome . $ext;
        } else {
            $destino = "file/doc/" . $tipo . "/" . $nome . $ext;
        }
        
        
        //echo $end_arquivo;
        if (file_exists($destino)) {
            return true;
        } else {
            return false;
        }
    }

    public static function novaRevisao($data) {
        $hoje = date("Y-m-d");
        if (Util_Time::difDiasCorrido($hoje, $data) < 30) {
            return "<span class=\"label label-success pull-right\">Novo!</span>";
        }
    }

}

?>
