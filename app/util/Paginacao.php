<?php

class Util_Paginacao {

    public function __construct($arg, $extras) {
        $this->arg = $arg;
        if ($this->arg['index'] == "")
            $this->arg['index'] = 0;
        if ($this->arg['limit'] == "" || $this->arg['limit'] == 0)
            $this->arg['limit'] = 1;
        $this->extras = $extras;
    }

    public function exibe($ajax = false) {

        if (is_array($this->extras)) {

            reset($this->extras);
            while (list($key, $val) = each($this->extras)) {
                if (is_array($val)) {
                    while (list($key2, $val2) = each($val)) {
                        $ext .= "&" . $key . "%5B%5D=" . $val2;
                    }
                } else {
                    $ext .= "&" . $key . "=" . $val;
                }
            }
        }

        $link = $this->arg['link'] . $ext;

        $paginas = ceil($this->arg['total'] / $this->arg['limit']);

        $ret.="<div style=\"text-align: center\">";
        $ret.="<ul class=\"pagination\">";
        if ($this->arg['index'] > 0) {
            if ($ajax == true) {
                $ret .= "<li><a class=\"ajax-request\" href=\"#\" data-link=\"" . $link . "&index=0\"><i class=\"fa fa-backward\"></i></a></li>";
                $ret .= "<li><a class=\"ajax-request\" href=\"#\" data-link=\"" . $link . "&index=" . ($this->arg['index'] - 1) . "\"><i class=\"fa fa-chevron-left\"></i></a></li>";
            } else {
                $ret .= "<li><a href=\"" . $link . "&index=0\"><i class=\"fa fa-backward icon-red\"></i></a></li>";
                $ret .= "<li><a href=\"" . $link . "&index=" . ($this->arg['index'] - 1) . "\"><i class=\"fa fa-chevron-left icon-red\"></i></a></li>";
            }
        }

        //get limites superior e inferior
        $lSuperior = (($paginas - $this->arg['index']) > 5) ? ($this->arg['index'] + 5) : ($paginas - 1);
        $lInferior = (($this->arg['index']) < 5) ? 0 : ($this->arg['index'] - 5);

        for ($i = $lInferior; $i <= $lSuperior; $i++) {
            $ret.="<li";
            $ret.= ($this->arg['index'] == $i) ? " class=\"active\" " : "";
            if ($ajax == true) {
                $ret.= "><a class=\"ajax-request\" href=\"#\" data-link=\"" . $link . "&index=" . $i . "\">" . ($i + 1) . "</a></li>";
            } else {
                $ret.= "><a href=\"" . $link . "&index=" . $i . "\">" . ($i + 1) . "</a></li>";
            }
        }

        if ($this->arg['index'] < ($paginas - 1)) {
            if ($ajax == true) {
                $ret .= "<li><a class=\"ajax-request\" href=\"#\" data-link=\"" . $link . "&index=" . ($this->arg['index'] + 1) . "\"><i class=\"fa fa-chevron-right icon-red\"></i></a></li>";
                $ret .= "<li><a class=\"ajax-request\" href=\"#\" data-link=\"" . $link . "&index=" . ($paginas - 1) . "\"><i class=\"fa fa-forward icon-red\"></i></a></li>";
            } else {
                $ret .= "<li><a href=\"" . $link . "&index=" . ($this->arg['index'] + 1) . "\"><i class=\"fa fa-chevron-right icon-red\"></i></a></li>";
                $ret .= "<li><a href=\"" . $link . "&index=" . ($paginas - 1) . "\"><i class=\"fa fa-forward icon-red\"></i></a></li>";
            }
        }

        $ret.="</ul>";
        $ret.="</div>";

        return $ret;
    }

}
