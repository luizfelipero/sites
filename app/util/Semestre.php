<?php

class Util_Semestre {

    private $ano;
    private $semestre;

    function __construct($arg) {

        //possiveis entradas
        //1234567890
        //2016-01-31   data
        //2016-01      mes
        //2016-1       semestre

        if (!$arg) {
            $this->ano = date('Y');
            $mes = date('m');
            if ($mes <= 6) {
                $this->semestre = 1;
            } else {
                $this->semestre = 2;
            }
        } else if (is_array($arg)) {
            
            $this->ano = $arg['ano'];
            $this->semestre = $arg['semestre'];
        } else if (strlen($arg) > 6) {
            $partes = explode("-", $arg);

            $this->ano = $partes[0];
            if ($partes[1] <= 6) {
                $this->semestre = 1;
            } else {
                $this->semestre = 2;
            }
        } else {
            $partes = explode("-", $arg);
            $this->ano = $partes[0];
            $this->semestre = $partes[1];
        }
    }

    function getDataInicio() {
        if ($this->semestre == 1) {
            return $this->ano . "-01-01";
        } else {
            return $this->ano . "-07-01";
        }
    }

    function getDataFinal() {
        if ($this->semestre == 1) {
            return $this->ano . "-06-30";
        } else {
            return $this->ano . "-12-31";
        }
    }

    function getMesInicio($createObject = false) {
        if ($this->semestre == 1) {
            $retorno = $this->ano . "-01";
        } else {
            $retorno = $this->ano . "-07";
        }

        if ($createObject) {
            $retorno = new Util_Mes($retorno);
        }
        return $retorno;
    }

    function getMesFinal($createObject = false) {
        if ($this->semestre == 1) {
            $retorno = $this->ano . "-06";
        } else {
            $retorno = $this->ano . "-12";
        }

        if ($createObject) {
            $retorno = new Util_Mes($retorno);
        }
        return $retorno;
    }

    public function getMesesSemestre() {

        return array(
            'primeiro' => $this->getMesInicio(),
            'segundo' => $this->getMesFinal()
        );
    }

    public function getMesesSemestrePpr() {

        return array(
            'primeiro' => $this->getMesInicio(),
            'segundo' => $this->getMesFinalPpr()
        );
    }

    function getMesFinalPpr($createObject = false) {
        if ($this->ano == date("Y")) {
            if ($this->semestre == 1) {
                if (date('m') > 6) {
                    $retorno = $this->ano . "-06";
                } else {
                    $retorno = $this->ano . "-05";
                }
            } else {
                $retorno = $this->ano . "-11";
            }
        } else {
            if ($this->semestre == 1) {
                $retorno = $this->ano . "-06";
            } else {
                $retorno = $this->ano . "-12";
            }
        }

        if ($createObject) {
            $retorno = new Util_Mes($retorno);
        }
        return $retorno;
    }

    function getProximoSemestre() {
        return $this->getSemestreDepois(1);
    }

    function getAnteriorSemestre() {
        return $this->getSemestreAntes(1);
    }

    function getSemestreDepois($quantos = 1) {

        $semestre = $this->semestre;
        $ano = $this->ano;

        if (($quantos + $semestre) > 2) {
            $semestre = ($quantos + $semestre - 2);
            $ano++;
        } else {
            $semestre = $semestre + $quantos;
        }
        return new Util_Semestre(array(
            "semestre" => $ano . "-" . $semestre
                )
        );
    }

    function getSemestreAntes($quantos = 1) {

        $semestre = $this->semestre;
        $ano = $this->ano;

        if (($semestre - $quantos) < 1) {
            $semestre = ($semestre - $quantos + 2);
            $ano--;
        } else {
            $semestre = $semestre - $quantos;
        }
        return new Util_Semestre(array(
            "semestre" => $ano . "-" . $semestre
                )
        );
    }

    public function getSemestreCompleto() {
        return $this->ano . "-" . $this->semestre;
    }

    public function getMeses() {

        if ($this->semestre == 1) {
            $base = 1;
        } else {
            $base = 7;
        }

        for ($i = 0; $i < 6; $i++) {
            $mes = $base + $i;
            $retorno[] = new Util_Mes($mes, $this->ano);
        }
        return $retorno;
    }

    public function getNome() {

        return $this->ano . "." . $this->semestre;
    }

    //REGULAR GETTERS

    public function getAno() {
        return $this->ano;
    }

    function getSemestre() {
        return $this->semestre;
    }

}
?>
