<?php

class Util_PaginacaoLetras {

    public function __construct($arg, $extras) {
        $this->arg = $arg;
        $this->extras = $extras;
    }

    public function exibe() {

        if (is_array($this->extras)) {

            reset($this->extras);
            while (list($key, $val) = each($this->extras)) {
                $ext .= "&" . $key . "=" . $val;
            }
        }

        $link = $this->arg['link'] . $ext;

        $paginas = 26;

        $ret.="<div style=\"text-align: center\">";
        $ret.="<ul class=\"pagination\">";
        $ret .= "<li><a href=\"" . $link . "&inicial=\">Todas</a></li>";
        for ($i = 65, $j = 0; $j < 26; $i++, $j++) {
            $ret.="<li";
            $ret.= ($this->arg['default'] == chr($i)) ? " class=\"active\" " : "";
            $ret.= "><a href=\"" . $link . "&inicial=" . chr($i) . "\">" . chr($i) . "</a></li>";
        }
        $ret.="</ul>";
        $ret.="</div>";

        return $ret;
    }

}