<?php

class Util_IteratorCor {
    
    private $possiveis;
    private $ponteiro;
    private $cores;
    
    public function __construct() {
        $this->ponteiro = 0;
        $this->possiveis = array("label-success","label-warning","label-danger","label-info");
        $this->cores = array();
    }
    
    public function getCor($proximo) {
        
        if (!isset($this->cores[$proximo])) {
            $this->cores[$proximo] = $this->possiveis[$this->ponteiro];
            $this->ponteiro = ($this->ponteiro + 1) % count($this->possiveis);
        }
        return $this->cores[$proximo]; 
        
    }
    
    
}

