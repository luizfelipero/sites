<?php

class Util_Seguranca {

    //put your code here

    static function preventSqlInjectionLogin($campo, $removeEspaco = true) {

        $campo = str_replace("'", "", $campo);
        $campo = str_replace("-", "", $campo);
        $campo = str_replace("\"", "", $campo);
        if ($removeEspaco) {
            $campo = str_replace(" ", "", $campo);
        }
        $campo = str_replace(";", "", $campo);

        $campo = Util_Seguranca::preventSqlInjection($campo);

        return $campo;
    }

    static function preventSqlInjection($campo) {

        $campo = str_replace("'", "\'", $campo);
        $campo = str_replace(";", "", $campo);
        $campo = str_replace("=", "", $campo);

        return $campo;
    }

    static function preventSqlInjectionArray($array) {

        foreach ($array as $key => $campo) {

            $campo = str_replace("'", "\'", $campo);
            $campo = str_replace(";", "", $campo);
            $campo = str_replace("=", "", $campo);
            $campo = str_replace("\"", "&quot;", $campo);
            $array[$key] = $campo;
        }



        return $array;
    }

}

?>
