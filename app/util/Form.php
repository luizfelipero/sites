<?php

class Util_Form {

    public static function makeLists($objs, $extra) {

        $comp = "";
        $seletores = array("selecione", "none", "tg", "t", "g");
        $seletoresValores = array("", "", "tg", "t", "g");
        $seletoresRotulos = array("--Selecione--", "---", "Todos e Geral", "Todos", "Geral");

        //Se tiver seletor, coloca ele como 'selected'
        if (is_array($extra['seletores'])) {
            for ($i = 0; $i < count($seletores); $i++) {

                if (in_array($seletores[$i], $extra['seletores'])) {
                    $comp = "<option value=\"" . $seletoresValores[$i] . "\"";
                    if ($seletores[$i] == $extra['default'])
                        $comp .= " selected = \"selected\"";
                    $comp .= ">" . $seletoresRotulos[$i] . "</option>";
                    $retorno = $retorno . $comp;
                }
            }
        }

        //Coloca 'selected' nos objects que tão como default
        foreach ($objs as $obj) {

            if (is_object($obj)) {
                $id = $obj->getId();
                if ($obj instanceof Infra_Conta || $obj instanceof Infra_Servico) {
                    $nome = $obj->getAbrev();
                } else {
                    $nome = $obj->getNome();
                }
            } else {
                $id = $obj['id'];
                $nome = $obj['nome'];
            }

            $comp = "<option value=\"" . $id . "\"";

            if (is_array($extra['default'])) {
                if (in_array($id, $extra['default'])) {
                    $comp .= " selected = \"selected\"";
                }
            } else {
                if ($id == $extra['default']) {
                    $comp .= " selected = \"selected\"";
                }
            }
            $comp .= ">";
            $comp .= $nome;
            $comp .= "</option>";

            if ($extra['ord'] == 'c' || $extra['ord'] == '')
                $retorno .= $comp;
            else if ($extra['ord'] == 'd')
                $retorno = $comp . $retorno;
        }


        return $retorno;
    }

    public static function makeListsCasas($objs, $extra) {

        $comp = "";
        $seletores = array("selecione", "none", "tg", "t", "g");
        $seletoresValores = array("", "", "tg", "t", "g");
        $seletoresRotulos = array("--Selecione--", "---", "Todos e Geral", "Todos", "Geral");

        //Se tiver seletor, coloca ele como 'selected'
        if (is_array($extra['seletores'])) {
            for ($i = 0; $i < count($seletores); $i++) {

                if (in_array($seletores[$i], $extra['seletores'])) {
                    $comp = "<option value=\"" . $seletoresValores[$i] . "\"";
                    if ($seletores[$i] == $extra['default'])
                        $comp .= " selected = \"selected\"";
                    $comp .= ">" . $seletoresRotulos[$i] . "</option>";
                    $retorno = $retorno . $comp;
                }
            }
        }

        //Coloca 'selected' nos objects que tão como default
        foreach ($objs as $obj) {

            $comp = "<option value=\"" . $obj->getId() . "\"";

            if (is_array($extra['default'])) {
                if (in_array($obj->getId(), $extra['default'])) {
                    $comp .= " selected = \"selected\"";
                }
            } else {
                if ($obj->getId() == $extra['default']) {
                    $comp .= " selected = \"selected\"";
                }
            }
            $comp .= ">";

            $comp .= $obj->getNomeCompleto();

            $comp .= "</option>";

            if ($extra['ord'] == 'c' || $extra['ord'] == '')
                $retorno .= $comp;
            else if ($extra['ord'] == 'd')
                $retorno = $comp . $retorno;
        }


        return $retorno;
    }

    public static function makeListHoras($default = "", $inicio = 7, $fim = 18) {

        for ($i = $inicio; $i <= $fim; $i++) {
            $ret .= '<option value="' . $i . '"';
            if ($i == $default) {
                $ret .= ' selected="selected"';
            }

            $ret .= '>' . $i . ':00</option>';
        }
        return $ret;
    }

    public static function makeListUnidades($extra = "") {

        $arg[] = array("ativo", "=", 1);
        $arg[] = array("producao", "=", 0);

        $objs = Adm_UnidadeMapper::getInstance()->fetchAll($arg, array("order" => "tipo_id, superior_id, nome"));

        if ($extra['selecione'] == "selecione") {
            $comp = '<option value=""';
            $comp .= ">--Selecione--</option>";
            $retorno = $retorno . $comp;
        }


        foreach ($objs as $obj) {

            $comp = "<option value=\"" . $obj->getId() . "\"";


            //Se $extra['default'] for um array, faz foreach (casos dos select2 multiple)
            if (is_array($extra['default'])) {

                if (in_array($obj->getId(), $extra['default'])) {
                    $comp .= " selected = \"selected\"";
                }
            } else {

                if ($obj->getId() == $extra['default']) {
                    $comp .= " selected = \"selected\"";
                }
            }
            $comp .= ">";
            $comp .= $obj->getNomeCompleto();
            $comp .= "</option>";

            if ($extra['ord'] == 'c' || $extra['ord'] == '')
                $retorno .= $comp;
            else if ($extra['ord'] == 'd')
                $retorno = $comp . $retorno;
        }


        return $retorno;
    }

    public static function makeListsCores($objs, $extra) {

        $comp = "";
        foreach ($objs as $obj) {
            if ($extra['default'] != "") {
                $cor = "#" . $obj->getHexDec();
                $selected = "";
                if ($extra['default'][0] == $cor) {
                    $selected = 'selected="selected"';
                }
            }
            $comp = '<option ' . $selected . ' value="' . $obj->getHexdec() . '"';
            $comp .= ' style="background-color:#' . $obj->getHexdec() . '; padding:5px;">';
            $comp .= $obj->getNome();
            $comp .= '</option>';

            if ($extra['ord'] == 'c' || $extra['ord'] == '') {
                $retorno .= $comp;
            } else if ($extra['ord'] == 'd') {
                $retorno = $comp . $retorno;
            }
        }

        return $retorno;
    }

    public static function makeListAno($default = "", $qtdFim = 1) {

        $anoBase = 2010;
        $anoFim = date("Y") + $qtdFim;
        $ano = $anoBase;
        $dif = $anoFim - $anoBase;
        if ($default == "")
            $default = date("Y");

        for ($a = 0; $a <= $dif; $a++) {

            $ret .= "<option value=\"" . $ano . "\"";
            if ($ano == $default) {
                $ret .= " selected = \"selected\"";
            }
            $ret .= ">" . $ano . "</option>";
            $ano++;
        }

        return $ret;
    }

    public static function makeListMes($default = "") {

        $meses = array('jan', 'fev', 'mar', 'abr', 'mai', 'jun', 'jul', 'ago', 'set', 'out', 'nov', 'dez');

        if ($default == "")
            $default = date('m');

        for ($i = 1; $i <= 12; $i++) {
            $ret .= "<option value=\"" . $i . "\"";
            if ($i == $default) {
                $ret .= " selected = \"selected\"";
            }

            $ret .= ">" . $meses[($i - 1)] . "</option>";
        }
        return $ret;
    }

    public static function makeListLetras() {
        $base = 65;
        $ret .= "<option value=\"\">Todos</option>";
        for ($i = 0; $i <= 26; $i++) {
            $ret .= "<option value=\"" . chr($base + $i) . "\"";
            $ret .= ">" . chr($base + $i) . "</option>";
        }
        return $ret;
    }

    public static function makeRadioSimNao($id, $default, $disabled) {

        $ret = '<div class="radio">';
        $ret .= '<label>';
        $ret .= '<input name="' . $id . '" type="radio" value="1"';
        if ($default == 1) {
            $ret .= ' checked="checked" '; //espaço pra separar dentro da marcação
        }
        if ($disabled == 1) {
            $ret .= ' disabled '; //espaço pra separar dentro da marcação
        }
        $ret .= "> Sim";
        $ret .= '</div>';
        $ret .= '</label>';


        $ret .= '<div class="radio">';
        $ret .= '<label>';
        $ret .= '<input name="' . $id . '" type="radio" value="2"';
        if ($default == 2) {
            $ret .= ' checked="checked" '; //espaço pra separar dentro da marcação
        }
        if ($disabled == 1) {
            $ret .= ' disabled '; //espaço pra separar dentro da marcação
        }
        $ret .= "> N&atilde;o";
        $ret .= '</div>';
        $ret .= '</label>';

        return $ret;
    }

    public static function makeSelectUnidadesInserir($nivel = 3) {
        $unidades = Util_Form::makeLists(Adm_UnidadeMapper::getInstance()->fetchAll(array(array("nivel", "=", $nivel))), array("seletores" => array("selecione")));

        $first = true;
        $retorno = '<label>Unidade</label>';
        for ($i = $nivel; $i < 6; $i++) {
            $retorno .= '<select name="un_nivel' . $i . '" id="un_nivel' . $i . '" class="selectUpdate2 form-control ' . ($first ? "" : "hide") . '" data-url="?m=Adm&c=Unidade&a=ListarFilhasJson" data-destino="un_nivel' . ($i + 1) . '">' . ($first ? $unidades : "") . '</select>';
            $first = false;
        }
        $retorno .= '<select name="un_nivel6" id="un_nivel6" class="selectUpdate2 form-control hide"></select>';

        return $retorno;
    }

    public static function makeSelectUnidadesEditar($unidade_id) {
        $unidade = Adm_UnidadeMapper::getInstance()->findById($unidade_id);
        $arvore = $unidade->getArvore();

        $arvore[3] != null ? ($nivel3 = Util_Form::makeLists($arvore[2]->getSubordinadas(), array("default" => $arvore[3]->getId(), "seletores" => array("selecione")))) : $hideNivel3 = "hide";
        $arvore[4] != null ? ($nivel4 = Util_Form::makeLists($arvore[3]->getSubordinadas(), array("default" => $arvore[4]->getId(), "seletores" => array("selecione")))) : $hideNivel4 = "hide";
        $arvore[5] != null ? ($nivel5 = Util_Form::makeLists($arvore[4]->getSubordinadas(), array("default" => $arvore[5]->getId(), "seletores" => array("selecione")))) : $hideNivel5 = "hide";
        $arvore[6] != null ? ($nivel6 = Util_Form::makeLists($arvore[5]->getSubordinadas(), array("default" => $arvore[6]->getId(), "seletores" => array("selecione")))) : $hideNivel6 = "hide";

        return <<<EOD
            <label>Unidade</label>
            <select name="un_nivel3" id="un_nivel3" class="selectUpdate2 form-control $hideNivel3" data-url="?m=Adm&c=Unidade&a=ListarFilhasJson" data-destino="un_nivel4">
                $nivel3
            </select>
            <select name="un_nivel4" id="un_nivel4" class="selectUpdate2 form-control $hideNivel4" data-url="?m=Adm&c=Unidade&a=ListarFilhasJson" data-destino="un_nivel5">
                $nivel4
            </select>
            <select name="un_nivel5" id="un_nivel5" class="selectUpdate2 form-control $hideNivel5" data-url="?m=Adm&c=Unidade&a=ListarFilhasJson" data-destino="un_nivel6">
                $nivel5
            </select>
            <select name="un_nivel6" id="un_nivel6" class="selectUpdate2 form-control $hideNivel6">
                $nivel6
            </select>
EOD;
    }

    public static function makeUnidadePicker($name = "unidade", $obrigatorio = false, $unidade = "", $disabled = "") {
        //$text = $name;
        //$hidden = $name . "_id";
        //$obrigatorio = $obrigatorio ? "obrigatorio" : "";
        //$unidade_nome = $unidade != "" ? $unidade->getNome() : "";
        //$unidade_id = $unidade != "" ? $unidade->getId() : "";
        //args pro makeListUnidades
        $arg = array("selecione" => "selecione");
        if ($unidade != "") {
            $arg = array("default" => $unidade);
        }

        if ($obrigatorio == true) {
            $obrigatorio = "obrigatorio";
        }
        if ($disabled == "disabled") {
            $disabled = "disabled";
        }

        $ret = '<select ' . $disabled . ' name="' . $name . '_id" id="' . $name . '_id" class="form-control select2 ' . $obrigatorio . '">' .
                Util_Form::makeListUnidades($arg) .
                '</select>';
        return $ret;
    }

}

?>