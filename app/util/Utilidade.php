<?php

class Util_Utilidade {

    public static function trataZero($num) {

        if ($num == "" || ($num > -0.00001 && $num < 0.00001)) {
            return 0;
        } else {
            return $num;
        }
    }

    public static function exibeBlankSpace($qtd = 1) {
        for ($i = 1; $i <= $qtd; $i++) {
            $string .= "&nbsp;";
        }
        return $string;
    }

    public static function addZeros($var, $max) {
        while (mb_strlen($var) < $max) {
            $var = "0" . $var;
        }
        return $var;
    }
    public static function addEspacosEsq($var, $max) {
        while (mb_strlen($var) < $max) {
            $var = " " . $var;
        }
        return $var;
    }
    public static function addEspacosDir($var, $max) {
        while (mb_strlen($var) < $max) {
            $var = $var . " ";
        }
        return $var;
    }

    public static function exibeValor($num) {
        if ($num == "")
            $num = 0;
        $num = number_format($num, 2, ',', '.');
        return "R$ " . $num;
    }

    public static function formataValor($num) {
        if (strpos($num, ",") !== false) {
            $num = str_replace(".", "", $num);
            $num = str_replace(",", ".", $num);
        }
        return (float) $num;
    }

    public static function exibeNumero($num) {
        if ($num == "")
            $num = 0;

        $num = number_format($num, 2, ',', '.');
        return $num;
    }

    //DELETAR
    public static function exibeNumeroSemPonto($num) {
//        if ($num === "") {
//            $num = 0;
//        }
//
//        $num = number_format($num, 2, ',', '');
        return $num;
    }

    public static function formataNumero($num) {

        if ($num == "")
            $num = 0;
        if (strpos($num, ",") !== false) {
            $num = str_replace(".", "", $num);
            $num = str_replace(",", ".", $num);
        }
        return $num;
    }

    public static function exibePorcentagem($num, $casas = 0) {
        $num = $num * 100;
        $num = number_format($num, $casas, ',', '');
        $num = $num . "%";
        return $num;
    }

    public function exibeFarol($valor, $balizador = 0, $rotulos = "") {

        if ($valor > $balizador) {
            $ret = "&nbsp;<span class=\"label label-success\">";
            if ($rotulos != "")
                $ret .= $rotulos[0];
            else
                $ret.= "";
        } else {
            $ret = "&nbsp;<span class=\"label label-danger\">";
            if ($rotulos != "")
                $ret .= $rotulos[1];
            else
                $ret.= "";
        }
        $ret .= "&nbsp;</span>&nbsp;";
        return $ret;
    }

    public function farol($valor, $balizador = 0, $rotulo = "", $class = "badge", $classExtra = "", $opcoes = "") {

        if ($opcoes == "")
            $opcoes = array("success", "warning", "danger");
        if ($opcoes == "pcp")
            $opcoes = array("info", "success", "danger");
        if ($valor > $balizador) {
            $opcao = 0;
        } else if ($valor == $balizador) {
            $opcao = 1;
        } else {
            $opcao = 2;
        }

        $ret = "<span class=\"" . $class . " " . $classExtra . " " . $class . "-" . $opcoes[$opcao] . "\">";
        if (is_array($rotulo))
            $ret .= $rotulo[$opcao];
        else if ($rotulo !== "") {
            $ret.= $rotulo;
        } else {
            $ret.= "";
        }
        $ret .= "</span>";
        return $ret;
    }

    public function exibeFarolPrint($valor, $balizador = 0, $rotulos = "") {

        $ret = "<img src=\"img_p/farol_";
        if ($valor > $balizador)
            $ret .= "verde";
        else
            $ret .= "vermelho";
        $ret .= ".png\" border=\"0\">";
        return $ret;
    }

    //DELETAR
    public function calculaMeses($mes, $qtd, $dir) {

        for ($i = 0; $i < $qtd; $i++) {
            $ret[] = $mes;
            $mes = $mes->calculaMes($dir);
        }
        if ($dir == (-1)) {
            //inverte array
            $ret = array_reverse($ret);
        }
        return $ret;
    }

    public static function exibeData($data) {

        if ($data != "" && $data != "0000-00-00") {
            $ano = substr($data, 0, 4);
            $mes = substr($data, 5, 2);
            $dia = substr($data, 8, 2);

            $datar = $dia . "/" . $mes . "/" . $ano;
            //echo $datar;
        }
        return $datar;
    }

    public static function exibeDia($data) {
        $dateTime = new DateTime($data);
        $dia = $dateTime->format('w');

        switch ($dia) {
            case 0: $datar = "Domingo";
                break;
            case 1: $datar = "Segunda";
                break;
            case 2: $datar = "Terça";
                break;
            case 3: $datar = "Quarta";
                break;
            case 4: $datar = "Quinta";
                break;
            case 5: $datar = "Sexta";
                break;
            case 6: $datar = "Sábado";
                break;
        }

        return $datar;
    }

    public static function exibeDataDiaMes($data) {

        if ($data != "" && $data != "00-00") {
            $mes = substr($data, 0, 2);
            $dia = substr($data, 3, 2);

            $datar = $dia . "/" . $mes;
        }
        return $datar;
    }

    public static function formataHora($hora) {

        $hora = explode(":", $hora);
        $horas = $hora[0] * 60;
        $minutos = $hora[1];
        //foreach ($hora as $h) {
        //    $decimal += ($h / $i);
        //    $i*=60;
        //}

        return $horas + $minutos;
    }

    public static function exibeHora($hora) {

        if ($hora != "" && $hora != "00:00:00") {
            if (strpos($hora, ":")) { //se for uma hora do tipo 12:12 (normalmente vem da ata de reunião)
                $horas = substr($hora, 0, 2);
                $minutos = substr($hora, 3, 2);
                $segundos = substr($hora, 5, 2);
            } else { //alocação
                //$horas pega até o ponto
                $horaF = $hora / 60;
                //print_r($horas); die(" here");
                $horaF = explode(".", $horaF);
                $horaF = $horaF[0];
                $horas = Util_Utilidade::addZeros($horaF, 2);
                $minutos = $hora % 60;
                $minutos = Util_Utilidade::addZeros($minutos, 2);
            }

            $horar = $horas . ":" . $minutos;
            //echo $datar;
        }
        return $horar;
    }

    public static function exibeDataHora($data) {
        //01234567890123456789
        if ($data != "" && $data != "0000-00-00 00:00:00") {
            $ano = substr($data, 0, 4);
            $mes = substr($data, 5, 2);
            $dia = substr($data, 8, 2);

            $hora = substr($data, 11, 2);
            $minuto = substr($data, 14, 2);

            $datar = $dia . "/" . $mes . "/" . $ano . " às " . $hora . ":" . $minuto;
            //echo $datar;
        }
        return $datar;
    }

    public static function formataData($data) {

        $teste = substr($data, 2, 1);
        if ($teste == "-" || $teste == "/") {

            $data = str_replace("_", "", $data);
            $data = str_replace("/", "", $data);
            $data = str_replace("-", "", $data);

            if ($data != "") {
                $ano = substr($data, 4, 4);
                $mes = substr($data, 2, 2);
                $dia = substr($data, 0, 2);

                $datar = $ano . "-" . $mes . "-" . $dia;
                //echo $datar;
            }
            return $datar;
        } else {
            return $data;
        }
    }

    public static function formataDataPicker($data) {

        if ($data != "") {
            $data = Util_Utilidade::formataData($data);
            $retorno = date("m-d-Y", strtotime($data));
            $retorno = str_replace("-", "/", $retorno);
            return $retorno;
        }
    }

    public static function totalizador($objs) {
        $total = 0;
        if ($objs != "") {
            foreach ($objs as $obj) {
                if ($obj->getValor() != "") {
                    $total += $obj->getValor();
                }
            }
        }
        return $total;
    }

    public static function exibeTelefone($tel) {

        if ($tel == "") {
            $ret = "-";
        } else if (strlen($tel) == 11) { //ddd e nono digito
            //01 23456 7890
            $ret = "(" . substr($tel, 0, 2) . ")" . substr($tel, 2, 5) . substr($tel, 7, 4);
        } else if (strlen($tel) == 10) { //ddd e sem nono digito
            //01 2345 6789
            $ret = "(" . substr($tel, 0, 2) . ")" . substr($tel, 2, 4) . substr($tel, 6, 4);
        } else if (strlen($tel) == 9) { //sem ddd e nono digito
            //01234 5678
            $ret = substr($tel, 0, 5) . substr($tel, 5, 4);
        } else {
            //0123 4567
            $ret = substr($tel, 0, 4) . substr($tel, 4, 4);
        }
        return $ret;
    }
    
    public static function exibeTelefone2($tel) {

        if ($tel == "" || $tel == "-") {
            $ret = "";
        } else if (strlen($tel) == 11) { //ddd e nono digito
            //01 23456 7890
            $ret[0] = substr($tel, 0, 2);
            $ret[1] = substr($tel, 2, 5) . substr($tel, 7, 4);
        } else if (strlen($tel) == 10) { //ddd e sem nono digito
            //01 2345 6789
            $ret[0] = substr($tel, 0, 2);
            $ret[1] = substr($tel, 2, 4) . substr($tel, 6, 4);
        }
        
        return $ret;
    }

    public static function formataTelefone($tel) {
        if ($tel == "") {
            return "-";
        } else {
            $tel = str_replace("(", "", $tel);
            $tel = str_replace(")", "", $tel);
            $tel = str_replace("-", "", $tel);
            $tel = str_replace(".", "", $tel);
            $tel = str_replace(" ", "", $tel);
            //$tel = srt_replace(" ", "", $tel);
            return $tel;
        }
    }

    public static function exibeCep($cep) {
        if ($cep == "") {
            $ret = "-";
        } else if (strlen($cep) == 8) {
            $ret = substr($cep, 0, 4) . "-" . substr($cep, 4, 7);
        }
        return $ret;
    }

    public static function formataCep($cep) {
        if ($cep == "") {
            return "-";
        } else {
            $cep = str_replace("-", "", $cep);
            return $cep;
        }
    }

    public static function exibeNumeroContrato($numeroContrato) {
        if ($numeroContrato == "") {
            $ret = "-";
        } else if (strlen($numeroContrato) == 13) {
            $ret = substr($numeroContrato, 0, 12) . "-" . substr($numeroContrato, 12, 13);
        }
        return $ret;
    }

    public static function formataNumeroContrato($numeroContrato) {
        if ($numeroContrato == "") {
            return "-";
        } else {
            $numeroContrato = str_replace("-", "", $numeroContrato);
            return $numeroContrato;
        }
    }

    public static function formataCnpj($cnpj) {

        if (strpos($cnpj, ".") === false) {
            return $cnpj;
        } else {
            $cnpj = str_replace(".", "", $cnpj);
            $cnpj = str_replace("/", "", $cnpj);
            $cnpj = str_replace("-", "", $cnpj);
            return $cnpj;
        }
    }

    public static function exibeCpf($cpf) {

        if (strpos($cpf, ".") !== false || $cpf == "") {
            return $cpf;
        } else {
            $tam = strlen($cpf);
            $cpfp[3] = substr($cpf, -2);
            $cpfp[2] = substr($cpf, -5, 3);
            $cpfp[1] = substr($cpf, -8, 3);
            if ($tam == 11) {
                $cpfp[0] = substr($cpf, 0, 3);
            } else if ($tam == 10) {
                $cpfp[0] = "0" . substr($cpf, 0, 2);
            } else if ($tam == 9) {
                $cpfp[0] = "00" . substr($cpf, 0, 1);
            }
            $cpf = $cpfp[0] . "." . $cpfp[1] . "." . $cpfp[2] . "-" . $cpfp[3];
            return $cpf;
        }
    }

    public static function formataCpf($cpf) {

        if (strpos($cpf, ".") === false) {
            return $cpf;
        } else {
            $cpf = str_replace(".", "", $cpf);
            $cpf = str_replace("-", "", $cpf);
            return $cpf;
        }
    }

    public static function exibePis($pis) {

        if (strpos($pis, ".") !== false || $pis == "") {
            return $pis;
        } else {
            $tam = strlen($pis);
            $pisp[3] = substr($pis, -1);
            $pisp[2] = substr($pis, -3, 2);
            $pisp[1] = substr($pis, -8, 5);
            if ($tam == 11) {
                $pisp[0] = substr($pis, 0, 3);
            } else if ($tam == 10) {
                $pisp[0] = "0" . substr($pis, 0, 2);
            } else if ($tam == 9) {
                $pisp[0] = "00" . substr($pis, 0, 1);
            }
            $pis = $pisp[0] . "." . $pisp[1] . "." . $pisp[2] . "-" . $pisp[3];

            return $pis;
        }
    }

    public static function formataPis($pis) {

        if (strpos($pis, ".") === false) {
            return $pis;
        } else {
            $pis = str_replace(".", "", $pis);
            $pis = str_replace("-", "", $pis);
            return $pis;
        }
    }

    public static function realcarTrAtraso($data1, $hoje = "") {

        if ($hoje == "")
            $hoje = date("Y-m-d");
        if ($data1 < $hoje)
            return "class=\"danger\"";
        else
            return "";
    }

    public static function realcarTrAtrasoAsp($previsto, $proposto) {

        if ($previsto != "0000-00-00" && $proposto != "0000-00-00") {
            if ($previsto > $proposto) {
                return 'class="danger"';
            } else {
                return "";
            }
        }
    }

    public static function realcarTrAtrasoHelp($data1, $hoje = "") {

        if ($hoje == "") {
            $hoje = date("Y-m-d");
        }
        $diffUtil = Prod_CalendarioDiaMapper::getInstance()->getDiferencaUteis($data1, $hoje);

        if ($data1 < $hoje || $data1 == $hoje) { //Atrasado
            return 'class="danger"';
        } if ($data1 > $hoje && $diffUtil == 1) { //Falta 1 Dia
            return 'class="warning"';
        } else if ($data1 > $hoje && $diffUtil == 2) { //Falta 2 Dias
            return 'class="info"';
        } else {
            return "";
        }
    }

    public static function realcarTrAtrasoAcao($data, $status) {

        $hoje = date("Y-m-d");
        if ($status == 12) {
            return 'class="success"';
        } else if ($status == 7 || $status == 9) {
            return 'class="info"';
        } else if ($data < $hoje && $status != 11 && $status != 12) {
            return 'class="danger"';
        } else {
            return "";
        }
    }

    public static function realcarTrAtrasoAgend($data) {
        //usado em agendamento de vendas
        $hoje = date("Y-m-d");
        if ($data < $hoje) { //se tá atrasado
            return 'class="danger"';
        } else if ($data == $hoje) { //se é pra hoje
            return 'class="info"';
        }
    }

    public static function realcarTrVmProsp($dataControladoria) {
        if ($dataControladoria != "0000-00-00") {
            //usado nas pendencias do VM prosperidade
            $hoje = date("Y-m-d");
            $diferenca = Prod_CalendarioDiaMapper::getInstance()->getDiferencaUteis($dataControladoria, $hoje);
            if ($diferenca <= 30) { //se tá atrasado
                return 'class="info"';
            } else if ($diferenca > 30 && $diferenca <= 45) {
                return 'class="warning"';
            } else if ($diferenca > 45) {
                return 'class="danger"';
            }
        }
    }

    public static function trAtraso($qtdDias) {

        if ($qtdDias > 30)
            return "class=\"danger\"";
        else
            return "";
    }

    public static function removeAcentos($palavra) {

        $array1 = array("á", "à", "â", "ã", "ä", "é", "è", "ê", "ë", "í", "ì", "î", "ï", "ó", "ò", "ô", "õ", "ö", "ú", "ù", "û", "ü", "ç"
            , "Á", "À", "Â", "Ã", "Ä", "É", "È", "Ê", "Ë", "Í", "Ì", "Î", "Ï", "Ó", "Ò", "Ô", "Õ", "Ö", "Ú", "Ù", "Û", "Ü", "Ç");
        $array2 = array("a", "a", "a", "a", "a", "e", "e", "e", "e", "i", "i", "i", "i", "o", "o", "o", "o", "o", "u", "u", "u", "u", "c"
            , "A", "A", "A", "A", "A", "E", "E", "E", "E", "I", "I", "I", "I", "O", "O", "O", "O", "O", "U", "U", "U", "U", "C");
        return str_replace($array1, $array2, $palavra);
    }

    public static function wrapTexto($texto, $tam) {

        if (strlen($texto) > $tam) {
            $saida = mb_substr($texto, 0, $tam) . "...";
        } else {
            $saida = $texto;
        }

        return $saida;
    }

    public static function cpfValido($cpf) {
        // determina um valor inicial para o digito $d1 e $d2
        // pra manter o respeito ;)
        $d1 = 0;
        $d2 = 0;
        // remove tudo que não seja número
        $cpf = preg_replace("/[^0-9]/", "", $cpf);
        // lista de cpf inválidos que serão ignorados
        $ignore_list = array(
            '00000000000',
            '01234567890',
            '11111111111',
            '22222222222',
            '33333333333',
            '44444444444',
            '55555555555',
            '66666666666',
            '77777777777',
            '88888888888',
            '99999999999'
        );
        // se o tamanho da string for dirente de 11 ou estiver
        // na lista de cpf ignorados já retorna false
        if (strlen($cpf) != 11 || in_array($cpf, $ignore_list)) {
            return false;
        } else {
            // inicia o processo para achar o primeiro
            // número verificador usando os primeiros 9 dígitos
            for ($i = 0; $i < 9; $i++) {
                // inicialmente $d1 vale zero e é somando.
                // O loop passa por todos os 9 dígitos iniciais
                $d1 += $cpf[$i] * (10 - $i);
            }
            // acha o resto da divisão da soma acima por 11
            $r1 = $d1 % 11;
            // se $r1 maior que 1 retorna 11 menos $r1 se não
            // retona o valor zero para $d1
            $d1 = ($r1 > 1) ? (11 - $r1) : 0;
            // inicia o processo para achar o segundo
            // número verificador usando os primeiros 9 dígitos
            for ($i = 0; $i < 9; $i++) {
                // inicialmente $d2 vale zero e é somando.
                // O loop passa por todos os 9 dígitos iniciais
                $d2 += $cpf[$i] * (11 - $i);
            }
            // $r2 será o resto da soma do cpf mais $d1 vezes 2
            // dividido por 11
            $r2 = ($d2 + ($d1 * 2)) % 11;
            // se $r2 mair que 1 retorna 11 menos $r2 se não
            // retorna o valor zeroa para $d2
            $d2 = ($r2 > 1) ? (11 - $r2) : 0;
            // retona true se os dois últimos dígitos do cpf
            // forem igual a concatenação de $d1 e $d2 e se não
            // deve retornar false.
            return (substr($cpf, -2) == $d1 . $d2) ? true : false;
        }
    }

    public static function getUnidadeEspecifica($arg1, $arg2 = "", $arg3 = "", $arg4 = "", $arg5 = "", $arg6 = "") {
        if ($arg6 != "" && $arg6 != 0) {
            $retorno = $arg6;
        } else if ($arg5 != "" && $arg5 != 0) {
            $retorno = $arg5;
        } else if ($arg4 != "" && $arg4 != 0) {
            $retorno = $arg4;
        } else if ($arg3 != "" && $arg3 != 0) {
            $retorno = $arg3;
        } else if ($arg2 != "" && $arg2 != 0) {
            $retorno = $arg2;
        } else if ($arg1 != "" && $arg1 != 0) {
            $retorno = $arg1;
        }
        return $retorno;
    }

    public static function flipDiagonal($arr) {
        $out = array();
        foreach ($arr as $key => $subarr) {
            foreach ($subarr as $subkey => $subvalue) {
                $out[$subkey][$key] = $subvalue;
            }
        }
        return $out;
    }

    public static function caixaAlta($string) {
        return strtr(strtoupper($string), array(
            "à" => "À",
            "á" => "Á",
            "â" => "Â",
            "ã" => "Ã",
            "é" => "É",
            "ê" => "Ê",
            "í" => "Í",
            "ó" => "Ó",
            "ô" => "Ô",
            "õ" => "Õ",
            "ú" => "Ú",
            "ü" => "Ü",
            "ç" => "Ç"
        ));
    }

    public static function trataCaracteres($val) {
        //echo "tratacaracteres";
        $val = addslashes($val);
        //permanece porque mesmo com a aspas escapada precisa substituir por &quot;
        $val = str_replace("\\\"", "&quot;", $val);
        //escapa o caractere da polegada
        $val = trim($val);
        return $val;
    }

    public static function getTurno($data, $hora) {

        if ($data == "") {
            $data = date('Y-m-d');
        }
        if ($hora == "") {
            $hora = date('H:i');
        }

        if (($hora >= '06:00') && ($hora <= '12:29')) {
            $turno = 1;
        } else if (($hora >= '12:30') && ($hora <= '18:00')) {
            $turno = 2;
        }

        $dataTurno = array($data, $turno);

        return $dataTurno;
    }

    public static function getProxTurno($data, $turno) {
        if ($data != "") {
            $dataSistema = $data;
        }
        if ($turno == "") {
            $dataTurnoSistema = Util_Utilidade::getTurno($dataSistema);
            $data = $dataTurnoSistema['0'];
            $turno = $dataTurnoSistema['1'];
        }
        if ($turno === 1) {
            $proxTurno = $turno + 1;
            $proxData = $data;
        } else {
            $proxData = Prod_CalendarioDiaMapper::getInstance()->somaDiasCorrido($data, 1);
            $proxTurno = 1;
        }
        $proximo = array($proxData, $proxTurno);

        return $proximo;
    }

    public static function exibeTexto($texto) {
        $texto = str_replace("&quot;", '"', $texto);
        return $texto;
    }

    public static function escureceCor($cor, $sharp = true) {

        if (strlen($cor) > 6) {
            $cor = substr($cor, 1, 6);
        }

        list($r, $g, $b) = array_map('hexdec', str_split($cor, 2));

        $r = ceil($r * 0.75);
        $g = ceil($g * 0.75);
        $b = ceil($b * 0.75);

        $retorno = Util_Utilidade::addZeros(dechex($r), 2) . Util_Utilidade::addZeros(dechex($g), 2) . Util_Utilidade::addZeros(dechex($b), 2);

        if ($sharp) {
            $retorno = "#" . $retorno;
        }

        return $retorno;
    }

}

?>